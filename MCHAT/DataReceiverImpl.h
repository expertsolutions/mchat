/************************************************************************/
/* Name     : MCC\DataReceiverImpl.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 12 Jul 2016                                               */
/************************************************************************/
#pragma once
#include <boost\variant.hpp>
#include "Router\router_compatibility.h"
#include "CHatBase.h"
#include "DataReceiver.h"
#include "MessageParser.h"
#include "Socket\SpawnClient.h"
#include "Socket\SpawnServer.h"
#include "Socket\SslSpawnClient.h"
#include "Socket\SslSpawnServer.h"
#include "MessageCollector.h"


namespace attachments
{
	struct TAttachmentsParam;
}

struct NetProxy
{
	boost::variant<spawn_client::ClientProxy, ssl_spawn_client::ClientProxy> ClientProxy;
	boost::variant<spawn_server::ServerProxy, ssl_spawn_server::ServerProxy> ServerProxy;
};

class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{
	friend class CDataReceiver < CDataReceiverImpl >;

public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();

	//CMailConnectorImpl* getConnector();

	void TestReplyEventHandler();
	//{
	//	S2MCC_ReplyEventHandler(_msg, _from, _to);
	//}

private:
	int init_impl(CSystemLog* pLog);
	int start();
	bool makeGarbageCleaner(int expiredTime);

	//std::wstring _makeBadRequest(std::wstring error, std::wstring couse)const;
	std::wstring _makeBadRequest(std::string couse)const;
	std::wstring _makeOKRequest(std::string data)const;
	std::wstring _makeOKRequest()const;
	std::wstring _makeOKCookieRequest(const std::string& aData)const;
	std::wstring _make404NotFound()const;
	std::wstring _makeMessagesByCid(const ChatId& chatId)const;
	std::wstring _makeStatusOnlyMessage(const ChatId& chatId)const;
	CMessage _makeMCPMessage(MCCID mccid, SessionId sid, std::wstring content, std::wstring contentType, std::wstring ChannelId, MessageList && attachments, std::wstring Title, std::wstring ChannelName, std::wstring messageId)const;
	void _makeChatTimeout(MCCID mccid, SessionScriptId ssid);
	void _makeInitMessage(
		MCCID mccid, 
		const ChatId& chatId, 
		std::wstring&& market_code, 
		std::wstring&& customer_name, 
		const std::wstring& ip,
		std::wstring && os_version,
		std::wstring && app_version,
		std::wstring && connection_type,
		std::wstring && phone_model)const;
	

	void _sendBeginSession(MCCID mccid, std::wstring&& contact, std::wstring&& ChannelId, std::wstring&& split, std::wstring&& ipAddress, std::wstring&& topic_name)const;
	void _sendReceiveMessageRequestAck(MCCID mccid, SessionId sid, MCPId mid, std::wstring requestId, std::wstring ChannelId, std::wstring Title, std::wstring ChannelName)const;
	void _sendReceiveMessage(MCCID mccid, MCPId mid, const CMessage& msg)const;
	void _sendSessionEnded(MCCID mccid, SessionScriptId ssid)const;
	void _sendSessionTimeout(MCCID mccid, SessionScriptId ssid)const;
	void _sendReplyToMessageAck(MCCID mccid, SessionScriptId ssid)const;
	void _sendReplyToMessageAck(MCCID mccid, SessionScriptId ssid, std::wstring requestId)const;

	void _sendChatMessageToServer(const CMessage& msg) const;
	void _sendSessionInfoMessage(MCCID mccid, SessionId sid, MCPId mid, std::wstring reason)const;

	void TestAttachments();

private:
	//void InitTimer();

	//router api
	void on_routerConnect();
	void on_routerDeliverError(const CMessage&, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);
	void on_appStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	std::wstring get_ModuleName() const noexcept { return L"MCHAT"; }
	void stop();

	//void OnTimerExpired();


	// handlers
	void S2MCC_ReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void MCP2MCC_ReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)const;

	void SM2MCC_BeginSessionAckHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void SM2MCC_SessionDeletedHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void MCP2MCC_ReceiveMessageRequestHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void S2MCC_SessionUpdateHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void S2MCC_SessionTransferHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);

	void M2MHS_UploadFileCompletedHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void M2MHS_UploadFileFailedHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);

	void M2MHS_DownloadFileCompletedHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void M2MHS_DownloadFileFailedHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);

	std::wstring onChatMessage(const CMessage& _msg, const TMessageStatus& status) const;
	void OnConnectionStatusMessage(SessionScriptId sessionScriptId, CONNECTION_STATUS error, const std::wstring& message) const;

	std::wstring onGetSessionStatus(const CMessage& _msg, const TMessageStatus& status)const;
	std::wstring onGetOnlySessionStatus(const CMessage& _msg, const TMessageStatus& status)const;
	std::wstring onGetMessagesBySID(const CMessage& _msg, const TMessageStatus& status)const;
	std::wstring onStartSession(const CMessage& _msg, const TMessageStatus& status)const;
	std::wstring onChatTextMessage(const CMessage& _msg, const TMessageStatus& status)const;
	std::wstring onTransfer(const CMessage& _msg, const TMessageStatus& status)const;
	std::wstring onEndSession(const CMessage& _msg, const TMessageStatus& status)const;

	void ConnectHandlers();

	std::string MakeHttpJsonMessage() const;
	void SendMultFilesMessage(
		const MCCID& aMccid,
		CMessage&& aMsg) const;

	//CMessage CreateAttachMessage(
	//	const std::wstring& aMessageId,
	//	const MCCID& aMccid,
	//	const ChatId& cid,
	//	CMessage aAtachmentMsg) const;

	void SendAttachMessage(
		const std::wstring& aMessageId,
		const MCCID& aMccid,
		const ChatId& aCid,
		const attachments::TAttachmentsParam& aAtachmentParams) const;

	CMessage CreateAttachMessage(
		const MCCID& aMccid,
		const ChatId& aCid,
		const std::wstring& aFileName,
		std::string&& aContent) const;

	//CMessage CreateLongTextMessage(
	//	const MCCID& aMccid,
	//	const ChatId& cid,
	//	const std::wstring& aFileName,
	//	std::string&& aContent,
	//	bool aIsHttpServer) const;

	//CMessage CreateUploadUrlMessage(
	//	const std::wstring& aMessageId,
	//	const MCCID& aMccid,
	//	const ChatId& cid,
	//	const std::wstring& aFileName,
	//	size_t aFileSize,
	//	const std::wstring& aUrl) const;

	//CMessage CreateDownloadUrlMessage(
	//	const std::wstring& aMessageId,
	//	const MCCID& aMccid,
	//	const ChatId& cid,
	//	const std::wstring& aFileName,
	//	size_t aFileSize,
	//	const std::wstring& aUrl) const;

	CMessage CreateFileStreamMessage(
		const ChatId& cid,
		const std::wstring& aFileName,
		std::string&& aContent) const;

	void SendDownloadAttachmentMessage(
		const std::wstring& aMessageId,
		const MCCID& aMccid,
		const ChatId& cid,
		const std::wstring& aMcpFileName,
		const std::wstring& aRealFileName,
		size_t aFileSize,
		const std::wstring& aUrl) const;

	void SendUploadAttachmentMessage(
		const std::wstring& aMessageId,
		const MCCID& aMccid,
		const ChatId& cid,
		const std::wstring& aFileName,
		size_t aFileSize,
		const std::wstring& aUrl) const;

	void MakeServer(
		const std::string& aUri,
		const std::string& aPort,
		const size_t aClientId,
		const std::string& aCertificate,
		const std::string& aPrivateKey,
		const NetProxy& aProxy);

	void MakeClient(const NetProxy& aProxy, const CMessage& msg) const;

public:

private:
	CSystemLog * m_log;
	boost::asio::io_service& r_io;

	using MessageMapping = std::map<std::wstring, ChatTextMessageHandler_s>;

	MessageMapping _handlers;

	std::wstring _sAttachmentFolder;
	std::wstring _sAttachmentString;
	std::wstring _sAttachmentDisk;
	std::wstring _sFtpHostname;
	std::wstring _sFtpUsername;
	std::wstring _sFtpPassword;

	std::wstring _sHttpServerHostname;
	std::wstring _sHttpPort;
	unsigned int _uCookieExpiredTime;
	bool _bIsSsl = false;

	std::string _sClientHost;
	std::string _sClientPort;
	std::string _sClientPartyId;
	std::string _xApiEnvironment;

	NetProxy _netProxy;

	mutable CMessageCollector _collector;
	mutable CChatInfoCollector _infoCollector;
};

/******************************* eof *************************************/

