/************************************************************************/
/* Name     : MCHAT\MessageCollector.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jul 2016                                               */
/************************************************************************/
#pragma once
#include <string>
#include <list>
#include <algorithm>
#include "Patterns/singleton.h"
#include "ChatBase.h"

enum class CHAT_STATUS
{
	CS_CREATED = 1,
	CS_STARTED,
	CS_ENQUEUED,
	CS_EXPIRED,
	CS_FINISHED
};

enum class USER_TYPE
{
	UT_UNKNOWN = 0,
	UT_CLIENT,
	UT_EXTERNAL,
	UT_OPERATOR,
	UT_BOT
};

static std::wstring statusToString(CHAT_STATUS status)
{
	std::wstring result = L"Unknown";
	switch (status)
	{
	case CHAT_STATUS::CS_CREATED:
	{
		result = L"Created";
		break;
	}
	case CHAT_STATUS::CS_STARTED:
	{
		result = L"Started";
		break;
	}
	case CHAT_STATUS::CS_ENQUEUED:
	{
		result = L"Enqueued";
		break;
	}
	case CHAT_STATUS::CS_EXPIRED:
	{
		result = L"Expired";
		break;
	}
	case CHAT_STATUS::CS_FINISHED:
	{
		result = L"Finished";
		break;

	}
	}
	return result;
}

struct Skill
{
	std::wstring id;
	//std::wstring name;
};

struct Operator
{
	bool isBot{ false };
	std::wstring id;
	std::wstring name;
	Skill skill;
};

class CChatInfoCollector
{
	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
	{
		_log->LogStringModule(level, L"Chat collector", formatMessage, std::forward<TArgs>(args)...);
	}

	class CChatInfo
	{
	public:
		CChatInfo() = default;
		CChatInfo(
			MCCID mccid,
			ChatId cid,
			SessionScriptId ssid,
			SessionId sid,
			MCPId mid,
			time_t t,
			const std::wstring& channel,
			const std::wstring& topic,
			const std::wstring& contact,
			bool isHttpServer)
			: _mccid(mccid)
			, _cid(cid)
			, _scriptId(ssid)
			, _sessionId(sid)
			, _mcpId(mid)
			, _timestamp(t)
			, _channel(channel)
			, _status(CHAT_STATUS::CS_CREATED)
			, _topic_name(topic)
			, _contact(contact)
			, _bIsHttpServer(isHttpServer)
		{}

		auto getMCCID()const noexcept { return _mccid; }
		auto getChatId()const noexcept { return _cid; }
		auto getTimeStamp()const noexcept { return _timestamp; }
		auto getScriptId()const noexcept { return _scriptId; }
		auto getSessionId()const noexcept { return _sessionId; }
		auto isSessionStarted()const noexcept { return _status == CHAT_STATUS::CS_STARTED; }
		auto getMCPId()const noexcept { return _mcpId; }
		auto isSessionActive()const noexcept { return _status != CHAT_STATUS::CS_FINISHED; }
		//auto getOperatorId()const noexcept { return _operator.id; }
		//auto getOperatorName()const noexcept { return _operator.name; }
		//auto getOperatorSkill()const noexcept { return _operator.skill; }

		Operator getOperator() const noexcept { return _operator; }

		auto getStatus()const noexcept { return _status; }
		auto getStatusString()const noexcept { return statusToString(_status); }
		auto getChannel()const noexcept { return _channel; }
		auto getTopicName()const noexcept { return _topic_name; }
		auto getContact()const noexcept { return _contact; }
		auto isHttpServer()const noexcept { return _bIsHttpServer; }

		void setTimeStamp(time_t t) noexcept { _timestamp = t; }
		void setScriptId(SessionScriptId id) noexcept { _scriptId = id; }
		void setSessionId(SessionId id) noexcept { _sessionId = id; }
		void setMCPId(MCPId id) noexcept { _mcpId = id; }
		void setStatus(CHAT_STATUS newStatus) noexcept { _status = newStatus; }
		void setOperator(const Operator& oper) { _operator = oper; }

	private:
		MCCID			_mccid{0};
		ChatId          _cid;
		SessionScriptId _scriptId{ 0 };
		SessionId       _sessionId{ 0 };
		time_t			_timestamp{ 0 };
		bool			_bSessionStarted{ false };
		MCPId           _mcpId{ 0 };
		bool			_bIsActive{ true };
		std::wstring    _channel;
		CHAT_STATUS     _status;
		std::wstring    _topic_name;
		std::wstring    _contact;
		bool			_bIsHttpServer{ false };
		Operator		_operator;
	};

public:

	using ChatInfoCollector = std::map<MCCID, CChatInfo>;
	using IdList = std::map<MCCID, SessionScriptId>;

	void Init(CSystemLog* log)
	{
		_log = log;
	}

	auto AddChatInfo(MCCID _mccid, ChatId cid, SessionScriptId ssid,
		SessionId sid,
		MCPId mid,
		const std::wstring& channel,
		const std::wstring& topic_name,
		const std::wstring& contact,
		bool isHttpServer,
		time_t t = time(nullptr))
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (infoList.find(_mccid) != infoList.cend())
			throw (std::runtime_error((boost::format("ChatInfo with ChatId = %llu has already exist") % _mccid).str()));

		LogStringModule(LEVEL_FINEST, L"Add chat with MCCID == %llu", _mccid);

		return infoList.emplace(_mccid, CChatInfo(
			_mccid, 
			std::move(cid), 
			ssid, 
			sid, 
			mid, 
			t, 
			channel, 
			topic_name, 
			contact,
			isHttpServer));
	}

	const CChatInfo get_info(const ChatId& _id)const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = std::find_if(infoList.begin(), infoList.end(), [&_id](const auto& pair)
		{
			if (pair.second.getChatId() == _id)
				return true;
			return false;
		});

		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by ChatId = %s") % wtos(_id)).str()));
		}

		return cit->second;
	}

	const CChatInfo get_info(MCCID _id)const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		return cit->second;
	}

	void set_info(MCCID _id, SessionScriptId ssid, SessionId sid, CHAT_STATUS newStatus)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setScriptId(ssid);
		cit->second.setSessionId(sid);
		cit->second.setStatus(newStatus);
	}

	void set_operator(MCCID _id, const Operator& oper)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}
		cit->second.setOperator(oper);
	}

	void set_mcpid(MCCID _id, MCPId _mid)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setMCPId(_mid);
	}

	void delete_info(MCCID _id)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}
		infoList.erase(cit);
	}

	template <class TID>
	bool ifExist(TID &&cid)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return std::find_if(infoList.begin(), infoList.end(), [cid = std::forward<TID>(cid)](const auto& pair)
		{
			if (pair.second.getChatId() == cid)
				return true;
			return false;
		}) != infoList.end();
	}

	auto isSessionActive(MCCID _id)const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return get_info(_id).isSessionActive();
	}

	void update_status(MCCID _id, CHAT_STATUS newStatus)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setStatus(newStatus);

	}

	void update_timestamp(MCCID _id, time_t t = time(nullptr))
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setTimeStamp(t);

	}

	IdList getExpiredChatList(time_t curtime)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		IdList expiredList;
		for each (const auto& pair in infoList)
		{
			if (pair.second.getTimeStamp() < curtime)
				expiredList[pair.first] = pair.second.getScriptId();
		}
		return expiredList;
	}


private:
	CSystemLog* _log;
	ChatInfoCollector infoList;

	mutable std::mutex _mutex;
};

class CMessageCollector
{
	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
	{
		_log->LogStringModule(level, L"Message collector", formatMessage, std::forward<TArgs>(args)...);
	}

	template <class TMsg>
	void push_message(MCCID cid, TMsg&& message)
	{
		auto cit = _messages.find(cid);
		if (cit == _messages.end())
		{
			MessageList newList = { CMessage(std::forward<TMsg>(message)) };
			_messages.emplace(cid, std::move(newList));
		}
		else
		{
			cit->second.emplace_back(std::forward<TMsg>(message));
		}

	}
public:
	using MessageList = std::list<CMessage>;
	using ChatCollector = std::map<MCCID, MessageList>;

	void Init(CSystemLog* log)
	{
		_log = log;
	}

	const MessageList get_messages(MCCID _id)const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = _messages.find(_id);
		if (cit == _messages.end())
		{
			throw (std::runtime_error((boost::format("Cannot find chat messages by MCCID = %llu") % _id).str()));
		}

		return cit->second;
	}

	template <class Fn, typename... TArgs>
	void get_last_message(MCCID _id, Fn& _Func, TArgs&&... args)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = _messages.find(_id);
		if (cit == _messages.end())
		{
			throw (std::runtime_error((boost::format("Cannot find chat messages by MCCID = %llu") % _id).str()));
		}

		if (cit->second.empty())
		{
			throw (std::runtime_error((boost::format("Chat message list for MCCID = %llu empty") % _id).str()));
		}

		_Func(*(cit->second.rbegin()), std::forward<TArgs>(args)...);
	}

	template <class TMsg>
	void push_to_mcp_message(MCCID cid, TMsg&& message)
	{
		std::lock_guard<std::mutex> lock(_mutex);

		push_message(cid, std::forward<TMsg>(message));
	}

	template <class TMsg>
	void push_to_server_message(MCCID cid, TMsg&& message)
	{
		std::lock_guard<std::mutex> lock(_mutex);

		push_message(cid, std::forward<TMsg>(message));
	}

	void deleteChat(MCCID _id)
	{
		std::lock_guard<std::mutex> lock(_mutex);

		LogStringModule(LEVEL_FINEST, L"Delete chat messages with MCCID == %llu", _id);

		auto cit = _messages.find(_id);
		if (cit == _messages.end())
		{
			throw (std::runtime_error((boost::format("Cannot find messages by MCCID = %llu") % _id).str()));
		}
		_messages.erase(cit);
	}

	bool ifExist(MCCID cid)
	{
		std::lock_guard<std::mutex> lock(_mutex);

		return _messages.find(cid) != _messages.end();
	}

	template <class Fn, typename... TArgs>
	void ForEachMessage(MCCID _id, Fn& _Func, TArgs&&... args)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = _messages.find(_id);
		if (cit == _messages.end())
		{
			throw (std::runtime_error((boost::format("Cannot find messages by MCCID = %llu") % _id).str()));
		}

		for (auto& msg : cit->second)
		{
			_Func(msg, std::forward<TArgs>(args)...);
		}
	}

private:
	mutable std::mutex _mutex;
	ChatCollector _messages;
	CSystemLog* _log;
};

/******************************* eof *************************************/
