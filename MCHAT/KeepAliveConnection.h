/************************************************************************/
/* Name     : MCHAT\KeepAliveConnection.h                               */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 23 May 2017                                               */
/************************************************************************/

#pragma once
#include "Socket/AbstractConnection.h"

const int K_TCP_PACK_SIZE = 512;

class KeepAliveConnection : public AbstractConnection, public std::enable_shared_from_this<KeepAliveConnection>
{
	using Socket = boost::asio::ip::tcp::socket;
public:
	explicit KeepAliveConnection(boost::asio::io_service& aIoService, CSystemLog* log, ChatTextMessageHandler_s /*handler*/)
		: AbstractConnection(aIoService, log)
		, _socket(aIoService)
	{
		_connection_name = L"Keep Alive";
	}
public:
	void startListening() override
	{
		boost::asio::spawn(_strand,
			[self = shared_from_this()](boost::asio::yield_context yield)
		{
			try
			{
				self->LogStringModule(LEVEL_FINEST, L"========================Tick===================");
				self->_socket.close();
			}
			catch (std::exception& e)
			{
				self->LogStringModule(LEVEL_FINEST, L"Keep Alive Exception: %s", stow(e.what()).c_str());

				self->_socket.close();
				self->_timer.cancel();
			}
			catch (...)
			{
				self->LogStringModule(LEVEL_WARNING, L"Keep Alive Connection failed with error: unhandled exception caught");
			}

		});
	}

	Socket& GetSocketRef() { return _socket; }

private:
	Socket _socket;
};

/******************************* eof *************************************/