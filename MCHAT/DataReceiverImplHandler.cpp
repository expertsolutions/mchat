/************************************************************************/
/* Name     : MCHAT\DataReceiverImpHandler.cpp                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Jul 2016                                               */
/************************************************************************/
#include "stdafx.h"

#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "DataReceiverImpl.h"
#include "idGenerator.h"
#include "AttachmentsHelper.h"
#include "Patterns/string_functions.h"
#include "Patterns\time_functions.h"
#include "MessageCollector.h"
#include "Codec.h"
#include "utils.h"
#include "tgbot/net/HttpParser.h"


static CMessage MakeAuthorMsg(const Operator& oper)
{
	CMessage author;
	author[L"userType"] = oper.isBot ? static_cast<int>(USER_TYPE::UT_BOT) : static_cast<int>(USER_TYPE::UT_OPERATOR);
	author[L"name"] = oper.name;
	author[L"id"] = oper.id;
	return author;
}

static CMessage MakeSkiilGroupMsg(const Operator& oper)
{
	CMessage skillGroup;
	skillGroup[L"id"] = oper.skill.id;
	//skillGroup[L"name"] = oper.skill.name;
	return skillGroup;
}

/************************************************************************/
/***************************   OUTCOMING   ******************************/
/************************************************************************/
void CDataReceiverImpl::S2MCC_ReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
{
	MCCID mccid = _to.AsMessageParamterInt();

	decltype(auto) info = _infoCollector.get_info(mccid);
	_infoCollector.update_timestamp(mccid);

	CMessage msg(L"SCRIPTMESSAGE");
	msg[L"cid"] = info.getChatId();
	msg[L"contentType"] = L"TEXT";
	msg[L"content"] = _msg[L"Message"];
	msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_AUTOREPLY);
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
	msg[L"scriptId"] = info.getScriptId();

	_sendChatMessageToServer(msg);

	_collector.push_to_server_message(mccid, std::move(msg));

	_sendReplyToMessageAck(mccid, info.getScriptId());
}

void CDataReceiverImpl::MCP2MCC_ReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)const
{
	try
	{
		std::wstring requestId = _msg.SafeReadParam(L"requestId", core::checked_type::String, L"").AsWideStr();

		MCCID mccid = _to.AsMessageParamterInt();
		decltype(auto) info = _infoCollector.get_info(mccid);

		if (info.getStatus() != CHAT_STATUS::CS_ENQUEUED)
		{
			_infoCollector.update_timestamp(mccid);

			std::wstring request;

			try
			{
				request = _msg.SafeReadParam(L"data", CMessage::CheckedType::String, L"").AsWideStr();
			}
			catch (std::exception& error)
			{
				m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"Exception while read data, error: %s", stow(error.what()));
			}

			CMessage reqMsg;
			try
			{
				reqMsg = CMessageParser::ToMessage(request);
			}
			catch (std::exception& error)
			{
				m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"Exception while parse request, error: %s", stow(error.what()));
			}

			std::wstring message;
			try
			{
				message = reqMsg.SafeReadParam(L"message", core::checked_type::String, L"").AsWideStr();
			}
			catch (std::exception& error)
			{
				m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"Exception while read message, error: %s", stow(error.what()));
			}

			std::wstring messageId = reqMsg.SafeReadParam(L"messageId", core::checked_type::String, L"").AsWideStr();

			auto attachmentList = attachments::extractAttachments(reqMsg);

			Operator oper = info.getOperator();

			MessageList msgList;

			if (!attachmentList.empty())
			{
				for (const auto& param : attachmentList)
				{
					CMessage msg(L"CHATMESSAGE");
					msg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::RECIEVED);
					msg[L"cid"] = info.getChatId();
					msg[L"messageId"] = messageId;
					msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_OPERATOR_CHATSESSION);
					msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
					msg[L"scriptId"] = info.getScriptId();
					msg[L"author"] = MakeAuthorMsg(oper);
					msg[L"skillGroup"] = MakeSkiilGroupMsg(oper);

					std::wstring content, contentType, destination;
					if (info.isHttpServer())
					{
						//msg[L"contentType"] = L"URL";
						//msg[L"content"] = L"File in attachment";

						//std::wstring content = L"File in attachment";
						//contentType = L"URL";

						std::wstring serverPath = L"http://" + _sHttpServerHostname + L"/" + _sAttachmentFolder;
						std::wstring fileName, localPath;
						std::tie(localPath, fileName) = attachments::GetLocalPathAndFileNameFromSource(param.Url, _sAttachmentFolder);

						SendDownloadAttachmentMessage(
							messageId,
							mccid,
							info.getChatId(),
							fileName,
							param.SourceName,
							param.FileSize,
							localPath);

						continue;
					}
					else
					{
						msg[L"contentType"] = L"FILE";
						msg[L"fileName"] = param.SourceName;

						std::wstring content = stow(encodeBase64(
							std::move(attachments::readAttachment(
								param.Url,
								param.SourceName,
								_sAttachmentDisk,
								_sAttachmentString,
								_sAttachmentFolder))));

						msg[L"content"] = content;
					}

					msgList.emplace_back(msg);
				}
			}
			else
			{
				CMessage msg(L"CHATMESSAGE");
				msg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::RECIEVED);
				msg[L"cid"] = info.getChatId();
				msg[L"contentType"] = L"TEXT";
				//msg[L"content"] = stow(to_utf8(message));

				try
				{
					boost::replace_all(message, L"\\", L"\\\\");
					boost::replace_all(message, L"\"", L"\\\"");
					boost::replace_all(message, L"\n", L"\\n");
					boost::replace_all(message, L"\r", L"\\r");
				}
				catch (std::exception& error)
				{
					m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"Exception while replace chars, error: %s", stow(error.what()));
				}

				try
				{
					msg[L"content"] = message;
				}
				catch (std::exception& error)
				{
					m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"Exception while write content, error: %s", stow(error.what()));
				}

				msg[L"messageId"] = messageId;
				msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_OPERATOR_CHATSESSION);
				msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
				msg[L"scriptId"] = info.getScriptId();
				msg[L"author"] = MakeAuthorMsg(oper);
				msg[L"skillGroup"] = MakeSkiilGroupMsg(oper);

				msgList.emplace_back(msg);
			}

			for (auto & msg : msgList)
			{
				try
				{
					_sendChatMessageToServer(msg);
				}
				catch (std::exception& error)
				{
					m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"Exception while send answer, error: %s", stow(error.what()));
				}

				_collector.push_to_server_message(mccid, std::move(msg));
			}
		}

		_sendReplyToMessageAck(mccid, info.getMCPId(), requestId);
	}
	catch (std::exception& error)
	{
		m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"Exception while processing MCP2MCC_ReplyToMessage, error: %s", stow(error.what()));
	}
	catch (...)
	{
		m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"unknown exception while processing MCP2MCC_ReplyToMessage");
	}


}
void CDataReceiverImpl::SM2MCC_BeginSessionAckHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	bool   bSessionExist = _msg.SafeReadParam(L"SessionExists", CMessage::CheckedType::Int, 0).AsInt() ? true : false;
	SessionScriptId ssid = _msg.SafeReadParam(L"SessionScriptId", CMessage::CheckedType::Int64, 0).AsInt64();
	SessionId        sid = _msg.SafeReadParam(L"SessionId", CMessage::CheckedType::Int64, 0).AsInt64();
	__int64   iErrorCode = _msg.SafeReadParam(L"ErrorCode", CMessage::CheckedType::Int64, 0).AsInt64();


	MCCID cid = _to.AsMessageParamterInt();
	if (!iErrorCode && !bSessionExist)
	{
		_infoCollector.set_info(cid, ssid, sid, CHAT_STATUS::CS_ENQUEUED);
		return;
	}

	std::wstring sErrorDescription;
	if (iErrorCode)
	{
		sErrorDescription = _msg.SafeReadParam(L"ErrorDescription", CMessage::CheckedType::String, L"Unknown error").AsWideStr();
	}
	else if (bSessionExist)
	{
		sErrorDescription = L"Your session is duplicated. Please wait for a while and then try again";
	}

	auto info = _infoCollector.get_info(cid);

	CMessage msg(L"SCRIPTMESSAGE");
	msg[L"cid"] = info.getChatId();
	msg[L"contentType"] = L"TEXT";
	msg[L"content"] = sErrorDescription;
	msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_SESSION_CLOSED_BY_OPERATOR);
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
	msg[L"scriptId"] = info.getScriptId();

	_sendChatMessageToServer(msg);

	_infoCollector.delete_info(cid);
	_collector.deleteChat(cid);
}

void CDataReceiverImpl::SM2MCC_SessionDeletedHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	//MCCID mccid = _to.GetChildID();
	MCCID mccid = _to.AsMessageParamterInt();
	std::wstring sReason = _msg.SafeReadParam(L"Reason", CMessage::CheckedType::String, L"Session has deleted by unknown reason").AsWideStr();
	ENUM_MESSAGE_CODE statusEnded = static_cast<ENUM_MESSAGE_CODE>(_msg.SafeReadParam(L"Status", CMessage::CheckedType::Int64, static_cast<__int64>(ENUM_MESSAGE_CODE::MD_SESSION_CLOSED_BY_OPERATOR)).AsInt64());

	ChatId cid;
	MCPId mid;
	SessionId ssid;
	SessionScriptId sid;
	ENUM_MESSAGE_CODE code;
	auto status = CHAT_STATUS::CS_CREATED;
	if (true)
	{
		decltype(auto) info = _infoCollector.get_info(mccid);
		cid = info.getChatId();
		status = info.getStatus();
		mid = info.getMCPId();
		ssid = info.getSessionId();
		sid = info.getScriptId();
		code = status == CHAT_STATUS::CS_EXPIRED ? ENUM_MESSAGE_CODE::MD_SESSION_CLOSED_BY_TIMEOUT : ENUM_MESSAGE_CODE::MD_SESSION_CLOSED_BY_OPERATOR;
	}
	
	_infoCollector.delete_info(mccid);

	if (code != ENUM_MESSAGE_CODE::MD_SESSION_CLOSED_BY_ABONENT)
	{
		CMessage msg(L"END_SESSION");

		msg[L"status"] = statusToString(CHAT_STATUS::CS_FINISHED);
		msg[L"contentType"] = L"TEXT";
		
		msg[L"code"] = static_cast<int>(code);
		msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
		msg[L"cid"] = cid;
		msg[L"scriptId"] = sid;

		if (status == CHAT_STATUS::CS_ENQUEUED)
		{
			msg.SetName(L"SESSION_STATUS");
			msg[L"message"] = sReason;
		}
		else
		{
			msg[L"content"] = sReason;
		}


		_sendChatMessageToServer(msg);
	}

	_collector.deleteChat(mccid);
}

void CDataReceiverImpl::MCP2MCC_ReceiveMessageRequestHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	/*
	Name: MCP2ICCP/OAM_receiveMessageRequest; source = "MCP";
 ScrGenAddr = 0x00000000-00000000; destination = "ICCP/OAM"; requestId = "{68050e30-56fd-4d6a-b30b-93d95053855e}"; destinationAddress = "0BB8413C0104F390"; DestinationAddress = 0x0BB84515-0E49E586; 
 ScriptID = 0x0BB8413C-0104F390; methodName = "receiveMessageRequest"; 
 data = "{
	"callStart":"2016-10-12 13:07:38.342",
	"comp":"ms-d60409-01",
	"uui":"MCM#MS-CTIEVENT005#0BB8413C0104F390#0BB845150E49E586",
	"calledId":"0510089",
	"citrix":"MS-CTXACRM001",
	"callId":"4596",
	"agent":"75931",
	"guid":"{68050e30-56fd-4d6a-b30b-93d95053855e}",
	"callAccept":"2016-10-12 13:07:39.948",
	"ucid":"00001045961476266857",
	"callerId":"79652911591",
	"split":"0594959",
	"user":"apolitov",
	"agentDetails":{
		"agentSkills":"",
		"agentId":"2147515285",
		"agentProfile":"29772",
		"agentLoginName":"APolitov"
	}
}"; SourceAddress = 0x0BB8435F-00000057;
	*/
	//MCCID cid = _to.GetChildID();

	//try
	//{
		MCCID cid = _to.AsMessageParamterInt();
		MCPId mid = _from.AsMessageParamterInt();

		_msg.CheckParam(L"data", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		std::wstring data = _msg.SafeReadParam(L"data", CMessage::CheckedType::String, L"").AsWideStr();
		std::wstring requestId = _msg.SafeReadParam(L"requestId", CMessage::CheckedType::String, L"").AsWideStr();
		bool isBot = _msg.SafeReadParam(L"isBot", CMessage::CheckedType::Bool, false).AsBool();

		CMessage jsondata = CMessageParser::ToMessage(data);

		jsondata.CheckParam(L"agentDetails", CMessage::CheckedType::RawData, CMessage::ParamType::Mandatory);
		CMessage agentDetailsMsg = jsondata.SafeReadParam(L"agentDetails", CMessage::CheckedType::RawData, L"").AsMessage();

		std::wstring agentId = agentDetailsMsg.SafeReadParam(L"agentId", CMessage::CheckedType::String, L"").AsWideStr();
		std::wstring agentName = agentDetailsMsg.SafeReadParam(L"agentLoginName", CMessage::CheckedType::String, L"").AsWideStr();
		//std::wstring agentSkills = agentDetailsMsg.SafeReadParam(L"agentSkills", CMessage::CheckedType::String, L"").AsWideStr();

		std::wstring split = jsondata.SafeReadParam(L"split", CMessage::CheckedType::String, L"").AsWideStr();

		Skill skill;
		skill.id = split;

		Operator oper;
		oper.isBot = isBot;
		oper.id = agentId;
		oper.name = agentName;
		oper.skill = skill;

		_infoCollector.set_operator(cid, oper);

		_infoCollector.set_mcpid(cid, mid);
		_infoCollector.update_status(cid, CHAT_STATUS::CS_STARTED);

		decltype(auto) info = _infoCollector.get_info(cid);

		_sendReceiveMessageRequestAck(cid, info.getSessionId(), mid, std::move(requestId), info.getContact(), info.getTopicName(), info.getChannel());

		CMessage msg(L"TRANSFER");
		msg[L"cid"] = info.getChatId();
		//msg[L"id"] = info.getOperatorId();
		//msg[L"name"] = info.getOperatorName();
		msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
		msg[L"scriptId"] = info.getScriptId();
		msg[L"author"] = MakeAuthorMsg(oper);
		msg[L"skillGroup"] = MakeSkiilGroupMsg(oper);

		_sendChatMessageToServer(msg);
}

void CDataReceiverImpl::S2MCC_SessionUpdateHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	int iEWT = utils::toNum<int, wchar_t>(_msg.SafeReadParam(L"EWT", CMessage::CheckedType::String, L"1").AsWideStr());
	MCCID cid = _to.AsMessageParamterInt();

	decltype(auto) info = _infoCollector.get_info(cid);

	CMessage msg(L"SESSION_STATUS");
	msg[L"waitTime"] = iEWT;
	msg[L"status"] = info.getStatusString();
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
	msg[L"cid"] = info.getChatId();
	msg[L"scriptId"] = info.getScriptId();
	_sendChatMessageToServer(msg);

	_collector.push_to_server_message(cid, std::move(msg));
}

void CDataReceiverImpl::S2MCC_SessionTransferHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	int iEWT = utils::toNum<int, wchar_t>(_msg.SafeReadParam(L"EWT", CMessage::CheckedType::String, L"1").AsWideStr());
	std::wstring message = _msg.SafeReadParam(L"Message", CMessage::CheckedType::String, L"").AsWideStr();
	MCCID mccid = _to.AsMessageParamterInt();

	decltype(auto) info = _infoCollector.get_info(mccid);

	if (info.getStatus() == CHAT_STATUS::CS_ENQUEUED)
	{
		m_log->LogStringModule(LEVEL_FINEST, L"CDataReceiverImpl", L"Session with MCCID == \"%llu\" has already in Transfer", mccid);
		return;
	}

	if (!message.empty())
	{
		auto cid = info.getChatId();
		IdGenerator generator;

		CMessage msg(L"TRANSFER");
		msg[L"cid"] = cid;
		msg[L"messageId"] = generator->makeSId();
		msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_AUTOREPLY);
		msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
		msg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::NEW);
		msg[L"scriptId"] = info.getScriptId();

		if (message.size() > attachments::MaxTextSize)
		{
			auto content = wtos(message);

			if (info.isHttpServer())
			{
				// ToDo: !!!
			}
			else
			{
				auto fileName = attachments::CreateGUID() + L".txt";;
				auto attachParam = attachments::saveAttachment(
					cid,
					_sAttachmentDisk, 
					_sAttachmentFolder,
					_sAttachmentString,
					std::move(content),
					std::move(fileName));

				msg[L"contentType"] = L"FILE";
				msg[L"content"] = L"File in attachment";

				msg[L"fileName"] = attachParam.SourceName;
				msg[L"fileSize"] = attachParam.FileSize;
				msg[L"fileUrl"] = attachParam.Url;
			}
		}
		else
		{
			msg[L"contentType"] = L"TEXT";
			msg[L"content"] = _msg[L"Message"];
		}

		m_log->LogStringModule(LEVEL_FINEST, L"[TEST]", L"Push Transfer: %s", msg.Dump().c_str());
		_collector.push_to_mcp_message(mccid, std::move(msg));
	}

	_infoCollector.update_status(mccid, CHAT_STATUS::CS_ENQUEUED);
	
	CMessage msg(L"SESSION_STATUS");
	msg[L"waitTime"] = iEWT;
	msg[L"status"] = static_cast<int>(CHAT_STATUS::CS_ENQUEUED);
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
	msg[L"cid"] = info.getChatId();
	msg[L"scriptId"] = info.getScriptId();

	_sendChatMessageToServer(msg);

	_collector.push_to_server_message(mccid, std::move(msg));
}

void CDataReceiverImpl::M2MHS_UploadFileCompletedHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	MCCID mccid = _to.AsMessageParamterInt();
	
	std::wstring messageId = _msg.SafeReadParam(L"MessageId", core::checked_type::String, L"").AsWideStr();
	std::wstring commitUrl = utils::toLower(_msg.SafeReadParam(L"CommitUrl0", core::checked_type::String, L"").AsWideStr());
	std::wstring fileName = _msg.SafeReadParam(L"FileName0", core::checked_type::String, L"").AsWideStr();
	size_t fileSize = _msg.SafeReadParam(L"FileSize0", core::checked_type::Int, 0).AsInt();

	decltype(auto) info = _infoCollector.get_info(mccid);
	ChatId cid = info.getChatId();
	Operator oper = info.getOperator();

	CMessageCollector::MessageList messagesToSend;

	CMessage chatMsg(L"CHATMESSAGE");
	chatMsg[L"cid"] = cid;
	chatMsg[L"messageId"] = messageId;

	chatMsg[L"fileName"] = fileName;
	chatMsg[L"fileSize"] = fileSize;
	chatMsg[L"fileUrl"] = commitUrl;
	chatMsg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_ABONENT_CHATSESSION);
	chatMsg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
	chatMsg[L"scriptId"] = info.getScriptId();
	chatMsg[L"author"] = MakeAuthorMsg(oper);
	chatMsg[L"skillGroup"] = MakeSkiilGroupMsg(oper);

	CMessage attachment;
	attachment[L"name"] = fileName;
	attachment[L"size"] = fileSize;
	attachment[L"url"] = commitUrl;
	messagesToSend.push_back(attachment);

	if (info.getStatus() == CHAT_STATUS::CS_STARTED)
	{
		CMessage mcpMsg = std::move(_makeMCPMessage(
			mccid,
			info.getSessionId(),
			L"File in attachment",
			L"URL",
			info.getContact(),
			std::move(messagesToSend),
			info.getTopicName(),
			info.getChannel(),
			messageId));

		_sendReceiveMessage(info.getMCCID(), info.getMCPId(), mcpMsg);

		chatMsg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::SENT);
	}
	else if (info.getStatus() == CHAT_STATUS::CS_ENQUEUED)
	{
		chatMsg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::NEW);
	}

	_infoCollector.update_timestamp(info.getMCCID());

	m_log->LogStringModule(LEVEL_FINEST, L"[TEST]", L"Push Message: %s", chatMsg.Dump().c_str());
	_collector.push_to_mcp_message(info.getMCCID(), std::move(chatMsg));
}

void CDataReceiverImpl::M2MHS_UploadFileFailedHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	MCCID mccid = _to.AsMessageParamterInt();

	m_log->LogStringModule(LEVEL_FINEST, L"[TEST]", L"Upload file failed, mccid: %llu, message dump: %s",
		mccid,
		_msg.Dump().c_str());
}

void CDataReceiverImpl::M2MHS_DownloadFileCompletedHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	MCCID mccid = _to.AsMessageParamterInt();

	std::wstring messageId = _msg.SafeReadParam(L"MessageId", core::checked_type::String, L"").AsWideStr();
	std::wstring commitUrl = utils::toLower(_msg.SafeReadParam(L"CommitUrl0", core::checked_type::String, L"").AsWideStr());
	std::wstring fileName = _msg.SafeReadParam(L"FileName0", core::checked_type::String, L"").AsWideStr();
	size_t fileSize = _msg.SafeReadParam(L"FileSize0", core::checked_type::Int, 0).AsInt();

	decltype(auto) info = _infoCollector.get_info(mccid);
	ChatId cid = info.getChatId();
	//Operator oper = info.getOperator();

	CMessage msg(L"CHATMESSAGE");
	msg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::RECIEVED);
	msg[L"cid"] = info.getChatId();
	msg[L"messageId"] = messageId;
	msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_OPERATOR_CHATSESSION);
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
	msg[L"contentType"] = L"URL";
	msg[L"content"] = L"File in attachment";
	msg[L"scriptId"] = info.getScriptId();
	//msg[L"author"] = MakeAuthorMsg(oper);
	//msg[L"skillGroup"] = MakeSkiilGroupMsg(oper);

	CMessage attachment;
	attachment[L"name"] = fileName;
	attachment[L"size"] = fileSize;
	attachment[L"url"] = commitUrl;

	msg[L"attachment"] = attachment;

	_sendChatMessageToServer(msg);
	_collector.push_to_server_message(mccid, std::move(msg));
}

void CDataReceiverImpl::M2MHS_DownloadFileFailedHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	MCCID mccid = _to.AsMessageParamterInt();

	m_log->LogStringModule(LEVEL_FINEST, L"[TEST]", L"Download file failed, mccid: %llu, message dump: %s",
		mccid,
		_msg.Dump().c_str());
}


/************************************************************************/
/***************************   INCOMING   *******************************/
/************************************************************************/

std::wstring CDataReceiverImpl::onChatMessage(const CMessage & _msg, const TMessageStatus& tms) const
{
	m_log->LogStringModule(LEVEL_FINEST, L"[IN]", L"Incoming message: %s", _msg.Dump().c_str());

	MessageMapping::const_iterator cit = _handlers.find(tms.methodName);

	if (cit == _handlers.end())
		throw (std::runtime_error((boost::format("Cannot find method by name = %s") % wtos(_msg.GetName()).c_str()).str()));

	if (!cit->second)
		throw (std::runtime_error((boost::format("Method name = %s isn't initialized") % wtos(_msg.GetName()).c_str()).str()));

	return cit->second(_msg, tms);
}

std::wstring CDataReceiverImpl::onGetSessionStatus(const CMessage & _msg, const TMessageStatus& status) const
{
	if (!_infoCollector.ifExist(status.chatId))
	{
		return _make404NotFound();
	}

	return _makeMessagesByCid(status.chatId);
}

std::wstring CDataReceiverImpl::onGetOnlySessionStatus(const CMessage & _msg, const TMessageStatus & status) const
{
	if (!_infoCollector.ifExist(status.chatId))
	{
		return _make404NotFound();
	}

	return _makeStatusOnlyMessage(status.chatId);
}

std::wstring CDataReceiverImpl::onGetMessagesBySID(const CMessage & _msg, const TMessageStatus& status) const
{
	return L"OK";
}

std::wstring CDataReceiverImpl::onStartSession(const CMessage & _msg, const TMessageStatus& status) const
{
	std::string reason;
	bool bBadRequest = false;

	try
	{
		_msg.CheckParam(L"id", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"channel", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"topic", CMessage::CheckedType::RawData, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"customer", CMessage::CheckedType::RawData, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"timestamp", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
	}
	catch (const std::runtime_error& e)
	{
		bBadRequest = true;
		reason = e.what();
	}

	bool isHttpServer =
		_msg.SafeReadParam(L"httpServerEnable", CMessage::CheckedType::String, L"false").AsWideStr() == L"false" ? false : true;
	
	if (isHttpServer && (_sHttpServerHostname.empty() || _sHttpPort.empty()))
	{
		bBadRequest = true;
		reason = "Unsupported http-file request";
	}

	if (bBadRequest)
	{
		// make bad request
		return _makeBadRequest(std::move(reason));
	}

	std::wstring 
		cid, 
		contact, 
		custoner_name, 
		channel, 
		ipAddress, 
		topic_name, 
		topic_split,
		os_version,
		app_version,
		connection_type,
		phone_model;

	try
	{
		CMessage topic = _msg[L"topic"].AsMessage();
		topic.CheckParam(L"name", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		topic.CheckParam(L"split", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);

		CMessage customer = _msg[L"customer"].AsMessage();
		customer.CheckParam(L"name", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		customer.CheckParam(L"number", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		
		cid = _msg.SafeReadParam(L"id", CMessage::CheckedType::String, L"").AsWideStr();
		contact = customer.SafeReadParam(L"number", CMessage::CheckedType::String, L"").AsWideStr();

		if (contact.empty())
		{
			throw std::runtime_error("MSISDN required");
		}
		channel = _msg.SafeReadParam(L"channel", CMessage::CheckedType::String, L"").AsWideStr();

		custoner_name = customer.SafeReadParam(L"name", CMessage::CheckedType::String, L"").AsWideStr();
		ipAddress = customer.SafeReadParam(L"ipAddress", CMessage::CheckedType::String, L"").AsWideStr();
		os_version = customer.SafeReadParam(L"OSVersion", CMessage::CheckedType::String, L"").AsWideStr();
		app_version = customer.SafeReadParam(L"appVersion", CMessage::CheckedType::String, L"").AsWideStr();
		connection_type = customer.SafeReadParam(L"connectionType", CMessage::CheckedType::String, L"").AsWideStr();
		phone_model = customer.SafeReadParam(L"phoneModel", CMessage::CheckedType::String, L"").AsWideStr();

		topic_name = topic.SafeReadParam(L"name", CMessage::CheckedType::String, L"").AsWideStr();
		topic_split = topic.SafeReadParam(L"split", CMessage::CheckedType::String, L"").AsWideStr();
	}
	catch (const std::runtime_error& e)
	{
		bBadRequest = true;
		reason = e.what();
	}

	std::wstring market_code = _msg.SafeReadParam(L"marketCode", CMessage::CheckedType::String, L"").AsWideStr();

	if (bBadRequest)
	{
		// make bad request
		return _makeBadRequest(std::move(reason));
	}

	topic_name = std::move(from_utf8(wtos(std::move(topic_name))));
	
	if (!custoner_name.empty())
		custoner_name = std::move(from_utf8(wtos(std::move(custoner_name))));

	if (_infoCollector.ifExist(cid))
	{
		return _makeBadRequest((boost::format("Chat with id = %s has already exist") % cid.c_str()).str());
	}

	//CChatInfoCollector::CChatInfo info(cid, SessionScriptId(0), SessionId(0), MCPId(0));

	IdGenerator generator;
	MCCID mccid = generator->makeId(GetClientId());

	channel = (channel == L"APP") ? L"MP" : L"SITE";

	_infoCollector.AddChatInfo(
		mccid, 
		cid, 
		SessionScriptId(0), 
		SessionId(0), 
		MCPId(0), 
		channel, 
		topic_name, 
		contact,
		isHttpServer);
	//collector->push_to_mcp_message(mccid, _msg);

	_makeInitMessage(
		mccid, 
		cid, 
		std::move(market_code), 
		std::move(custoner_name), 
		ipAddress,
		std::move(os_version),
		std::move(app_version),
		std::move(connection_type),
		std::move(phone_model));

	_sendBeginSession(mccid, std::move(contact), std::move(channel), std::move(topic_split), std::move(ipAddress), std::move(topic_name));

	return _makeOKCookieRequest(isHttpServer ? MakeHttpJsonMessage() : "");
}

static bool CheckOCPChatMessage(const CMessage& _msg, std::string& ourErrorReason)
{
	bool bBadRequest = false;
	
	try
	{
		_msg.CheckParam(L"id", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"contentType", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"content", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"code", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
		_msg.CheckParam(L"timestamp", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);

		auto contentType = _msg.SafeReadParam(L"contentType", CMessage::CheckedType::String, L"").AsWideStr();
		if (contentType == L"URL")
		{
			_msg.CheckParam(L"attachment", CMessage::CheckedType::RawData, CMessage::ParamType::Mandatory);
		}
	}
	catch (const std::runtime_error& e)
	{
		bBadRequest = true;
		ourErrorReason = e.what();
	}

	return bBadRequest;
}

std::wstring CDataReceiverImpl::onChatTextMessage(const CMessage& _msg, const TMessageStatus& status) const
{
	std::string reason;
	if (CheckOCPChatMessage(_msg, reason))
	{
		// make bad request
		return _makeBadRequest(std::move(reason));
	}

	std::wstring cid = status.chatId;
	
	if (!_infoCollector.ifExist(cid))
	{
		return _make404NotFound();
	}

	decltype(auto) info = _infoCollector.get_info(cid);

	std::string content      = _msg.SafeReadParam(L"content", CMessage::CheckedType::String, L"").AsStr();
	int code                 = utils::toNum<int, wchar_t>(_msg.SafeReadParam(L"code", CMessage::CheckedType::String, L"").AsWideStr());
	std::string _sFileName	 = _msg.SafeReadParam(L"fileName", CMessage::CheckedType::String, L"").AsStr();
	std::wstring messageId   = _msg.SafeReadParam(L"id", CMessage::CheckedType::String, L"").AsWideStr();
	std::wstring contentType = _msg.SafeReadParam(L"contentType", CMessage::CheckedType::String, L"").AsWideStr();

	attachments::TAttachmentsParam attachmentsParams;
	if (_msg.HasParam(L"attachment"))
	{
		CMessage incomingAttachment = _msg[L"attachment"].AsMessage();
		attachmentsParams.SourceName = incomingAttachment[L"fileName"].AsWideStr();
		attachmentsParams.FileSize = utils::toNum<size_t>(incomingAttachment[L"fileSize"].AsWideStr());
		attachmentsParams.Url = utils::toLower(incomingAttachment[L"url"].AsWideStr());
	}

	std::wstring fileName;
	if (!_sFileName.empty())
	{
		fileName = from_utf8(_sFileName);
	}

	CMessageCollector::MessageList messagesToSend;

	CMessage attachment;
	if (contentType == L"TEXT" && content.size() > attachments::MaxTextSize) // [MCH-670]
	{
		m_log->LogStringModule(LEVEL_FINEST, L"[!!!]", L"Text size is longer than %i -> making attachment instead", attachments::MaxTextSize);

		contentType = info.isHttpServer() ? L"URL" : L"FILE";

		if (fileName.empty())
		{
			fileName = attachments::CreateGUID() + L".txt";
		}

		if (info.isHttpServer())
		{
			attachmentsParams = attachments::saveAttachment(
				cid,
				_sAttachmentDisk,
				_sAttachmentFolder,
				_sAttachmentString,
				std::move(content),
				fileName);

			SendAttachMessage(
				messageId,
				info.getMCCID(),
				cid,
				attachmentsParams);
		}
		else
		{
			attachment = CreateFileStreamMessage(cid, fileName, std::move(content));
		}
	}
	else if (contentType == L"TEXT")
	{
		content = wtos(from_utf8(content));
	}
	else if (contentType == L"URL")
	{
		SendAttachMessage(
			messageId,
			info.getMCCID(),
			cid,
			attachmentsParams);
	}
	else if (contentType == L"FILE")
	{
		attachment = CreateAttachMessage(
			info.getMCCID(),
			cid,
			fileName,
			std::move(content));

		content = "File in attachment";
	}
	else
	{
		throw std::logic_error(std::string("Unknown contentType: ") + wtos(contentType));
	}

	if (contentType != L"URL")
	{
		CMessage chatMsg(L"CHATMESSAGE");
		chatMsg[L"cid"] = cid;
		chatMsg[L"messageId"] = messageId;
		chatMsg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::NEW);
		chatMsg[L"contentType"] = contentType;
		chatMsg[L"scriptId"] = info.getScriptId();

		if (contentType == L"FILE")
		{
			chatMsg[L"fileName"] = attachment.SafeReadParam(L"name", CMessage::CheckedType::String, L"").AsWideStr();
			chatMsg[L"fileSize"] = attachment.SafeReadParam(L"size", CMessage::CheckedType::Int, 0).AsInt();
			chatMsg[L"fileUrl"] = attachment.SafeReadParam(L"url", CMessage::CheckedType::String, L"").AsWideStr();
			messagesToSend.push_back(attachment);
		}

		chatMsg[L"code"] = code;
		chatMsg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
		chatMsg[L"content"] = stow(content);

		if (code == static_cast<int>(ENUM_MESSAGE_CODE::MD_SESSION_CLOSED_BY_ABONENT))
		{
			_sendSessionEnded(info.getMCCID(), info.getScriptId());
		}
		else
		{
			if (info.getStatus() == CHAT_STATUS::CS_STARTED)
			{
				CMessage mcpMsg = std::move(_makeMCPMessage(
					info.getMCCID(),
					info.getSessionId(),
					std::move(stow(std::move(content))),
					contentType,
					info.getContact(),
					std::move(messagesToSend),
					info.getTopicName(),
					info.getChannel(),
					messageId));

				_sendReceiveMessage(info.getMCCID(), info.getMCPId(), mcpMsg);

				chatMsg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::SENT);
			}
		}

		_infoCollector.update_timestamp(info.getMCCID());

		m_log->LogStringModule(LEVEL_FINEST, L"[TEST]", L"Push Message: %s", chatMsg.Dump().c_str());
		_collector.push_to_mcp_message(info.getMCCID(), std::move(chatMsg));
	}

	return _makeOKRequest();
}

std::wstring CDataReceiverImpl::onTransfer(const CMessage & _msg, const TMessageStatus& status) const
{
	return L"OK";
}

std::wstring CDataReceiverImpl::onEndSession(const CMessage & _msg, const TMessageStatus& status) const
{
	return L"OK";
}


void CDataReceiverImpl::TestAttachments()
{
	CMessage reply(L"MCP2ICCP/OAM_replyToMessage");
	std::wstring requestId = L"1231231231";
	std::wstring messageId = L"adasdadsada";

	std::wstring fileName = L"_in.txt";
	DWORD dwDataSize = 1000;
	std::wstring uri = L"http://ms-ccvm002/Attachments/";

	reply[L"requestId"] = requestId;

	boost::property_tree::wptree requestTree;

	requestTree.put(L"message", L"Text test message");
	requestTree.put(L"messageId", messageId);

	boost::property_tree::wptree attachmentTree, arrayTree;
	attachmentTree.put(L"name", fileName);
	attachmentTree.put(L"size", dwDataSize);
	attachmentTree.put(L"url", uri);

	arrayTree.push_back(std::make_pair(L"", attachmentTree));
	requestTree.add_child(L"attachments", arrayTree);

	std::wstring data = core::support::ptree_parser::tree_to_json_string(requestTree);

	reply[L"data"] = data;

	std::wstring request = reply.SafeReadParam(L"data", CMessage::CheckedType::String, L"").AsWideStr();
	CMessage reqMsg = CMessageParser::ToMessage(request);

	std::wstring message = reqMsg.SafeReadParam(L"message", core::checked_type::String, L"").AsWideStr();
	messageId = reqMsg.SafeReadParam(L"messageId", core::checked_type::String, L"").AsWideStr();

	auto attachmentList = attachments::extractAttachments(reqMsg);
}

void CDataReceiverImpl::OnConnectionStatusMessage(SessionScriptId sessionScriptId, CONNECTION_STATUS error, const std::wstring& message) const
{
	CMessage msg(L"MCC2ANY_ConnectionStatus");
	msg[L"SessionScriptId"] = sessionScriptId;
	msg[L"Status"] = static_cast<int>(error);
	msg[L"Message"] = message;

	CLIENT_ADDRESS addr_from(GetClientId());
	CLIENT_ADDRESS addr_to(sessionScriptId);

	this->SendFromTo(std::move(msg), addr_from, addr_to);
}

/******************************* eof *************************************/