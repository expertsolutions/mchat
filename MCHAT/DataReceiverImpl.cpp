/************************************************************************/
/* Name     : MCC\DataReceiver.cpp                                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 08 Feb 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <future>
#include <type_traits>
#include <boost/asio/spawn.hpp>
#include "DataReceiverImpl.h"
#include "tgbot/net/HttpParser.h"
#include "Configuration\SystemConfiguration.h"
#include "Log\SystemLog.h"
#include "WinAPI/ProcessHelper.h"
#include "ThreadHelper.h"
//#include "AttachmentsHelper.h"
//#include "MakeServer.h"
#include "MessageCollector.h"
#include "utils.h"
#include "IdGenerator.h"
#include "Socket/Connection.h"
#include "Socket/SslConnection.h"
#include "KeepAliveConnection.h"
#include "Codec.h"
#include "AttachmentsHelper.h"
#include "Utils.h"
#include "../Common/Messages/m2mhs.h"
#include "BoostVisitor.h"
//
#include "Socket/SpawnClient.h"
#include "Socket/SpawnServer.h"
#include "Socket/SslSpawnClient.h"
#include "Socket/SslSpawnServer.h"

const int EXPIREDTIME = 30;

std::string MakeDate(unsigned int min = 0)
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[100];

	time(&rawtime);
	rawtime += 60 * min;
	//timeinfo = localtime(&rawtime);
	timeinfo = gmtime(&rawtime);

	//strftime(buffer, sizeof(buffer), "%a, %d %b %Y %H:%M:%S %Z", timeinfo);
	strftime(buffer, sizeof(buffer), "%a, %d %b %Y %H:%M:%S", timeinfo);
	std::string str(buffer);

	//return str;
	return str + " GMT";
}
//

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& io) :
	m_log(NULL), 
	r_io(io)//,
	//_strand(io)
{
}


CDataReceiverImpl::~CDataReceiverImpl()
{
	int test = 0;
}

void CDataReceiverImpl::set_Subscribes()
{
	subscribe(L"S2MCC_replyToMessage", &CDataReceiverImpl::S2MCC_ReplyEventHandler);
	subscribe(L"MCP2ICCP/OAM_replyToMessage", &CDataReceiverImpl::MCP2MCC_ReplyEventHandler);
	subscribe(L"ANY2SM_BeginSessionAck", &CDataReceiverImpl::SM2MCC_BeginSessionAckHandler);
	subscribe(L"SM2ANY_SessionDeleted", &CDataReceiverImpl::SM2MCC_SessionDeletedHandler);
	subscribe(L"MCP2ICCP/OAM_receiveMessageRequest", &CDataReceiverImpl::MCP2MCC_ReceiveMessageRequestHandler);
	subscribe(L"S2MCC_SessionUpdate", &CDataReceiverImpl::S2MCC_SessionUpdateHandler);
	subscribe(L"S2MCC_SessionTransfer", &CDataReceiverImpl::S2MCC_SessionTransferHandler);
	subscribe(L"M2MHS_UploadFileCompleted", &CDataReceiverImpl::M2MHS_UploadFileCompletedHandler);
	subscribe(L"M2MHS_UploadFileFailed", &CDataReceiverImpl::M2MHS_UploadFileFailedHandler);
	subscribe(L"M2MHS_DownloadFileCompleted", &CDataReceiverImpl::M2MHS_DownloadFileCompletedHandler);
	subscribe(L"M2MHS_DownloadFileFailed", &CDataReceiverImpl::M2MHS_DownloadFileFailedHandler);
}

void CDataReceiverImpl::ConnectHandlers()
{
	_handlers[L"CHATMESSAGE"] = std::bind(&CDataReceiverImpl::onChatTextMessage, this, std::placeholders::_1, std::placeholders::_2);
	_handlers[L"GET_SESSION_STATUS"] = std::bind(&CDataReceiverImpl::onGetSessionStatus, this, std::placeholders::_1, std::placeholders::_2);
	//_handlers[L"GET_ONLY_SESSION_STATUS"] = std::bind(&CDataReceiverImpl::onGetOnlySessionStatus, this, std::placeholders::_1, std::placeholders::_2);
	//_handlers[L"GET_MESSAGES_BYSID"] = std::bind(&CDataReceiverImpl::onGetMessagesBySID, this, std::placeholders::_1, std::placeholders::_2);
	_handlers[L"START_SESSION"] = std::bind(&CDataReceiverImpl::onStartSession, this, std::placeholders::_1, std::placeholders::_2);
	//_handlers[L"TRANSFER"] = std::bind(&CDataReceiverImpl::onTransfer, this, std::placeholders::_1, std::placeholders::_2);
	//_handlers[L"END_SESSION"] = std::bind(&CDataReceiverImpl::onEndSession, this, std::placeholders::_1, std::placeholders::_2);
}


//void CDataReceiverImpl::TestReplyEventHandler()
//{
//
//	boost::asio::spawn(r_io, [&](boost::asio::yield_context yield)
//	{
//		boost::asio::steady_timer timer(r_io);
//
//		timer.expires_from_now(std::chrono::milliseconds(30000));
//		timer.async_wait(yield); //wait for 10 seconds
//
//		/*
//		Name: MCP2ICCP/OAM_receiveMessageRequest; source = "MCP";
//		ScrGenAddr = 0x00000000-00000000; destination = "ICCP/OAM"; requestId = "{68050e30-56fd-4d6a-b30b-93d95053855e}"; destinationAddress = "0BB8413C0104F390"; DestinationAddress = 0x0BB84515-0E49E586;
//		ScriptID = 0x0BB8413C-0104F390; methodName = "receiveMessageRequest";
//		data = "{
//		"callStart":"2016-10-12 13:07:38.342",
//		"comp":"ms-d60409-01",
//		"uui":"MCM#MS-CTIEVENT005#0BB8413C0104F390#0BB845150E49E586",
//		"calledId":"0510089",
//		"citrix":"MS-CTXACRM001",
//		"callId":"4596",
//		"agent":"75931",
//		"guid":"{68050e30-56fd-4d6a-b30b-93d95053855e}",
//		"callAccept":"2016-10-12 13:07:39.948",
//		"ucid":"00001045961476266857",
//		"callerId":"79652911591",
//		"split":"0594959",
//		"user":"apolitov",
//		"agentDetails":{
//		"agentSkills":"",
//		"agentId":"2147515285",
//		"agentProfile":"29772",
//		"agentLoginName":"APolitov"
//		}
//		}"; SourceAddress = 0x0BB8435F-00000057;
//		*/
//		CMessage msg(L"MCP2ICCP/OAM_receiveMessageRequest");
//		msg[L"data"] = L"{\"callStart\":\"2016-10-12 13:07:38.342\",\"comp\" : \"ms-d60409-01\",\"uui\" : \"MCM#MS-CTIEVENT005#0BB8413C0104F390#0BB845150E49E586\",\"calledId\" : \"0510089\",\"citrix\" : \"MS-CTXACRM001\",\"callId\" : \"4596\",\"agent\" : \"75931\",\"guid\" : \"{68050e30-56fd-4d6a-b30b-93d95053855e}\",\"callAccept\" : \"2016-10-12 13:07:39.948\",\"ucid\" : \"00001045961476266857\",\"callerId\" : \"79652911591\",\"split\" : \"0594959\",\"user\" : \"apolitov\",\"agentDetails\" : {\"agentSkills\":\"\",	\"agentId\" : \"2147515285\",\"agentProfile\" : \"29772\",	\"agentLoginName\" : \"APolitov\"}}";
//		msg[L"requestId"] = L"{68050e30-56fd-4d6a-b30b-93d95053855e}";
//
//		singleton_auto_pointer<CChatInfoCollector> infoCollector;
//		decltype(auto) info = infoCollector->get_info(L"78503A1D3135430DB28B221D70E33354");
//
//		CLIENT_ADDRESS addr_from(1312313213);
//		CLIENT_ADDRESS addr_to(info.getMCCID());
//
//
//		this->MCP2MCC_ReceiveMessageRequestHandler(msg, addr_from, addr_to);
//
//		//singleton_auto_pointer<CConnectionCollector> collector;
//		//collector->Init(m_log);
//		//collector->SetIncomingMessageHandler(std::bind(&CDataReceiverImpl::onChatTextMessage, this, std::placeholders::_1));
//	});
//
//
//	
//}

std::string CDataReceiverImpl::MakeHttpJsonMessage() const
{
	CMessage msg(L"http_params");
	msg[L"Host"] = _sHttpServerHostname;
	msg[L"Port"] = _sHttpPort;

	return wtos(CMessageParser::ToStringUtf(msg));
}

void CDataReceiverImpl::SendAttachMessage(
	const std::wstring& aMessageId,
	const MCCID& aMccid,
	const ChatId& aCid,
	const attachments::TAttachmentsParam& aAtachmentParams) const
{
	SendUploadAttachmentMessage(
		aMessageId,
		aMccid,
		aCid,
		aAtachmentParams.SourceName,
		aAtachmentParams.FileSize,
		aAtachmentParams.Url);
}

	CMessage CDataReceiverImpl::CreateAttachMessage(
		const MCCID& aMccid,
		const ChatId& cid,
		const std::wstring& aFileName,
		std::string&& aContent) const
	{
		auto content = std::move(decodeBase64_rar(std::move(aContent)));
		return CreateFileStreamMessage(cid, aFileName, std::move(content));
	}

	void CDataReceiverImpl::SendDownloadAttachmentMessage(
		const std::wstring& aMessageId,
		const MCCID& aMccid,
		const ChatId& cid,
		const std::wstring& aMcpFileName,
		const std::wstring& aRealFileName,
		size_t aFileSize,
		const std::wstring& aUrl) const
	{
		std::wstring outUrl = L"http://" + _sHttpServerHostname + L"/" + _sAttachmentFolder;
		std::wstring ftpUrl = L"http://" + _sFtpHostname + L"/" + aUrl + L"/" + aMcpFileName;

		outUrl = attachments::CreateRemotePath(cid, outUrl, aMcpFileName);

		SendMultFilesMessage(
			aMccid,
			m2mhs::Create_M2MHS_DownloadFile(
				aMessageId,
				utils::toLower(ftpUrl),
				utils::toLower(outUrl),
				utils::toLower(outUrl),
				aRealFileName,
				aFileSize));
	}

	void CDataReceiverImpl::SendUploadAttachmentMessage(
		const std::wstring& aMessageId,
		const MCCID& aMccid,
		const ChatId& cid,
		const std::wstring& aFileName,
		size_t aFileSize,
		const std::wstring& aUrl) const
	{
		std::wstring mcpUrl = L"http://" + _sFtpHostname + L"/" + _sAttachmentFolder;
		std::wstring ftpUrl = L"http://" + _sFtpHostname;

		std::wstring realFileName; 
		std::tie(std::ignore, realFileName) = attachments::GetLocalPathAndFileNameFromSource(aUrl, _sAttachmentString);

		mcpUrl = attachments::CreateRemotePath(cid, mcpUrl, realFileName);
		ftpUrl = attachments::CreateRemotePath(cid, ftpUrl, realFileName);

		SendMultFilesMessage(
			aMccid,
			m2mhs::Create_M2MHS_UploadFile(
				aMessageId,
				utils::toLower(aUrl),
				utils::toLower(ftpUrl),
				utils::toLower(mcpUrl),
				aFileName,
				aFileSize));
	}

	CMessage CDataReceiverImpl::CreateFileStreamMessage(
		const ChatId& cid,
		const std::wstring& aFileName,
		std::string&& aContent) const
	{
		CMessage msg;

		auto attachParam = attachments::saveAttachment(
			cid,
			_sAttachmentDisk,
			_sAttachmentFolder,
			_sAttachmentString,
			std::move(aContent),
			std::move(aFileName));

		msg[L"name"] = attachParam.SourceName;
		msg[L"size"] = attachParam.FileSize;
		msg[L"url"] = attachParam.Url;

		return msg;
	}

int CDataReceiverImpl::init_impl(CSystemLog* pLog)
{
	if (!pLog)
		return -1111;

	m_log = pLog;

	_collector.Init(m_log);
	_infoCollector.Init(m_log);

	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	ConnectHandlers();

	boost::asio::spawn(r_io, [&](boost::asio::yield_context yield)
	{
		boost::asio::steady_timer timer(r_io);

		timer.expires_from_now(std::chrono::milliseconds(100));
		timer.async_wait(yield); //wait for 10 seconds

		start();
	});

	return 0;
}

void CDataReceiverImpl::MakeServer(
	const std::string& aUri,
	const std::string& aPort,
	const size_t aClientId,
	const std::string& aCertificate,
	const std::string& aPrivateKey,
	const NetProxy& aProxy)
{
	BoostHelper::Visit(aProxy.ServerProxy,
		[&](const spawn_server::ServerProxy& aProxy)
		{
			using Proxy = spawn_server::ServerProxy;
			
			Proxy::TConnectionParams params;
			params._uri = aUri;
			params._port = aPort;
			params._clientId = aClientId;
			params._handler = std::bind(&CDataReceiverImpl::onChatMessage, this, std::placeholders::_1, std::placeholders::_2);

			Proxy::MakeServer<ChatConnection>(&r_io, std::move(params), m_log);
		},
		[&](const ssl_spawn_server::ServerProxy& aProxy)
		{
			using Proxy = ssl_spawn_server::ServerProxy;

			Proxy::TConnectionParams params;
			params._uri = aUri;
			params._port = aPort;
			params._clientId = GetClientId();
			params._sertificate = aCertificate;
			params._private_key = aPrivateKey;
			params._handler = std::bind(&CDataReceiverImpl::onChatMessage, this, std::placeholders::_1, std::placeholders::_2);

			Proxy::MakeServer<ChatSslConnection>(&r_io, std::move(params), m_log);
		});
}



int CDataReceiverImpl::start()
{
	SystemConfig settings;

	_sAttachmentFolder = utils::toLower(settings->safe_get_config_value(L"AttachmentFolder", std::wstring(L"Attachments\\outbox\\")));
	_sAttachmentDisk = utils::toLower(settings->safe_get_config_value(L"AttachmentDisk", std::wstring(L"c")));
	_sAttachmentString = utils::toLower(settings->safe_get_config_value(L"AttachmentString", std::wstring(L"http://ms-ccvm002/Attachments/")));
	
	_sFtpHostname = utils::toLower(settings->safe_get_config_value(L"FtpHostname", std::wstring(L"")));
	_sFtpUsername = settings->safe_get_config_value(L"FtpUsername", std::wstring(L""));
	_sFtpPassword = settings->safe_get_config_value(L"FtpPassword", std::wstring(L""));

	_sHttpServerHostname = utils::toLower(settings->safe_get_config_value(L"HttpHost", std::wstring(L"")));
	_sHttpPort = settings->safe_get_config_value(L"HttpPort", std::wstring(L""));

	//_isHttpServetEnable = settings->safe_get_config_value(L"HttpServerEnable", std::wstring(L"false")) == L"true";

	_uCookieExpiredTime = settings->safe_get_config_value(L"CookieExpiredTime", EXPIREDTIME);

	_sClientHost = wtos(settings->safe_get_config_value(L"ClientHost", std::wstring(L"0.0.0.0")));
	_sClientPort = wtos(settings->safe_get_config_value(L"ClientPort", std::wstring(L"80")));
	_sClientPartyId = wtos(settings->safe_get_config_value(L"PartyId", std::wstring{}));

	_xApiEnvironment = wtos(settings->safe_get_config_value(L"XApiEnvironment", std::wstring{}));

	_bIsSsl = settings->safe_get_config_bool(L"SSL", false);

	if (_bIsSsl)
	{
		_netProxy.ClientProxy = ssl_spawn_client::ClientProxy{};
		_netProxy.ServerProxy = ssl_spawn_server::ServerProxy{};
	}
	else
	{
		_netProxy.ClientProxy = spawn_client::ClientProxy{};
		_netProxy.ServerProxy = spawn_server::ServerProxy{};
	}

	if (true)
	{
		MakeServer(
			wtos(settings->safe_get_config_value(L"ServerHost", std::wstring(L"0.0.0.0"))),
			wtos(settings->safe_get_config_value(L"ServerPort", std::wstring(L"80"))),
			GetClientId(),
			wtos(settings->safe_get_config_value(L"SSLSertificate", std::wstring(L""))),
			wtos(settings->safe_get_config_value(L"SSLPrivateKey", std::wstring(L""))),
			_netProxy);
	}

	if (true)
	{
		spawn_server::ServerProxy::TConnectionParams params;

		params._uri = wtos(settings->safe_get_config_value(L"ServerHost", std::wstring(L"0.0.0.0")));
		params._port = wtos(settings->safe_get_config_value(L"KeepAlivePort", std::wstring(L"81")));
		params._clientId = GetClientId();

		spawn_server::ServerProxy::MakeServer<KeepAliveConnection>(&r_io, std::move(params), m_log);
	}

	if (true)
	{
		int iTimeToExpired = settings->safe_get_config_value(L"ChatExpiredTime", EXPIREDTIME);
		makeGarbageCleaner(iTimeToExpired);
	}

	return 0;
}


void CDataReceiverImpl::stop()
{
	r_io.stop();
}

void CDataReceiverImpl::on_routerConnect()
{
	m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
}

void CDataReceiverImpl::on_routerDeliverError(const CMessage& _msg, const CLIENT_ADDRESS &_from, const CLIENT_ADDRESS &_to)
{
	m_log->LogStringModule(LEVEL_FINE, L"Router client", _msg.Dump().c_str());
}

void CDataReceiverImpl::on_appStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	if (nStatus == /*static_cast<int>*/(core::ClientStatus::CS_DISCONNECTED))
	{
		try
		{

		}
		catch (...)
		{

		}
	}

}

static __int64 CreateMCCID(int num_message, uint32_t _client_id)
{
	__int64 mccid = _client_id;
	mccid = (mccid << 32) | num_message;
	return mccid;
}

bool CDataReceiverImpl::makeGarbageCleaner(int expiredTime)
{
	boost::asio::spawn(r_io, [io_service = &r_io, pLog = m_log, expiredTime = expiredTime, self = this](boost::asio::yield_context yield)
	{
		try
		{
			boost::asio::steady_timer timer(*io_service);
			boost::system::error_code errorCode;

			while (!(*io_service).stopped())
			{
				timer.expires_from_now(std::chrono::seconds(expiredTime));
				timer.async_wait(yield[errorCode]); //wait for timer
				
				pLog->LogStringModule(LEVEL_FINEST, L"steady timer", L"========================Tick===================");
				
				if (errorCode)
				{
					throw errorCode;
				}

				CChatInfoCollector::IdList expiredList = self->_infoCollector.getExpiredChatList(time(nullptr) - expiredTime);

				for each (const auto& pair in expiredList)
				{
					self->_makeChatTimeout(pair.first, pair.second);
				}

			}

		}
		catch (boost::system::error_code& error)
		{
			pLog->LogStringModule(LEVEL_WARNING, L"Cleaner", L"Socket failed with error: %s", stow(error.message()));
		}
		catch (...)
		{
			pLog->LogStringModule(LEVEL_WARNING, L"Cleaner", L"Socket failed with error: unhandled exception caught");
		}
	});

	return true;
}

std::wstring CDataReceiverImpl::_makeBadRequest(std::string couse)const
{
	return stow(TgBot::HttpParser::getInstance().generateResponse(std::string("\n{ \"reason\":\"") + couse + "\" }", "application/json", 500, "Internal Server Error"));
}

std::wstring CDataReceiverImpl::_makeOKRequest(std::string data) const
{
	return stow(TgBot::HttpParser::getInstance().generateResponse(data, "application/json", 200, "OK"));
}

std::wstring CDataReceiverImpl::_makeOKRequest() const
{
	return stow(TgBot::HttpParser::getInstance().generateResponse("", "text/plain", 200, "OK"));
}

std::wstring CDataReceiverImpl::_makeOKCookieRequest(const std::string& aData) const
{
	std::string cookie = utils::toStr<char>(GetClientId());
	return stow(TgBot::HttpParser::getInstance().generateResponse(aData, "text/plain", 200, "OK", false, cookie, MakeDate(_uCookieExpiredTime)));
}


std::wstring CDataReceiverImpl::_make404NotFound() const
{
	return stow(TgBot::HttpParser::getInstance().generateResponse("", "text/plain", 404, "Not Found"));
}

std::wstring CDataReceiverImpl::_makeMessagesByCid(const ChatId& chatId) const
{
	CMessage msg(L"MESSAGES_BYSID");
	decltype(auto) info = _infoCollector.get_info(chatId);

	msg[L"status"] = info.getStatusString();

	Operator oper = info.getOperator();

	CMessage operatorMsg(L"operator");
	operatorMsg[L"id"] = oper.id;
	operatorMsg[L"name"] = oper.name;

	msg[L"operator"] = operatorMsg;
	if (_collector.ifExist(info.getMCCID()))
	{
		MessageList messagesToSend;
		for each (const auto& _msg in _collector.get_messages(info.getMCCID()))
		{
			if (_msg == L"CHATMESSAGE" || _msg == L"SCRIPTMESSAGE")
			{
				CMessage chatMsg(L"CHATMESSAGE");
				chatMsg[L"cid"]			= _msg[L"cid"];
				chatMsg[L"contentType"] = _msg[L"contentType"];
				chatMsg[L"content"]		= _msg[L"content"];
				chatMsg[L"code"]		= _msg[L"code"];
				chatMsg[L"timestamp"]	= _msg[L"timestamp"];
				chatMsg[L"scriptId"]    = _msg[L"scriptId"];
				
				if (_msg.IsParam(L"author"))
				{
					chatMsg[L"author"] = _msg[L"author"];
				}

				if (_msg.IsParam(L"skillGroup"))
				{
					chatMsg[L"skillGroup"] = _msg[L"skillGroup"];
				}

				messagesToSend.emplace_back(std::move(chatMsg));
			}
		}

		if (!messagesToSend.empty())
		{
			msg[L"messages"] = core::to_raw_data(messagesToSend);
		}
	}

	return _makeOKRequest(wtos(CMessageParser::ToStringUtf(msg)));
}

std::wstring CDataReceiverImpl::_makeStatusOnlyMessage(const ChatId& chatId) const
{
	CMessage msg(L"MESSAGES_BYSID");

	decltype(auto) info = _infoCollector.get_info(chatId);

	msg[L"status"] = info.getStatusString();

	Operator oper = info.getOperator();

	CMessage operMsg(L"operator");
	operMsg[L"id"] = oper.id;
	operMsg[L"name"] = oper.name;

	msg[L"operator"] = operMsg;

	return _makeOKRequest(wtos(CMessageParser::ToStringUtf(msg)));
}

CMessage CDataReceiverImpl::_makeMCPMessage(MCCID mccid, SessionId sid, std::wstring content, std::wstring contentType, std::wstring ChannelId, MessageList && attachments, std::wstring Title, std::wstring ChannelName, std::wstring messageId) const
{
	CMessage msg(L"MCP2ICCP/OAM_receiveMessage");

	CMessage jsondata;

	jsondata[L"channelType"] = 14;
	jsondata[L"channelId"] = std::move(ChannelId);
	jsondata[L"sessionId"] = sid;
	jsondata[L"channelName"] = std::move(ChannelName);
	jsondata[L"messageReceiver"] = L"MCC_CHAT";
	jsondata[L"messageTitle"] = Title;
	jsondata[L"MCCID"] = mccid;

	jsondata[L"messageId"] = messageId;
	jsondata[L"messageDate"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));

	if (!attachments.size())
	{
		jsondata[L"message"] = std::move(content);
		msg[L"jsondata"] = CMessageParser::ToStringUnicode(jsondata);
	}
	else
	{
		jsondata[L"message"] = L"File in attachment";
		jsondata[L"attachments"] = core::to_raw_data(attachments);
		msg[L"jsondata"] = CMessageParser::ToStringUnicode(std::move(jsondata)/*, L"attachments", std::move(attachments)*/);
	}

	return msg;
}

void CDataReceiverImpl::_makeChatTimeout(MCCID mccid, SessionScriptId ssid)
{
	if (ssid)
		_sendSessionTimeout(mccid, ssid);
	else
	{
		m_log->LogString(LEVEL_WARNING, L"Chat with id = %llu has expired earlier than the script was launched", mccid);

		_infoCollector.delete_info(mccid);
	}
}

void CDataReceiverImpl::_makeInitMessage(
	MCCID mccid, 
	const ChatId& cid, 
	std::wstring && market_code, 
	std::wstring && customer_name, 
	const std::wstring & ip,
	std::wstring && os_version,
	std::wstring && app_version,
	std::wstring && connection_type,
	std::wstring && phone_model) const
{
	IdGenerator generator;

	CMessage chatMsg(L"INIT_CHATMESSAGE");
	chatMsg[L"cid"] = cid;
	chatMsg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::NEW);
	chatMsg[L"contentType"] = L"TEXT";
	chatMsg[L"messageId"] = generator->makeId(0);
	chatMsg[L"content"] = (boost::wformat(
		L"Market code: %s\n Customer name: %s\n Ip address: %s\n "\
		L"OS version: %s\n App version: %s\n Connection type: %s\n Phone model: %s"
		) % std::move(market_code) %std::move(customer_name) % ip
		% std::move(os_version) % std::move(app_version) % std::move(connection_type) % std::move(phone_model)).str();

	m_log->LogStringModule(LEVEL_FINEST, L"[TEST]", L"Push Init: %s", chatMsg.Dump().c_str());
	_collector.push_to_mcp_message(mccid, std::move(chatMsg));
}

void CDataReceiverImpl::_sendBeginSession(MCCID mccid, std::wstring&& contact, std::wstring&& ChannelId, std::wstring&& split, std::wstring&& ipAddress, std::wstring&& topic_name) const
{
	CMessage msg(L"ANY2SM_BeginSession");
	msg[L"ChannelName"] = ChannelId;// L"LC";
	msg[L"ChannelId"] = std::move(contact);
	msg[L"Split"] = std::move(split);
	msg[L"IpAddress"] = ipAddress;
	msg[L"ChannelType"] = L"14";
	msg[L"TopicName"] = std::move(topic_name);

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(CLIENT_ADDRESS::Any());

	this->SendFromTo(std::move(msg), addr_from, addr_to);

}

void CDataReceiverImpl::_sendReceiveMessageRequestAck(MCCID mccid, SessionId sid, MCPId mid, std::wstring requestId, std::wstring ChannelId, std::wstring Title, std::wstring ChannelName) const
{
	CMessage msg(L"MCP2ICCP/OAM_receiveMessageRequestAck");
	msg[L"requestId"] = requestId;
	msg[L"intStatus"] = 0;

	msg[L"ftpHostname"] = _sFtpHostname;
	msg[L"ftpUsername"] = _sFtpUsername;
	msg[L"ftpPassword"] = _sFtpPassword;

	MessageList messagesToSend;

	if (_collector.ifExist(mccid))
	{
		_collector.ForEachMessage(mccid, [&](CMessage & _msg)
		{
			if (_msg == L"CHATMESSAGE" || _msg == L"INIT_CHATMESSAGE" || _msg == L"TRANSFER")
			{
				MESSAGE_STATUS status = static_cast<MESSAGE_STATUS>(_msg.SafeReadParam(L"messageStatus", CMessage::CheckedType::Int, static_cast<int>(MESSAGE_STATUS::NEW)).AsInt());
				//if message has already sent -> skip it
				if (status == MESSAGE_STATUS::SENT
					|| status == MESSAGE_STATUS::RECIEVED
					|| status == MESSAGE_STATUS::DELEYIED)
				{
					return;
				}

				m_log->LogString(LEVEL_FINEST, L"Message: %s", _msg.Dump());

				_msg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::SENT);

				std::wstring contentType = _msg.SafeReadParam(L"contentType", CMessage::CheckedType::String, L"").AsWideStr();
				std::wstring content = _msg.SafeReadParam(L"content", CMessage::CheckedType::String, L"").AsWideStr();

				CMessage msg(L"MCP2ICCP/OAM_receiveMessage");

				if (_msg == L"INIT_CHATMESSAGE")
					msg[L"url"] = content;


				if (contentType == L"TEXT")
					msg[L"message"] = std::move(content);
				else
				{
					CMessage attachMessage;
					attachMessage[L"name"] = _msg[L"fileName"];
					attachMessage[L"size"] = _msg[L"fileSize"]; 
					attachMessage[L"url"] = _msg[L"fileUrl"];

					MessageList attachments = { attachMessage };
					msg[L"attachments"] = core::to_raw_data(attachments);
				}

				msg[L"channelType"] = 14;
				msg[L"channelId"] = ChannelId;
				msg[L"sessionId"] = sid;
				msg[L"channelName"] = ChannelName;
				msg[L"messageReceiver"] = L"MCC_CHAT";
				msg[L"messageTitle"] = Title;
				msg[L"MCCID"] = mccid;

				msg[L"messageId"] = _msg[L"messageId"];
				msg[L"messageDate"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));

				messagesToSend.emplace_back(msg);
			}
		});
	}

	if (!messagesToSend.empty())
	{
		CMessage jsondata;
		jsondata[L"jsondata"] = core::to_raw_data(messagesToSend);

		std::wstring response = CMessageParser::ToStringUnicode(jsondata);

		int pos1 = response.find(L"[");
		int pos2 = response.rfind(L"]");

		if (pos1 > 0 && pos2 > 0)
		{
			msg[L"jsondata"] = response.substr(pos1, pos2 - pos1 + 1);
		}
	}

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(mid);
	SendFromTo(std::move(msg), addr_from, addr_to);
}

void CDataReceiverImpl::_sendReceiveMessage(MCCID mccid, MCPId mid, const CMessage& msg) const
{
	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(mid);

	this->SendFromTo(msg, addr_from, addr_to);
}

void CDataReceiverImpl::_sendSessionEnded(MCCID mccid, SessionScriptId ssid) const
{
	m_log->LogString(LEVEL_FINEST, L"This chat with ChatId = %llu has ended by Abonent", mccid);

	CMessage msg(L"MCC2S_SessionEnded");

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(ssid);
	this->SendFromTo(std::move(msg), addr_from, addr_to);
}

void CDataReceiverImpl::_sendSessionTimeout(MCCID mccid, SessionScriptId ssid) const
{
	m_log->LogString(LEVEL_FINEST, L"This chat with ChatId = %llu has expired", mccid);

	CMessage msg(L"MCC2S_SessionTimeout");

	_infoCollector.update_status(mccid, CHAT_STATUS::CS_EXPIRED);

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(ssid);
	this->SendFromTo(std::move(msg), addr_from, addr_to);
}

void CDataReceiverImpl::_sendReplyToMessageAck(MCCID mccid, SessionScriptId ssid) const
{
	CMessage msg(L"S2MCC_replyToMessageAck");
	msg[L"intStatus"] = 0;

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(ssid);
	this->SendFromTo(std::move(msg), addr_from, addr_to);

}

void CDataReceiverImpl::_sendReplyToMessageAck(MCCID mccid, SessionScriptId ssid, std::wstring requestId) const
{
	CMessage msg(L"MCP2ICCP/OAM_replyToMessageAck");
	msg[L"requestId"] = requestId;
	msg[L"intStatus"] = 0;

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(ssid);
	this->SendFromTo(std::move(msg), addr_from, addr_to);

}

void CDataReceiverImpl::MakeClient(const NetProxy& aProxy, const CMessage& msg) const
{
	BoostHelper::Visit(
		aProxy.ClientProxy,
		[&](const spawn_client::ClientProxy& aProxy)
		{
			using Proxy = spawn_client::ClientProxy;

			Proxy::TConnectionParams params;
			params._uri = _sClientHost;
			params._port = _sClientPort;
			params._partyId = _sClientPartyId;
			params._clientId = GetClientId();
			params._msg = msg;
			params._xApiEnvironment = _xApiEnvironment;
			params._statusHandler = std::bind(
				&CDataReceiverImpl::OnConnectionStatusMessage,
				this,
				std::placeholders::_1,
				std::placeholders::_2,
				std::placeholders::_3);

			Proxy::MakeClient(std::move(params), &r_io, m_log);

		},
		[&](const ssl_spawn_client::ClientProxy& aProxy)
		{
			using Proxy = ssl_spawn_client::ClientProxy;

			Proxy::TConnectionParams params;
			params._uri = _sClientHost;
			params._port = _sClientPort;
			params._partyId = _sClientPartyId;
			params._clientId = GetClientId();
			params._msg = msg;
			params._xApiEnvironment = _xApiEnvironment;
			params._statusHandler = std::bind(
				&CDataReceiverImpl::OnConnectionStatusMessage,
				this,
				std::placeholders::_1,
				std::placeholders::_2,
				std::placeholders::_3);

			Proxy::MakeClient(std::move(params), &r_io, m_log);

		});
}

void CDataReceiverImpl::_sendChatMessageToServer(const CMessage& msg) const
{
	MakeClient(_netProxy, msg);
}

void CDataReceiverImpl::_sendSessionInfoMessage(MCCID mccid, SessionId sid, MCPId mid, std::wstring reason) const
{
	CMessage msg(L"MCP2ICCP/OAM_sessionInfo");
	msg[L"sessionId"] = sid;
	msg[L"cause"] = 1;
	msg[L"code"] = 1;
	msg[L"text"] = reason;

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(mid);
	this->SendFromTo(std::move(msg), addr_from, addr_to);
}

void CDataReceiverImpl::SendMultFilesMessage(
	const MCCID& aMccid,
	CMessage&& aMsg) const
{
	CLIENT_ADDRESS addr_from(aMccid);
	CLIENT_ADDRESS addr_to(CLIENT_ADDRESS::Any());

	this->SendFromTo(std::move(aMsg), addr_from, addr_to);
}

/******************************* eof *************************************/