#include "stdafx.h"
//#include "MakeServer.h"
////#include "ServerSocket.h"
//
//#include <boost/asio/spawn.hpp>
//#include "MessageCollector.h"
//#include "MessageParser.h"
//#include "tgbot/net/HttpParser.h"
//#include "Connection.h"
//#include "KeepAliveConnection.h"
//
//using boost::asio::ip::tcp;
//
//
//template <class TConnection>
//bool MakeServer(boost::asio::io_service* io_service, TConnectionServerParams params, CSystemLog * pLog)
//{
//	boost::asio::spawn(*io_service, [io_service = io_service, params = std::move(params), pLog = pLog](boost::asio::yield_context yield)
//	{
//		auto LogStringModule = [pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//		{
//			pLog->LogStringModule(level, TConnection::connectionName()/*L"Keep Alive Server"*/, formatMessage, std::forward<decltype(args)>(args)...);
//		};
//
//
//		try
//		{
//			singleton_auto_pointer<CMessageCollector> collector; //!! do not delete !!
//
//			LogStringModule(LEVEL_FINEST, L"Open socket (host: %s, port: %s)...", stow(params._uri), stow(params._port));
//
//			boost::asio::ip::tcp::resolver resolver(*io_service);
//			boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//			boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//			boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//			LogStringModule(LEVEL_FINEST, L"Add acceptor...");
//			/// Acceptor used to listen for incoming connections.
//			boost::asio::ip::tcp::acceptor acceptor_(*io_service);
//			acceptor_.open(endpoint.protocol());
//			acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
//			acceptor_.bind(endpoint);
//			acceptor_.listen();
//
//			boost::system::error_code errorCode;
//
//			LogStringModule(LEVEL_INFO, L"Waiting for connection...");
//
//			while (!(*io_service).stopped())
//			{
//				boost::asio::ip::tcp::socket _socket(*io_service);
//
//				acceptor_.async_accept(_socket, yield[errorCode]);
//
//				if (errorCode)
//					throw errorCode;
//
//				std::make_shared<TConnection>(std::move(_socket), pLog)->startListening();
//			}
//		}
//		catch (boost::system::error_code& error)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//		}
//		catch (...)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//		}
//
//	});
//
//	int test = 0;
//	return false;
//}
//
//bool makeMyServer(boost::asio::io_service* io_service, TConnectionServerParams params, CSystemLog * pLog)
//{
//	return MakeServer<ChatConnection>(io_service, params, pLog);
//}
//
//bool makeKeepAliveServer(boost::asio::io_service * io_service, TConnectionServerParams params, CSystemLog * pLog)
//{
//	return MakeServer<KeepAliveConnection>(io_service, params, pLog);;
//}
//
//const int TCP_PACK_SIZE = 1024;
//static unsigned int _count = 0;
//
//bool makeMyClient(boost::asio::io_service* io_service, TConnectionClientParams params, CSystemLog * pLog)
//{
//	boost::asio::spawn(*io_service, [io_service = io_service, params = std::move(params), pLog = pLog](boost::asio::yield_context yield)
//	{
//		std::wstring clientName = L"Client " + std::to_wstring(++_count);
//
//		auto LogStringModule = [pLog, clientName = std::move(clientName)](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//		{
//			pLog->LogStringModule(level, clientName.c_str(), formatMessage, std::forward<decltype(args)>(args)...);
//		};
//
//		auto read_complete_predicate = [&](int data_len)
//		{
//			if (!data_len)
//				return true;
//			return 	data_len < TCP_PACK_SIZE;
//		};
//
//		auto addPartyId = [](const std::string aPartyId)
//		{
//			if (aPartyId.empty())
//			{
//				return std::string{};
//			}
//
//			return std::string{ "/" } +aPartyId;
//		};
//
//		boost::asio::steady_timer timer(*io_service);
//		boost::system::error_code errorCode;
//
//		singleton_auto_pointer<CMessageCollector> collector;
//		std::string errorMsg;
//
//		const auto &requestMsg = params._msg;
//		SessionScriptId sessionScriptId = requestMsg.SafeReadParam(
//			L"scriptId",
//			CMessage::CheckedType::Int64,
//			0).AsUInt64();
//
//		try
//		{
//			// makeClient
//			LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());
//
//			boost::asio::ip::tcp::resolver resolver(*io_service);
//			boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//			boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//			boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//			boost::asio::ip::tcp::socket _socket(*io_service);
//			boost::system::error_code errorCode;
//
//			boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);
//
//			if (errorCode)
//			{
//				LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
//				throw errorCode;
//			}
//
//			std::wstring request = CMessageParser::ToStringUtf(requestMsg);
//			std::string url;
//			bool bPut = false;
//
//			requestMsg.CheckParam(L"cid", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
//			ChatId cid = requestMsg.SafeReadParam(L"cid", CMessage::CheckedType::String, L"").AsWideStr();
//				
//			if (requestMsg == L"CHATMESSAGE" || requestMsg == L"END_SESSION" || requestMsg == L"SCRIPTMESSAGE")
//			{
//				url = "http://" + params._uri + addPartyId(params._partyId) + "/chatsessions/" + wtos(cid) + "/chatmessages";
//			}
//			if (requestMsg == L"TRANSFER")
//			{
//				bPut = true;
//				url = "http://" + params._uri + addPartyId(params._partyId) + "/chatsessions/" + wtos(cid) + "/operator";
//			}
//			if (requestMsg == L"SESSION_STATUS")
//			{
//				bPut = true;
//				url = "http://" + params._uri + addPartyId(params._partyId) + "/chatsessions/" + wtos(cid);
//			}
//
//			std::vector<TgBot::HttpReqArg> args;
//			args.push_back(TgBot::HttpReqArg("jsondata", wtos(request)));
//
//			std::string httpRequest = TgBot::HttpParser::getInstance().generateRequest(url, args, false, bPut);
//
//			LogStringModule(LEVEL_FINEST, L"Write: %s", httpRequest.c_str());
//
//			std::string in_data;
//			int len = 0;
//
//			_socket.async_write_some(boost::asio::buffer(httpRequest), yield[errorCode]);
//
//			if (errorCode)
//			{
//				throw errorCode;
//			}
//
//			do
//			{
//				char reply_[TCP_PACK_SIZE] = {};
//				_socket.async_read_some(boost::asio::buffer(reply_), yield);
//				if (errorCode)
//				{
//					throw errorCode;
//				}
//
//				len = strlen(reply_);
//
//				in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);
//
//			} while (!read_complete_predicate(len));
//
//			LogStringModule(LEVEL_FINEST, L"Read: %s", in_data.c_str());
//
//			if (in_data == "500")
//			{
//				collector->SetStatus(sessionScriptId, CONNECTION_STATUS::ERROR_ANS, L"500");
//			}
//			else
//			{
//				collector->SetStatus(sessionScriptId, CONNECTION_STATUS::SUCCESS_ANS, L"OK");
//			}
//		}
//		catch (boost::system::error_code& error)
//		{
//			if (error != boost::asio::error::eof)
//			{
//				errorMsg = error.message();
//			}
//		}
//		catch (std::runtime_error & error)
//		{
//			errorMsg = error.what();
//		}
//		catch (...)
//		{
//			errorMsg = "unhandled exception caught";
//		}
//
//		if (!errorMsg.empty())
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(errorMsg));
//			collector->SetStatus(sessionScriptId, CONNECTION_STATUS::NO_ANS, stow(errorMsg));
//		}
//	});
//
//
//	int test = 0;
//	return false;
//}
//
//
///******************************* eof *************************************/