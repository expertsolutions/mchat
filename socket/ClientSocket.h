#pragma once

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <boost/coroutine/asymmetric_coroutine.hpp>
#include <boost/coroutine/symmetric_coroutine.hpp>

#include "Log/SystemLog.h"

char reply_[1028];


class CClientSocket
{
public:

	using read_until_predicate = std::function<bool(const std::string&)>;

	CClientSocket(CSystemLog* pLog,
		const TConnectionParams& params,
		boost::asio::io_service &io_service,
		boost::coroutines::symmetric_coroutine<void>::call_type& call,
		boost::coroutines::symmetric_coroutine<void>::yield_type& yield)
		: _call(&call), _yield(&yield),
		_uri(params._uri), _port(params._port), _log(pLog)
	{
		boost::asio::ip::tcp::resolver resolver(io_service);
		boost::asio::ip::tcp::resolver::query query(_uri, _port);
		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);

		_socket = std::make_unique<boost::asio::ip::tcp::socket>(io_service);

		boost::system::error_code errorCode;

		boost::asio::async_connect(_socket->lowest_layer(), iterator,
			[&](const boost::system::error_code& error, boost::asio::ip::tcp::resolver::iterator)
		{
			errorCode = error;

			(*_call)();
		}
		);

		(*_yield)();

		if (errorCode)
			throw errorCode;
	}

	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring&_lpszModule, const std::wstring& formatMessage, TArgs&&... args)
	{
		if (!_log)
			return;
		_log->LogStringModule(level, std::wstring(L" " + _lpszModule), formatMessage, std::forward<TArgs>(args)...);
	}

	//std::string readUntil_if(read_until_predicate call_back)
	//{
	//	std::string in_data;

	//	do
	//	{
	//		in_data += readSome();

	//	} while (!call_back(in_data));

	//	return in_data;
	//}

	std::string readSome()
	{
		std::string in_data;

		boost::system::error_code errorCode;

		_socket->async_read_some(boost::asio::buffer(reply_, sizeof(reply_)),
			[&](const boost::system::error_code& error,
				size_t bytes_transferred)
		{
			if (!error && bytes_transferred)
			{

				in_data.append(reply_, bytes_transferred);
			}

			errorCode = error;
			(*_call)();
		}
		);

		(*_yield)();

		if (errorCode && errorCode != boost::asio::error::eof)
			throw errorCode;

		LogStringModule(LEVEL_FINEST, L"socket", L"Read: %s", stow(in_data).c_str());

		return in_data;
	}

	template <class T>
	void write(T&& command)
	{
		//std::cout << "write:" << command << std::endl;

		LogStringModule(LEVEL_FINEST, L"socket", L"Write: %s", stow(command).c_str());

		size_t size = command.size();

		boost::system::error_code errorCode;

		_socket->async_write_some(boost::asio::buffer(std::forward<T>(command), size),
			[&](const boost::system::error_code& error,
				size_t bytes_transferred)
		{
			errorCode = error;

			(*_call)();
		}
		);

		(*_yield)();

		if (errorCode)
			throw errorCode;

	}

private:

	boost::coroutines::symmetric_coroutine<void>::call_type* _call{ 0 };
	boost::coroutines::symmetric_coroutine<void>::yield_type* _yield{ 0 };

	std::unique_ptr<boost::asio::ip::tcp::socket> _socket;

	std::string _uri;
	std::string _port;

	CSystemLog* _log;
};
