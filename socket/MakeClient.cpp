//#include "stdafx.h"
//#include <boost/thread.hpp>
//#include "boost/asio/ssl.hpp"
//#include <boost/asio/steady_timer.hpp>
//#include <boost/asio/spawn.hpp>
//#include "MakeClient.h"
//#include "ClientSocket.h"
//#include "MessageCollector.h"
//
//#include "tgbot/net/httpparser.h"
//#include "tgbot/tgtypeparser.h"
//
//
//const int TCP_PACK_SIZE = 1024;
//
//bool makeMyClient(TConnectionParams& params, CSystemLog *pLog)
//{
//	auto LogStringModule = [pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//	{
//		if (!pLog)
//			return;
//		pLog->LogStringModule(level, L"client", formatMessage, std::forward<decltype(args)>(args)...);
//	};
//
//	auto read_complete_predicate = [&](int data_len)
//	{
//		if (!data_len)
//			return true;
//		return 	data_len < TCP_PACK_SIZE;
//	};
//
//
//	boost::asio::io_service io_service;
//	boost::asio::spawn(io_service, [&](boost::asio::yield_context yield)
//	{
//		boost::asio::ip::tcp::resolver resolver(io_service);
//		boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//		boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//		boost::asio::ip::tcp::socket _socket(io_service);
//
//		boost::system::error_code errorCode;
//
//		_socket.async_connect(endpoint, yield[errorCode]);
//
//		if (errorCode)
//		{
//			throw errorCode;
//		}
//
//		_socket.async_write_some(boost::asio::buffer(params._request), yield[errorCode]);
//
//		if (errorCode)
//		{
//			throw errorCode;
//		}
//
//		std::string in_data;
//		int len = 0;
//		try
//		{
//			do
//			{
//				char reply_[TCP_PACK_SIZE] = {};
//				_socket.async_read_some(boost::asio::buffer(reply_), yield);
//				if (errorCode)
//				{
//					throw errorCode;
//				}
//
//				len = strlen(reply_);
//
//				in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);
//
//			} while (!read_complete_predicate(len));
//		}
//		catch (boost::system::error_code& error)
//		{
//			if (error != boost::asio::error::eof)
//				LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//		}
//		catch (std::runtime_error & error)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
//		}
//		catch (...)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//		}
//
//		_socket.cancel();
//
//		params._response = in_data;
//
//	});
//
//	io_service.run();
//
//	int test = 0;
//
//	return true;
//}
//
//bool makeMyClient_JustSend(TConnectionParams & params, CSystemLog * pLog)
//{
//	auto LogStringModule = [pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//	{
//		if (!pLog)
//			return;
//		pLog->LogStringModule(level, L"client", formatMessage, std::forward<decltype(args)>(args)...);
//	};
//
//	int size_response = 0;
//	auto read_complete_predicate = [&](const std::string& data)
//	{
//		if (size_response && size_response == data.size())
//		{
//			return true;
//		}
//
//		if (data.empty())
//			return true;
//
//		size_response = data.size();
//
//		return false;
//	};
//
//
//	boost::asio::io_service io_service;
//	boost::asio::spawn(io_service, [&](boost::asio::yield_context yield)
//	{
//		boost::asio::ip::tcp::resolver resolver(io_service);
//		boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//		boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//		boost::asio::ip::tcp::socket _socket(io_service);
//
//		boost::system::error_code errorCode;
//
//		_socket.async_connect(endpoint, yield[errorCode]);
//
//		if (errorCode)
//		{
//			throw errorCode;
//		}
//
//		_socket.async_write_some(boost::asio::buffer(params._request), yield[errorCode]);
//
//		if (errorCode)
//		{
//			throw errorCode;
//		}
//
//	});
//
//	io_service.run();
//
//	int test = 0;
//
//	return true;
//}
//
//static std::string GetFileSource(const std::wstring& aFileName)
//{
//	std::string source;
//	return source;
//}
//
//static std::string MakeRequest(const CMessage& aRequestMsg, const TConnectionClientParams& aParams, bool aKeepAlive)
//{
//	aRequestMsg.CheckParam(L"id", CMessage::CheckedType::Int64, CMessage::ParamType::Mandatory);
//
//	ChatId cid = aRequestMsg.SafeReadParam(L"id", CMessage::CheckedType::Int64, 0).AsInt64();
//
//	std::vector<TgBot::HttpReqArg> args;
//	args.push_back(TgBot::HttpReqArg("chat_id", cid));
//	std::string url;
//
//	std::wstring contentType = aRequestMsg.SafeReadParam(L"contentType", CMessage::CheckedType::String, L"text").AsWideStr();
//	if (contentType == MessageContentType::TEXT)
//	{
//		url = std::string("https://") + aParams._uri + "/bot" + aParams._token + "/sendMessage";
//
//		std::wstring text = aRequestMsg.SafeReadParam(L"content", CMessage::CheckedType::String, L"").AsWideStr();
//		args.push_back(TgBot::HttpReqArg("text", wtos(text)));
//
//		bool bButtonEnable = aRequestMsg.SafeReadParam(L"makeButton", CMessage::CheckedType::Bool, false).AsBool();
//		if (bButtonEnable)
//		{
//			TgBot::ReplyKeyboardMarkup::Ptr rpk = std::make_shared<TgBot::ReplyKeyboardMarkup>();
//			rpk->keyboard.push_back({ TgBot::KeyboardButton("PUSH HERE TO SUBMIT", true) });
//			rpk->oneTimeKeyboard = true;
//			rpk->resizeKeyboard = true;
//
//			args.push_back(TgBot::HttpReqArg("reply_markup", TgBot::TgTypeParser::getInstance().parseGenericReply(rpk)));
//		}
//
//	}
//	else if (contentType == MessageContentType::FILE)
//	{
//		url = std::string("https://") + aParams._uri + "/bot" + aParams._token + "/sendDocument";
//
//		aRequestMsg.CheckParam(L"fileUrl", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
//		std::string fileName = wtos(aRequestMsg.SafeReadParam(L"fileName", CMessage::CheckedType::String, L"").AsWideStr());
//		std::wstring fileUrl = aRequestMsg.SafeReadParam(L"fileUrl", CMessage::CheckedType::String, L"").AsWideStr();
//
//		args.push_back(TgBot::HttpReqArg("document", fileUrl));
//		args.push_back(TgBot::HttpReqArg("caption", fileName));
//		//args.push_back(TgBot::HttpReqArg(fileName, GetFileSource(fileUrl), true, "multipart/form-data", fileName));
//	}
//
//	
//
//	//if (disableWebPagePreview) {
//	//	args.push_back(HttpReqArg("disable_web_page_preview", disableWebPagePreview));
//	//}
//	//if (disableNotification) {
//	//	args.push_back(HttpReqArg("disable_notification", disableNotification));
//	//}
//	//if (replyToMessageId) {
//	//	args.push_back(HttpReqArg("reply_to_message_id", replyToMessageId));
//	//}
//	//if (replyMarkup) {
//	//	args.push_back(HttpReqArg("reply_markup", TgTypeParser::getInstance().parseGenericReply(replyMarkup)));
//	//}
//	//if (!parseMode.empty()) {
//	//	args.push_back(HttpReqArg("parse_mode", parseMode));
//	//}
//
//
//
//	
//
//	std::string request = TgBot::HttpParser::getInstance().generateRequest(url, args, aKeepAlive);
//
//	return request;
//}
//
//static unsigned int sClientNumber = 0;
//
//static void SendMessages(
//	boost::asio::io_service *aIoService, 
//	TConnectionClientParams&& aParams, 
//	CMessageCollector::MessageList&& aMsgList,
//	CSystemLog *aLog)
//{
//	if (!aIoService || aIoService->stopped() || aMsgList.empty())
//	{
//		return;
//	}
//
//	//aIoService->post([io_service = aIoService, params = std::move(aParams), msg_list = std::move(aMsgList), aLog = aLog]()
//	//{
//		boost::asio::spawn(*aIoService, [io_service = aIoService, params = std::move(aParams), msg_list = std::move(aMsgList), aLog = aLog](boost::asio::yield_context yield)
//		{
//			auto LogStringModule = [aLog](LogLevel level, const std::wstring& aClientName, const std::wstring& formatMessage, auto&& ...args)
//			{
//				if (!aLog)
//				{
//					return;
//				}
//				aLog->LogStringModule(level, aClientName, formatMessage, std::forward<decltype(args)>(args)...);
//			};
//
//			auto read_complete_predicate = [&](int data_len)
//			{
//				if (!data_len)
//					return true;
//				return 	data_len < TCP_PACK_SIZE;
//			};
//
//			std::wstring logInfoMessage = L"Send client messages";
//			LogStringModule(LEVEL_INFO, logInfoMessage, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());
//
//			boost::asio::ip::tcp::resolver resolver(*io_service);
//			boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//			boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//			boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//			boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
//			ctx.set_default_verify_paths();
//
//			boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket(*io_service, ctx);
//
//			boost::system::error_code errorCode;
//			boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);
//
//			if (errorCode)
//			{
//				LogStringModule(LEVEL_INFO, logInfoMessage, L"errorCode: %s", errorCode.message().c_str());
//				throw errorCode;
//			}
//
//			_socket.set_verify_mode(boost::asio::ssl::verify_none);
//			_socket.set_verify_callback([&](bool preverified,
//				boost::asio::ssl::verify_context& ctx)
//			{
//				// The verify callback can be used to check whether the certificate that is
//				// being presented is valid for the peer. For example, RFC 2818 describes
//				// the steps involved in doing this for HTTPS. Consult the OpenSSL
//				// documentation for more details. Note that the callback is called once
//				// for each certificate in the certificate chain, starting from the root
//				// certificate authority.
//
//				// In this example we will simply print the certificate's subject name.
//				char subject_name[256];
//				X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
//				X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
//
//				std::cout << subject_name << std::endl;
//
//				LogStringModule(LEVEL_INFO, logInfoMessage, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");
//
//				return preverified;
//			});
//
//			_socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);
//
//			if (errorCode)
//			{
//				LogStringModule(LEVEL_INFO, logInfoMessage,  L"errorCode: %s", errorCode.message().c_str());
//				throw errorCode;
//			}
//
//			auto size = msg_list.size();
//			decltype(size) count = 0;
//
//			for (const auto& msg : msg_list)
//			{
//				auto clientNumber = std::wstring(L"Client ") + std::to_wstring(++sClientNumber);
//				try
//				{
//					std::string request = MakeRequest(msg, params, ++count != size);
//
//					LogStringModule(LEVEL_FINEST, clientNumber, L"Write: %s", stow(request).c_str());
//					_socket.async_write_some(boost::asio::buffer(request), yield[errorCode]);
//					if (errorCode)
//					{
//						LogStringModule(LEVEL_INFO, clientNumber, L"errorCode: %s", errorCode.message().c_str());
//						throw errorCode;
//					}
//
//					std::string in_data;
//					int len = 0;
//
//					LogStringModule(LEVEL_INFO, clientNumber, L"Readiing...");
//					do
//					{
//						char reply_[TCP_PACK_SIZE] = {};
//						_socket.async_read_some(boost::asio::buffer(reply_), yield);
//						if (errorCode)
//						{
//							throw errorCode;
//						}
//
//						len = strlen(reply_);
//
//						in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);
//
//					} while (!read_complete_predicate(len));
//
//					LogStringModule(LEVEL_FINEST, clientNumber, L"Read: %s", stow(in_data).c_str());
//
//					std::string serverResponse = TgBot::HttpParser::getInstance().parseResponse(in_data);
//
//					if (serverResponse.find("<html>") != serverResponse.npos) {
//						throw std::runtime_error("tgbot-cpp library have got html page instead of json response. Maybe you entered wrong bot token.");
//					}
//
//					boost::property_tree::ptree result = TgBot::TgTypeParser::getInstance().parseJson(serverResponse);
//
//					try {
//						if (result.get<bool>("ok", false)) {
//							boost::property_tree::ptree ok = result.get_child("result");
//						}
//						else {
//							throw std::runtime_error(result.get("description", ""));
//						}
//					}
//					catch (boost::property_tree::ptree_error& e) {
//						throw std::runtime_error("tgbot-cpp library can't parse json response. " + std::string(e.what()));
//					}
//				}
//				catch (boost::system::error_code& error)
//				{
//					if (error != boost::asio::error::eof)
//					{
//						LogStringModule(LEVEL_WARNING, logInfoMessage, L"Socket failed with error: %s", stow(error.message()));
//					}
//				}
//				catch (std::runtime_error & error)
//				{
//					LogStringModule(LEVEL_WARNING, logInfoMessage, L"Socket failed with error: %s", stow(error.what()));
//				}
//				catch (...)
//				{
//					LogStringModule(LEVEL_WARNING, logInfoMessage, L"Socket failed with error: unhandled exception caught");
//				}
//			}
//		});
//	//});
//}
//
//
//bool _makeMySslClient(boost::asio::io_service *aIoService, TConnectionClientParams&& aParams, CSystemLog *aLog)
//{
//	singleton_auto_pointer<CMessageCollector> collector;
//
//	boost::asio::spawn(*aIoService, [io_service = aIoService, params = std::move(aParams), aLog = aLog](boost::asio::yield_context yield)
//	{
//		auto LogStringModule = [aLog = aLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//		{
//			if (!aLog)
//				return;
//			aLog->LogStringModule(level, L"client cycle", formatMessage, std::forward<decltype(args)>(args)...);
//		};
//
//		try
//		{
//			boost::asio::steady_timer timer(*io_service);
//			boost::system::error_code errorCode;
//
//			singleton_auto_pointer<CMessageCollector> collector;
//
//			while (!(*io_service).stopped())
//			{
//				timer.expires_from_now(std::chrono::seconds(params._iClientTimeout));
//				timer.async_wait(yield[errorCode]); //wait for timer
//
//				if (errorCode)
//				{
//					throw errorCode;
//				}
//
//				if (collector->isEmpty())
//				{
//					continue;
//				}
//
//				CMessageCollector::MessageList requestList = collector->get_serverListMessages();
//
//				TConnectionClientParams copy(params);
//				SendMessages(io_service, std::move(copy), std::move(requestList), aLog);
//			}
//		}
//		catch (boost::system::error_code& error)
//		{
//			if (error != boost::asio::error::eof)
//			{
//				LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//			}
//		}
//		catch (std::runtime_error & error)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
//		}
//		catch (...)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//		}
//	});
//
//	return true;
//}
//
//
//
//bool makeMySslClient(boost::asio::io_service *, TConnectionParams& params, CSystemLog *pLog)
//{
//	auto LogStringModule = [pLog = pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//	{
//		if (!pLog)
//			return;
//		pLog->LogStringModule(level, L"client", formatMessage, std::forward<decltype(args)>(args)...);
//	};
//	auto read_complete_predicate = [&](int data_len)
//	{
//		if (!data_len)
//			return true;
//		return 	data_len < TCP_PACK_SIZE;
//	};
//
//	boost::asio::io_service io_service;
//	boost::asio::spawn(io_service, [&](boost::asio::yield_context yield)
//	{
//		LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());
//
//		boost::asio::ip::tcp::resolver resolver(io_service);
//		boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//		boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//		boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
//		ctx.set_default_verify_paths();
//
//		boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket(io_service, ctx);
//
//		boost::system::error_code errorCode;
//		boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);
//
//		if (errorCode)
//		{
//			LogStringModule(LEVEL_INFO, L"errorCode: %s", errorCode.message().c_str());
//			throw errorCode;
//		}
//
//		_socket.set_verify_mode(boost::asio::ssl::verify_none);
//		_socket.set_verify_callback([&](bool preverified,
//			boost::asio::ssl::verify_context& ctx)
//		{
//			// The verify callback can be used to check whether the certificate that is
//			// being presented is valid for the peer. For example, RFC 2818 describes
//			// the steps involved in doing this for HTTPS. Consult the OpenSSL
//			// documentation for more details. Note that the callback is called once
//			// for each certificate in the certificate chain, starting from the root
//			// certificate authority.
//
//			// In this example we will simply print the certificate's subject name.
//			char subject_name[256];
//			X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
//			X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
//
//			std::cout << subject_name << std::endl;
//
//			LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");
//
//			return preverified;
//		});
//
//		_socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);
//
//		if (errorCode)
//		{
//			LogStringModule(LEVEL_INFO, L"errorCode: %s", errorCode.message().c_str());
//			throw errorCode;
//		}
//
//		LogStringModule(LEVEL_FINEST, L"Write: %s", stow(params._request).c_str());
//		_socket.async_write_some(boost::asio::buffer(params._request), yield[errorCode]);
//		if (errorCode)
//		{
//			throw errorCode;
//		}
//
//		std::string in_data;
//		int len = 0;
//
//		try
//		{
//			do
//			{
//				char reply_[TCP_PACK_SIZE] = {};
//				_socket.async_read_some(boost::asio::buffer(reply_), yield);
//				if (errorCode)
//				{
//					throw errorCode;
//				}
//
//				len = strlen(reply_);
//
//				in_data.append(reply_, len < TCP_PACK_SIZE ? len: TCP_PACK_SIZE);
//
//			} 
//			while (!read_complete_predicate(len));
//
//		}
//		catch (boost::system::error_code& error)
//		{
//			if (error != boost::asio::error::eof)
//			{
//				LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//			}
//		}
//		catch (std::runtime_error & error)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
//		}
//		catch (...)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//		}
//
//		LogStringModule(LEVEL_FINEST, L"Read: %s", stow(in_data).c_str());
//
//		params._response = in_data;
//
//	});
//
//	io_service.run();
//
//	int test = 0;
//
//	return true;
//}
//
////const int TCP_PACK_SIZE = 1024;
//
//
//bool makeMySslClient(boost::asio::io_service* io_service, TConnectionClientParams&& params, CSystemLog * pLog)
//{
//	return _makeMySslClient(io_service, std::move(params), pLog);
//
//	//boost::asio::spawn(*io_service, [io_service = io_service, params = std::move(params), pLog = pLog](boost::asio::yield_context yield)
//	//{
//	//	std::wstring sClientName = L"Client " + std::to_wstring(++sClientNumber);
//	//	auto LogStringModule = [pLog, sClientName](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//	//	{
//	//		pLog->LogStringModule(level, sClientName, formatMessage, std::forward<decltype(args)>(args)...);
//	//	};
//
//	//	auto read_complete_predicate = [&](int data_len)
//	//	{
//	//		if (!data_len)
//	//			return true;
//	//		return 	data_len < TCP_PACK_SIZE;
//	//	};
//
//	//	try
//	//	{
//	//		boost::asio::steady_timer timer(*io_service);
//	//		boost::system::error_code errorCode;
//
//	//		singleton_auto_pointer<CMessageCollector> collector;
//
//	//		while (!(*io_service).stopped())
//	//		{
//	//			timer.expires_from_now(std::chrono::seconds(params._iClientTimeout));
//	//			timer.async_wait(yield[errorCode]); //wait for timer
//
//	//			if (errorCode)
//	//			{
//	//				throw errorCode;
//	//			}
//
//	//			if (collector->isEmpty())
//	//				continue;
//
//	//			try
//	//			{
//	//				CMessageCollector::MessageList requestList = collector->get_serverListMessages();
//
//	//				// makeClient
//	//				LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());
//
//	//				boost::asio::ip::tcp::resolver resolver(*io_service);
//	//				boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//	//				boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//	//				boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//
//	//				boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
//	//				ctx.set_default_verify_paths();
//
//	//				boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket(*io_service, ctx);
//
//	//				boost::system::error_code errorCode;
//	//				boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);
//
//	//				if (errorCode)
//	//				{
//	//					LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
//	//					throw errorCode;
//	//				}
//
//	//				_socket.set_verify_mode(boost::asio::ssl::verify_none);
//	//				_socket.set_verify_callback([&](bool preverified,
//	//					boost::asio::ssl::verify_context& ctx)
//	//				{
//	//					// The verify callback can be used to check whether the certificate that is
//	//					// being presented is valid for the peer. For example, RFC 2818 describes
//	//					// the steps involved in doing this for HTTPS. Consult the OpenSSL
//	//					// documentation for more details. Note that the callback is called once
//	//					// for each certificate in the certificate chain, starting from the root
//	//					// certificate authority.
//
//	//					// In this example we will simply print the certificate's subject name.
//	//					char subject_name[256];
//	//					X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
//	//					X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
//
//	//					std::cout << subject_name << std::endl;
//
//	//					LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");
//
//	//					return preverified;
//	//				});
//	//				_socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);
//
//	//				if (errorCode)
//	//				{
//	//					LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
//	//					throw errorCode;
//	//				}
//
//	//				for each (const auto& requestMsg in requestList)
//	//				{
//	//					requestMsg.CheckParam(L"id", CMessage::CheckedType::Int64, CMessage::ParamType::Mandatory);
//
//	//					bool bButtonEnable = requestMsg.SafeReadParam(L"makeButton", CMessage::CheckedType::Bool, false).AsBool();
//	//					ChatId cid = requestMsg.SafeReadParam(L"id", CMessage::CheckedType::Int64, 0).AsInt64();
//	//					std::wstring text = requestMsg.SafeReadParam(L"content", CMessage::CheckedType::String, false).AsWideStr();
//
//	//					std::vector<TgBot::HttpReqArg> args;
//	//					args.push_back(TgBot::HttpReqArg("chat_id", cid));
//	//					args.push_back(TgBot::HttpReqArg("text", wtos(text)));
//	//					//if (disableWebPagePreview) {
//	//					//	args.push_back(HttpReqArg("disable_web_page_preview", disableWebPagePreview));
//	//					//}
//	//					//if (disableNotification) {
//	//					//	args.push_back(HttpReqArg("disable_notification", disableNotification));
//	//					//}
//	//					//if (replyToMessageId) {
//	//					//	args.push_back(HttpReqArg("reply_to_message_id", replyToMessageId));
//	//					//}
//	//					//if (replyMarkup) {
//	//					//	args.push_back(HttpReqArg("reply_markup", TgTypeParser::getInstance().parseGenericReply(replyMarkup)));
//	//					//}
//	//					//if (!parseMode.empty()) {
//	//					//	args.push_back(HttpReqArg("parse_mode", parseMode));
//	//					//}
//
//
//	//					if (bButtonEnable)
//	//					{
//	//						TgBot::ReplyKeyboardMarkup::Ptr rpk = std::make_shared<TgBot::ReplyKeyboardMarkup>();
//	//						rpk->keyboard.push_back({ TgBot::KeyboardButton("PUSH HERE TO SUBMIT", true) });
//	//						rpk->oneTimeKeyboard = true;
//	//						rpk->resizeKeyboard = true;
//
//	//						args.push_back(TgBot::HttpReqArg("reply_markup", TgBot::TgTypeParser::getInstance().parseGenericReply(rpk)));
//	//					}
//
//	//					std::string url = std::string("https://") + params._uri + "/bot" + params._token + "/sendMessage";
//
//	//					std::string request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);
//
//
//
//	//					//std::wstring request = CMessageParser::ToString(requestMsg);
//	//					//std::string url;
//
//	//					//requestMsg.CheckParam(L"Id", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
//	//					//ChatId cid = requestMsg.SafeReadParam(L"Id", CMessage::CheckedType::String, L"").AsWideStr();
//
//	//					//if (requestMsg == L"CHATMESSAGE" || requestMsg == L"END_SESSION")
//	//					//{
//	//					//	url = "http://" + params._uri + "/chatsessions/" + wtos(cid) + "/chatmessages";
//	//					//}
//	//					//if (requestMsg == L"TRANSFER")
//	//					//{
//	//					//	url = "http://" + params._uri + "/chatsessions/" + wtos(cid) + "/operator";
//	//					//}
//	//					//if (requestMsg == L"SESSION_STATUS")
//	//					//{
//	//					//	url = "http://" + params._uri + "/chatsessions/" + wtos(cid);
//	//					//}
//	//					//std::vector<TgBot::HttpReqArg> args;
//	//					//args.push_back(TgBot::HttpReqArg("jsondata", wtos(request)));
//
//
//	//					//std::string httpRequest = TgBot::HttpParser::getInstance().generateRequest(url, args, false);
//
//	//					LogStringModule(LEVEL_FINEST, L"Write: %s", request.c_str());
//
//	//					std::string in_data;
//	//					int len = 0;
//
//	//					_socket.async_write_some(boost::asio::buffer(request), yield[errorCode]);
//
//	//					LogStringModule(LEVEL_FINEST, L"1");
//	//					if (errorCode)
//	//					{
//	//						LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
//	//						throw errorCode;
//	//					}
//
//	//					LogStringModule(LEVEL_FINEST, L"2");
//
//	//					try
//	//					{
//	//						LogStringModule(LEVEL_FINEST, L"Reading answer...", request.c_str());
//	//						do
//	//						{
//	//							char reply_[TCP_PACK_SIZE] = {};
//	//							_socket.async_read_some(boost::asio::buffer(reply_), yield);
//	//							if (errorCode)
//	//							{
//	//								throw errorCode;
//	//							}
//
//	//							len = strlen(reply_);
//
//	//							in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);
//
//	//						} while (!read_complete_predicate(len));
//	//					}
//	//					catch (boost::system::error_code& error)
//	//					{
//	//						if (error != boost::asio::error::eof)
//	//							LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//	//					}
//	//					catch (std::runtime_error & error)
//	//					{
//	//						LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
//	//					}
//	//					catch (...)
//	//					{
//	//						LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//	//					}
//
//	//					if (in_data.empty())
//	//						continue;
//
//	//					LogStringModule(LEVEL_FINEST, L"Read: %s", in_data.c_str());
//
//	//					std::string serverResponse = TgBot::HttpParser::getInstance().parseResponse(in_data);
//
//	//					if (serverResponse.find("<html>") != serverResponse.npos) {
//	//						throw std::runtime_error("tgbot-cpp library have got html page instead of json response. Maybe you entered wrong bot token.");
//	//					}
//
//	//					boost::property_tree::ptree result = TgBot::TgTypeParser::getInstance().parseJson(serverResponse);
//
//	//					try {
//	//						if (result.get<bool>("ok", false)) {
//	//							boost::property_tree::ptree ok = result.get_child("result");
//	//						}
//	//						else {
//	//							throw std::runtime_error(result.get("description", ""));
//	//						}
//	//					}
//	//					catch (boost::property_tree::ptree_error& e) {
//	//						throw std::runtime_error("tgbot-cpp library can't parse json response. " + std::string(e.what()));
//	//					}
//
//	//					//if (in_data == "500")
//	//					//{
//	//					//	//ToDo: Error answer here
//	//					//}
//
//	//				}
//	//			}
//	//			catch (boost::system::error_code& error)
//	//			{
//	//				if (error != boost::asio::error::eof)
//	//					LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//	//			}
//	//			catch (std::runtime_error & error)
//	//			{
//	//				LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
//	//			}
//	//			catch (...)
//	//			{
//	//				LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//	//			}
//	//		}
//
//	//	}
//	//	catch (boost::system::error_code& error)
//	//	{
//	//		LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//	//	}
//	//	catch (...)
//	//	{
//	//		LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//	//	}
//	//});
//
//
//	//int test = 0;
//	//return false;
//}
//
//
////class connection : public std::enable_shared_from_this<connection>
////{
////public:
////	explicit connection(std::string request, boost::asio::io_service * io_service, boost::asio::ssl::context context, UpdateHandler handler, CSystemLog* log)
////		: 
////		_request(std::move(request)),
////		_io_service(io_service),
////		_socket(*_io_service, std::move(context)),
////		_handler(handler),
////		_log(log)
////	{
////	}
////
////	~connection()
////	{
////		//LogStringModule(LEVEL_FINEST, L"destructor");
////	}
////
////	template<typename... TArgs>
////	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
////	{
////		_log->LogStringModule(level, L"connection", formatMessage, std::forward<TArgs>(args)...);
////	}
////
////	void start(boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
////	{
////		boost::asio::spawn(*_io_service,
////			[self = shared_from_this(), endpoint_iterator](boost::asio::yield_context yield)
////		{
////			try
////			{
////				self->LogStringModule(LEVEL_INFO, L"17");
////
////				boost::system::error_code errorCode;
////
////				boost::asio::async_connect(self->_socket.lowest_layer(), endpoint_iterator, yield[errorCode]);
////
////				if (errorCode)
////				{
////					self->LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
////					throw errorCode;
////				}
////
////				self->_socket.set_verify_mode(boost::asio::ssl::verify_none);
////				self->_socket.set_verify_callback([&](bool preverified,
////					boost::asio::ssl::verify_context& ctx)
////				{
////					// The verify callback can be used to check whether the certificate that is
////					// being presented is valid for the peer. For example, RFC 2818 describes
////					// the steps involved in doing this for HTTPS. Consult the OpenSSL
////					// documentation for more details. Note that the callback is called once
////					// for each certificate in the certificate chain, starting from the root
////					// certificate authority.
////
////					// In this example we will simply print the certificate's subject name.
////					char subject_name[256];
////					X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
////					X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
////
////					std::cout << subject_name << std::endl;
////
////					self->LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");
////
////					return preverified;
////				});
////				self->LogStringModule(LEVEL_INFO, L"18");
////
////
////				self->_socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);
////
////				self->LogStringModule(LEVEL_INFO, L"19");
////
////				if (errorCode)
////				{
////					self->LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
////					throw errorCode;
////				}
////
////				//boost::system::error_code errorCode;
////
////				self->_socket.async_write_some(boost::asio::buffer(self->_request), yield[errorCode]);
////
////				if (errorCode)
////				{
////					throw errorCode;
////				}
////
////				std::string in_data;
////				//do
////				//{
////				char reply_[1024] = {};
////				self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
////				if (errorCode)
////				{
////					throw errorCode;
////				}
////
////				in_data += reply_;
////
////				//} while (!read_complete_predicate(in_data));
////
////				//LogStringModule(LEVEL_FINEST, L"Read: %", stow(in_data).c_str());
////
////				if (self->_handler)
////					self->_handler(in_data);
////				//singleton_auto_pointer<CConnectionCollector> collector;
////
////				//char in_data[1024] = {};
////				//while (!self->_strand.get_io_service().stopped())
////				//{
////				//	ZeroMemory(in_data, 1024);
////
////				//	self->_timer.expires_from_now(std::chrono::hours(24));
////				//	self->_socket.async_read_some(boost::asio::buffer(in_data), yield);
////
////				//	std::wstring answer(stow(in_data));
////
////				//	self->LogStringModule(LEVEL_FINEST, L"Read: %s", answer.c_str());
////
////				//	CMessage msg = CMessageParser::ToMessage(std::move(answer));
////
////				//	msg[L"MCCID"] = self->_sessionId;
////
////				//	self->_collector.push_message(msg);
////				//	collector->Recieve(msg);
////
////				//	std::wstring response = CMessageParser::MakeResponse(std::move(msg));
////
////				//	if (response.empty())
////				//		continue;
////
////				//	self->LogStringModule(LEVEL_FINEST, L"Write: %s", response.c_str());
////				//	boost::asio::async_write(self->_socket, boost::asio::buffer(wtos(response), response.size()), yield);
////				//}
////			}
////			catch (std::exception& e)
////			{
////				self->LogStringModule(LEVEL_FINEST, L"Exception: %s", stow(e.what()).c_str());
////				//self->_socket.close();
////			}
////		});
////	}
////private:
////	std::string _request;
////	boost::asio::io_service * _io_service;
////	boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket;
////	UpdateHandler _handler;
////	CSystemLog* _log;
////};
////
////bool makeMySslClient(UpdateHandler handler, boost::asio::io_service * io_service, TConnectionParams & params, CSystemLog * pLog)
////{
////	boost::asio::spawn(*io_service, [&](boost::asio::yield_context yield)
////	{
////
////		auto LogStringModule = [pLog = pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
////		{
////			if (!pLog)
////				return;
////			pLog->LogStringModule(level, L"client", formatMessage, std::forward<decltype(args)>(args)...);
////		};
////
////		LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());
////
////		boost::asio::ip::tcp::resolver resolver(*io_service);
////		LogStringModule(LEVEL_INFO, L"12");
////		boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
////		LogStringModule(LEVEL_INFO, L"13");
////		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
////		LogStringModule(LEVEL_INFO, L"14");
////		boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
////		LogStringModule(LEVEL_INFO, L"15");
////
////		boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
////		//ctx.load_verify_file(params._sertificate);
////		ctx.set_default_verify_paths();
////		LogStringModule(LEVEL_INFO, L"16");
////
////		//boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket(*io_service, ctx);
////
////		////connect(_socket.lowest_layer(), iterator);
////		//LogStringModule(LEVEL_INFO, L"17");
////
////		//boost::system::error_code errorCode;
////
////		//boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);
////
////		//if (errorCode)
////		//{
////		//	LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
////		//	throw errorCode;
////		//}
////
////		//_socket.set_verify_mode(boost::asio::ssl::verify_none);
////		//_socket.set_verify_callback([&](bool preverified,
////		//	boost::asio::ssl::verify_context& ctx)
////		//{
////		//	// The verify callback can be used to check whether the certificate that is
////		//	// being presented is valid for the peer. For example, RFC 2818 describes
////		//	// the steps involved in doing this for HTTPS. Consult the OpenSSL
////		//	// documentation for more details. Note that the callback is called once
////		//	// for each certificate in the certificate chain, starting from the root
////		//	// certificate authority.
////
////		//	// In this example we will simply print the certificate's subject name.
////		//	char subject_name[256];
////		//	X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
////		//	X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
////
////		//	std::cout << subject_name << std::endl;
////
////		//	LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");
////
////		//	return preverified;
////		//});
////		//LogStringModule(LEVEL_INFO, L"18");
////
////
////		//_socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);
////
////		//LogStringModule(LEVEL_INFO, L"19");
////
////		//if (errorCode)
////		//{
////		//	LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
////		//	throw errorCode;
////		//}
////
////		std::make_shared<connection>(std::move(params._request), io_service, std::move(ctx), handler, pLog)->start(std::move(iterator));
////
////		//LogStringModule(LEVEL_FINEST, L"Write: %", stow(params._request).c_str());
////
////		//_socket.async_write_some(boost::asio::buffer(params._request), yield[errorCode]);
////
////		//if (errorCode)
////		//{
////		//	throw errorCode;
////		//}
////
////		//std::string in_data;
////		////do
////		////{
////		//char reply_[1024] = {};
////		//_socket.async_read_some(boost::asio::buffer(reply_), yield);
////		//if (errorCode)
////		//{
////		//	throw errorCode;
////		//}
////
////		//in_data += reply_;
////
////		////} while (!read_complete_predicate(in_data));
////
////		////LogStringModule(LEVEL_FINEST, L"Read: %", stow(in_data).c_str());
////
////		//params._response = in_data;
////
////	});
////	return true;
////}
//
//
//
//
//bool makeMySslServer(boost::asio::io_service *io_service, TConnectionServerParams&& params, CSystemLog *pLog)
//{
//	boost::asio::spawn(*io_service, [/*&LogStringModule,*/ io_service = io_service, params = std::move(params), pLog = pLog](boost::asio::yield_context yield)
//	{
//		auto LogStringModule = [pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
//		{
//			pLog->LogStringModule(level, L"Server", formatMessage, std::forward<decltype(args)>(args)...);
//		};
//
//
//		try
//		{
//			singleton_auto_pointer<CMessageCollector> collector;
//			singleton_auto_pointer<CChatInfoCollector> infoCollector;
//
//			LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());
//
//			LogStringModule(LEVEL_FINEST, L"Open socket...");
//
//			boost::asio::ip::tcp::resolver resolver(*io_service);
//			boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//			boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//			boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//			LogStringModule(LEVEL_FINEST, L"Add acceptor...");
//			/// Acceptor used to listen for incoming connections.
//			boost::asio::ip::tcp::acceptor acceptor_(*io_service);
//			LogStringModule(LEVEL_FINEST, L"1");
//			acceptor_.open(endpoint.protocol());
//			LogStringModule(LEVEL_FINEST, L"2");
//			acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
//			LogStringModule(LEVEL_FINEST, L"3");
//			acceptor_.bind(endpoint);
//			LogStringModule(LEVEL_FINEST, L"4");
//			acceptor_.listen();
//			LogStringModule(LEVEL_FINEST, L"5");
//
//			LogStringModule(LEVEL_FINEST, L"Add ssl context...");
//			boost::asio::ssl::context ctx(*io_service, boost::asio::ssl::context::sslv23);
//
//			ctx.set_options(
//				boost::asio::ssl::context::default_workarounds
//				| boost::asio::ssl::context::no_sslv2
//				/*| boost::asio::ssl::context::single_dh_use*/);
//			//ctx.set_password_callback(boost::bind(&server::get_password, this));
//			ctx.use_certificate_chain_file(params._sertificate);
//			//ctx.use_private_key_file("D:/Programming/Projects/MCHAT/Obj/PEM/MMBot_home.key", boost::asio::ssl::context::pem);
//			ctx.use_private_key_file(params._private_key, boost::asio::ssl::context::pem);
//			//ctx.use_tmp_dh_file("dh512.pem");
//
//
//			//ctx.load_verify_file(params._sertificate);
//
//
//			boost::system::error_code errorCode;
//
//			LogStringModule(LEVEL_INFO, L"Waiting for connection...");
//
//
//			while (!(*io_service).stopped())
//			{
//				//boost::asio::ssl::context context_(*io_service, boost::asio::ssl::context::sslv23);
//				//context_.set_options(
//				//	boost::asio::ssl::context::default_workarounds
//				//	| boost::asio::ssl::context::no_sslv2
//				//	| boost::asio::ssl::context::single_dh_use);
//				////context_.set_password_callback(boost::bind(&server::get_password, this));
//				//context_.use_certificate_chain_file("server.pem");
//				////context_.use_private_key_file("server.pem", boost::asio::ssl::context::pem);
//				//context_.use_tmp_dh_file("dh512.pem");
//
//				//boost::asio::ip::tcp::socket _socket(*io_service);
//
//				auto connect = std::make_shared<connection>(*io_service, ctx, pLog);
//
//				LogStringModule(LEVEL_FINEST, L"6");
//				acceptor_.async_accept(connect->get().lowest_layer(), yield[errorCode]);
//
//				LogStringModule(LEVEL_FINEST, L"7");
//
//				if (errorCode)
//					throw errorCode;
//
//				LogStringModule(LEVEL_FINEST, L"8");
//				connect->startListening();
//
//				//auto& _socket = connect->get();
//
//				//_socket.set_verify_mode(boost::asio::ssl::verify_none);
//				//_socket.set_verify_callback([&](bool preverified,
//				//	boost::asio::ssl::verify_context& ctx)
//				//{
//				//	// The verify callback can be used to check whether the certificate that is
//				//	// being presented is valid for the peer. For example, RFC 2818 describes
//				//	// the steps involved in doing this for HTTPS. Consult the OpenSSL
//				//	// documentation for more details. Note that the callback is called once
//				//	// for each certificate in the certificate chain, starting from the root
//				//	// certificate authority.
//
//				//	// In this example we will simply print the certificate's subject name.
//				//	char subject_name[256];
//				//	X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
//				//	X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
//
//				//	std::cout << subject_name << std::endl;
//
//				//	LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");
//
//				//	return preverified;
//				//});
//				//LogStringModule(LEVEL_FINEST, L"11");
//				//_socket.async_handshake(boost::asio::ssl::stream_base::server, yield[errorCode]);
//
//				//LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(errorCode.message()));
//
//				//LogStringModule(LEVEL_FINEST, L"12");
//
//				//LogStringModule(LEVEL_FINEST, L"9");
//
//				//connect->get().set_verify_mode(boost::asio::ssl::verify_none);
//				//connect->get().set_verify_callback([&](bool preverified,
//				//	boost::asio::ssl::verify_context& ctx)
//				//{
//				//	// The verify callback can be used to check whether the certificate that is
//				//	// being presented is valid for the peer. For example, RFC 2818 describes
//				//	// the steps involved in doing this for HTTPS. Consult the OpenSSL
//				//	// documentation for more details. Note that the callback is called once
//				//	// for each certificate in the certificate chain, starting from the root
//				//	// certificate authority.
//
//				//	// In this example we will simply print the certificate's subject name.
//				//	char subject_name[256];
//				//	X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
//				//	X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
//
//				//	std::cout << subject_name << std::endl;
//
//				//	LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");
//
//				//	return preverified;
//				//});
//
//				//connect->get().async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);
//
//				//if (errorCode)
//				//	throw errorCode;
//
//				//connect->startListening();
//
//				//std::make_shared<connection>(std::move(_socket),/* connectionId,*/ pLog)->startListening();
//			}
//		}
//		catch (boost::system::error_code& error)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
//		}
//		catch (std::runtime_error & error)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
//		}
//		catch (...)
//		{
//			LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
//		}
//
//	});
//
//	return true;
//
//}
//
//
//
/////********************************/////
//
//typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;
//
//class session
//{
//public:
//	session(boost::asio::io_service& io_service, boost::asio::ssl::context& context)
//		: socket_(io_service, context)
//	{
//	}
//
//	ssl_socket::lowest_layer_type& socket()
//	{
//		return socket_.lowest_layer();
//	}
//
//	void start()
//	{
//		socket_.async_handshake(boost::asio::ssl::stream_base::server,
//			boost::bind(&session::handle_handshake, this,
//				boost::asio::placeholders::error));
//	}
//
//	void handle_handshake(const boost::system::error_code& error)
//	{
//		if (!error)
//		{
//			socket_.async_read_some(boost::asio::buffer(data_, max_length),
//				boost::bind(&session::handle_read, this,
//					boost::asio::placeholders::error,
//					boost::asio::placeholders::bytes_transferred));
//		}
//		else
//		{
//			delete this;
//		}
//	}
//
//	void handle_read(const boost::system::error_code& error,
//		size_t bytes_transferred)
//	{
//		if (!error)
//		{
//			boost::asio::async_write(socket_,
//				boost::asio::buffer(data_, bytes_transferred),
//				boost::bind(&session::handle_write, this,
//					boost::asio::placeholders::error));
//		}
//		else
//		{
//			delete this;
//		}
//	}
//
//	void handle_write(const boost::system::error_code& error)
//	{
//		if (!error)
//		{
//			socket_.async_read_some(boost::asio::buffer(data_, max_length),
//				boost::bind(&session::handle_read, this,
//					boost::asio::placeholders::error,
//					boost::asio::placeholders::bytes_transferred));
//		}
//		else
//		{
//			delete this;
//		}
//	}
//
//private:
//	ssl_socket socket_;
//	enum { max_length = 1024 };
//	char data_[max_length];
//};
//
//class server
//{
//public:
//	server(boost::asio::io_service& io_service, std::string uri, std::string port)
//		: io_service_(io_service),
//		resolver_(io_service),
//		acceptor_(io_service,
//			resolver_.resolve(boost::asio::ip::tcp::resolver::query(uri, port))->endpoint()),
//			//boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
//		context_(io_service, boost::asio::ssl::context::sslv23)
//	{
//
//		//boost::asio::ip::tcp::resolver resolver(io_service);
//		//boost::asio::ip::tcp::resolver::query query(uri, port);
//		//boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//		//boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//		
//		
//		//resolver.resolve(boost::asio::ip::tcp::resolver::query(params._uri, params._port))->endpoint()
//
//		//LogStringModule(LEVEL_FINEST, L"Add acceptor...");
//		///// Acceptor used to listen for incoming connections.
//		//boost::asio::ip::tcp::acceptor acceptor_(*io_service);
//		//LogStringModule(LEVEL_FINEST, L"1");
//		//acceptor_.open(endpoint.protocol());
//		//LogStringModule(LEVEL_FINEST, L"2");
//		//acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
//		//LogStringModule(LEVEL_FINEST, L"3");
//		//acceptor_.bind(endpoint);
//		//LogStringModule(LEVEL_FINEST, L"4");
//		//acceptor_.listen();
//		//LogStringModule(LEVEL_FINEST, L"5");
//
//
//		context_.set_options(
//			boost::asio::ssl::context::default_workarounds
//			| boost::asio::ssl::context::no_sslv2/*
//			| boost::asio::ssl::context::single_dh_use*/);
//		//context_.set_password_callback(boost::bind(&server::get_password, this));
//		//context_.use_certificate_chain_file("server.pem");
//		context_.use_certificate_chain_file("D:/Programming/Projects/MCHAT/Obj/PEM/MMBot_home.pem");
//		//context_.use_private_key_file("server.pem", boost::asio::ssl::context::pem);
//		context_.use_private_key_file("D:/Programming/Projects/MCHAT/Obj/PEM/MMBot_home.key", boost::asio::ssl::context::pem);
//		//context_.use_tmp_dh_file("dh512.pem");
//
//		session* new_session = new session(io_service_, context_);
//		acceptor_.async_accept(new_session->socket(),
//			boost::bind(&server::handle_accept, this, new_session,
//				boost::asio::placeholders::error));
//	}
//
//	std::string get_password() const
//	{
//		return "test";
//	}
//
//	void handle_accept(session* new_session,
//		const boost::system::error_code& error)
//	{
//		if (!error)
//		{
//			new_session->start();
//			new_session = new session(io_service_, context_);
//			acceptor_.async_accept(new_session->socket(),
//				boost::bind(&server::handle_accept, this, new_session,
//					boost::asio::placeholders::error));
//		}
//		else
//		{
//			delete new_session;
//		}
//	}
//
//private:
//	boost::asio::io_service& io_service_;
//	boost::asio::ip::tcp::resolver resolver_;
//	boost::asio::ip::tcp::acceptor acceptor_;
//	boost::asio::ssl::context context_;
//};
//
//bool makeMySslServer2(boost::asio::io_service *io_service, TConnectionServerParams&& params, CSystemLog *pLog)
//{
//	using namespace std; // For atoi.
//
//	boost::asio::io_service io;
//
//	server s(io, params._uri, /*atoi*/params._port);
//	io.run();
//
//	return true;
//}