///************************************************************************/
///* Name     : MCHAT\ServerSocket.h                                      */
///* Author   : Andrey Alekseev                                           */
///* Project  : MCHAT                                                     */
///* Company  : Expert Solutions                                          */
///* Date     : 17 Jul 2016                                               */
///************************************************************************/
//
//#pragma once
//
//#include <set>
//#include <mutex>
//#include <numeric>
//#include <fstream>
////#include <boost/asio.hpp>
////#include <boost/bind.hpp>
////#include <boost/asio/ip/tcp.hpp>
////#include <boost/asio/steady_timer.hpp>
////#include <boost/asio/spawn.hpp>
//
//#include <boost/coroutine/asymmetric_coroutine.hpp>
//#include <boost/coroutine/symmetric_coroutine.hpp>
//
////#include <singleton.h>
//#include "MakeServer.h"
//#include "MessageCollector.h"
//
//
//
////#include "Log/SystemLog.h"
//
////const unsigned int c_waitTimeSec = 10;
//
//class CServerSocket
//{
//public:
//	CServerSocket(CSystemLog* pLog,
//		const TConnectionParams& params,
//		boost::asio::io_service &io_service,
//		boost::coroutines::symmetric_coroutine<void>::call_type& call,
//		boost::coroutines::symmetric_coroutine<void>::yield_type& yield)
//		: _call(&call), _yield(&yield), _log(pLog)
//	{
//
//		singleton_auto_pointer<CConnectionCollector> collector;
//
//		LogStringModule(LEVEL_FINEST, L"Open socket...");
//
//		boost::asio::ip::tcp::resolver resolver(io_service);
//		boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
//		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
//		boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
//
//		LogStringModule(LEVEL_FINEST, L"Add acceptor...");
//		/// Acceptor used to listen for incoming connections.
//		boost::asio::ip::tcp::acceptor acceptor_(io_service);
//		acceptor_.open(endpoint.protocol());
//		acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
//		acceptor_.bind(endpoint);
//		acceptor_.listen();
//
//		boost::system::error_code errorCode;
//
//		IdGenerator generator;
//
//		LogStringModule(LEVEL_INFO, L"Waiting for connection...");
//
//
//		while (!io_service.stopped())
//		{
//			boost::asio::ip::tcp::socket _socket(io_service);
//
//			acceptor_.async_accept(_socket,
//				[&](const boost::system::error_code& error)
//			{
//				errorCode = error;
//
//				(*_call)();
//			}
//			);
//
//			(*_yield)();
//
//
//			if (errorCode)
//				throw errorCode;
//
//			//IdType connectionId = generator->makeId(params._clientId);
//			//connections.emplace
//			//	(
//			//		connectionId, 
//			//		std::make_shared<connection>(std::move(_socket), connectionId, _log)
//			//	).first->second->startListening();
//
//
//
//			//collector->emplace(generator->makeId(params._clientId), std::move(_socket)).lock()->startListening();;
//			
//			IdType connectionId = generator->makeId(params._clientId);
//			ConnectionNotSafePtr pCon = std::make_shared<connection>(std::move(_socket), connectionId, _log);
//			collector->emplace(connectionId, pCon);
//
//			pCon->startListening();
//
//			//IdType connectionId = generator->makeId(params._clientId);
//			//ConnectionPtr pCon = collector->emplace(connectionId, std::move(_socket));
//			//if (pCon.lock())
//			//{
//			//	pCon.lock()->startListening();
//			//}
//
//			//collector->emplace(generator->makeId(params._clientId), std::move(_socket));;
//		}
//
//
//	}
//private:
//
//	template<typename... TArgs>
//	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
//	{
//		_log->LogStringModule(level, L"Server", formatMessage, std::forward<TArgs>(args)...);
//	}
//
//private:
//
//	boost::coroutines::symmetric_coroutine<void>::call_type* _call{ 0 };
//	boost::coroutines::symmetric_coroutine<void>::yield_type* _yield{ 0 };
//
//	std::unique_ptr<boost::asio::ip::tcp::socket> _socket;
//
//	CSystemLog* _log;
//};
//
///******************************* eof *************************************/