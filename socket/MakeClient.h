#pragma once
#include <string>
//#include "TestSocket.h"

struct TConnectionParams
{
	std::string _uri;
	std::string _port;
	std::string _request;
	std::string _response;
	//std::string _sertificate;
	std::string _token;
	int _iServerTimeout;
	uint32_t  _clientId;
};

class CSystemLog;

bool makeMyClient(TConnectionParams& params, CSystemLog *pLog);

bool makeMyClient_JustSend(TConnectionParams& params, CSystemLog *pLog);

namespace boost
{
	namespace asio
	{
		class io_service;
	}
}

bool makeMySslClient(boost::asio::io_service *io_service, TConnectionParams& params, CSystemLog *pLog);


struct TConnectionServerParams
{
	std::string _uri;
	std::string _port;
	std::string _token;
	int _iServerTimeout;
	std::string _sertificate;
	std::string _private_key;
	uint32_t  _clientId;
	std::string _server_uri;
	std::string _server_port;
};

struct TConnectionClientParams
{
	std::string _uri;
	std::string _port;
	int _iClientTimeout;
	uint32_t  _clientId;
	std::string _token;

	//std::string _request;
	//std::string _response;
};


bool makeMySslClient(boost::asio::io_service *io_service, TConnectionClientParams&& params, CSystemLog *pLog);
bool makeMySslServer(boost::asio::io_service *io_service, TConnectionServerParams&& params, CSystemLog *pLog);
bool makeMySslServer2(boost::asio::io_service *io_service, TConnectionServerParams&& params, CSystemLog *pLog);


//class CMyClient
//{
//
//};
