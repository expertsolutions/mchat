#include "stdafx.h"
#include "CppUnitTest.h"

//#include <boost/property_tree/ptree.hpp>
//#include <boost/property_tree/json_parser.hpp>

#include "Patterns/ptree_parser.h"

#include "MakeClient.h"
#include "Log/SystemLog.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(Test_Hello_world)
		{
			boost::property_tree::wptree requestTree, responseTree;

			requestTree.put(L"message_name", L"ChatSessionCreated");
			requestTree.put(L"abonent_id", L"9067837625");
			requestTree.put(L"channel_id", L"WEB");
			requestTree.put(L"channel_name", L"�������������� ������");
			requestTree.put(L"message_text", L"some text");
			requestTree.put(L"message_guid", L"9F485474B4864A3F");
			requestTree.put(L"message_type", L"none");

			std::wstring request = core::support::ptree_parser::tree_to_json_string(requestTree);
			
			responseTree.put(L"message_name", L"ChatSessionCreated_ack");
			responseTree.put(L"message_guid", L"9F485474B4864A3F");
			std::wstring response = core::support::ptree_parser::tree_to_json_string(responseTree);

			Assert::AreEqual(make_request(request), response);
		}

		TEST_METHOD(Test_CustomerSentMessage)
		{
			boost::property_tree::wptree requestTree, responseTree;

			requestTree.put(L"type", L"CHATMESSAGE");
			requestTree.put(L"CustomerCTN", L"9067837625");
			requestTree.put(L"ChannelID", L"WEB");
			//requestTree.put(L"channel_name", L"�������������� ������");
			requestTree.put(L"Content", L"some text");
			requestTree.put(L"Id", L"1F485474B4864A3F");
			requestTree.put(L"ContentType", L"text");
			requestTree.put(L"CustomerName", L"�������");


			std::wstring request = core::support::ptree_parser::tree_to_json_string(requestTree);

			responseTree.put(L"type", L"CONFIRM");
			responseTree.put(L"Id", L"1F485474B4864A3F");
			std::wstring response = core::support::ptree_parser::tree_to_json_string(responseTree);

			Assert::AreEqual(make_request(request), response);
		}

		TEST_METHOD(Test_GetMessagesById)
		{
			boost::property_tree::wptree requestTree, responseTree, MessagearrayTree;
			requestTree.put(L"type", L"CHATMESSAGE");
			requestTree.put(L"SessionId", L"A27AAA70F2AF4B7C947512019BDCC95F");
			requestTree.put(L"Id", L"78503A1D3135430DB28B221D70E33354");
			requestTree.put(L"Timestamp", L"2010-08-18 08:15:30");
			requestTree.put(L"Content", L"some text");
			requestTree.put(L"ContentType", L"text");
			requestTree.put(L"Direction", L"I");
			requestTree.put(L"Autoreply", L"0");

			MessagearrayTree.push_back(std::make_pair(L"", requestTree));
			std::wstring request = core::support::ptree_parser::tree_to_json_string(requestTree), 
				response;

			response = make_send(request);

			requestTree.put(L"Id", L"78503A1D3135430DB28B221D70E33355");
			requestTree.put(L"Content", L"some text text");
			requestTree.put(L"Timestamp", L"2010-08-18 08:15:31");

			MessagearrayTree.push_back(std::make_pair(L"", requestTree));
			request = core::support::ptree_parser::tree_to_json_string(requestTree);
			
			response = make_send(request);

			requestTree.clear();
			requestTree.put(L"type", L"GET_MESSAGES_BYSID");
			requestTree.put(L"SessionId", L"A27AAA70F2AF4B7C947512019BDCC95F");
			requestTree.put(L"Timestamp", L"2010-08-18 08:15:33");

			request = core::support::ptree_parser::tree_to_json_string(requestTree);

			//response
			responseTree.put(L"type", L"MESSAGES_BYSID");
			responseTree.put(L"SessionId", L"A27AAA70F2AF4B7C947512019BDCC95F");
			responseTree.add_child(L"Messagearray", MessagearrayTree);

			response = core::support::ptree_parser::tree_to_json_string(responseTree);

			std::wstring response2 = make_request(request);

			responseTree = core::support::ptree_parser::tree_from_string_json(response2);
			responseTree.erase(L"Timestamp");

			Assert::AreEqual(core::support::ptree_parser::tree_to_json_string(responseTree), response);
		}

	private:
		std::wstring make_request(const std::wstring& _a);

		std::wstring make_send(const std::wstring& _a);

	};


	std::wstring UnitTest1::make_request(const std::wstring & _request)
	{
		//singleton_auto_pointer<CSystemLog> log;
		//log->LogString(LEVEL_INFO, L"Starting run");

		TConnectionParams params;

		params._uri = "172.16.72.34";
		params._port = "80";
		params._request = wtos(_request);
		makeMyClient(params, nullptr);

		return stow(params._response);
	}

	std::wstring UnitTest1::make_send(const std::wstring & _request)
	{
		//singleton_auto_pointer<CSystemLog> log;
		//log->LogString(LEVEL_INFO, L"Starting run");

		TConnectionParams params;

		params._uri = "172.16.72.34";
		params._port = "80";
		params._request = wtos(_request);
		makeMyClient_JustSend(params, nullptr);

		return stow(params._response);
	}

}