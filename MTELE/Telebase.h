/************************************************************************/
/* Name     : MTELE\telebase.h			                                */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 27 Sep 2016                                               */
/************************************************************************/
#pragma once
#include "Base.h"

using ChatId			= long long;//uint64_t;

using TMessageStatus = TMessageStatus_t<ChatId>;

using ChatTextMessageHandler = Base::ChatTextMessageHandler<ChatId>;

/******************************* eof *************************************/