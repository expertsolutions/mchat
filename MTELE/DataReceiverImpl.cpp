/************************************************************************/
/* Name     : MTELE\DataReceiverImpl.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Jul 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/application/aspects/path.hpp>
#include "DataReceiverImpl.h"
#include "MakeClient.h"

#include "tgbot/net/TgLongPoll.h"
#include "tgbot/net/HttpParser.h"
#include "WinApi/ProcessHelper.h"
#include "MessageCollector.h"
#include "MessageParser.h"
#include "Utils.h"

const int EXPIREDTIME = 30;
const int CHATTIMETODIE = 600;

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& io) :
	m_log(NULL),
	r_io(io)
{
}

CDataReceiverImpl::~CDataReceiverImpl()
{
	int test = 0;
}

void CDataReceiverImpl::set_Subscribes()
{
	subscribe(L"S2MCC_replyToMessage", &CDataReceiverImpl::S2MCC_ReplyEventHandler);
	subscribe(L"MCP2ICCP/OAM_replyToMessage", &CDataReceiverImpl::MCP2MCC_ReplyEventHandler);
	subscribe(L"ANY2SM_BeginSessionAck", &CDataReceiverImpl::SM2MCC_BeginSessionAckHandler);
	subscribe(L"SM2ANY_SessionDeleted", &CDataReceiverImpl::SM2MCC_SessionDeletedHandler);
	subscribe(L"MCP2ICCP/OAM_receiveMessageRequest", &CDataReceiverImpl::MCP2MCC_ReceiveMessageRequestHandler);
	subscribe(L"S2MCC_SessionUpdate", &CDataReceiverImpl::S2MCC_SessionUpdateHandler);
	subscribe(L"S2MCC_SessionTransfer", &CDataReceiverImpl::S2MCC_SessionTransferHandler);
}

int CDataReceiverImpl::init_impl(CSystemLog* pLog)
{
	if (!pLog)
		return -1111;

	m_log = pLog;
	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	boost::asio::spawn(r_io, [&](boost::asio::yield_context yield)
	{
		boost::asio::steady_timer timer(r_io);

		timer.expires_from_now(std::chrono::milliseconds(100));
		timer.async_wait(yield); //wait for timer

		start();
	});

	return 0;
}

int CDataReceiverImpl::start()
{
	SystemConfig settings;

	int iClientTimeout = settings->safe_get_config_int(_T("ClientTimeout"), EXPIREDTIME);
	_sClientHost = wtos(settings->safe_get_config_value(L"ClientHost", std::wstring(L"api.telegram.org")));
	_sClientPort = wtos(settings->safe_get_config_value(L"ClientPort", std::wstring(L"https")));

	std::string server_host = wtos(settings->safe_get_config_value(L"ServerHost", std::wstring(L"0.0.0.0")));
	std::string server_port = wtos(settings->safe_get_config_value(L"ServerPort", std::wstring(L"443")));

	std::string webhook_host = wtos(settings->safe_get_config_value(L"WebHook", std::wstring(L"0.0.0.0")));

	_sAttachmentFolder = settings->safe_get_config_value(L"AttachmentFolder", std::wstring(L"c:\\Attachments\\outbox\\"));
	_sAttachmentString = settings->safe_get_config_value(L"AttachmentString", std::wstring(L"http://ms-ccvm002/Attachments/"));

	_sLoadAttachmentCommand = settings->safe_get_config_value(L"LoadAttachmentCommand", std::wstring());

	m_log->LogString(LEVEL_WARNING, L"Starting...");

	_sToken = wtos(settings->safe_get_config_value(L"BotToken", std::wstring(L"0")));
	int iChatExpiredTime = settings->safe_get_config_int(_T("ChatExpiredTime"), CHATTIMETODIE);
	std::string sCertificate = wtos(settings->safe_get_config_value(L"SSLSertificate", std::wstring(L"")));
	std::string sPrivateKey = wtos(settings->safe_get_config_value(L"SSLPrivateKey", std::wstring(L"")));

	if (sCertificate.empty())
	{
		throw std::runtime_error("Cannot find SSLSertificate");
	}

	if (sPrivateKey.empty())
	{
		throw std::runtime_error("Cannot find SSLPrivateKey");
	}

	//if (true) test123
	//{
	//	TConnectionParams params;
	//	params._uri = _sClientHost;
	//	params._port = _sClientPort;
	//	//params._token = _sToken;
	//	//params._request = _sClientHost + ":" + _sClientPort + "/bot" + _sToken + "/getFile?file_id=BQADAgADAwADVXAcDzokRxkHKS";

	//	TgBot::Api api(_sToken, &r_io, m_log, std::move(params));
	//	api.getDocument("BQADAgADAwADVXAcDzokRxkHKSTdAg");
	//}

	std::string pem_location = wtos(/*extra_api::*/CProcessHelper::GetCurrentFolderName());
	sCertificate = pem_location + sCertificate;
	sPrivateKey = pem_location + sPrivateKey;

	singleton_auto_pointer<CMessageCollector> collector;
	singleton_auto_pointer<CChatInfoCollector> info;
	collector->Init(m_log);
	info->Init(m_log);

	collector->SetIncomingMessageHandler(std::bind(&CDataReceiverImpl::onChatMessage, this, std::placeholders::_1, std::placeholders::_2));

	//// WEBHOOK
	if (true)
	{
		TConnectionServerParams params;
		params._uri = /*server_host*/webhook_host;
		params._port = server_port;
		params._token = _sToken;
		params._iServerTimeout = iClientTimeout;
		params._clientId = GetClientId();
		params._sertificate = sCertificate;
		params._private_key = sPrivateKey;

		makeMySslServer(&r_io, std::move(params), m_log);

		bool failed = false;
		//
		if (true)
		{
			TConnectionParams params;
			params._uri = _sClientHost;
			params._port = _sClientPort;

			TgBot::Api api(_sToken, &r_io, m_log, std::move(params));
			//std::string url = "https://" + server_host + "/" + token;
			std::string url = server_host + ":" + server_port + "/" + _sToken;

			TgBot::InputFile::Ptr ssl = std::make_shared<TgBot::InputFile>(std::move(sCertificate));
			ssl->mimeType = "application/x-pem-file";

			try
			{
				m_log->LogString(LEVEL_INFO, L"Set Webhook to: %s", stow(url).c_str());
				api.setWebhook(url, ssl);
				m_log->LogString(LEVEL_INFO, L"Set Webhook OK");
			}
			catch (std::exception& e) 
			{
				m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
				failed = true;
			}
			catch (...) 
			{
				m_log->LogString(LEVEL_WARNING, L"Unknown Exception while webhooking");
				failed = true;
			}

			if (failed)
			{
				try
				{
					m_log->LogString(LEVEL_INFO, L"Get Webhook info: %s", stow(url).c_str());
					if (auto webhook = api.getWebhookInfo(url))
					{
						m_log->LogString(LEVEL_WARNING, L"Set webhook error: %s", webhook->lastErrorMessage.c_str());
					}
					m_log->LogString(LEVEL_INFO, L"Get Webhook OK");
				}
				catch (std::exception& e)
				{
					m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
				}
				catch (...)
				{
					m_log->LogString(LEVEL_WARNING, L"Unknown Exception while webhooking");
				}
			}
		}
	}

	//////////

	//// UPDATE
	//if (true)
	//{
	//	//TConnectionServerParams params;
	//	TConnectionParams params;
	//	params._uri = _sClientHost;
	//	params._port = _sClientPort;
	//	params._token = _sToken;
	//	params._iServerTimeout = iClientTimeout;
	//	params._clientId = GetClientId();
	//	//params._sertificate = sCertificate;

	//	makeMyServer(std::move(params));
	//}
	/////////

	// Start client
	if (true)
	{
		TConnectionClientParams params;
		params._uri = _sClientHost;
		params._port = _sClientPort;
		params._iClientTimeout = iClientTimeout;
		params._clientId = GetClientId();
		params._token = _sToken;

		makeMySslClient(&r_io, std::move(params), m_log);
	}

	// Start garbage collector
	if (true)
	{
		TConnectionClientParams params;
		makeGarbageCleaner(iChatExpiredTime);
	}

	return 0;
}



void CDataReceiverImpl::stop()
{
	r_io.stop();
}

void CDataReceiverImpl::_sendReceiveMessage(MCCID mccid, MCPId mid, const CMessage& msg) const
{
	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(mid);

	this->SendFromTo(msg, addr_from, addr_to);
}

void CDataReceiverImpl::_sendBeginSession(MCCID mccid, int32_t date, MessageId msgId, std::wstring contact, std::wstring split, std::wstring ipAddress) const
{
	CMessage msg(L"ANY2SM_BeginSession");
	msg[L"ChannelId"] = std::move(contact);
	msg[L"ChannelName"] = L"Tlg";
	msg[L"Split"] = /*L"Mobile_test#A#";*/ std::move(split);
	msg[L"IpAddress"] = /*L"127.0.0.1";*/ipAddress;
	msg[L"ChannelType"] = L"16";
	msg[L"TopicName"] = L"Unknown";

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(CLIENT_ADDRESS::Any());

	this->SendFromTo(std::move(msg), addr_from, addr_to);
}

void CDataReceiverImpl::_sendReceiveMessageRequestAck(
	MCCID mccid, 
	SessionId sid, 
	MCPId mid, 
	std::wstring requestId, 
	std::wstring ChannelId) const
{
	std::lock_guard<std::mutex> lock(_mutex);
	r_io.post([mccid = mccid, sid = sid, mid = mid, requestId = requestId, ChannelId = ChannelId, pThis = this]()
	{
		try
		{
			CMessage msg(L"MCP2ICCP/OAM_receiveMessageRequestAck");
			msg[L"requestId"] = requestId;
			msg[L"intStatus"] = 0;

			singleton_auto_pointer<CMessageCollector> collector;

			CMessageCollector::MessageList messagesToSend;

			collector->ForEachMessage(mccid, [&](CMessage & _msg)
			{
				if (_msg != L"TELEMESSAGE" && _msg != L"TRANSFER")
				{
					return;
				}

				MESSAGE_STATUS status = static_cast<MESSAGE_STATUS>(_msg.SafeReadParam(L"messageStatus", CMessage::CheckedType::Int, static_cast<int>(MESSAGE_STATUS::NEW)).AsInt());
				if (status == MESSAGE_STATUS::SENT || status == MESSAGE_STATUS::RECIEVED)
				{
					return;
				}

				_msg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::SENT);

				std::wstring contentType = _msg.SafeReadParam(L"contentType", CMessage::CheckedType::String, L"").AsWideStr();
				std::wstring content = _msg.SafeReadParam(L"content", CMessage::CheckedType::String, L"").AsWideStr();

				CMessage msg(L"MCP2ICCP/OAM_receiveMessage");

				if (contentType != L"FILE")
					msg[L"message"] = std::move(content);
				else
				{
					msg[L"message"] = L"file in attachment";
					msg[L"attachments"] = _msg[L"attachments"];
				}


				msg[L"channelType"] = 16;
				msg[L"channelId"] = ChannelId;
				msg[L"sessionId"] = sid;
				msg[L"channelName"] = L"Tlg";
				msg[L"messageReceiver"] = L"MCC_TELE";
				msg[L"messageTitle"] = L"Title";
				msg[L"MCCID"] = mccid;

				msg[L"messageId"] = _msg[L"messageId"];
				msg[L"messageDate"] = core::support::time_to_string<std::wstring>(core::support::now());

				messagesToSend.emplace_back(msg);
			});

			pThis->m_log->LogString(LEVEL_FINEST, L"Got %i messages to send", messagesToSend.size());

			if (!messagesToSend.empty())
			{
				CMessage jsondata;
				jsondata[L"jsondata"] = core::to_raw_data(messagesToSend);

				std::wstring response = CMessageParser::ToStringUnicode(jsondata);

				//std::wstring response = CMessageParser::ToString(std::move(messagesToSend)).c_str();

				int pos1 = response.find(L"[");
				int pos2 = response.rfind(L"]");

				if (pos1 > 0 && pos2 > 0)
				{
					msg[L"jsondata"] = response.substr(pos1, pos2 - pos1 + 1);
				}
			}

			CLIENT_ADDRESS addr_from(mccid);
			CLIENT_ADDRESS addr_to(mid);
			pThis->SendFromTo(std::move(msg), addr_from, addr_to);
		}
		catch (std::runtime_error& e)
		{
			pThis->m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
		}
		catch (std::exception& e) {
			pThis->m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
		}
		catch (...) {
			pThis->m_log->LogString(LEVEL_WARNING, L"Unknown exception");
		}


	});

}

void CDataReceiverImpl::_sendSessionTimeout(MCCID mccid, SessionScriptId ssid) const
{
	m_log->LogString(LEVEL_FINEST, L"This chat with ChatId = %llu has expired", mccid);

	CMessage msg(L"MCC2S_SessionTimeout");

	singleton_auto_pointer<CChatInfoCollector> infoCollector;
	infoCollector->update_status(mccid, CHAT_STATUS::CS_EXPIRED);

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(ssid);
	this->SendFromTo(std::move(msg), addr_from, addr_to);
}



void CDataReceiverImpl::_sendReplyToMessageAck(MCCID mccid, SessionScriptId ssid) const
{
	CMessage msg(L"S2MCC_replyToMessageAck");
	msg[L"intStatus"] = 0;

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(ssid);
	this->SendFromTo(std::move(msg), addr_from, addr_to);

}

void CDataReceiverImpl::_sendReplyToMessageAck(MCCID mccid, SessionScriptId ssid, std::wstring requestId) const
{
	CMessage msg(L"MCP2ICCP/OAM_replyToMessageAck");
	msg[L"requestId"] = requestId;
	msg[L"intStatus"] = 0;

	CLIENT_ADDRESS addr_from(mccid);
	CLIENT_ADDRESS addr_to(ssid);
	this->SendFromTo(std::move(msg), addr_from, addr_to);

}

void CDataReceiverImpl::_sendTeleMessageToServer(MCCID aMccid, CMessage&& aMsg)
{
	std::lock_guard<std::mutex> lock(_mutex);
	r_io.post([msg = std::move(aMsg), mccid = aMccid, pThis = this]()
	{
		singleton_auto_pointer<CMessageCollector> collector;
		collector->push_to_server_message(mccid, std::move(msg));
	});
}


void CDataReceiverImpl::on_routerConnect()
{
	m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
}

void CDataReceiverImpl::on_routerDeliverError(const CMessage& msg, const CLIENT_ADDRESS &, const CLIENT_ADDRESS &)
{
	m_log->LogStringModule(LEVEL_FINE, L"Router client", msg.Dump().c_str());

	activateAlarm(static_cast<int>(MTELEErrors::ERR_DELIVER),
		GetErrorNameByCode(MTELEErrors::ERR_DELIVER),
		(boost::wformat(L"Message %s deliver error") % msg.GetName()).str(),
		L"Check address");
}

void CDataReceiverImpl::on_appStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	if (nStatus == (core::ClientStatus::CS_DISCONNECTED))
	{
		try
		{

		}
		catch (...)
		{

		}
	}

}

/************************ GET UPDATE ************************/
void CDataReceiverImpl::makeMyServer(TConnectionParams params)
{
	boost::asio::spawn(r_io, [pThis = this,
		params = std::move(params),
		p_io = &r_io,
		//pCallBack = &_callBack,
		m_log = m_log](boost::asio::yield_context yield)
	{
		TgBot::Api api(params._token, p_io, m_log, /*std::move*/params);

		pThis->_botname = stow(api.getMe()->username);

		//*pCallBack = [&api](int64_t chatId, std::string message, const TgBot::GenericReply::Ptr& object)
		//{
		//	api.sendMessage(chatId, message, false, 0, object);
		//};

		boost::asio::steady_timer timer(*p_io);
		boost::system::error_code errorCode;

		try
		{
			// get last update number
			std::vector<TgBot::Update::Ptr> updates = api.getUpdates(0, 100, 1);
			if (updates.size())
				pThis->_lastUpdateId = updates[updates.size() - 1]->updateId + 1;

			// run
			bool first_call = true;
			while (!(*p_io).stopped())
			{
				timer.expires_from_now(std::chrono::seconds(first_call ? 1 : params._iServerTimeout));
				first_call = false;
				timer.async_wait(yield[errorCode]); //wait for timer

				if (errorCode)
				{
					throw errorCode;
				}

				std::vector<TgBot::Update::Ptr> updates = api.getUpdates(/*std::bind(&CDataReceiverImpl::onGetUpdates, pThis, std::placeholders::_1), */pThis->_lastUpdateId, 100, 1/* static_cast<int>(iLongPoolTimer / 2)*/);

				for (TgBot::Update::Ptr& item : updates)
				{
					if (item->updateId >= pThis->_lastUpdateId) {
						pThis->_lastUpdateId = item->updateId + 1;
					}
					if (item->message)
						pThis->tele_UpdateHandler(item->message);
				}
			}

		}
		catch (std::runtime_error& e)
		{
			m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
		}
		catch (std::exception& e) {
			m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
		}

	});

}

std::wstring CDataReceiverImpl::getDocumentLink(std::wstring && fileid)
{
	std::wstring documentLink, fileUrl;

	std::vector<TgBot::HttpReqArg> args;
	args.push_back(TgBot::HttpReqArg("file_id", wtos(fileid)));

	std::string url = std::string("https://") + _sClientHost + "/bot" + _sToken + "/getFile";

	std::string request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);

	TConnectionParams params;
	params._uri = _sClientHost;
	params._port = _sClientPort;
	params._token = _sToken;
	params._request = request;//_sClientHost + ":" + _sClientPort + "/bot" + _sToken + "/getFile?file_id=" + wtos(std::move(fileid));

	boost::asio::io_service io;
	makeMySslClient(&io, params, m_log->GetInstance());
	io.run();

	TgBot::HttpParser::HeadersMap headers;
	std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(params._response, headers));

	CMessage file_msg = CMessageParser::ToMessage(std::move(serverResponse));

	m_log->LogString(LEVEL_FINEST, L"File msg DUMP: %s", file_msg.Dump());

	bool bOk = file_msg.SafeReadParam(L"ok", CMessage::CheckedType::String, L"false").AsWideStr() == L"false" ? false : true;
	if (bOk)
	{
		CMessage result = file_msg[L"result"].AsMessage();

		fileUrl = result.SafeReadParam(L"file_path", CMessage::CheckedType::String, L"").AsWideStr();

		m_log->LogString(LEVEL_FINEST, L"remote file url: %s", fileUrl.c_str());

		fileUrl = (boost::wformat(L"https://api.telegram.org/file/bot%s/%s") % stow(_sToken) % fileUrl).str();

		m_log->LogString(LEVEL_FINEST, L"remote file url: %s", fileUrl.c_str());
	}
	else
	{
		m_log->LogString(LEVEL_FINEST, L"loadDocument: cannot parse");
	}

	documentLink = _sLoadAttachmentCommand + fileUrl;

	m_log->LogString(LEVEL_FINEST, L"Remote document link: %s", documentLink.c_str());

	return documentLink;
}

//std::wstring CDataReceiverImpl::loadDocument(MCCID mccid, const std::wstring & filename, std::wstring && fileid)
//{
//	//http://stackoverflow.com/questions/34170546/getfile-method-in-telegram-bot-api
//
//
//	/*std::wstring localPath = _sAttachmentFolder;
//	std::wstring fileUrl = _sAttachmentString;
//	std::wstring cid = utils::toStr<wchar_t>(mccid);
//
//	if (localPath[localPath.length() - 1] != ('\\'))
//		localPath += L"\\";
//
//	if (fileUrl[fileUrl.length() - 1] != ('/'))
//		fileUrl += L"/";
//
//	localPath += cid;
//	fileUrl += cid;
//	if (!boost::filesystem::exists(localPath) && !boost::filesystem::create_directories(localPath))
//	{
//		throw(std::runtime_error((boost::format("Cannot create \"%s\" folder") % localPath.c_str()).str()));
//	}
//	else
//	{
//
//		localPath += L"\\";
//		localPath += filename;
//
//		fileUrl += L"/";
//		fileUrl += filename;
//	}*/
//	
//	std::wstring fileUrl;
//
//	std::vector<TgBot::HttpReqArg> args;
//	args.push_back(TgBot::HttpReqArg("file_id", wtos(fileid)));
//
//	std::string url = std::string("https://") + _sClientHost + "/bot" + _sToken + "/getFile";
//
//	std::string request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);
//
//	TConnectionParams params;
//	params._uri = _sClientHost;
//	params._port = _sClientPort;
//	params._token = _sToken;
//	params._request = request;//_sClientHost + ":" + _sClientPort + "/bot" + _sToken + "/getFile?file_id=" + wtos(std::move(fileid));
//
//	boost::asio::io_service io;
//	makeMySslClient(&io, params, m_log->GetInstance());
//	io.run();
//	
//	TgBot::HttpParser::HeadersMap headers;
//	std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(params._response, headers));
//
//	CMessage file_msg = CMessageParser::ToMessage(std::move(serverResponse));
//
//	bool bOk = file_msg.SafeReadParam(L"ok", CMessage::CheckedType::String, L"false").AsWideStr() == L"false" ? false : true;
//	if (bOk)
//	{
//		fileUrl = file_msg.SafeReadParam(L"file_path", CMessage::CheckedType::String, L"").AsWideStr();
//		fileUrl = (boost::wformat(L"https://api.telegram.org/file/bot%s/%s") % _sToken % fileUrl).str();
//
//		m_log->LogString(LEVEL_FINEST, L"Got file url: %s", fileUrl.c_str());
//	}
//	else
//	{
//		m_log->LogString(LEVEL_FINEST, L"loadDocument: cannot parse");
//	}
//
//	//boost::asio::io_service io;
//
//	//io.post([localPath =std::move(localPath),
//	//	fileid = std::move(fileid), 
//	//	p_io = &io,
//	//	pLog = m_log,
//	//	uri = _sClientHost, 
//	//	port = _sClientPort,
//	//	token = _sToken]()
//	//{
//
//	//	TConnectionParams params;
//	//	params._uri = uri;
//	//	params._port = port;
//	//	params._token = token;
//	//	params._request = uri + ":" + port + "/bot" + token + "/getFile?file_id=" + wtos(std::move(fileid));
//
//	//	//TgBot::Api api(token, p_io, pLog, std::move(params));
//
//	//	
//
//	//	//auto file_msg = CMessageParser::ToMessage(stow(params._response));
//	//	//bool bOk = file_msg.SafeReadParam(L"ok", CMessage::CheckedType::String, L"false").AsWideStr() == L"false" ? false : true;
//	//	//if (bOk)
//	//	//{
//
//
//
//	//	//	std::fstream ofs(localPath, std::fstream::ios_base::out | std::fstream::ios_base::binary);
//	//	//	ofs.write(params._response.c_str(), params._response.size());
//	//	//	ofs.close();
//	//	//}
//	//	//else
//	//	//{
//	//	//	pLog->LogStringModule(LEVEL_FINEST, L"loadDocument", L"Cannot load document, name = %s, fileId = %s", localPath.c_str(), fileid.c_str());
//	//	//}
//
//	//});
//
//	//io.run();
//	//boost::asio::spawn(r_io, [pThis = this,
//	//	localPath = localPath,
//	//	p_io = &r_io,
//	//	m_log = m_log](boost::asio::yield_context yield)
//	//{
//	//	boost::system::error_code errorCode;
//
//	//	try
//	//	{
//	//	}
//	//	catch (std::runtime_error& e)
//	//	{
//	//		m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
//	//	}
//	//	catch (std::exception& e) {
//	//		m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
//	//	}
//
//	//});
//
//	return fileUrl;
//}

void CDataReceiverImpl::_makeChatTimeout(MCCID mccid, SessionScriptId ssid)const
{
	if (ssid)
		_sendSessionTimeout(mccid, ssid);
	else
	{
		m_log->LogString(LEVEL_WARNING, L"Chat with id = %llu has expired earlier than the script was launched", mccid);

		if (true)
		{
			singleton_auto_pointer<CChatInfoCollector> infoCollector;
			decltype(auto) info = infoCollector->get_info(mccid);

			CMessage msg(L"TELEMESSAGE_Reply");
			msg[L"id"] = info.getChatId();
			msg[L"contentType"] = L"TEXT";
			msg[L"content"] = L"Session was deleted by timeout";
			msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());

			singleton_auto_pointer<CMessageCollector> collector;
			collector->push_to_server_message(mccid, std::move(msg));

			collector->deleteChat(mccid);
		}

		singleton_auto_pointer<CChatInfoCollector> infoCollector;
		infoCollector->delete_info(mccid);
	}
}

bool CDataReceiverImpl::makeGarbageCleaner(int expiredTime)
{
	boost::asio::spawn(r_io, [io_service = &r_io, pLog = m_log, expiredTime = expiredTime, pThis = this](boost::asio::yield_context yield)
	{
		try
		{
			boost::asio::steady_timer timer(*io_service);
			boost::system::error_code errorCode;

			singleton_auto_pointer<CChatInfoCollector> infoCollector;

			while (!(*io_service).stopped())
			{
				timer.expires_from_now(std::chrono::seconds(expiredTime));
				timer.async_wait(yield[errorCode]); //wait for timer

				pLog->LogStringModule(LEVEL_FINEST, L"steady timer", L"========================Tick===================");

				if (errorCode)
				{
					throw errorCode;
				}

				CChatInfoCollector::IdList expiredList = infoCollector->getExpiredChatList(time(nullptr) - expiredTime);

				for each (const auto& pair in expiredList)
				{
					pThis->_makeChatTimeout(pair.first, pair.second);
				}
			}
		}
		catch (boost::system::error_code& error)
		{
			pLog->LogStringModule(LEVEL_WARNING, L"Cleaner", L"Socket failed with error: %s", stow(error.message()));
		}
		catch (...)
		{
			pLog->LogStringModule(LEVEL_WARNING, L"Cleaner", L"Socket failed with error: unhandled exception caught");
		}
	});

	return true;
}

std::wstring CDataReceiverImpl::_makeOKRequest() const
{
	return stow(TgBot::HttpParser::getInstance().generateResponse("", "text/plain", 200, "OK"));
	//return L"200 OK";
}

CMessage CDataReceiverImpl::_makeMCPMessage(
	MCCID mccid, 
	SessionId sid, 
	std::wstring content, 
	std::wstring contentType, 
	std::wstring ChannelId, 
	MessageId messageId, 
	MessageList && attachments) const
{
	CMessage msg(L"MCP2ICCP/OAM_receiveMessage");

	CMessage jsondata;
	jsondata[L"channelType"] = 16;
	jsondata[L"channelId"] = std::move(ChannelId);
	jsondata[L"sessionId"] = sid;
	jsondata[L"channelName"] = L"Tlg";
	jsondata[L"messageReceiver"] = L"MCC_TELE";
	jsondata[L"messageTitle"] = L"Title";
	jsondata[L"MCCID"] = mccid;

	jsondata[L"messageId"] = messageId;
	jsondata[L"messageDate"] = core::support::time_to_string<std::wstring>(core::support::now());

	if (!attachments.size())
	{
		jsondata[L"message"] = std::move(content);
		msg[L"jsondata"] = CMessageParser::ToStringUnicode(jsondata);
	}
	else
	{
		jsondata[L"attachments"] = core::to_raw_data(attachments);
		msg[L"jsondata"] = CMessageParser::ToStringUnicode(std::move(jsondata)/*, L"attachments", std::move(attachments)*/);
	}

	return msg;

}

/******************************* eof *************************************/