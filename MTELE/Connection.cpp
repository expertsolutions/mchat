/************************************************************************/
/* Name     : MTELE\Connection.cpp                                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 14 Nov 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "Connection.h"
#include "MessageParser.h"
#include "MessageCollector.h"

#include "tgbot/net/HttpParser.h"

const int TCP_PACK_SIZE = 1024;

std::atomic<unsigned int> connection::_count = 0;

void connection::startListening()
{
	boost::asio::spawn(_strand,
		[self = shared_from_this()/*, &collector*/](boost::asio::yield_context yield)
	{
		try
		{
			self->LogStringModule(LEVEL_FINEST, L"10");
			boost::system::error_code errorCode;

			self->_timer.expires_from_now(std::chrono::seconds(3));

			self->_socket.set_verify_mode(boost::asio::ssl::verify_none);
			self->_socket.set_verify_callback([&](bool preverified,
				boost::asio::ssl::verify_context& ctx)
			{
				// The verify callback can be used to check whether the certificate that is
				// being presented is valid for the peer. For example, RFC 2818 describes
				// the steps involved in doing this for HTTPS. Consult the OpenSSL
				// documentation for more details. Note that the callback is called once
				// for each certificate in the certificate chain, starting from the root
				// certificate authority.

				// In this example we will simply print the certificate's subject name.
				char subject_name[256];
				X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
				X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

				std::cout << subject_name << std::endl;

				self->LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");

				return preverified;
			});
			self->LogStringModule(LEVEL_FINEST, L"11");
			self->_socket.async_handshake(boost::asio::ssl::stream_base::server, yield[errorCode]);

			//self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(errorCode.message()));

			self->LogStringModule(LEVEL_FINEST, L"12");

			if (errorCode)
				throw errorCode;

			//singleton_auto_pointer<CConnectionCollector> collector;
			singleton_auto_pointer<CMessageCollector> collector;

			self->LogStringModule(LEVEL_FINEST, L"13");
			//char in_data[1024] = {};
			//while (!self->_strand.get_io_service().stopped())
			//{
			//	ZeroMemory(in_data, 1024);

			//	self->_timer.expires_from_now(std::chrono::seconds(10));

			//	self->LogStringModule(LEVEL_FINEST, L"14");
			//	self->_socket.async_read_some(boost::asio::buffer(in_data), yield);
			//	self->LogStringModule(LEVEL_FINEST, L"15");

			//	std::wstring answer(stow(in_data));

			//	self->LogStringModule(LEVEL_FINEST, L"Read: %s", answer.c_str());

			//	//TgBot::HttpParser::HeadersMap headers;

			//	//std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(wtos(answer), headers));

			//	//CMessage msg = CMessageParser::ToMessage2(std::move(serverResponse));

			//	//TMessageStatus tms = ParseMessageStatus(stow(headers.find("status")->second));

			//	//std::wstring response = collector->Recieve(/*self->_sessionId,*/ std::move(msg), std::move(tms));

			//	////std::wstring response = CMessageParser::MakeResponse(std::move(msg));

			//	////if (response.empty())
			//	////	continue;

			//	//self->LogStringModule(LEVEL_FINEST, L"Write: %s", response.c_str());
			//	//boost::asio::async_write(self->_socket, boost::asio::buffer(wtos(response), response.size()), yield);
			//}

			std::string in_data;

			size_t bytes_transferred = 0;
			do
			{
				char reply_[TCP_PACK_SIZE] = {};
				int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
				if (errorCode)
				{
					throw errorCode;
				}

				bytes_transferred += len;

				in_data.append(reply_, len);

				self->LogStringModule(LEVEL_FINEST, L"..................Read....................:\n %s \n................................", in_data.c_str());

			} while (in_data.find("\r\n\r\n") == std::string::npos);


			//int pos = in_data.find("\r\n\r\n");
			//std::string s1 = in_data.substr(0, pos);
			//std::string s2 = in_data.substr(pos + strlen("\r\n\r\n"), in_data.size() - pos - strlen("\r\n\r\n"));

			//auto size1 = s1.size();
			//auto size2 = s2.size();

			//size_t bytes_transferred = boost::asio::read_until(self->_socket, buff_response, "\r\n\r\n");
			//size_t num_additional_bytes = buff_response.size() - bytes_transferred;
			size_t num_additional_bytes = bytes_transferred/*in_data.size()*/ - (in_data.find("\r\n\r\n") + strlen("\r\n\r\n"));

			self->LogStringModule(LEVEL_FINEST, L"bytes_transferred == %i, %i byte need to read", bytes_transferred, num_additional_bytes);

			//std::istream is(&buff_response);
			//is >> in_data;

			TgBot::HttpParser::HeadersMap headers;
			std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(in_data, headers));

			auto header_it = headers.find("content-length");
			if (header_it != headers.end())
			{
				auto content_length = stoull(header_it->second);
				if (content_length > num_additional_bytes)
				{
					try
					{
						do
						{
							char reply_[TCP_PACK_SIZE] = {};
							int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
							if (errorCode)
							{
								throw errorCode;
							}

							num_additional_bytes += len;

							in_data.append(reply_, len);

						} while (content_length > num_additional_bytes);
					}
					catch (boost::system::error_code& error)
					{
						if (error != boost::asio::error::eof)
							self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
					}
					catch (std::runtime_error & error)
					{
						self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
					}
					catch (...)
					{
						self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
					}
				}
			}
			else
			{
				//throw (std::runtime_error((boost::format("Invalid http header: %s") % in_data.c_str()).str()));
				self->LogStringModule(LEVEL_FINEST, L"No content-length, nothing to read");
			}

			std::wstring answer(stow(in_data));

			self->LogStringModule(LEVEL_FINEST, L"Read: %s", answer.c_str());
			serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(wtos(answer), headers));

			CMessage msg = CMessageParser::ToMessage(std::move(serverResponse));

			TMessageStatus tms;// = ParseMessageStatus(stow(headers.find("status")->second));

			//msg.SetName(tms.methodName);
			std::wstring response = collector->Recieve(/*self->_sessionId,*/ std::move(msg), std::move(tms));

			self->LogStringModule(LEVEL_FINEST, L"Write: %s", response.c_str());
			boost::asio::async_write(self->_socket, boost::asio::buffer(wtos(response), response.size()), yield);


		}
		catch (boost::system::error_code& error)
		{
			if (error != boost::asio::error::eof)
				self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
		}
		catch (std::runtime_error & error)
		{
			self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
		}
		catch (std::exception& e)
		{
			self->LogStringModule(LEVEL_FINEST, L"Exception: %s", stow(e.what()).c_str());

			//self->_socket.close();
			self->_socket.lowest_layer().close();
			self->_timer.cancel();
		}
		catch (...)
		{
			self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
		}

	});

	boost::asio::spawn(_strand,
		[self = shared_from_this()/*, &collector*/](boost::asio::yield_context yield)
	{
		while (self->_socket.lowest_layer().is_open())
		{
			boost::system::error_code ignored_ec;
			self->_timer.async_wait(yield[ignored_ec]);
			if (self->_timer.expires_from_now() <= std::chrono::seconds(0))
			{
				//collector->deleteById(self->_sessionId);
				self->LogStringModule(LEVEL_WARNING, L"Socket async timer timeout expired");
				self->_socket.lowest_layer().close();
			}
		}
	});

}
/******************************* eof *************************************/