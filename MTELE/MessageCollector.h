/************************************************************************/
/* Name     : MTELE\MessageCollector.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 27 Sep 2016                                               */
/************************************************************************/
#pragma once
#include <list>
#include <map>
#include "Patterns/singleton.h"
#include "telebase.h"
#include "Router\router_compatibility.h"

#include "Connection.h"


namespace stuff {
	template< typename ContainerT, typename PredicateT >
	void erase_if(ContainerT& items, const PredicateT& predicate) {
		for (auto it = items.begin(); it != items.end(); ) {
			if (predicate(*it)) it = items.erase(it);
			else ++it;
		}
	};
}

//class CMessageCollector : public singleton <CMessageCollector>
//{
//	friend class singleton <CMessageCollector>;
//
//public:
//	//using IdCollector = std::map<ChatId, SessionScriptId>;
//	using IdCollector = std::set<ChatId>;
//
//	class CMessageContext
//	{
//	public:
//		template <class TMessage>
//		CMessageContext(TMessage&& msg, 
//			SessionScriptId ssid, 
//			SessionId sid, 
//			MCPId mid,
//			bool bStarted, 
//			time_t t)
//			:_body(std::forward<TMessage>(msg)), 
//			_scriptId(ssid), 
//			_sessionId(sid), 
//			_mcpId(mid),
//			_timestamp(t), 
//			_bSessionStarted(bStarted)
//		{}
//
//		auto getBody()const noexcept { return _body; }
//		auto getMessageName()const { return _body.GetName(); }
//		auto getTimeStamp()const noexcept { return _timestamp; }
//		auto getScriptId()const noexcept { return _scriptId; }
//		auto getSessionId()const noexcept { return _sessionId; }
//		auto isSessionStarted()const noexcept { return _bSessionStarted; }
//		auto getMCPId()const noexcept { return _mcpId; }
//
//		//void setTimeStamp(time_t t)  noexcept { _timestamp = t; }
//		//void setScriptId (SessionScriptId id)noexcept { _scriptId = id; }
//		//void setSessionId(SessionId id) noexcept { _sessionId = id; }
//		//void setSessionStarted(bool started = true)noexcept { _bSessionStarted = started; }
//
//	private:
//		SessionScriptId _scriptId{ 0 };
//		SessionId       _sessionId{ 0 };
//		time_t   _timestamp{0};
//		CMessage _body;
//		bool _bSessionStarted = false;
//		MCPId           _mcpId{ 0 };
//	};
//
//	using MessageContextList = std::list<CMessageContext>;
//
//
//private:
//	CMessageCollector() = default;
//
//	~CMessageCollector()
//	{
//		int test = 0;
//	}
//
//
//	template<typename... TArgs>
//	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
//	{
//		_log->LogStringModule(level, L"Message collector", formatMessage, std::forward<TArgs>(args)...);
//	}
//	
//	auto GetContextByChatId(ChatId cid)const
//	{
//		const CMessageContext* context = nullptr;
//
//		auto cit = _chat2MessagesMap.find(cid);
//		if (cit != _chat2MessagesMap.end() && cit->second.size())
//		{
//			context = &cit->second.back();
//		}
//		return context;
//	}
//
//
//public:
//	void SetChatLivePeriod(int seconds_to_die) noexcept { _chat_live_period = seconds_to_die;}
//
//
//	void Init(CSystemLog* log)
//	{
//		_log = log;
//	}
//
//	//SessionScriptId GetScriptIdByChatId(ChatId cid)const
//	//{
//	//	SessionScriptId sid = 0;
//
//	//	auto cit = _chat2MessagesMap.find(cid);
//	//	if (cit != _chat2MessagesMap.end() && cit->second.size())
//	//	{
//	//		sid = cit->second.back().getScriptId();
//	//	}
//
//	//	return sid;
//	//}
//
//	//SessionId GetSessionIdByChatId(ChatId cid)const
//	//{
//	//	SessionId sid = 0;
//
//	//	auto cit = _chat2MessagesMap.find(cid);
//	//	if (cit != _chat2MessagesMap.end() && cit->second.size())
//	//	{
//	//		sid = cit->second.back().getSessionId();
//	//	}
//
//	//	return sid;
//	//}
//
//
//	auto GetScriptIdByChatId(ChatId cid)const
//	{
//		SessionScriptId ssid = 0;
//
//		if (auto pContext = GetContextByChatId(cid))
//		{
//			ssid = pContext->getScriptId();
//			//LogStringModule(LEVEL_FINEST, L"GetScriptIdByChatId, ScriptId == %llu, msgName == %s", ssid, pContext->getBody().GetName().c_str());
//			_log->LogStringModule(LEVEL_FINEST, L"Message collector", L"GetScriptIdByChatId, ScriptId == %llu, msgName == %s", ssid, pContext->getMessageName().c_str());
//		}
//
//		return ssid;
//	}
//
//	auto GetSessionIdByChatId(ChatId cid)const
//	{
//		SessionId sid = 0;
//
//		if (auto pContext = GetContextByChatId(cid))
//			sid = pContext->getSessionId();
//
//		return sid;
//	}
//
//	//template <class TMessage>
//	//void AddMessage(ChatId _id, TMessage&& init_msg, time_t timestamp = time(nullptr))
//	//{
//	//	CMessage message(std::forward<TMessage>(init_msg));
//	//	message[(boost::wformat(L"timestamp%i") % _id).str()] = timestamp;
//
//	//	auto cit = _chat2MessagesMap.find(_id);
//	//	if (cit != _chat2MessagesMap.end())
//	//	{
//	//		cit->second.emplace_back(std::move(message));
//	//	}
//	//	else
//	//	{
//	//		MessageCollector newList = { std::move(message) };
//	//		_chat2MessagesMap.emplace(_id, newList);
//	//	}
//	//}
//
//	template <class TMessage>
//	void AddMessageToNewChat(ChatId _cid, SessionScriptId _ssid, SessionId _sid, TMessage&& message, time_t timestamp = time(nullptr))
//	{
//		LogStringModule(LEVEL_FINEST, L"AddMessage %s, ChatId = %llu, ScriptId = %llu", message.GetName().c_str(), _cid, _ssid);
//		//CMessage message(std::forward<TMessage>(init_msg));
//		//message[(boost::wformat(L"timestamp%i") % _id).str()] = timestamp;
//
//		//auto cit = _chat2MessagesMap.find(_id);
//		//if (cit != _chat2MessagesMap.end())
//		//{
//		//	cit->second.emplace_back(std::move(message));
//		//}
//		//else
//		//{
//		//	MessageCollector newList = { std::move(message) };
//		//	_chat2MessagesMap.emplace(_id, newList);
//		//}
//
//		//CMessage message(std::forward<TMessage>(init_msg));
//		//message[(boost::wformat(L"timestamp%i") % _id).str()] = timestamp;
//		//CMessageContext context(std::forward<TMessage>(message), 0, timestamp)
//
//		auto cit = _chat2MessagesMap.find(_cid);
//		if (cit != _chat2MessagesMap.end())
//		{
//			//cit->second.emplace_back(std::forward<TMessage>(message), _ssid, _sid, false, timestamp);
//			throw (std::runtime_error((boost::format("Chat with \"chat_id\" == %llu has already exist") % _cid).str()));
//		}
//		else
//		{
//			MessageContextList newList = { CMessageContext(std::forward<TMessage>(message),_ssid, _sid, 0, false, timestamp) };
//			_chat2MessagesMap.emplace(_cid, std::move(newList));
//		}
//	}
//
//	template <class TMessage>
//	void AddMessage(ChatId _cid, SessionScriptId _ssid, SessionId _sid, MCPId _mid, TMessage&& message, bool bSessionStarted = true, time_t timestamp = time(nullptr))
//	{
//		MessageId msgId = 0;
//		try
//		{
//			msgId = message.SafeReadParam(L"MessageId", CMessage::CheckedType::Int64, 0).AsInt64();
//		}
//		catch (const std::exception&)
//		{
//			std::wstring s_msgId = message.SafeReadParam(L"MessageId", CMessage::CheckedType::String, L"").AsWideStr();
//			msgId = toNumX<MessageId, wchar_t>(s_msgId);
//		}
//		
//
//		LogStringModule(LEVEL_FINEST, L"AddMessage %s, ChatId = %llu, ScriptId = %llu, MCPId = %llu, messageId = %llu", message.GetName().c_str(),  _cid, _ssid, _mid, msgId);
//
//		auto cit = _chat2MessagesMap.find(_cid);
//		if (cit != _chat2MessagesMap.end())
//		{
//			cit->second.emplace_back(std::forward<TMessage>(message), _ssid, _sid, _mid, bSessionStarted, timestamp);
//		}
//		else
//		{
//		//	//std::string sError = (boost::format("AddMessage ERROR: cannot find chat by \"chat_id\" == %i") % _id).str();
//			throw (std::runtime_error((boost::format("AddMessage: cannot find chat by \"chat_id\" == %llu") % _cid).str()));
//		}
//	}
//
//	void DeleteChat(ChatId _cid)
//	{
//		LogStringModule(LEVEL_FINEST, L"DeleteChat, ChatId = %llu", _cid);
//
//		auto cit = _chat2MessagesMap.find(_cid);
//		if (cit != _chat2MessagesMap.end())
//		{
//			_chat2MessagesMap.erase(cit);
//		}
//	}
//
//	bool IfExist(ChatId _id)const noexcept
//	{
//		return (_chat2MessagesMap.find(_id) != _chat2MessagesMap.end());
//	}
//
//
//	const MessageContextList& GetChat(ChatId _id)
//	{
//		auto cti = _chat2MessagesMap.find(_id);
//		if (_chat2MessagesMap.find(_id) != _chat2MessagesMap.end())
//		{
//			return cti->second;
//		}
//
//		throw (std::runtime_error((boost::format("Cannot find chat by ChatId = %llu") % _id).str()));
//	}
//
//
//	//IdCollector DeleteLongLivingChats(time_t curtime = time(nullptr))
//	//{
//	//	//LogStringModule(LEVEL_FINEST, L"All old chats must die, curtime = %llu", curtime);
//
//	//	IdCollector oldChats;
//
//	//	time_t expiredTime = curtime - _chat_live_period;
//
//	//	stuff::erase_if(_chat2MessagesMap, [&oldChats, expiredTime, pThis = this](auto & chat_pair)
//	//	{
//	//		if (!chat_pair.second.size())
//	//		{
//	//			return true;
//	//		}
//	//		CMessageContext lastMessage = chat_pair.second.back();
//	//		//time_t msgTime = lastMessage.getBody().SafeReadParam((boost::wformat(L"timestamp%llu") % chat_pair.first).str(), CMessage::CheckedType::Int64, 0).AsInt64();
//	//		time_t msgTime = lastMessage.getTimeStamp();
//
//	//		//pThis->LogStringModule(LEVEL_FINEST, L"ChatId = %llu, curtime = %llu", chat_pair.first, msgTime);
//
//	//		if (msgTime && (msgTime < expiredTime))
//	//		{
//	//			oldChats.emplace(chat_pair.first, lastMessage.getScriptId());
//	//			return true;
//	//		}
//
//	//		return false;
//	//	});
//
//	//	return oldChats;
//	//}
//
//	template <class TFunction, typename... TArgs>
//	void DoForEveryMsgByChatId(ChatId id, TFunction func, TArgs&& ...args) const
//	{
//		auto cit = _chat2MessagesMap.find(id);
//		if (cit != _chat2MessagesMap.end())
//		{
//			for each (const auto& msg in cit->second)
//			{
//				func(msg, std::forward<TArgs>(args)...);
//			}
//		}
//	}
//
//	template <class TFunction, typename... TArgs>
//	void DoForEveryChat(TFunction func, TArgs&& ...args) const
//	{
//		for each (const auto & chat in _chat2MessagesMap)
//		{
//			func(chat.first, chat.second, std::forward<TArgs>(args)...);
//		}
//	}
//
//
//private:
//
//	using ChatCollector = std::map<ChatId, MessageContextList>;
//
//	ChatCollector _chat2MessagesMap;
//	//IdCollector   _chat2ScriptMap;
//
//	//void ff()
//	//{
//	//	for each (const auto & chat_pair in _chat2MessagesMap)
//	//	{
//	//		ChatId id = chat_pair.first;
//	//		MessageCollector list = chat_pair.second;
//	//		list.back();
//	//	}
//	//}
//
//	CSystemLog* _log;
//
//	int _chat_live_period = 60;
//};
//

enum class CHAT_STATUS
{
	CS_CREATED = 1,
	CS_STARTED,
	CS_ENQUEUED,
	CS_EXPIRED,
	CS_FINISHED
};

//enum class MESSAGE_STATUS
//{
//	NEW = 1,
//	SENT,
//	TRANSFER,
//	RECIEVED
//};

static std::wstring statusToString(CHAT_STATUS status)
{
	std::wstring result = L"Unknown";
	switch (status)
	{
	case CHAT_STATUS::CS_CREATED:
	{
		result = L"Created";
		break;
	}
	case CHAT_STATUS::CS_STARTED:
	{
		result = L"Started";
		break;
	}
	case CHAT_STATUS::CS_ENQUEUED:
	{
		result = L"Enqueued";
		break;
	}
	case CHAT_STATUS::CS_EXPIRED:
	{
		result = L"Expired";
		break;
	}
	case CHAT_STATUS::CS_FINISHED:
	{
		result = L"Finished";
		break;

	}
	}
	return result;
}

class CChatInfoCollector : public singleton <CChatInfoCollector>
{
	friend class singleton <CChatInfoCollector>;


	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
	{
		_log->LogStringModule(level, L"Chat collector", formatMessage, std::forward<TArgs>(args)...);
	}

private:

	class CChatInfo
	{
	public:



		CChatInfo() = default;
		CChatInfo(
			MCCID mccid,
			ChatId cid,
			SessionScriptId ssid,
			SessionId sid,
			MCPId mid,
			time_t t,
			std::wstring channel)
			:
			_mccid(mccid),
			_cid(cid),
			_scriptId(ssid),
			_sessionId(sid),
			_mcpId(mid),
			_timestamp(t),
			_channel(channel),
			_status(CHAT_STATUS::CS_CREATED)
		{}

		auto getMCCID()const noexcept { return _mccid; }
		auto getChatId()const noexcept { return _cid; }
		auto getTimeStamp()const noexcept { return _timestamp; }
		auto getScriptId()const noexcept { return _scriptId; }
		auto getSessionId()const noexcept { return _sessionId; }
		auto isSessionStarted()const noexcept { return _status == CHAT_STATUS::CS_STARTED; }
		auto getMCPId()const noexcept { return _mcpId; }
		auto isSessionActive()const noexcept { return _status != CHAT_STATUS::CS_FINISHED; }
		auto getOperatorId()const noexcept { return _operatorId; }
		auto getOperatorName()const noexcept { return _operatorName; }
		auto getStatus()const noexcept { return _status; }
		auto getStatusString()const noexcept { return statusToString(_status); }
		auto getChannel()const noexcept { return _channel; }
		auto getContact()const noexcept { return _contact; }

		void setTimeStamp(time_t t) noexcept { _timestamp = t; }
		void setScriptId(SessionScriptId id) noexcept { _scriptId = id; }
		void setSessionId(SessionId id) noexcept { _sessionId = id; }
		void setMCPId(MCPId id) noexcept { _mcpId = id; }
		//void setSessionStarted(bool started = true) noexcept { _bSessionStarted = started; }
		void setOperatorId(std::wstring operatorId) noexcept { _operatorId = std::move(operatorId); }
		void setOperatorName(std::wstring operatorName) noexcept { _operatorName = std::move(operatorName); }
		void setStatus(CHAT_STATUS newStatus) noexcept { _status = newStatus; }
		void setContact(std::wstring contact) noexcept { _contact = std::move(contact); }

	private:
		MCCID			_mccid{ 0 };
		ChatId          _cid;
		SessionScriptId _scriptId{ 0 };
		SessionId       _sessionId{ 0 };
		time_t			_timestamp{ 0 };
		//bool			_bSessionStarted{ false };
		MCPId           _mcpId{ 0 };
		//bool			_bIsActive{ true };
		std::wstring    _channel;
		std::wstring    _operatorId;
		std::wstring    _operatorName;
		CHAT_STATUS     _status;
		std::wstring    _contact;
	};

public:

	using ChatInfoCollector = std::map<MCCID, CChatInfo>;
	using IdList = std::map<MCCID, SessionScriptId>;


	void Init(CSystemLog* log)
	{
		_log = log;
	}

	auto AddChatInfo(MCCID _mccid, ChatId cid, SessionScriptId ssid,
		SessionId sid,
		MCPId mid,
		std::wstring channel,
		time_t t = time(nullptr))
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (infoList.find(cid) != infoList.cend())
			throw (std::runtime_error((boost::format("ChatInfo with MCCID = %llu has already exist") % _mccid).str()));

		LogStringModule(LEVEL_FINEST, L"Add chat with MCCID == %llu", _mccid);

		return infoList.emplace(_mccid, CChatInfo(_mccid, cid, ssid, sid, mid, t, channel));
	}

	//const CChatInfo& get_info(const ChatId& _id)const
	//{
	//	std::lock_guard<std::mutex> lock(_mutex);
	//	auto cit = std::find_if(infoList.begin(), infoList.end(), [&_id](const auto& pair)
	//	{
	//		if (pair.second.getChatId() == _id)
	//			return true;
	//		return false;
	//	});

	//	if (cit == infoList.end())
	//	{
	//		throw (std::runtime_error((boost::format("Cannot find ChatInfo by ChatId = %s") % wtos(_id)).str()));
	//	}

	//	return cit->second;
	//}

	auto get_chatCount()const noexcept
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return infoList.size();
	}

	const CChatInfo get_info(ChatId cid)const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = std::find_if(infoList.begin(), infoList.end(), [&](const auto& pair)
		{
			if (pair.second.getChatId() == cid)
				return true;
			return false;
		});

		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by ChatId = %llu") % cid).str()));
		}

		return cit->second;
	}


	const CChatInfo get_info(MCCID _id)const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		return cit->second;
	}

	void set_info(MCCID _id, SessionScriptId ssid, SessionId sid, CHAT_STATUS newStatus)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setScriptId(ssid);
		cit->second.setSessionId(sid);
		cit->second.setStatus(newStatus);
	}

	template <class TStr>
	void set_operator(MCCID _id, TStr&& id, TStr && name)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}
		cit->second.setOperatorId(std::forward<TStr>(id));
		cit->second.setOperatorName(std::forward<TStr>(name));

	}

	void set_mcpid(MCCID _id, MCPId _mid)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setMCPId(_mid);
	}

	template <class TContact>
	void set_contact(MCCID _id, TContact&& contact)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setContact(std::forward<TContact>(contact));
	}

	void delete_info(MCCID _id)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		LogStringModule(LEVEL_FINEST, L"Delete chat with MCCID == %llu", _id);

		infoList.erase(cit);
	}

	//template <class TID>
	//bool ifExist(TID &&cid)
	//{
	//	std::lock_guard<std::mutex> lock(_mutex);
	//	return std::find_if(infoList.begin(), infoList.end(), [cid = std::forward<TID>(cid)](const auto& pair)
	//	{
	//		if (pair.second.getChatId() == cid)
	//			return true;
	//		return false;
	//	}) != infoList.end();
	//}

	template <class TID>
	bool ifExist(const TID &cid)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return std::find_if(infoList.begin(), infoList.end(), [cid](const auto& pair)
		{
			return pair.second.getChatId() == cid;
		}) != infoList.end();
	}


	auto isSessionActive(MCCID _id)const
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return get_info(_id).isSessionActive();
	}

	void update_status(MCCID _id, CHAT_STATUS newStatus)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setStatus(newStatus);

	}

	void update_timestamp(MCCID _id, time_t t = time(nullptr))
	{
		auto& cit = infoList.find(_id);
		if (cit == infoList.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		cit->second.setTimeStamp(t);

	}

	IdList getExpiredChatList(time_t curtime)
	{
		IdList expiredList;
		for each (const auto& pair in infoList)
		{
			if (pair.second.getTimeStamp() < curtime)
				expiredList[pair.first] = pair.second.getScriptId();
		}
		return expiredList;
	}


private:
	CSystemLog* _log;
	ChatInfoCollector infoList;

	mutable std::mutex _mutex;
};

class CMessageCollector : public singleton <CMessageCollector>
{
	friend class singleton <CMessageCollector>;

private:

	template<typename... TArgs>
	void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
	{
		_log->LogStringModule(level, L"Message collector", formatMessage, std::forward<TArgs>(args)...);
	}

	CMessageCollector() = default;

	template <class TMsg>
	void push_message(MCCID cid, TMsg&& message)
	{
		auto cit = _messages.find(cid);
		if (cit == _messages.end())
		{
			MessageList newList = { CMessage(std::forward<TMsg>(message)) };
			_messages.emplace(cid, std::move(newList));
		}
		else
		{
			cit->second.emplace_back(std::forward<TMsg>(message));
		}

	}
public:
	using MessageList = std::list<CMessage>;
	using ChatCollector = std::map<MCCID, MessageList>;

	void Init(CSystemLog* log)
	{
		_log = log;
	}


	//const MessageList& get_messages(MCCID _id)const
	//{
	//	auto cit = _messages.find(_id);
	//	if (cit == _messages.end())
	//	{
	//		throw (std::runtime_error((boost::format("Cannot find chat by MCCID = %llu") % _id).str()));
	//	}

	//	return cit->second;
	//}

	template <class TMsg>
	void push_to_mcp_message(MCCID mccid, TMsg&& message)
	{
		std::lock_guard<std::mutex> lock(_mutex);

		//toMCPList.emplace_back(message);
		push_message(mccid, std::forward<TMsg>(message));
	}

	template <class TMsg>
	void push_to_server_message(MCCID mccid, TMsg&& message)
	{
		std::lock_guard<std::mutex> lock(_mutex);

		toServerList.emplace_back(message);
		push_message(mccid, std::forward<TMsg>(message));
	}

	void deleteChat(MCCID _id)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = _messages.find(_id);
		if (cit == _messages.end())
		{
			throw (std::runtime_error((boost::format("Cannot find ChatInfo by MCCID = %llu") % _id).str()));
		}

		LogStringModule(LEVEL_FINEST, L"Delete chat with MCCID == %llu", _id);

		_messages.erase(cit);
	}

	bool ifExist(MCCID cid)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return _messages.find(cid) != _messages.end();
	}

	bool isEmpty() const 
	{ 
		return toServerList.empty(); 
	}

	MessageList&& get_serverListMessages() { return std::move(toServerList); }

	void SetIncomingMessageHandler(ChatTextMessageHandler handler) { _onMessageIncomingHandler = handler; }

	template <class TMsg, class TStatus>
	std::wstring Recieve(/*MCCID connectionId,*/ TMsg&& msg, TStatus&& status)
	{
		if (_onMessageIncomingHandler)
		{
			return _onMessageIncomingHandler(std::forward<TMsg>(msg), std::forward<TStatus>(status));
		}
		throw (std::runtime_error("Recieve failed - COLLECTOR INITIALIZATION REQUIRED"));
		return std::wstring();
	}

	template <class Fn, typename... TArgs>
	void ForEachMessage(MCCID _id, Fn& _Func, TArgs&&... args)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		auto cit = _messages.find(_id);
		if (cit == _messages.end())
		{
			throw (std::runtime_error((boost::format("Cannot find messages by MCCID = %llu") % _id).str()));
		}

		for (auto& msg : cit->second)
		{
			_Func(msg, std::forward<TArgs>(args)...);
		}
	}
private:
	ChatCollector _messages;
	CSystemLog* _log;

	//MessageList toMCPList;
	MessageList toServerList;

	ChatTextMessageHandler _onMessageIncomingHandler{ nullptr };

	mutable std::mutex _mutex;
};


/******************************* eof *************************************/