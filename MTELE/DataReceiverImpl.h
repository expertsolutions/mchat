/************************************************************************/
/* Name     : MTELE\DataReceiverImpl.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Jul 2016                                               */
/************************************************************************/
#pragma once

#include <map>
#include "Router\router_compatibility.h"
#include "telebase.h"
#include "DataReceiver.h"
#include "Bot.h"
#include "tgbot/types/Message.h"
#include "tgbot/types/GenericReply.h"
#include "MessageParser.h"

enum class MTELEErrors
{
	ERR_DELIVER = 1/*,
	ERR_UNKNOWN_ACTION,
	ERR_TRUNCKGROUP_DIDN_FOUND,
	ERR_TIMESLOT_DIDN_FOUND,
	ERR_NOT_ENOUGH_OUTCOMING_LINES*/
};

static std::wstring GetErrorNameByCode(const MTELEErrors & _errorCode)
{
	std::wstring sError = L"Unknown error";
	switch (_errorCode)
	{
	case MTELEErrors::ERR_DELIVER:
	{
		sError = L"ErrRouterDeliver";
		break;
	}
	/*case ERR_UNKNOWN_ACTION:
	{
		sError = L"ErrUnknownAction";
		break;
	}
	case ERR_TRUNCKGROUP_DIDN_FOUND:
	{
		sError = L"ErrTrunckGroupDidntFound";
		break;
	}
	case ERR_TIMESLOT_DIDN_FOUND:
	{
		sError = L"ErrTimeSlotsDidntFound";
		break;
	}
	case ERR_NOT_ENOUGH_OUTCOMING_LINES:
	{
		sError = L"ErrNotEnoughOutcomingLines";
		break;
	}*/
	}

	return sError;
}



using CallBackBotFunc = std::function <void(int64_t chatId, std::string, const TgBot::GenericReply::Ptr&)>;

class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{
	friend class CDataReceiver < CDataReceiverImpl >;

	//using ContentId = std::string;
	//using ContentSource = std::string;
	//using ContentList = std::map < ContentId, ContentSource>;

	//struct CPreMessage
	//{
	//	ContentList _listAttachments;
	//	CMessage _message;
	//	unsigned int _downloadCountDown = 0;
	//};

	using MessageCollector = std::list<CMessage>;


public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();

	void TestReplyEventHandler()
	{
		CLIENT_ADDRESS _from;
		_from.SetAsQWORD((__int64)0x00063C40 << 32);
		CLIENT_ADDRESS _to;
		_to.SetAsQWORD((__int64)0x00055C40 << 32);
		CMessage msg(L"MCP2ICCP/OAM_replyToMessage");
		msg[L"data"] = L"{\"channelName\":\"Tlg\",\"comp\":\"ms-d60409-01\",\"channelType\":\"16\",\"channelId\":\"Tlg\",\"charset\":\"null\",\"citrix\":\"MS-CTXACRM001\",\"agent\":\"75931\",\"message\":\"dgsdfgsdfgdfg\n\",\"sessionId\":\"1153695750\",\"messageId\":\"ec9f25db30f1452aad7b\",\"user\":\"apolitov\",\"agentDetails\":{\"agentSkills\":\"\",\"agentId\":\"2147515285\",\"agentProfile\":\"29772\",\"agentLoginName\":\"APolitov\"},\"messageSender\":\"MMBeeBot\",\"messageDate\":\"2016-10-07 18:28:52.399\"}";
		msg[L"requestId"] = L"7b42d259f73f4735864550058797dada";
		
		MCP2MCC_ReplyEventHandler(msg, _from, _to);
	}

private:
	int init_impl(CSystemLog* pLog);
	int start();

	//void SendMessageToTele(ChatId id, std::string text)const;
	//void SendMessageToTele(ChatId id, std::string text, const TgBot::GenericReply::Ptr& object)const;

	//CMessage MakeMCP2ICCP_OAM_from_MCC2TA_NEWTELE(CMessage msg);
	//CMessage ConvertToJavaFormat(std::wstring newMsgName, CMessage msg);
	//CMessage ConvertFromJavaFormat(std::wstring newMsgName, CMessage msg);
	void _sendBeginSession(MCCID mccid, int32_t date, MessageId msgId,  std::wstring contact, std::wstring split, std::wstring ipAddress)const;
	void _sendReceiveMessage(MCCID mccid, MCPId mid, const CMessage& msg)const;
	void _sendReceiveMessageRequestAck(MCCID mccid, SessionId sid, MCPId mid, std::wstring requestId, std::wstring ChannelId)const;
	void _sendSessionTimeout(MCCID mccid, SessionScriptId ssid)const;
	void _sendReplyToMessageAck(MCCID mccid, SessionScriptId ssid)const;
	void _sendReplyToMessageAck(MCCID mccid, SessionScriptId ssid, std::wstring requestId)const;
	void _sendTeleMessageToServer(MCCID aMccid, CMessage&& aMsg);

	void _makeChatTimeout(MCCID mccid, SessionScriptId ssid)const;
	std::wstring _makeOKRequest()const;
	//CMessage _makeMCPMessage(MCCID mccid, SessionId sid, std::wstring content, std::wstring contentType, std::wstring ChannelId, std::wstring AttachStr, int messageId)const;

	CMessage _makeMCPMessage(
		MCCID mccid, 
		SessionId sid, 
		std::wstring content, 
		std::wstring contentType, 
		std::wstring ChannelId, 
		MessageId messageId, 
		MessageList && attachments) const;

private:
	//router api
	void on_routerConnect();
	void on_routerDeliverError(const CMessage&, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);
	void on_appStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	std::wstring get_ModuleName() const noexcept { return L"MTELE"; }
	void stop();
	//void save_LastUpdate(int)const;
	//int get_LastUpdate()const;

	//void OnTimerExpired();

	bool makeGarbageCleaner(int expiredTime);
	//void makeMyServer(TConnectionServerParams params);
	void makeMyServer(TConnectionParams params);
	//std::wstring loadDocument(MCCID mccid, const std::wstring& filename, std::wstring&& fileid);
	std::wstring getDocumentLink(std::wstring&& fileid);

	//void ApplyUpdates();
	//void SendResponse(const TgBot::Api& api, TConnectionParams);
	//void CompleteExpiredChats(time_t curtime = time(nullptr))const;

private:
	//void onReplyTemplate(std::wstring inMsgName, 
	//	std::wstring outMsgName, 
	//	CMessage _msg, 
	//	const CLIENT_ADDRESS& _from, 
	//	const CLIENT_ADDRESS& _to);

	// handlers
	void S2MCC_ReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void MCP2MCC_ReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void SM2MCC_BeginSessionAckHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void SM2MCC_SessionDeletedHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void MCP2MCC_ReceiveMessageRequestHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void S2MCC_SessionUpdateHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);
	void S2MCC_SessionTransferHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);

	//void onChatTextMessage(TgBot::Message::Ptr message);
	//void ReplyMessageToMCP(ChatId cid, SessionScriptId ssid, SessionId sid, CMessage message);

	//void onChatTimeOut(ChatId cid, SessionScriptId sid)const;
	//void onReplyResponse(const CMessage& message);

	void tele_UpdateHandler(TgBot::Message::Ptr message);
	std::wstring onChatMessage(CMessage message, const TMessageStatus& /*status*/);
	//void loadDocument(MessageId);
	//void loadPhoto();

	//uint32_t get_ClientId()const
	//{
	//	return m_router.BoundAddress();
	//}

	//template <class TMessage>
	//void sendToAny(TMessage&& message)const
	//{
	//	m_router.SendToAny(std::forward<TMessage>(message));
	//}

	//template <class TMessage>
	//void sendToAll(TMessage&& message)const
	//{
	//	m_router.SendToAll(std::forward<TMessage>(message));
	//}

	//template <class TMessage, class TAddress>
	//void sendTo(TMessage&& message, TAddress&& address)const
	//{
	//	m_router.SendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
	//}


public:
	//handler
	//void receive_onAccepted(MessagePtr message)const;
	//void receive_onCompleted(const std::wstring& _email, MessagePtr message)const;
	//void receive_onFailed(const std::string& message)const;

	//void send_onAccepted(const CMessage& msg)const;
	//void send_onCompleted(const CMessage& msg)const;
	//void send_onFailed(const CMessage& msg)const;

private:
	//void _addContentToDownload(MessageId msgId);


private:
	CSystemLog * m_log;

	boost::asio::io_service& r_io;

	//MessageCollector m_incomingMsgList;

	//CallBackBotFunc _callBack = nullptr;
	int _lastUpdateId = 0;
	//int _iChatExpiredTime = 0;
	std::wstring _botname;

	mutable std::mutex _mutex;

	//MessageCollector _msgListToTele;
	//MessageCollector _msgListToMCP;
	//using MessageContainer = std::map<MessageId, CPreMessage>;

	//MessageContainer _messages;
	std::wstring _sAttachmentFolder;
	std::wstring _sAttachmentString;
	std::string _sClientHost;
	std::string _sClientPort;
	std::string _sToken;

	std::wstring _sLoadAttachmentCommand;

};


/******************************* eof *************************************/