/************************************************************************/
/* Name     : MTELE\DataReceiverImplHandler.cpp                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Jul 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <cctype>
#include "Patterns/time_functions.h"
//#include "AttachmentsHelper.h"
#include "DataReceiverImpl.h"
#include "tgbot/net/httpparser.h"
#include "tgbot/tgtypeparser.h"
#include "MessageCollector.h"
#include "BotUtils.h"
#include "MessageParser.h"
#include "Utils.h"
#include "attachment/AttachmentsApi.h"
#include "IdGenerator.h"
#include "codec.h"

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

static MCCID CreateMCCID(ChatId aChatId, uint32_t aClientId)
{
	MCCID mccid = aClientId;
	mccid = (mccid << 32) | static_cast<uint32_t>(aChatId);
	return mccid;
}

/************************************************************************/
/*************************   FROM MCP  **********************************/
/************************************************************************/

/// S2MCC_Reply

void CDataReceiverImpl::S2MCC_ReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
{
	//onReplyTemplate(L"S2TELE_Reply", L"S2TELE_ReplyAck", std::move(__msg), _from, _to);
	std::wstring sText = _msg.SafeReadParam(L"Message", CMessage::CheckedType::String, L"").AsWideStr();

	if (sText.empty())
		return;

	MCCID mccid = _to.AsMessageParamterInt();
	singleton_auto_pointer<CChatInfoCollector> infoCollector;

	decltype(auto) info = infoCollector->get_info(mccid);
	infoCollector->update_timestamp(mccid);

	CMessage msg(L"TELEMESSAGE_Reply");
	msg[L"id"] = info.getChatId();
	msg[L"contentType"] = L"TEXT";
	msg[L"content"] = stow(to_utf8(sText));
	//msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_AUTOREPLY);
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());

	std::lock_guard<std::mutex> lock(_mutex);
	r_io.post([msg = std::move(msg), mccid = mccid, pThis = this]()
	{
		singleton_auto_pointer<CMessageCollector> collector;
		collector->push_to_server_message(mccid, std::move(msg));
	});

	_sendReplyToMessageAck(mccid, info.getScriptId());

}

///MCP2MCC_Reply

void CDataReceiverImpl::MCP2MCC_ReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
{
	std::wstring requestId = _msg.SafeReadParam(L"requestId", core::checked_type::String, L"").AsWideStr();

	MCCID mccid = _to.AsMessageParamterInt();
	singleton_auto_pointer<CChatInfoCollector> infoCollector;

	decltype(auto) info = infoCollector->get_info(mccid);

	if (info.getStatus() != CHAT_STATUS::CS_ENQUEUED)
	{
		infoCollector->update_timestamp(mccid);

		std::wstring request = _msg.SafeReadParam(L"data", CMessage::CheckedType::String, L"").AsWideStr();
		CMessage reqMsg = CMessageParser::ToMessage(request);

		std::wstring message = reqMsg.SafeReadParam(L"message", core::checked_type::String, L"").AsWideStr();
		std::wstring messageId = reqMsg.SafeReadParam(L"messageId", core::checked_type::String, L"").AsWideStr();

		auto attachmentList = attachments::extractAttachments(reqMsg);

		MessageList msgList;

		if (!attachmentList.empty())
		{
			for (const auto& param : attachmentList)
			{
				//std::wstring content = stow(encodeBase64(std::move(attachments::readAttachment(param.Url, param.SourceName, _sAttachmentString, _sAttachmentFolder))));

				CMessage msg(L"TELEMESSAGE_Reply");
				msg[L"id"] = info.getChatId();
				msg[L"contentType"] = L"FILE";
				//msg[L"content"] = message;
				msg[L"fileUrl"] = param.Url;
				msg[L"fileSize"] = param.FileSize;
				msg[L"fileName"] = param.SourceName;
				msg[L"messageId"] = messageId;
				msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_OPERATOR_CHATSESSION);
				msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));

				msgList.emplace_back(msg);
			}
		}
		else
		{
			CMessage msg(L"TELEMESSAGE_Reply");
			msg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::RECIEVED);
			msg[L"id"] = info.getChatId();
			msg[L"contentType"] = L"TEXT";
			//msg[L"content"] = stow(to_utf8(message));

			//boost::replace_all(message, L"\\", L"\\\\");
			//boost::replace_all(message, L"\"", L"\\\"");
			//boost::replace_all(message, L"\n", L"\\n");
			//boost::replace_all(message, L"\r", L"\\r");

			msg[L"content"] = message;
			msg[L"messageId"] = messageId;
			msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_OPERATOR_CHATSESSION);
			msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));

			msgList.emplace_back(msg);
		}

		std::lock_guard<std::mutex> lock(_mutex);
		r_io.post([msgList = std::move(msgList), mccid = mccid, pThis = this]()
		{
			singleton_auto_pointer<CMessageCollector> collector;
			for (const auto& msg : msgList)
			{
				collector->push_to_server_message(mccid, std::move(msg));
			}
		});
	}

	_sendReplyToMessageAck(mccid, info.getMCPId(), requestId);







	////onReplyTemplate(L"ReplyToMessage", L"MCP2ICCP/OAM_replyToMessageAck", std::move(__msg), _from, _to);

	//std::wstring requestId = _msg.SafeReadParam(L"requestId", core::checked_type::String, L"").AsWideStr();

	//MCCID mccid = _to.AsMessageParamterInt();
	//singleton_auto_pointer<CChatInfoCollector> infoCollector;

	//decltype(auto) info = infoCollector->get_info(mccid);
	//infoCollector->update_timestamp(mccid);

	//CMessage jsonMsg;

	////auto attachmentList = attachments::extractAttachments(_msg, jsonMsg);

	//MessageList msgList;

	////if (!attachmentList.empty())
	////{
	////	for (const auto& param : attachmentList)
	////	{
	////		std::wstring content = stow(attachments::readAttachment(param.Url, param.SourceName, _sAttachmentString, _sAttachmentFolder));

	////		//CMessage msg(L"CHATMESSAGE");
	////		//msg[L"id"] = info.getChatId();
	////		//msg[L"contentType"] = L"FILE";
	////		//msg[L"content"] = content;
	////		//msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_OPERATOR_CHATSESSION);
	////		//msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());

	////		//msgList.emplace_back(msg);
	////	}
	////}

	//CMessage msg(L"TELEMESSAGE_Reply");
	//msg[L"id"] = info.getChatId();
	//msg[L"contentType"] = L"TEXT";
	//msg[L"content"] = jsonMsg[L"message"];
	////msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_AUTOREPLY);
	//msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());//std::chrono::system_clock::now();

	//msgList.emplace_back(msg);

	//std::lock_guard<std::mutex> lock(_mutex);
	//r_io.post([msgList = std::move(msgList), mccid = mccid, pThis = this]()
	//{
	//	singleton_auto_pointer<CMessageCollector> collector;
	//	for (auto & msg : msgList)
	//		collector->push_to_server_message(mccid, std::move(msg));
	//});

	//_sendReplyToMessageAck(mccid, info.getMCPId(), requestId);

}

/// SM2MCC_BeginSessionAck

void CDataReceiverImpl::SM2MCC_BeginSessionAckHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	bool   bSessionExist = _msg.SafeReadParam(L"SessionExists", CMessage::CheckedType::Int, 0).AsInt() ? true : false;
	SessionScriptId ssid = _msg.SafeReadParam(L"SessionScriptId", CMessage::CheckedType::Int64, 0).AsInt64();
	SessionId        sid = _msg.SafeReadParam(L"SessionId", CMessage::CheckedType::Int64, 0).AsInt64();
	__int64   iErrorCode = _msg.SafeReadParam(L"ErrorCode", CMessage::CheckedType::Int64, 0).AsInt64();

	MCCID mccid = _to.AsMessageParamterInt();
	singleton_auto_pointer<CChatInfoCollector> infoCollector;

	/// test123
	ChatId cid = infoCollector->get_info(mccid).getChatId();

	m_log->LogString(LEVEL_FINEST, L"\n");
	m_log->LogString(LEVEL_FINEST, L"!!!!!!!!!!!!!");
	m_log->LogString(LEVEL_FINEST, L"MCCID: %i", mccid);
	m_log->LogString(LEVEL_FINEST, L"ChatId: %i", cid);
	m_log->LogString(LEVEL_FINEST, L"SessionId: %i", sid);
	m_log->LogString(LEVEL_FINEST, L"SessionScriptId: %i", ssid);
	m_log->LogString(LEVEL_FINEST, L"!!!!!!!!!!!!!\n");
	// end test123

	if (!iErrorCode && !bSessionExist)
	{
		infoCollector->set_info(mccid, ssid, sid, CHAT_STATUS::CS_ENQUEUED);
		return;
	}

	std::wstring sErrorDescription;
	if (iErrorCode)
	{
		sErrorDescription = _msg.SafeReadParam(L"ErrorDescription", CMessage::CheckedType::String, L"Unknown error").AsWideStr();
	}
	else if (bSessionExist)
	{
		sErrorDescription = L"Your session is duplicated. Please wait for a while and then try again";
	}

	auto info = infoCollector->get_info(mccid);

	CMessage msg(L"TELEMESSAGE_Reply");
	msg[L"id"] = info.getChatId();
	msg[L"contentType"] = L"TEXT";
	msg[L"content"] = sErrorDescription;
	msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_SESSION_CLOSED_BY_OPERATOR);
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));

	infoCollector->delete_info(mccid);

	std::lock_guard<std::mutex> lock(_mutex);
	r_io.post([msg = std::move(msg), mccid = mccid, pThis = this]()
	{
		singleton_auto_pointer<CMessageCollector> collector;
		collector->push_to_server_message(mccid, std::move(msg));

		collector->deleteChat(mccid);
	});
}

/// SM2MCC_SessionDeleted

void CDataReceiverImpl::SM2MCC_SessionDeletedHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	//m_log->LogString(LEVEL_FINEST, L"onSessionDeletedHandler: %s", _msg.Dump().c_str());

	MCCID mccid = _to.AsMessageParamterInt();
	std::wstring sReason = _msg.SafeReadParam(L"Reason", CMessage::CheckedType::String, L"").AsWideStr();
	singleton_auto_pointer<CChatInfoCollector> infoCollector;

	if (sReason.empty())
		sReason = L"Session has deleted by unknown reason";
	else
		sReason = stow(to_utf8(sReason));

	ChatId cid;

	if (true)
	{
		decltype(auto) info = infoCollector->get_info(mccid);
		cid = info.getChatId();
	}

	infoCollector->delete_info(mccid);

	CMessage msg(L"TELEMESSAGE_Reply");
	msg[L"content"] = sReason; //L"Session was ended by script";
	msg[L"contentType"] = L"TEXT";
	msg[L"id"] = cid;
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());

	std::lock_guard<std::mutex> lock(_mutex);
	r_io.post([msg = std::move(msg), mccid = mccid, pThis = this]()
	{
		singleton_auto_pointer<CMessageCollector> collector;
		collector->push_to_server_message(mccid, std::move(msg));

		collector->deleteChat(mccid);
	});
}

/// MCP2MCC_ReceiveMessageRequest

void CDataReceiverImpl::MCP2MCC_ReceiveMessageRequestHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	MCCID mccid = _to.AsMessageParamterInt();
	MCPId mid = _from.AsMessageParamterInt();

	_msg.CheckParam(L"data", CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
	std::wstring data = _msg.SafeReadParam(L"data", CMessage::CheckedType::String, L"").AsWideStr();
	std::wstring requestId = _msg.SafeReadParam(L"requestId", CMessage::CheckedType::String, L"").AsWideStr();

	if (true)
	{
		CMessage jsondata = CMessageParser::ToMessage(data);

		std::wstring agentId = jsondata.SafeReadParam(L"agentId", CMessage::CheckedType::String, L"").AsWideStr();
		std::wstring agentName = jsondata.SafeReadParam(L"agentLoginName", CMessage::CheckedType::String, L"").AsWideStr();

		singleton_auto_pointer<CChatInfoCollector> infoCollector;
		infoCollector->set_operator(mccid, std::move(agentId), std::move(agentName));
	}

	singleton_auto_pointer<CChatInfoCollector> infoCollector;
	infoCollector->set_mcpid(mccid, mid);
	infoCollector->update_status(mccid, CHAT_STATUS::CS_STARTED);

	decltype(auto) info = infoCollector->get_info(mccid);

	_sendReceiveMessageRequestAck(mccid, info.getSessionId(), mid, std::move(requestId), info.getContact());
}

/// S2MCC_SessionUpdate

void CDataReceiverImpl::S2MCC_SessionUpdateHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	int iEWT = utils::toNum<int, wchar_t>(_msg.SafeReadParam(L"EWT", CMessage::CheckedType::String, L"1").AsWideStr());
	//MCCID cid = _to.GetChildID();
	MCCID mccid = _to.AsMessageParamterInt();

	singleton_auto_pointer<CChatInfoCollector> infoCollector;
	decltype(auto) info = infoCollector->get_info(mccid);

	//CMessage msg(L"SESSION_STATUS");
	//msg[L"EWT"] = iEWT;
	//msg[L"status"] = info.getStatusString();
	//msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());
	//msg[L"id"] = info.getChatId();

	//std::lock_guard<std::mutex> lock(_mutex);
	//r_io.post([msg = std::move(msg), cid = cid, pThis = this]()
	//{
	//	singleton_auto_pointer<CMessageCollector> collector;
	//	collector->push_to_server_message(cid, std::move(msg));
	//});

	CMessage msg(L"TELEMESSAGE_Reply");
	msg[L"content"] = (boost::wformat(L"Status: \"%s\". Operator estimated waiting time: %i minutes") % info.getStatusString().c_str() % iEWT).str();
	msg[L"contentType"] = L"TEXT";
	msg[L"id"] = info.getChatId();
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());

	_sendTeleMessageToServer(mccid, std::move(msg));
}

void CDataReceiverImpl::S2MCC_SessionTransferHandler(CMessage _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & _to)
{
	int iEWT = utils::toNum<int, wchar_t>(_msg.SafeReadParam(L"EWT", CMessage::CheckedType::String, L"1").AsWideStr());
	std::wstring message = _msg.SafeReadParam(L"Message", CMessage::CheckedType::String, L"").AsWideStr();
	MCCID mccid = _to.AsMessageParamterInt();

	singleton_auto_pointer<CMessageCollector> collector;
	singleton_auto_pointer<CChatInfoCollector> infoCollector;

	decltype(auto) info = infoCollector->get_info(mccid);

	if (!message.empty())
	{
		CMessage msg(L"TRANSFER");
		msg[L"cid"] = info.getChatId();
		msg[L"messageId"] = CIdGenerator::GetInstance()->makeSId();
		msg[L"contentType"] = L"TEXT";
		msg[L"content"] = _msg[L"Message"];
		msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_AUTOREPLY);
		msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
		msg[L"messageStatus"] = static_cast<int>(MESSAGE_STATUS::NEW);

		m_log->LogStringModule(LEVEL_FINEST, L"[TEST]", L"Push Transfer: %s", msg.Dump().c_str());
		collector->push_to_mcp_message(mccid, std::move(msg));
	}

	infoCollector->update_status(mccid, CHAT_STATUS::CS_ENQUEUED);

	CMessage msg(L"TELEMESSAGE_Reply");
	msg[L"content"] = (boost::wformat(L"Status: \"%s\". Operator estimated waiting time: %i minutes") % info.getStatusString().c_str() % iEWT).str();
	msg[L"contentType"] = L"TEXT";
	msg[L"id"] = info.getChatId();
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());

	_sendTeleMessageToServer(mccid, std::move(msg));

	//CMessage msg(L"SESSION_STATUS");
	//msg[L"waitTime"] = iEWT;
	//msg[L"status"] = static_cast<int>(CHAT_STATUS::CS_ENQUEUED);
	//msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));
	//msg[L"cid"] = info.getChatId();

	//std::lock_guard<std::mutex> lock(_mutex);
	//r_io.post([msg = std::move(msg), mccid = mccid, pThis = this]()
	//{
	//	singleton_auto_pointer<CMessageCollector> collector;
	//	collector->push_to_server_message(mccid, std::move(msg));
	//});
}

/************************************************************************/
/***************************   FROM TELE   ******************************/
/************************************************************************/

/// Update handler

void CDataReceiverImpl::tele_UpdateHandler(TgBot::Message::Ptr message)
{
	m_log->LogString(LEVEL_FINEST, L"DUMP: %s", message->Dump());

	ChatId cid = message->chat->id;

	CMessage msg(L"TELEMESSAGE");
	msg[L"id"] = cid;
	msg[L"contentType"] = L"TEXT";
	msg[L"content"] = from_utf8(message->text);
	msg[L"messageId"] = message->messageId;
	msg[L"timestamp"] = core::time_type::clock::from_time_t(message->date);

	CMessage reply(L"TELEMESSAGE_Reply");
	reply[L"id"] = cid;
	reply[L"contentType"] = L"TEXT";
	reply[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());


	singleton_auto_pointer<CMessageCollector> collector;
	singleton_auto_pointer<CChatInfoCollector> infoCollector;

	if (!infoCollector->ifExist(cid))
	{
		TgBot::ReplyKeyboardMarkup::Ptr rpk = std::make_shared<TgBot::ReplyKeyboardMarkup>();
		rpk->keyboard.push_back({ TgBot::KeyboardButton("PUSH HERE TO SUBMIT", true) });
		rpk->oneTimeKeyboard = true;
		rpk->resizeKeyboard = true;

		//SendMessageToTele(cid, "Please submit your phone number to continue", rpk);

		MCCID mccid = CreateMCCID(static_cast<int>(cid), GetClientId());

		infoCollector->AddChatInfo(mccid, cid, SessionScriptId(0), SessionId(0), MCPId(0), L"Tlg");
		collector->push_to_mcp_message(mccid,msg);

		reply[L"content"] = L"Please submit your phone number to continue";
		reply[L"makeButton"] = true;
		collector->push_to_server_message(mccid, std::move(reply));

		return;
	}

	decltype(auto) info = infoCollector->get_info(cid);

	MCCID          mccid = info.getMCCID();
	//SessionScriptId ssid = info.getScriptId();
	//SessionId		 sid = info.getSessionId();
	MCPId		     mid = info.getMCPId();
	//bool bSessionStarted = info.isSessionStarted();
	auto sessionStatus   = info.getStatus();

	//m_log->LogString(LEVEL_FINEST, L"Last Message: Name = %s, SessionScriptId = %llu, SessionId = %llu, MCPId = %llu", lastMessage.getMessageName().c_str(), lastMessage.getScriptId(), lastMessage.getSessionId(), lastMessage.getMCPId());

	//if (!bSessionStarted)
	if (sessionStatus == CHAT_STATUS::CS_CREATED)
	{
		if (!message->contact || message->contact->phoneNumber.empty())
		{
			//SendMessageToTele(cid, "Please submit your phone number to continue");
			reply[L"content"] = L"Please submit your phone number to continue";
			collector->push_to_server_message(mccid, std::move(reply));
		}
		else
		{
			std::wstring contact = std::move(stow(message->contact->phoneNumber));
			infoCollector->set_contact(mccid, contact);

			MessageId message_id = utils::toStr<wchar_t>(message->messageId);

			_sendBeginSession(mccid,
				message->date,
				message_id,
				std::move(contact),
				L"", // split
				L"" // ipAddress
				);

			

			//SendMessageToTele(cid, "Session starting...");
			reply[L"content"] = L"Session starting...";
			collector->push_to_server_message(mccid, std::move(reply));


			////test123
			//CLIENT_ADDRESS _from;
			//_from.SetAsQWORD((__int64)0x00063C40 << 32);
			//CLIENT_ADDRESS _to;
			//_to.SetAsQWORD((__int64)mccid);
			//CMessage msg(L"MCP2ICCP/OAM_replyToMessage");
			////msg[L"data"] = L"{\"channelName\":\"Tlg\",\"comp\":\"ms-d60409-01\",\"channelType\":\"16\",\"channelId\":\"Tlg\",\"charset\":\"null\",\"citrix\":\"MS-CTXACRM001\",\"agent\":\"75931\",\"message\":\"dgsdfgsdfgdfg\",\"sessionId\":\"1153695750\",\"messageId\":\"ec9f25db30f1452aad7b\",\"user\":\"apolitov\",\"agentDetails\":{\"agentSkills\":\"\",\"agentId\":\"2147515285\",\"agentProfile\":\"29772\",\"agentLoginName\":\"APolitov\"},\"messageSender\":\"MMBeeBot\",\"messageDate\":\"2016-10-07 18:28:52.399\"}";
			////msg[L"data"] = L"{\"channelName\":\"Tlg\",\"comp\":\"shrru-a10500000\",\"channelType\":\"16\",\"channelId\":\"Tlg\",\"citrix\":\"okarpukhin\",\"charset\":\"null\",\"agent\":\"75931\",\"message\":\"������\",\"sessionId\":\"1168375821\",\"attachments\":[{\"name\":\"test_����_0.txt\",\"type\":\"1\",\"url\":\"http://tv-ocm002/Attachments/844501642796363861/outbox/d4f59f5fa995430da401a23623ee8e8e\",\"size\":\"6\"}],\"messageId\":\"3afc22e7371c4319a048\",\"user\":\"okarpukhin\",\"agentDetails\":{\"agentId\":\"test\",\"agentProfile\":\"11\",\"agentLoginName\":\"okarpukhin\"},\"messageSender\":\"MCC_TELE\",\"messageDate\":\"2016-10-28 15:17:34.678\"}";
			////msg[L"data"] = L"{\"channelName\":\"Tlg\", \"comp\" : \"shrru-a10500000\", \"channelType\" : \"16\", \"channelId\" : \"Tlg\", \"citrix\" : \"okarpukhin\", \"charset\" : \"null\", \"agent\" : \"75931\", \"message\" : \"123\", \"sessionId\" : \"1168375822\", \"attachments\" : [{\"name\":\"test_����_0.txt\", \"type\" : \"1\", \"url\" : \"http://tv-ocm002/Attachments/844501651315825434/outbox/ded5955bee744fe089c9c6dcd3bc636a\", \"size\" : \"6\"}], \"messageId\" : \"2cc25ff86b1a4290b3e8\", \"user\" : \"okarpukhin\", \"agentDetails\" : {\"agentId\":\"test\", \"agentProfile\" : \"11\", \"agentLoginName\" : \"okarpukhin\"}, \"messageSender\" : \"MCC_TELE\", \"messageDate\" : \"2016-10-28 16:07:43.533\"}";

			//msg[L"data"] = L"\"{\"channelName\":\"SITE\",\"comp\":\"ms-d140715-89\",\"channelType\":\"14\",\"channelId\":\"SITE\",\"citrix\":\"MS-CTXACRM001\",\"charset\":\"null\",\"agent\":\"75895\",\"message\":\"��\n\",\"sessionId\":\"44171286\",\"attachments\":[{\"name\":\"1 - Copy.txt\",\"type\":\"1\",\"url\":\"http://ms-mult001/Attachments/143639924583867/outbox/6e491d45d35f47f79786ec40535ac152\",\"size\":\"2\"}],\"messageId\":\"114d8e0488234b7a896b\",\"user\":\"itolstoy\",\"agentDetails\":{\"agentSkills\":\"\",\"agentId\":\"2147492581\",\"agentProfile\":\"29772\",\"agentLoginName\":\"ITolstoy\"},\"messageSender\":\"MCC_CHAT\",\"messageDate\":\"2016-11-16 17:36:38.674\"}";
			//msg[L"requestId"] = L"7b42d259f73f4735864550058797dada";

			//MCP2MCC_ReplyEventHandler(msg, _from, _to);



		}
	}
	//else
	//{
	//	_sendReceiveMessage(mccid, mid, msg);
	//}

	if (sessionStatus == CHAT_STATUS::CS_STARTED || sessionStatus == CHAT_STATUS::CS_ENQUEUED)
	{
		collector->push_to_mcp_message(mccid, std::move(msg));
	}

	if (sessionStatus == CHAT_STATUS::CS_STARTED)
	{
		_sendReceiveMessage(mccid, mid, msg);
	}
}

/// Webhook handler

std::wstring CDataReceiverImpl::onChatMessage(CMessage _msg, const TMessageStatus& /*status*/)
{
	m_log->LogString(LEVEL_FINEST, L"DUMP: %s", _msg.Dump());

	try
	{
		_msg.CheckParam(L"message", CMessage::CheckedType::RawData, CMessage::ParamType::Mandatory);
	}
	catch (const core::message_parameter_exception& /*_exception*/) 
	{
		m_log->LogString(LEVEL_WARNING, L"Message param is mandatory, skip");
		return _makeOKRequest();
	}

	CMessage message = _msg[L"message"].AsMessage();
	m_log->LogString(LEVEL_FINEST, L"Message DUMP: %s", message.Dump());

	MessageId message_id = utils::toNum<MessageId, wchar_t>(message.SafeReadParam(L"message_id", CMessage::CheckedType::String, L"0").AsWideStr());
	std::wstring text = message.SafeReadParam(L"text", CMessage::CheckedType::String, L"").AsWideStr();

	CMessage chat = message[L"chat"].AsMessage();
	//m_log->LogString(LEVEL_FINEST, L"chat: %s", chat.Dump());

	ChatId cid = utils::toNum<ChatId, wchar_t>(chat.SafeReadParam(L"id", CMessage::CheckedType::String, L"0").AsWideStr());

	int date = utils::toNum<int, wchar_t>(message.SafeReadParam(L"date", CMessage::CheckedType::String, L"0").AsWideStr());

	singleton_auto_pointer<CMessageCollector> collector;
	singleton_auto_pointer<CChatInfoCollector> infoCollector;

	bool bExist = infoCollector->ifExist(cid);
	MCCID mccid = 0;
	MCPId mid = 0;
	SessionScriptId ssid = 0;
	SessionId		 sid = 0;
	auto sessionStatus = CHAT_STATUS::CS_CREATED;
	std::wstring channelId;

	if (!bExist)
	{
		mccid = CreateMCCID(static_cast<uint32_t>(cid), GetClientId()); //18446744072593594040
		//test123
		int64_t testId = static_cast<int64_t>(mccid);
		if (testId < 0)
		{
			int test = 0;
		}
	}
	else
	{
		decltype(auto) info = infoCollector->get_info(cid);
		mccid = info.getMCCID();
		ssid = info.getScriptId();
		sid = info.getSessionId();
		mid = info.getMCPId();
		sessionStatus = info.getStatus();
		infoCollector->update_timestamp(mccid);
		channelId = info.getContact();
	}

	m_log->LogString(LEVEL_INFO, L"mccid: %llu", mccid);
	m_log->LogString(LEVEL_INFO, L"mccid: %s", std::to_wstring(mccid));

	CMessage chatMsg(L"TELEMESSAGE");
	chatMsg[L"id"] = cid;

	CMessageCollector::MessageList attachmentsMsgs;
	if (message.HasParam(L"document") && message[L"document"].IsRawData())
	{
		CMessage document = message[L"document"].AsMessage();

		m_log->LogString(LEVEL_FINEST, L"DUMP: %s", document.Dump());

		std::wstring fileName = document.SafeReadParam(L"file_name", CMessage::CheckedType::String, L"").AsWideStr();
		int fileSize = utils::toNum<int, wchar_t>(document.SafeReadParam(L"file_size", CMessage::CheckedType::String, L"0").AsWideStr());
		std::wstring fileId = document.SafeReadParam(L"file_id", CMessage::CheckedType::String, L"").AsWideStr();

		text = L"File in attachment";
		chatMsg[L"contentType"] = L"FILE";

		std::wstring url = getDocumentLink(std::move(fileId));

		CMessage attachMessage;
		attachMessage[L"name"] = fileName;
		attachMessage[L"size"] = fileSize;
		attachMessage[L"url"] = url;

		attachmentsMsgs.emplace_back(std::move(attachMessage));
	}
	else
	{
		chatMsg[L"contentType"] = L"TEXT";
	}
	
	chatMsg[L"content"] = text;//from_utf8(text);
	chatMsg[L"messageId"] = message_id;
	chatMsg[L"timestamp"] = core::time_type::clock::from_time_t(date);

	CMessage reply(L"TELEMESSAGE_Reply");
	reply[L"id"] = cid;
	reply[L"contentType"] = L"TEXT";
	reply[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());


	if (!bExist)
	{
		TgBot::ReplyKeyboardMarkup::Ptr rpk = std::make_shared<TgBot::ReplyKeyboardMarkup>();
		rpk->keyboard.push_back({ TgBot::KeyboardButton("PUSH HERE TO SUBMIT", true) });
		rpk->oneTimeKeyboard = true;
		rpk->resizeKeyboard = true;

		infoCollector->AddChatInfo(mccid, cid, SessionScriptId(0), SessionId(0), MCPId(0), L"Tlg");

		collector->push_to_mcp_message(mccid, std::move(chatMsg));

		reply[L"content"] = L"Please submit your phone number to continue";
		reply[L"makeButton"] = true;
		collector->push_to_server_message(mccid, std::move(reply));

		return _makeOKRequest();
	}

	if (sessionStatus == CHAT_STATUS::CS_CREATED)
	{
		if (!message.HasParam(L"contact"))
		{
			//SendMessageToTele(cid, "Please submit your phone number to continue");
			reply[L"content"] = L"Please submit your phone number to continue";
			reply[L"makeButton"] = true;
			collector->push_to_server_message(mccid, std::move(reply));
		}
		else
		{
			CMessage contact = message[L"contact"].AsMessage();
			m_log->LogString(LEVEL_FINEST, L"contact: %s", contact.Dump());

			std::wstring phoneNumber = contact.SafeReadParam(L"phone_number", CMessage::CheckedType::String, L"").AsWideStr();

			infoCollector->set_contact(mccid, phoneNumber);

			_sendBeginSession(mccid,
				date,
				message_id,
				std::move(phoneNumber),
				L"",// split
				L"" //ipAddress
				);

			//SendMessageToTele(cid, "Session starting...");
			reply[L"content"] = L"Session starting...";
			collector->push_to_server_message(mccid, std::move(reply));
		}
	}
	if (sessionStatus == CHAT_STATUS::CS_STARTED || sessionStatus == CHAT_STATUS::CS_ENQUEUED)
	{
		std::wstring contentType = L"TEXT";
		std::wstring attachStr = L"";

		CMessage mcpMsg = std::move(_makeMCPMessage(
			/*info.getMCCID()*/mccid,
			/*info.getSessionId()*/sid,
			std::move(text),
			contentType,
			/*info.getContact()*/channelId,
			message_id,
			std::move(attachmentsMsgs)
			));

		if (sessionStatus == CHAT_STATUS::CS_STARTED)
		{
			_sendReceiveMessage(mccid, mid, mcpMsg);
		}

		collector->push_to_mcp_message(mccid, std::move(chatMsg));
	}


	return _makeOKRequest();
}

//void CDataReceiverImpl::loadDocument(MessageId)
//{
//}

/******************************* eof *************************************/