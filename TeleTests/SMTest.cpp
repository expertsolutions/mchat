/************************************************************************/
/* Name     : TeleTest\SMTest.cpp                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 27 Aug 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "MessageParser.h"
#include "CreateMessages.h"

BOOST_AUTO_TEST_SUITE(sm_tests)

BOOST_AUTO_TEST_CASE(DeleteSession)
{
	try
	{
		CMyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeId() >> 32;
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		std::wstring MSISDN = L"9067837625";
		MCCID mccid = CreateLocalSession(router_client, params, log->GetInstance());

		MessageContainer msgList = {
			CreateSM2ANY_SessionDeleted(MSISDN, L"Thanks for contact us, see you soon", params.Sid, params.Ssid)
		};

		BOOST_CHECK(router_client.Start(std::move(msgList), mccid, log->GetInstance()) == true);

		//ToDo: send message from abonent

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(DuplicateSession)
{
	try
	{
		/// 1. Send CreateSession && Contact
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Create session");

		SystemConfig settings;
		std::wstring MSISDN = L"9067837625";

		ssl_veon_client::TConnectionParams params;
		params._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
		params._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
		params._method = TgBot::HTTP_METHOD::HM_POST;

		unsigned int update_id = 538984094;
		MessageId messageId = CIdGenerator::GetInstance()->makeId() >> 32;
		ChatId chat_id = CIdGenerator::GetInstance()->makeId() >> 32;

		MessageContainer msgList = {
			CreateStartSessionMessage(chat_id, update_id++, messageId++),
			CreateContactMessage(chat_id, update_id, messageId, MSISDN)
		};

		MCCID expectedMccid = 0;

		CMyRouterClient router_client;
		router_client.Expect(L"ANY2SM_BeginSession",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"ANY2SM_BeginSession");
			BOOST_CHECK(_msg[L"ChannelId"].AsWideStr() == MSISDN);
			BOOST_CHECK(_msg[L"ChannelName"].AsWideStr() == L"Tlg");
			BOOST_CHECK(_msg[L"Split"].AsWideStr() == L"");
			BOOST_CHECK(_msg[L"ChannelType"].AsWideStr() == L"16");
			BOOST_CHECK(_msg[L"TopicName"].AsWideStr() == L"Unknown");

			expectedMccid = _from.AsMessageParamterInt();
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), params, log->GetInstance()) == true);

		/// 2. Send answer from SM: ANY2SM_BeginSessionAck with error
		SessionId expectedSessionId = CIdGenerator::GetInstance()->makeId();
		SessionScriptId expectedSessionScriptId = CIdGenerator::GetInstance()->makeId();

		msgList = {
			CreateANY2SM_BeginSessionAck_Error(expectedSessionId, expectedSessionScriptId)
		};
		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		//ToDo: send message from abonent

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_SUITE_END()
/******************************* eof *************************************/