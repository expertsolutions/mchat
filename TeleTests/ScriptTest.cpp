/************************************************************************/
/* Name     : TeleTest\ScriptTest.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 02 Sep 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "MessageParser.h"
#include "CreateMessages.h"

BOOST_AUTO_TEST_SUITE(script_tests)

BOOST_AUTO_TEST_CASE(UpdateSession)
{
	try
	{
		CMyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeId() >> 32;
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		MCCID mccid = CreateLocalSession(router_client, params, log->GetInstance());

		MessageContainer msgList = {
			CreateS2MCC_SessionUpdate(params.Ssid)
		};

		BOOST_CHECK(router_client.Start(std::move(msgList), mccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(TransferSession)
{
	try
	{
		CMyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;
		SystemConfig settings;

		SessionParams sessionParams;
		sessionParams.Cid = CIdGenerator::GetInstance()->makeId() >> 32;
		sessionParams.Sid = CIdGenerator::GetInstance()->makeId();
		sessionParams.Ssid = router_client.GetClientId();// Must be the same
		sessionParams.Rid = CIdGenerator::GetInstance()->makeSId();

		ssl_veon_client::TConnectionParams veonParams;
		veonParams._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
		veonParams._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
		veonParams._method = TgBot::HTTP_METHOD::HM_POST;


		std::wstring MSISDN = L"9067837625";
		MCCID expectedMccid = CreateLocalSession(router_client, sessionParams, log->GetInstance());

		unsigned int update_id = 538984094;
		MessageId messageId = CIdGenerator::GetInstance()->makeId() >> 32;

		std::wstring transferMsg = L"Transfer message....";
		std::wstring abonentMsg = L"Message while session was in transfer";

		// transfer
		MessageContainer msgList = {
			CreateS2MCC_SessionTransfer(sessionParams.Ssid, transferMsg)
		};
		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(1));

		// abonent message
		msgList = {
			CreateSimpleTextMessage(sessionParams.Cid, update_id++, messageId, abonentMsg)
		};
		
		BOOST_CHECK(router_client.Start(std::move(msgList), veonParams, log->GetInstance()) == true);

		CMessage receiveMessageRequest = CreateReceiveMessageRequest();

		msgList = { receiveMessageRequest };
		router_client.Expect(L"MCP2ICCP/OAM_receiveMessageRequestAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_receiveMessageRequestAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == receiveMessageRequest[L"requestId"].AsWideStr());
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);

			//BOOST_CHECK(_msg[L"ftpHostname"].AsWideStr() == sFtpHostname);
			//BOOST_CHECK(_msg[L"ftpUsername"].AsWideStr() == sFtpUsername);
			//BOOST_CHECK(_msg[L"ftpPassword"].AsWideStr() == sFtpPassword);

			BOOST_CHECK(_msg.HasParam(L"jsondata"));
			std::wstring data = _msg.SafeReadParam(L"jsondata", CMessage::CheckedType::String, L"").AsWideStr();
			data = L"{\"jsondata\":" + data + L"}";
			CMessage data_msg = CMessageParser::ToMessage(data);

			auto jsondata = data_msg[L"jsondata"].AsMessagesVector();
			BOOST_CHECK(jsondata.size() == 2);

			for (const auto& msg : jsondata)
			{
				MessageId msgId = utils::toNum<MessageId>(msg[L"messageId"].AsWideStr());

				if (msgId == messageId)
				{
					BOOST_CHECK(msg[L"message"].AsWideStr() == abonentMsg);
				}
				else
				{
					BOOST_CHECK(msg[L"message"].AsWideStr() == transferMsg);
				}

				SessionId sessionId = utils::toNum<SessionId>(msg[L"sessionId"].AsWideStr());
				MCCID mccid = utils::toNum<MCCID>(msg[L"MCCID"].AsWideStr());

				BOOST_CHECK(msg[L"channelType"].AsWideStr() == L"16");
				BOOST_CHECK(msg[L"channelId"].AsWideStr() == MSISDN);
				BOOST_CHECK(sessionId == sessionParams.Sid);
				BOOST_CHECK(msg[L"channelName"].AsWideStr() == L"Tlg");
				BOOST_CHECK(msg[L"messageReceiver"].AsWideStr() == L"MCC_TELE");
				BOOST_CHECK(msg[L"messageTitle"].AsWideStr() == L"Title");
				BOOST_CHECK(mccid == expectedMccid);
			}
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_SUITE_END()
/******************************* eof *************************************/