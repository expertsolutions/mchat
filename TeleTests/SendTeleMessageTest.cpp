/************************************************************************/
/* Name     : TeleTest\SendTeleMessageTest.cpp                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Aug 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "MessageParser.h"
#include "MyRouterClient.h"
#include "CreateMessages.h"

BOOST_AUTO_TEST_SUITE(send_tele_messages_tests)

BOOST_AUTO_TEST_CASE(SendScriptMessage)
{
	try
	{
		CMyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeId() >> 32;
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same

		MCCID mccid = CreateLocalSession(router_client, params, log->GetInstance());

		MessageContainer msgList = {
			CreateScriptMessage(params.Cid, params.Sid, params.Ssid)
		};

		router_client.Expect(L"S2MCC_replyToMessageAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"S2MCC_replyToMessageAck");
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), mccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(SendMCPReplyToMessage)
{
	try
	{
		CMyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeId() >> 32;
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		MCCID mccid = CreateLocalSession(router_client, params, log->GetInstance());
		std::wstring MSISDN = L"9067837625";

		MessageContainer msgList = {
			CreateMCPReplyToMessage(params.Cid, params.Sid, params.Ssid, params.Rid, L"wefwefwef\n")
		};

		router_client.Expect(L"MCP2ICCP/OAM_replyToMessageAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_replyToMessageAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == params.Rid);
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), mccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(SendMCPReplyToMessage_WithAttachment)
{
	try
	{
		CMyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SystemConfig settings;
		std::wstring sAttachmentString = settings->safe_get_config_value(L"AttachmentString", std::wstring(L"http://ms-ccvm002/Attachments/"));
		std::wstring fileName = L"_in.txt";
		DWORD dwDataSize = 1000;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeId() >> 32;
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		MCCID mccid = CreateLocalSession(router_client, params, log->GetInstance());

		MessageContainer msgList = {
			CreateMCPReplyToMessage_WithAttachment(
				params.Cid, 
				params.Sid, 
				params.Ssid, 
				params.Rid, 
				fileName, 
				dwDataSize, 
				sAttachmentString)
		};

		router_client.Expect(L"MCP2ICCP/OAM_replyToMessageAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_replyToMessageAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == params.Rid);
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), mccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_SUITE_END()
/******************************* eof *************************************/