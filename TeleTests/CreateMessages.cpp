/************************************************************************/
/* Name     : TeleTests\CreateMessages.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 27 Aug 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "CreateMessages.h"

MCCID CreateLocalSession(CMyRouterClient& aRouterClient, const SessionParams& aParams, CSystemLog* aLog)
{
	/// 1. Send CreateSession && Contact && Customer Message
	aLog->LogString(LEVEL_INFO, L"Create session");

	SystemConfig settings;
	std::wstring MSISDN = L"9067837625";

	ssl_veon_client::TConnectionParams params;
	params._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
	params._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
	params._method = TgBot::HTTP_METHOD::HM_POST;

	unsigned int update_id = 538984094;
	MessageId messageId = CIdGenerator::GetInstance()->makeId() >> 32;
	ChatId chat_id = aParams.Cid;

	MessageId beginSessionId = 0;
	MessageId textMessageId = 0;
	std::wstring text = L"Text message here...";

	MessageContainer msgList = {
		CreateStartSessionMessage(chat_id, update_id++, messageId++),
		CreateContactMessage(chat_id, update_id++, beginSessionId = messageId++, MSISDN)
	};

	MCCID expectedMccid = 0;

	aRouterClient.Expect(L"ANY2SM_BeginSession",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg.GetName() == L"ANY2SM_BeginSession");
		BOOST_CHECK(_msg[L"ChannelId"].AsWideStr() == MSISDN);
		BOOST_CHECK(_msg[L"ChannelName"].AsWideStr() == L"Tlg");
		BOOST_CHECK(_msg[L"Split"].AsWideStr() == L"");
		BOOST_CHECK(_msg[L"ChannelType"].AsWideStr() == L"16");
		BOOST_CHECK(_msg[L"TopicName"].AsWideStr() == L"Unknown");

		//const auto incomingMessageId = _msg[L"MessageId"].AsUInt();

		expectedMccid = _from.AsMessageParamterInt();

		//BOOST_CHECK(incomingMessageId == beginSessionId);
	});

	BOOST_CHECK(aRouterClient.Start(std::move(msgList), params, aLog->GetInstance()) == true);

	/// 2. Send answer from SM: ANY2SM_BeginSessionAck
	SessionId expectedSessionId = aParams.Sid;
	SessionScriptId expectedSessionScriptId = aParams.Ssid;

	msgList = { CreateANY2SM_BeginSessionAck(expectedSessionId, expectedSessionScriptId) };

	BOOST_CHECK(aRouterClient.Start(std::move(msgList), expectedMccid, aLog->GetInstance()) == true);

	std::this_thread::sleep_for(std::chrono::seconds(1));

	/// 3. Send request from MCP: MCP2ICCP/OAM_receiveMessageRequest
	CMessage receiveMessageRequest = CreateReceiveMessageRequest();

	msgList = { receiveMessageRequest };
	aRouterClient.Expect(L"MCP2ICCP/OAM_receiveMessageRequestAck",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_receiveMessageRequestAck");
		BOOST_CHECK(_msg[L"requestId"].AsWideStr() == receiveMessageRequest[L"requestId"].AsWideStr());
		BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);

		//BOOST_CHECK(_msg[L"ftpHostname"].AsWideStr() == sFtpHostname);
		//BOOST_CHECK(_msg[L"ftpUsername"].AsWideStr() == sFtpUsername);
		//BOOST_CHECK(_msg[L"ftpPassword"].AsWideStr() == sFtpPassword);

		BOOST_CHECK(_msg.HasParam(L"jsondata"));
		std::wstring data = _msg.SafeReadParam(L"jsondata", CMessage::CheckedType::String, L"").AsWideStr();
		data = L"{\"jsondata\":" + data + L"}";
		CMessage data_msg = CMessageParser::ToMessage(data);

		auto jsondata = data_msg[L"jsondata"].AsMessagesVector();
		BOOST_CHECK(jsondata.size() == 1);

		for (const auto& msg : jsondata)
		{
			MessageId msgId = utils::toNum<MessageId>(msg[L"messageId"].AsWideStr());

			BOOST_CHECK(msg[L"message"].AsWideStr() == L"start message");

			SessionId sessionId = utils::toNum<SessionId>(msg[L"sessionId"].AsWideStr());
			MCCID mccid = utils::toNum<MCCID>(msg[L"MCCID"].AsWideStr());

			BOOST_CHECK(msg[L"channelType"].AsWideStr() == L"16");
			BOOST_CHECK(msg[L"channelId"].AsWideStr() == MSISDN);
			BOOST_CHECK(sessionId == expectedSessionId);
			BOOST_CHECK(msg[L"channelName"].AsWideStr() == L"Tlg");
			BOOST_CHECK(msg[L"messageReceiver"].AsWideStr() == L"MCC_TELE");
			BOOST_CHECK(msg[L"messageTitle"].AsWideStr() == L"Title");
			BOOST_CHECK(mccid == expectedMccid);
		}
	});

	BOOST_CHECK(aRouterClient.Start(std::move(msgList), expectedMccid, aLog->GetInstance()) == true);

	std::this_thread::sleep_for(std::chrono::seconds(1));

	return expectedMccid;
}


/******************************* eof *************************************/