/************************************************************************/
/* Name     : TeleTests\CreateMessages.h                                */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 06 Aug 2017                                               */
/************************************************************************/
#pragma once

#include "Router\router_compatibility.h"
#include "TeleBase.h"
#include "IdGenerator.h"
#include "MessageParser.h"
#include "MyRouterClient.h"
#include "attachment/AttachmentsApi.h"

const wchar_t FIRST_NAME[] = L"Beeline";
const wchar_t LAST_NAME[] = L"Beelinovich";

struct SessionParams
{
	ChatId Cid;
	SessionId Sid;
	SessionScriptId Ssid;
	RequestId Rid;
};

MCCID CreateLocalSession(CMyRouterClient& aRouterClient, const SessionParams& aParams, CSystemLog* aLog);

inline CMessage CreateSimpleTextMessage(
	const ChatId& aCid, 
	unsigned int aUpdateId, 
	const MessageId aMessageId,
	const std::wstring aMessageText)
{
	/*
	{"update_id":538984094,
	"message":{
		"message_id":2016,
		"from":{
			"id":253522005,
			"first_name":"Beeline",
			"last_name":"Beelinovich"
			},
		"chat":{
			"id":253522005,
			"first_name":"Beeline",
			"last_name":"Beelinovich",
			"type":"private"
			},
		"date":1479204070,
		"text":"123"
		}
	}
	*/

	CMessage msg, message, from, chat;

	from[L"id"] = aCid;
	from[L"first_name"] = FIRST_NAME;
	from[L"last_name"] = LAST_NAME;

	chat[L"id"] = aCid;
	chat[L"first_name"] = FIRST_NAME;
	chat[L"last_name"] = LAST_NAME;
	chat[L"type"] = L"private";

	message[L"message_id"] = aMessageId;
	message[L"from"] = from;
	message[L"chat"] = chat;
	message[L"date"] = 1479204070;
	message[L"text"] = aMessageText;

	msg[L"update_id"] = aUpdateId;
	msg[L"message"] = message;

	return msg;
}

template <class...TArgs>
inline CMessage CreateStartSessionMessage(TArgs&&... aArgs)
{
	return CreateSimpleTextMessage(std::forward<TArgs>(aArgs)..., L"start message");
}

inline CMessage CreateContactMessage(
	const ChatId& aCid, 
	unsigned int aUpdateId, 
	const MessageId aMessageId,
	const std::wstring& aPhoneNumber 
	)
{
	/*
	{"update_id":538984095,
	"message":{
		"message_id":2017,
		"from":{
			"id":253522005,
			"first_name":"Beeline",
			"last_name":"Beelinovich"
			},
		"chat":{
			"id":253522005,
			"first_name":"Beeline",
			"last_name":"Beelinovich",
			"type":"private"
		},
		"date":1479204129,
		"contact":{
			"phone_number":"79055505661",
			"first_name":"Beeline",
			"last_name":"Beelinovich",
			"user_id":253522005
		}
	}
	}
	*/

	CMessage msg, message, from, chat, contact;

	from[L"id"] = aCid;
	from[L"first_name"] = FIRST_NAME;
	from[L"last_name"] = LAST_NAME;

	chat[L"id"] = aCid;
	chat[L"first_name"] = FIRST_NAME;
	chat[L"last_name"] = LAST_NAME;
	chat[L"type"] = L"private";

	contact[L"phone_number"] = aPhoneNumber;
	contact[L"first_name"] = FIRST_NAME;
	contact[L"last_name"] = LAST_NAME;
	contact[L"user_id"] = CIdGenerator::GetInstance()->makeId();

	message[L"message_id"] = aMessageId;
	message[L"from"] = from;
	message[L"chat"] = chat;
	message[L"date"] = 1479204070;
	message[L"contact"] = contact;

	msg[L"update_id"] = aUpdateId;
	msg[L"message"] = message;

	return msg;
}

inline CMessage CreateANY2SM_BeginSessionAck(
	const SessionId& aSessionId, 
	const SessionScriptId& aSessionScriptId)
{
	CMessage msg(L"ANY2SM_BeginSessionAck");
	msg[L"SessionExists"] = false;
	msg[L"SessionScriptId"] = aSessionScriptId;
	msg[L"SessionId"] = aSessionId;

	return msg;
}

inline CMessage CreateANY2SM_BeginSessionAck_Error(
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId)
{
	CMessage msg(L"ANY2SM_BeginSessionAck");
	msg[L"SessionExists"] = true;
	msg[L"SessionScriptId"] = aSessionScriptId;
	msg[L"SessionId"] = aSessionId;
	msg[L"ErrorCode"] = 123;
	msg[L"ErrorDescription"] = L"Error message";

	return msg;
}

inline CMessage CreateReceiveMessageRequest()
{
	/*"agentDetails":{
		"agentSkills":"",
		"agentId":"2147515285",
		"agentProfile":"29772",
		"agentLoginName":"APolitov"
		}*/

	CMessage msg(L"MCP2ICCP/OAM_receiveMessageRequest");

	CMessage agentDetails;
	agentDetails[L"agentSkills"] = L"";
	agentDetails[L"agentId"] = L"2147515285";
	agentDetails[L"agentProfile"] = L"29772";
	agentDetails[L"agentLoginName"] = L"APolitov";

	CMessage data;
	data[L"agentDetails"] = agentDetails;

	msg[L"data"] = CMessageParser::ToStringUnicode(data);
	msg[L"requestId"] = L"{68050e30-56fd-4d6a-b30b-93d95053855e}";

	return msg;
}

inline CMessage CreateScriptMessage(
	const ChatId& aCid,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId)
{
	/*	
	S2MCC_replyToMessage; ChatId = ""; Message = "������� �� ��������� � ������! ��� ������ ������, �� ������� ��� � ��������� �����! ����� �������� �������� 1 ���.v3"; 
	SessionId = 0x00000000-70000100; SessionScriptId = 0x0BB83757-1F3C163F; 
	ScrGenAddr = 0x00000000-00000000; DestinationAddress = 0x0BB8376E-DFA2BE7E; ScriptID = 0x0BB83757-1F3C163F;
    */
	CMessage msg(L"S2MCC_replyToMessage");
	msg[L"Message"] = L"Thanks for contact us, see you soon";
	msg[L"SessionId"] = aSessionId;
	msg[L"ChatId"] = aCid;
	msg[L"SessionScriptId"] = aSessionScriptId;
	msg[L"ScriptID"] = aSessionScriptId;

	return msg;
}

inline CMessage CreateMCPReplyToMessage(
	const ChatId& aCid,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId,
	const RequestId& aRequestId,
	const std::wstring& aMessageText)

{
	/*
	MCP2ICCP/OAM_replyToMessage; source = "MCP"; ScrGenAddr = 0x00000000-00000000; destination = "ICCP/OAM"; 
	requestId = "5c5ec891f9894d009233ccd1a567a2eb"; destinationAddress = "0BB8374C1F70550F"; 
	DestinationAddress = 0x0BB8376E-8AD87623; ScriptID = 0x0BB8374C-1F70550F; methodName = "replyToMessage"; 
	data = "{"channelName":"SITE","comp":"ms-v40811-019","channelType":"14","channelId":"9035782957",
		"charset":"null","citrix":"MS-CTXACRM002","agent":"75950","message":"wefwefwef\n","sessionId":"1879048364",
		"messageId":"16c7d4347fe24eafa716","user":"amkoryakin",
		"agentDetails":{"agentSkills":"","agentId":"2147491509","agentProfile":"15056",
			"agentLoginName":"AMKoryakin"},
		"messageSender":"MCC_CHAT","messageDate":"2017-08-25 09:37:06.878"}"; SourceAddress = 0x0BB83761-0000011D;
	*/
	CMessage msg(L"MCP2ICCP/OAM_replyToMessage");
	msg[L"ScriptID"] = aSessionScriptId;
	msg[L"requestId"] = aRequestId;
	msg[L"methodName"] = L"replyToMessage";

	CMessage data;
	data[L"channelName"] = L"Tlg";
	data[L"comp"] = L"ms-v40811-019";
	data[L"channelType"] = 16;
	data[L"channelId"] = L"9035782957";
	data[L"message"] = aMessageText;
	data[L"sessionId"] = aSessionId;
	data[L"messageId"] = CIdGenerator::GetInstance()->makeSId();

	msg[L"data"] = CMessageParser::ToStringUnicode(data);

	return msg;
}

inline CMessage CreateMCPReplyToMessage_WithAttachment(
	const ChatId& aCid,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId,
	const RequestId& aRequestId,
	const std::wstring& aFileName,
	const unsigned int aFileSize,
	const std::wstring& aAttachmentString)
{
	CMessage msg(L"MCP2ICCP/OAM_replyToMessage");
	msg[L"ScriptID"] = aSessionScriptId;
	msg[L"requestId"] = aRequestId;
	msg[L"methodName"] = L"replyToMessage";

	CMessage data;
	data[L"channelName"] = L"Tlg";
	data[L"comp"] = L"ms-v40811-019";
	data[L"channelType"] = 16;
	data[L"channelId"] = L"9035782957";
	data[L"sessionId"] = aSessionId;
	data[L"messageId"] = CIdGenerator::GetInstance()->makeSId();

	// make attachments
	std::list<CMessage> attachmentsMsgs;
	std::wstring uri = attachments::GetUri(aCid, aAttachmentString, aFileName);

	CMessage attachMessage;
	attachMessage[L"name"] = aFileName;
	attachMessage[L"fileSize"] = aFileSize;
	attachMessage[L"url"] = uri;

	attachmentsMsgs.emplace_back(std::move(attachMessage));
	data[L"attachments"] = core::to_raw_data(attachmentsMsgs);

	msg[L"data"] = CMessageParser::ToStringUnicode(data);

	return msg;
}

inline CMessage CreateSM2ANY_SessionDeleted(
	const std::wstring& aChannleId,
	const std::wstring& aReason,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId)
{
	/*
	SM2ANY_SessionDeleted; ChannelId = "1231119999"; Status = 0x00000000-00000002; 
	Reason = "���� ���� ������!  �� ��������!"; SessionScriptId = 0x0BB83754-1F379FD9; ChannelName = "SITE"; 
	SessionCreator = 0x0BB8376E-BD15376F; SessionId = 0x00000000-70000159;
	*/

	CMessage msg(L"SM2ANY_SessionDeleted");

	msg[L"ChannelId"] = aChannleId;
	msg[L"Reason"] = aReason;
	msg[L"SessionId"] = aSessionId;
	msg[L"SessionScriptId"] = aSessionScriptId;
	msg[L"ScriptID"] = aSessionScriptId;

	return msg;
}

inline CMessage CreateS2MCC_SessionUpdate(const SessionScriptId& aSessionScriptId)
{
	/*
	S2MCC_SessionUpdate; EWT = "1"; ScrGenAddr = 0x00000000-00000000; DestinationAddress = 0x0BB8376E-A76DDD7E; 
	ScriptID = 0x0BB8374C-1F705636;
	*/

	CMessage msg(L"S2MCC_SessionUpdate");
	msg[L"ScriptID"] = aSessionScriptId;
	msg[L"EWT"] = L"1";

	return msg;
}

inline CMessage CreateS2MCC_SessionTransfer(const SessionScriptId& aSessionScriptId, const std::wstring& aMessage)
{
	/*
	S2MCC_SessionTransfer; 
	Message = "Transfer"; EWT ="1"; ScrGenAddr = 0x00000000-00000000; 
	DestinationAddress = 0x0BB8376E-A76DDD7E; ScriptID = 0x0BB8374C-1F705636;
	*/
	CMessage msg(L"S2MCC_SessionTransfer");
	msg[L"ScriptID"] = aSessionScriptId;
	msg[L"EWT"] = L"1";
	msg[L"Message"] = aMessage;

	return msg;
}

inline CMessage CreateAttachmentMessage(const ChatId& aCid,
	unsigned int aUpdateId,
	const MessageId aMessageId,
	const std::wstring& aFileName,
	const std::wstring& aFileId,
	int aFileSize)
{
	/*
	{"update_id":538984110,
	"message":
		{"message_id":2044,
		"from":
			{
				"id":253522005,
				"first_name":"Beeline",
				"last_name":"Beelinovich"
			},
		"chat":
			{
				"id":253522005,
				"first_name":"Beeline",
				"last_name":"Beelinovich",
				"type":"private"
			},
		"date":1479975935,
		"document":
			{
				"file_name":"_tmpCreateVXMLSrv.txt",
				"mime_type":"text/plain",
				"file_id":"BQADAgADAwADVXAcDzokRxkHKSTdAg",
				"file_size":2012
			}
		}
	}
	*/

	CMessage msg, message, from, chat, document;

	from[L"id"] = aCid;
	from[L"first_name"] = FIRST_NAME;
	from[L"last_name"] = LAST_NAME;

	chat[L"id"] = aCid;
	chat[L"first_name"] = FIRST_NAME;
	chat[L"last_name"] = LAST_NAME;
	chat[L"type"] = L"private";

	document[L"file_name"] = aFileName;
	document[L"mime_type"] = L"text/plain";
	document[L"file_id"] = aFileId;
	document[L"file_size"] = aFileSize;

	message[L"message_id"] = aMessageId;
	message[L"from"] = from;
	message[L"chat"] = chat;
	message[L"date"] = 1479204070;
	message[L"document"] = document;

	msg[L"update_id"] = aUpdateId;
	msg[L"message"] = message;

	return msg;


	/*
	11:25:35.950673 | FT DUMP: Name: Unknown; update_id = "538984110"; message = [Raw data array. Length: 2322];

	https://api.telegram.org/bot 245204529:AAF0qTSFtW9Euj9KJ8jBdaM8Pgy6GGi2OgQ /getFile?file_id= BQADAgADAwADVXAcDzokRxkHKS
	{"ok":true,"result":{"file_id":"BQADAgADAwADVXAcDzokRxkHKSTdAg","file_size":2012,"file_path":"document/file_1.txt"}}

	https://api.telegram.org/file/bot245204529:AAF0qTSFtW9Euj9KJ8jBdaM8Pgy6GGi2OgQ/ document/file_1.txt
	https://api.telegram.org/bot245204529:AAF0qTSFtW9Euj9KJ8jBdaM8Pgy6GGi2OgQ/document/file_1.txt
	*/


}

inline CMessage CreateChannelPostMessage(unsigned int aUpdateId)
{
	/*
	{"update_id":800802140,
	"channel_post":
		{
			"message_id":1040,
			"chat":
				{
					"id":-1001128377300,
					"title":"\u0646\u0645\u062f\u0633\u0631\u0627\u06cc \u0633\u0645\u06cc\u0631\u0627\u0628\u0627\u0646\u0648",
					"username":"namad1369samira",
					"type":"channel"
				},
			"date":1504963193,
			"photo":[{"file_id":"AgADBAADTasxG3RDoFFwuqxcGwnPBK5YXhkABPV-6ayCVCFGwHEEAAEC","file_size":931,"width":56,"height":90},
					{"file_id":"AgADBAADTasxG3RDoFFwuqxcGwnPBK5YXhkABOSzPEUevtONwXEEAAEC","file_size":11193,"width":200,"height":320},
					{"file_id":"AgADBAADTasxG3RDoFFwuqxcGwnPBK5YXhkABGPfWcSzRx-EwnEEAAEC","file_size":44278,"width":500,"height":800},
					{"file_id":"AgADBAADTasxG3RDoFFwuqxcGwnPBK5YXhkABIm1dqeZ_ORmw3EEAAEC","file_size":75009,"width":800,"height":1280}],
			"caption":"\u0627\u0646\u0631\u0698\u06cc \u0647\u0627 \u0631\u0633\u06cc\u062f\n\u0631\u0636\u0627\u06cc\u062a \u0645\u0634\u062a\u0631\u06cc \u0639\u0632\u06cc\u0632\u0645"
		}
	}
	*/

	CMessage msg, message;

	message[L"message_id"] = 123;

	msg[L"update_id"] = aUpdateId;
	msg[L"channel_post"] = message;

	return msg;
}

/******************************* eof *************************************/
