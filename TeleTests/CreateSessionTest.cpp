/************************************************************************/
/* Name     : TeleTest\CreateSessionTest.cpp                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 16 Jul 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "MessageParser.h"
#include "MyRouterClient.h"
#include "CreateMessages.h"

BOOST_AUTO_TEST_SUITE(create_session_tests)

BOOST_AUTO_TEST_CASE(CreateSession)
{
	try
	{
		/// 1. Send CreateSession && Contact
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Create session");

		SystemConfig settings;
		//std::wstring sFtpHostname = settings->safe_get_config_value(L"FtpHostname", std::wstring(L""));
		//std::wstring sFtpUsername = settings->safe_get_config_value(L"FtpUsername", std::wstring(L""));
		//std::wstring sFtpPassword = settings->safe_get_config_value(L"FtpPassword", std::wstring(L""));

		std::wstring MSISDN = L"9067837625";

		ssl_veon_client::TConnectionParams params;
		params._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
		params._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
		params._method = TgBot::HTTP_METHOD::HM_POST;

		unsigned int update_id = 538984094;
		MessageId messageId = CIdGenerator::GetInstance()->makeId() >> 32;
		ChatId chat_id = CIdGenerator::GetInstance()->makeId() >> 32;

		MessageContainer msgList = {
			CreateStartSessionMessage(chat_id, update_id++, messageId++),
			CreateContactMessage(chat_id, update_id, messageId, MSISDN)
		};

		MCCID expectedMccid = 0;

		CMyRouterClient router_client;
		router_client.Expect(L"ANY2SM_BeginSession",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"ANY2SM_BeginSession");
			BOOST_CHECK(_msg[L"ChannelId"].AsWideStr() == MSISDN);
			BOOST_CHECK(_msg[L"ChannelName"].AsWideStr() == L"Tlg");
			BOOST_CHECK(_msg[L"Split"].AsWideStr() == L"");
			BOOST_CHECK(_msg[L"ChannelType"].AsWideStr() == L"16");
			BOOST_CHECK(_msg[L"TopicName"].AsWideStr() == L"Unknown");

			expectedMccid = _from.AsMessageParamterInt();
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), params, log->GetInstance()) == true);

		/// 2. Send answer from SM: ANY2SM_BeginSessionAck
		SessionId expectedSessionId = CIdGenerator::GetInstance()->makeId();
		SessionScriptId expectedSessionScriptId = CIdGenerator::GetInstance()->makeId();

		msgList = {
			CreateANY2SM_BeginSessionAck(expectedSessionId, expectedSessionScriptId)
		};
		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(1));

		/// 3. Send request from MCP: MCP2ICCP/OAM_receiveMessageRequest
		CMessage receiveMessageRequest = CreateReceiveMessageRequest();

		msgList = { receiveMessageRequest };
		router_client.Expect(L"MCP2ICCP/OAM_receiveMessageRequestAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_receiveMessageRequestAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == receiveMessageRequest[L"requestId"].AsWideStr());
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);

			//BOOST_CHECK(_msg[L"ftpHostname"].AsWideStr() == sFtpHostname);
			//BOOST_CHECK(_msg[L"ftpUsername"].AsWideStr() == sFtpUsername);
			//BOOST_CHECK(_msg[L"ftpPassword"].AsWideStr() == sFtpPassword);

			BOOST_CHECK(_msg.HasParam(L"jsondata"));
			std::wstring data = _msg.SafeReadParam(L"jsondata", CMessage::CheckedType::String, L"").AsWideStr();
			data = L"{\"jsondata\":" + data + L"}";
			CMessage data_msg = CMessageParser::ToMessage(data);

			auto jsondata = data_msg[L"jsondata"].AsMessagesVector();
			BOOST_CHECK(jsondata.size() == 1);

			for (const auto& msg : jsondata)
			{
				if (msg.HasParam(L"url")) // init_msg
				{
					BOOST_CHECK(msg[L"url"].AsWideStr() == L"market_code: ToDo: <market code>, customer_name: ToDo: <subscriber name>, ip_address: ");
					BOOST_CHECK(msg[L"message"].AsWideStr() == L"market_code: ToDo: <market code>, customer_name: ToDo: <subscriber name>, ip_address: ");
				}
				else //chatmessage
				{
					//BOOST_CHECK(msg[L"messageId"].AsWideStr() == messageId);
					BOOST_CHECK(msg[L"message"].AsWideStr() == L"start message");
				}

				SessionId sessionId = utils::toNum<SessionId>(msg[L"sessionId"].AsWideStr());
				MCCID mccid = utils::toNum<MCCID>(msg[L"MCCID"].AsWideStr());

				BOOST_CHECK(msg[L"channelType"].AsWideStr() == L"16");
				BOOST_CHECK(msg[L"channelId"].AsWideStr() == MSISDN);
				BOOST_CHECK(sessionId == expectedSessionId);
				BOOST_CHECK(msg[L"channelName"].AsWideStr() == L"Tlg");
				BOOST_CHECK(msg[L"messageReceiver"].AsWideStr() == L"MCC_TELE");
				BOOST_CHECK(msg[L"messageTitle"].AsWideStr() == L"Title");
				BOOST_CHECK(mccid == expectedMccid);
			}
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(SessionTimeoutBeforeScriptStarted)
{
	/*
		Before start make ChatExpiredTime == 10 sec in tele.config
	*/

	try
	{
		/// 1. Send CreateSession && Contact
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Create session");

		SystemConfig settings;
		std::wstring MSISDN = L"9067837625";

		ssl_veon_client::TConnectionParams params;
		params._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
		params._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
		params._method = TgBot::HTTP_METHOD::HM_POST;

		unsigned int update_id = 538984094;
		MessageId messageId = CIdGenerator::GetInstance()->makeId() >> 32;
		ChatId chat_id = CIdGenerator::GetInstance()->makeId() >> 32;

		MessageContainer msgList = {
			CreateStartSessionMessage(chat_id, update_id++, messageId++)
		};

		MCCID expectedMccid = 0;

		CMyRouterClient router_client;
		BOOST_CHECK(router_client.Start(std::move(msgList), params, log->GetInstance()) == true);

		// wait for timeout
		std::this_thread::sleep_for(std::chrono::seconds(30));

		// Create session again
		msgList = {
			CreateStartSessionMessage(chat_id, update_id++, messageId++),
			CreateContactMessage(chat_id, update_id, messageId, MSISDN)
		};

		router_client.Expect(L"ANY2SM_BeginSession",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"ANY2SM_BeginSession");
			BOOST_CHECK(_msg[L"ChannelId"].AsWideStr() == MSISDN);
			BOOST_CHECK(_msg[L"ChannelName"].AsWideStr() == L"Tlg");
			BOOST_CHECK(_msg[L"Split"].AsWideStr() == L"");
			BOOST_CHECK(_msg[L"ChannelType"].AsWideStr() == L"16");
			BOOST_CHECK(_msg[L"TopicName"].AsWideStr() == L"Unknown");

			expectedMccid = _from.AsMessageParamterInt();
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), params, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(SessionTimeoutAfterScriptStarted)
{
	/*
	Before start make ChatExpiredTime == 10 sec in tele.config
	*/

	try
	{
		/// 1. Send CreateSession && Contact
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Create session");

		SystemConfig settings;
		std::wstring MSISDN = L"9067837625";

		ssl_veon_client::TConnectionParams params;
		params._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
		params._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
		params._method = TgBot::HTTP_METHOD::HM_POST;

		unsigned int update_id = 538984094;
		MessageId messageId = CIdGenerator::GetInstance()->makeId() >> 32;
		ChatId chat_id = CIdGenerator::GetInstance()->makeId() >> 32;

		MessageContainer msgList = {
			CreateStartSessionMessage(chat_id, update_id++, messageId++),
			CreateContactMessage(chat_id, update_id, messageId, MSISDN)
		};

		MCCID expectedMccid = 0;

		CMyRouterClient router_client(120);
		router_client.Expect(L"ANY2SM_BeginSession",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"ANY2SM_BeginSession");
			BOOST_CHECK(_msg[L"ChannelId"].AsWideStr() == MSISDN);
			BOOST_CHECK(_msg[L"ChannelName"].AsWideStr() == L"Tlg");
			BOOST_CHECK(_msg[L"Split"].AsWideStr() == L"");
			BOOST_CHECK(_msg[L"ChannelType"].AsWideStr() == L"16");
			BOOST_CHECK(_msg[L"TopicName"].AsWideStr() == L"Unknown");

			expectedMccid = _from.AsMessageParamterInt();
		});
		BOOST_CHECK(router_client.Start(std::move(msgList), params, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(1));

		/// 2. Send answer from SM: ANY2SM_BeginSessionAck and wait for session timeout
		SessionId expectedSessionId = CIdGenerator::GetInstance()->makeId();
		SessionScriptId expectedSessionScriptId = router_client.GetClientId(); // must be the same

		msgList = {
			CreateANY2SM_BeginSessionAck(expectedSessionId, expectedSessionScriptId)
		};

		router_client.Expect(L"MCC2S_SessionTimeout",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCC2S_SessionTimeout");
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(1));

		// 3. Delete session
		msgList = {
			CreateSM2ANY_SessionDeleted(MSISDN, L"Thanks for contact us, see you soon", expectedSessionId, expectedSessionScriptId)
		};

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}


BOOST_AUTO_TEST_SUITE_END()
/******************************* eof *************************************/