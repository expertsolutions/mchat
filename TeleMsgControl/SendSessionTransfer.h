#pragma once
#include "CreateMessages.h"

CMessage SessionTransfer(const ChatId& /*aCid*/,
	const SessionId& /*aSessionId*/,
	const SessionScriptId& aSessionScriptId)
{
	std::wstring transferText = L"Session has transfered...";
	return CreateS2MCC_SessionTransfer(aSessionScriptId, transferText);
}
