#pragma once

#include "CreateMessages.h"
#include "IdGenerator.h"

CMessage ReplyToMessage(const ChatId& aCid,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId)
{
	RequestId requestId = CIdGenerator::GetInstance()->makeSId();
	std::wstring messageText = L"Operator message text...";

	return CreateMCPReplyToMessage(aCid, aSessionId, aSessionScriptId, requestId, messageText);
}
