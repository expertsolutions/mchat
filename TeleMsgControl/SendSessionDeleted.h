#pragma once
#include "CreateMessages.h"

CMessage SessionDeleted(const ChatId& /*aCid*/,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId)
{
	RequestId channleId = CIdGenerator::GetInstance()->makeSId();
	std::wstring reason = L"Goodbuy...";

	return CreateSM2ANY_SessionDeleted(channleId, reason, aSessionId, aSessionScriptId);
}

