#pragma once

#include "CreateMessages.h"

CMessage ReceiveMessageRequest(const ChatId& /*aCid*/,
	const SessionId& /*aSessionId*/,
	const SessionScriptId& /*aSessionScriptId*/)
{
	return CreateReceiveMessageRequest();
}