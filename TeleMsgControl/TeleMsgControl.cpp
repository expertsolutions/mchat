// TeleMsgControl.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>
#include "Router\router_compatibility.h"

#include "SendReceiveMessageRequest.h"
#include "SendReplyToMessage.h"
#include "SendSessionTransfer.h"
#include "SendSessionDeleted.h"
#include "SendReplyToMessageAttachment.h"

using MessageHandler = std::function<CMessage(const ChatId& aCid,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId)>;

using MessageMap = std::map<std::string, MessageHandler>;

static MessageMap CreateMessageMap()
{
	MessageMap messages;

	messages.emplace("MCP2ICCP/OAM_receiveMessageRequest", ReceiveMessageRequest);
	messages.emplace("MCP2ICCP/OAM_replyToMessage", ReplyToMessage);
	messages.emplace("MCP2ICCP/OAM_replyToMessageAttachment", ReplyToMessageAttachment);
	messages.emplace("S2MCC_SessionTransfer", SessionTransfer);
	messages.emplace("SM2ANY_SessionDeleted", SessionDeleted);

	return messages;
}


int main(int argc, char* argv[])
{
	try
	{
		// Check command line arguments.
		if (argc != 2)
		{
			std::cerr << "Usage: http_server <message name>\n";
			std::cerr << "  For IPv4, try:\n";
			std::cerr << "    TeleMsgControl.exe MCP2ICCP/OAM_receiveMessageRequest.\n";
			return 1;
		}

		auto handlers = CreateMessageMap();

		auto handler = handlers[argv[1]];

		SystemConfig settings;
		MCCID mccid = boost::lexical_cast<MCCID>(settings->get_config_value<std::wstring>(L"MCCID"));
		ChatId chatId = boost::lexical_cast<ChatId>(settings->get_config_value<std::wstring>(L"ChatId"));
		SessionId sessionId = boost::lexical_cast<SessionId>(settings->get_config_value<std::wstring>(L"SessionId"));
		SessionScriptId sessionScriptId = boost::lexical_cast<SessionScriptId>(settings->get_config_value<std::wstring>(L"SessionScriptId"));

		CLIENT_ADDRESS addr_to(mccid);

		CRouterManager router;

		//Wait for router connected
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		router.SendTo(handler(chatId, sessionId, sessionScriptId), addr_to);

		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
	catch (std::exception& e)
	{
		std::cerr << "exception: " << e.what() << "\n";
	}

	return 0;

}

