#pragma once

#include "CreateMessages.h"
#include "IdGenerator.h"

CMessage ReplyToMessageAttachment(const ChatId& aCid,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId)
{
	RequestId requestId = CIdGenerator::GetInstance()->makeSId();

	std::wstring fileName = L"SetPostWindowsInstallSettings.log";
	int fileSize = 3133;
	std::wstring attachmentString = L"http://217.118.84.167/Attachments/";

	return CreateMCPReplyToMessage_WithAttachment(
		aCid, 
		aSessionId, 
		aSessionScriptId, 
		requestId, 
		fileName, 
		fileSize, 
		attachmentString);
}
