/************************************************************************/
/* Name     : MCC\codec.h                                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 01 Apr 2016                                               */
/************************************************************************/
#pragma once
#include <string>

std::string encodeBase64(const std::string &);
std::string decodeBase64(const std::string &);
std::string decodeBase64_rar(const std::string &);

//std::string QuotedPrintableEncode(const std::string &input);
//std::string QuotedPrintableDecode(const std::string &input);

//std::wstring DecodeText(const::std::string& _charset, const std::string& _encoding, const std::string& _source);
//std::wstring DecodeSubject(const std::string& _source);
//
//std::wstring decodeBase64_Safe(const std::string &);



/******************************* eof *************************************/