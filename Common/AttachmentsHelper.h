/************************************************************************/
/* Name     : MCHAT\AttachmentsHelper.h                                 */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Oct 2016                                               */
/************************************************************************/
#pragma once

#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "Router/router_compatibility.h"

#include "utils.h"

namespace attachments
{
	static const unsigned int MaxTextSize = 2000;

	struct TAttachmentsParam
	{
		std::wstring Url;
		std::wstring SourceName;
		int FileSize;
		std::wstring SourceUrl;

		TAttachmentsParam() = default;

		TAttachmentsParam(
			const std::wstring& _url, 
			const std::wstring& _sourceName, 
			int size,
			const std::wstring& _sourceUrl)
			: Url(_url)
			, SourceName(_sourceName)
			, FileSize(size)
			, SourceUrl(_sourceUrl)
		{}
	};
	using AttachmentsList = std::vector<TAttachmentsParam>;

	inline AttachmentsList extractAttachments(const CMessage& reqMsg)
	{
		AttachmentsList attachments;
		if (reqMsg.HasParam(L"attachments"))
		{
			auto messages = reqMsg[L"attachments"].AsMessagesVector();
			for (const auto& msg : messages)
			{
				std::wstring url = utils::toLower(msg[L"url"].AsWideStr());
				int size = utils::toNum<int, wchar_t>(msg[L"size"].AsWideStr()); 
				std::wstring name = utils::toLower(msg[L"name"].AsWideStr());

				attachments.emplace_back(url, name, size, L"");
			}
		}

		return attachments;
	}

	//static std::wstring getLocalPath(
	//	const std::wstring& aAttachPath, 
	//	const std::wstring& aAttachmentString)
	//{
	//	std::wstring localPath;

	//	int pos = aAttachPath.find(aAttachmentString);
	//	if (pos >= 0)
	//	{
	//		localPath = aAttachPath.substr(pos + aAttachmentString.length(), aAttachPath.length());
	//	}

	//	return localPath;
	//}
	inline std::pair<std::wstring, std::wstring> GetLocalPathAndFileNameFromSource(
		const std::wstring& aSource,
		const std::wstring& aAttachmentString)
	{
		std::pair<std::wstring, std::wstring> result;

		int pos = aSource.find(aAttachmentString);
		if (pos >= 0)
		{
			result.first = aSource.substr(pos + aAttachmentString.length(), aSource.length());
		}

		pos = result.first.rfind('/');
		if (pos < 0)
		{
			pos = result.first.rfind('\\');
		}

		if (pos < 0)
		{
			throw std::runtime_error(std::string("Cannot get file name: ") + wtos(aSource));
		}

		result.second = std::wstring(result.first.cbegin() + pos + 1, result.first.cend());

		int offset = 0;
		if (result.first[0] == '/' || result.first[0] == '\\')
		{
			offset = 1;
		}

		result.first = std::wstring(result.first.cbegin() + offset, result.first.cbegin() + pos);

		return result;
	}

	inline std::string GetFileSource(const std::string& aLocalPath)
	{
		std::string outSource;
		try
		{
			std::ifstream _in(aLocalPath.c_str(), std::ios::binary);
			DWORD   dwDataSize = 0;
			if (_in.seekg(0, std::ios::end))
			{
				dwDataSize = static_cast<DWORD>(_in.tellg());
			}
			std::string buff(dwDataSize, 0);
			if (dwDataSize && _in.seekg(0, std::ios::beg))
			{
				std::copy(std::istreambuf_iterator< char>(_in),
					std::istreambuf_iterator< char >(),
					buff.begin());

				outSource = buff;
			}
			_in.close();
		}
		catch (std::runtime_error &e)
		{
			outSource = (boost::format("Exception when reading \"%s\", what: %s") % aLocalPath.c_str() % e.what()).str();
		}
		catch (...)
		{
			outSource = (boost::format("Exception when reading \"%s\"") % aLocalPath.c_str()).str();
		}

		return outSource;
	}

	inline std::string readAttachment(
		const std::wstring& attachPath,
		const std::wstring& /*attachSourceName*/,
		const std::wstring& sAttachmentDisk,
		const std::wstring& sAttachmentString,
		const std::wstring& sAttachmentFolder)
	{
		std::string sSource;

		std::wstring localPath = sAttachmentDisk;

		if (localPath.find(L":") == std::wstring::npos)
		{
			localPath += L":";
		}

		if (sAttachmentDisk[sAttachmentDisk.length() - 1] != ('\\'))
		{
			localPath += L"\\";
		}

		localPath += sAttachmentFolder;
		if (sAttachmentFolder[sAttachmentFolder.length() - 1] != ('\\'))
		{
			localPath += L"\\";
		}

		std::wstring folder, fileName;

		std::tie(folder, fileName) = GetLocalPathAndFileNameFromSource(attachPath, sAttachmentString);
		if (!folder.empty())
		{
			sSource = GetFileSource(wtos(localPath + L"\\" + folder + L"\\" + fileName));
		}

		return sSource;
	}

	inline std::wstring CreateGUID()
	{
		boost::uuids::random_generator gen;
		auto callUUID = gen();
		return boost::lexical_cast<std::wstring>(callUUID);
	}



	inline std::wstring CreateLocalFolder(
		ChatId cid,
		const std::wstring& aAttchmentDisk,
		const std::wstring& aAttachmentFolder)
	{
		auto result = aAttchmentDisk;

		if (result.find(L":") == std::wstring::npos)
		{
			result += L":";
		}

		if (result[result.length() - 1] != ('\\'))
			result += L"\\";

		result += aAttachmentFolder;
		if (result[result.length() - 1] != ('\\'))
			result += L"\\";

		result += cid;
		return result;
	}

	inline std::wstring CreateLocalPath(
		const std::wstring& aLocalFolder,
		const std::wstring& attachName)
	{
		auto result = aLocalFolder;

		result += L"\\";
		result += attachName;

		return result;
	}

	inline std::wstring CreateRemotePath(
		ChatId cid,
		const std::wstring& sAttachmentString,
		const std::wstring& attachName)
	{
		auto result = sAttachmentString;
		if (result[result.length() - 1] != ('/'))
			result += L"/";

		result += cid;
		result += L"/";
		result += attachName;

		return result;
	}

	inline TAttachmentsParam saveAttachment(
		ChatId cid,
		const std::wstring& aAttachmentDisk,
		const std::wstring& aAttachmentFolder,
		const std::wstring& aAttachmentString,
		const std::string& aContent,
		const std::wstring& aAttachName)
	{
		std::wstring localFolder = CreateLocalFolder(
			cid,
			aAttachmentDisk,
			aAttachmentFolder);

		std::wstring localPath = CreateLocalPath(
			localFolder,
			aAttachName);

		std::wstring fileUrl = CreateRemotePath(
			cid,
			aAttachmentString,
			aAttachName);

		if (!boost::filesystem::exists(localFolder)
			&& !boost::filesystem::create_directories(localFolder))
		{
			throw(std::runtime_error((boost::format("Cannot create \"%s\" folder") % localFolder.c_str()).str()));
		}
		else
		{
			std::fstream ofs(localPath, std::fstream::ios_base::out | std::fstream::ios_base::binary);
			ofs.write(aContent.c_str(), aContent.size());
			ofs.close();
		}

		return TAttachmentsParam(fileUrl, aAttachName, aContent.size(), localPath);
	}

};


/******************************* eof *************************************/