/************************************************************************/
/* Name     : Common\m2mhs.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 15 Oct 2018                                               */
/************************************************************************/

#pragma once

#include "Base.h"

namespace m2mhs
{
	CMessage Create_M2MHS_UploadFile(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize);

	CMessage Create_M2MHS_DownloadFile(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize);

	CMessage Create_M2MHS_UploadFileCompleted(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize);

	CMessage Create_M2MHS_UploadFileFailed(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& /*aCommitUrl*/,
		const std::wstring& /*aFileName*/,
		size_t /*aFileSize*/);

	CMessage Create_M2MHS_DownloadFileCompleted(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize);

	CMessage Create_M2MHS_DownloadFileFailed(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& /*aCommitUrl*/,
		const std::wstring& /*aFileName*/,
		size_t /*aFileSize*/);

} // namespace m2mhs

  /******************************* eof *************************************/
