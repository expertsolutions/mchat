/************************************************************************/
/* Name     : Common\mcc2m.cpp                                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 04 Jan 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include "mcc2m.h"
#include "utils.h"

namespace mcc2m
{
	static CMessage Create_MCC2M_CHATMESSAGE(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		bool aText,
		ENUM_MESSAGE_CODE aCode,
		const std::wstring& aContent,
		const MessageVector& aAttachments,
		const std::wstring& aTimeStampNow
		)
	{
		CMessage msg(L"MCC2M_CHATMESSAGE");
		msg[L"id"] = aMessageId;
		msg[L"cid"] = aCid;
		msg[L"code"] = static_cast<int>(aCode);
		msg[L"timestamp"] = aTimeStampNow;

		std::wstring content = aContent;
		boost::replace_all(content, L"\\", L"\\\\");
		boost::replace_all(content, L"\"", L"\\\"");
		boost::replace_all(content, L"\n", L"\\n");
		boost::replace_all(content, L"\r", L"\\r");

		msg[L"content"] = content;

		if (aText)
		{
			msg[L"contentType"] = L"TEXT";
		}
		else
		{
			msg[L"contentType"] = L"FILE";
			msg[L"attachments"] = core::to_raw_data(aAttachments);
		}

		return msg;
	}

	CMessage Create_MCC2M_CHATMESSAGE_plain_text(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const std::wstring& aContent,
		const std::wstring& aTimeStampNow)
	{
		return Create_MCC2M_CHATMESSAGE(
			aMessageId,
			aCid,
			true,
			aCode,
			aContent,
			{},
			aTimeStampNow);
	}

	CMessage Create_MCC2M_CHATMESSAGE_file_list(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const MessageVector& aAttachments, 
		const std::wstring& aTimeStampNow)
	{
		return Create_MCC2M_CHATMESSAGE(
			aMessageId,
			aCid,
			false,
			aCode,
			L"File in attachment",
			aAttachments,
			aTimeStampNow);
	}

	CMessage Create_MCC2M_TRANSFER(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		const std::wstring& aSupportPartId,
		const std::wstring& aOperatorName,
		const std::wstring& aReason,
		const std::size_t aEWT,
		const E_CHAT_STATUS aSessionStatus,
		const std::wstring& aTimeStampNow)
	{
		CMessage msg(L"MCC2M_TRANSFER");
		msg[L"id"] = aMessageId;
		msg[L"cid"] = aCid;
		msg[L"name"] = aOperatorName;
		msg[L"content"] = aReason;
		msg[L"operatorId"] = aSupportPartId;
		msg[L"waitTime"] = aEWT;
		msg[L"status"] = static_cast<int>(aSessionStatus);
		msg[L"timestamp"] = aTimeStampNow;

		return msg;
	}

	CMessage Create_MCC2M_SESSION_STATUS(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		E_CHAT_STATUS aStatus,
		const std::size_t aEWT,
		const std::wstring& aReason,
		const std::wstring& aTimeStampNow)
	{
		CMessage msg(L"MCC2M_SESSION_STATUS");
		msg[L"id"] = aMessageId;
		msg[L"cid"] = aCid;
		msg[L"status"] = static_cast<int>(aStatus);
		msg[L"reason"] = aReason;
		msg[L"waitTime"] = aEWT;
		msg[L"timestamp"] = aTimeStampNow;

		return msg;
	}

	CMessage Create_MCC2M_END_SESSION(
		const std::wstring& aId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const std::wstring& aReason,
		const std::wstring& aTimeStampNow)
	{
		CMessage msg(L"MCC2M_END_SESSION");

		msg[L"id"] = aId;
		msg[L"cid"] = aCid;
		msg[L"status"] = static_cast<int>(E_CHAT_STATUS::CS_FINISHED);
		msg[L"contentType"] = L"TEXT";

		msg[L"code"] = static_cast<int>(aCode);
		msg[L"timestamp"] = aTimeStampNow;
		msg[L"content"] = aReason;

		return msg;
	}
}

/******************************* eof *************************************/