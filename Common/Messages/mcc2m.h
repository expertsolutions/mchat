/************************************************************************/
/* Name     : Common\mcc2m.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 04 Jan 2017                                               */
/************************************************************************/

#pragma once

#include "Base.h"

namespace mcc2m
{
	CMessage Create_MCC2M_CHATMESSAGE_plain_text(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const std::wstring& aContent,
		const std::wstring& timeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	CMessage Create_MCC2M_CHATMESSAGE_file_list(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const MessageVector& aAttachments, 
		const std::wstring& timeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	CMessage Create_MCC2M_TRANSFER(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		const std::wstring& aSupportPartId,
		const std::wstring& aOperatorName,
		const std::wstring& aReason,
		const std::size_t aEWT,
		const E_CHAT_STATUS aSessionStatus,
		const std::wstring& aTimeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	CMessage Create_MCC2M_SESSION_STATUS(
		const std::wstring& aMessageId,
		const ChatId& aCid,
		E_CHAT_STATUS aStatus,
		std::size_t aEwt,
		const std::wstring& aReason,
		const std::wstring& timeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	CMessage Create_MCC2M_END_SESSION(
		const std::wstring& aId,
		const ChatId& aCid,
		ENUM_MESSAGE_CODE aCode,
		const std::wstring& aReason,
		const std::wstring& aTimeStampNow = core::support::time_to_string<std::wstring>(core::time_type::clock::now()));
}

/******************************* eof *************************************/