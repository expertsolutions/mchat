/************************************************************************/
/* Name     : Common\mcc2other.cpp                                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 07 Jan 2017                                               */
/************************************************************************/

#include "stdafx.h"
#include "mcc2other.h"
#include "utils.h"
#include "MessageParser.h"

namespace mcc2other
{
	CMessage Create_ANY2SM_BeginSession(
		const std::wstring& aContact,
		const std::wstring& aChannelId,
		E_CHANNEL_TYPE aChannelType,
		const std::wstring& aSplit,
		const std::wstring& aIpAddress)
	{
		CMessage msg(L"ANY2SM_BeginSession");
		msg[L"ChannelName"] = aChannelId;;
		msg[L"ChannelId"] = aContact;
		msg[L"Split"] = aSplit;
		msg[L"IpAddress"] = aIpAddress;
		msg[L"ChannelType"] = utils::toStrW(static_cast<int>(aChannelType));

		return msg;
	}

	static CMessage Create_MCP2ICCP_OAM_replyToMessage(
		const std::wstring& aRequestId,
		const std::wstring& aMessageId,
		const std::wstring& aText,
		bool bText,
		const std::wstring& aUri = L"",
		const std::wstring& aName = L"",
		const std::size_t aSize = 0)
	{
		CMessage reply(L"MCP2ICCP/OAM_replyToMessage");

		reply[L"requestId"] = aRequestId;

		boost::property_tree::wptree requestTree, responseTree;

		requestTree.put(L"message", aText);
		requestTree.put(L"messageId", aMessageId);

		if (!bText)
		{
			boost::property_tree::wptree attachmentTree, arrayTree;
			attachmentTree.put(L"name", aName);
			attachmentTree.put(L"fileSize", aSize);
			attachmentTree.put(L"url", aUri);

			arrayTree.push_back(std::make_pair(L"", attachmentTree));
			requestTree.add_child(L"attachments", arrayTree);
		}

		std::wstring data = core::support::ptree_parser::tree_to_json_string(requestTree);

		reply[L"data"] = data;

		return reply;
	}

	CMessage Create_MCP2ICCP_OAM_replyToMessage_PlainText(
		const std::wstring& aRequestId,
		const std::wstring& aMessageId,
		const std::wstring& aText)
	{
		return Create_MCP2ICCP_OAM_replyToMessage(
			aRequestId, 
			aMessageId,
			aText,
			true);
	}

	CMessage Create_MCP2ICCP_OAM_replyToMessage_FileList(
		const std::wstring& aRequestId,
		const std::wstring& aMessageId,
		const std::wstring& aUri,
		const std::wstring& aName,
		const std::size_t aSize)
	{
		return Create_MCP2ICCP_OAM_replyToMessage(
			aRequestId, 
			aMessageId,
			L"File in attachment",
			false,
			aUri,
			aName,
			aSize);
	}

	CMessage Create_MCP2ICCP_OAM_receiveMessageRequest(
		const std::wstring& aRequestId,
		const std::wstring& aAgentId,
		const std::wstring& aAgentName)
	{
		CMessage receive(L"MCP2ICCP/OAM_receiveMessageRequest");

		receive[L"requestId"] = aRequestId;

		CMessage agentDetails;
		agentDetails[L"agentId"] = aAgentId;
		agentDetails[L"agentLoginName"] = aAgentName;

		CMessage jsondata;
		jsondata[L"agentDetails"] = agentDetails;

		receive[L"data"] = CMessageParser::ToStringUnicode(jsondata);

		return receive;
	}

	CMessage Create_MCP2ICCP_OAM_receiveMessageRequestAck_submessage(
		const std::wstring& aMessageId,
		const SessionId& aSid,
		const MCCID aMccid,
		const std::wstring& aContent,
		const std::wstring& aContentType,
		const std::wstring& aChannelId,
		E_CHANNEL_TYPE aChannelType,
		const std::wstring& aChannelName,
		const std::wstring& aMessageReceiver,
		const std::wstring& aTopicName,
		MessageVector&& aAttachments,
		const std::wstring& aTimestamp,
		bool aMakeUrl)
	{
		CMessage msg(L"CHATMESSAGE");

		if (aMakeUrl)
		{
			msg[L"url"] = aContent;
		}

		if (aContentType != L"FILE")
			msg[L"message"] = std::move(aContent);
		else
		{
			msg[L"message"] = L"file in attachment";

			MessageVector attachments;
			
			std::transform(aAttachments.cbegin(), aAttachments.cend(), std::back_inserter(attachments),
				[](const CMessage& aMsg)
			{
				CMessage attachment;

				std::wstring url = aMsg.SafeReadParam(L"url", core::checked_type::String, L"").AsWideStr();
				unsigned int size = aMsg.SafeReadParam(L"size", core::checked_type::Int, 0).AsInt();
				std::wstring name = aMsg.SafeReadParam(L"name", core::checked_type::String, L"").AsWideStr();

				attachment[L"url"] = url;
				attachment[L"fileSize"] = size;
				attachment[L"name"] = name;

				return attachment;
			});


			msg[L"attachments"] = core::to_raw_data(attachments);
		}

		msg[L"channelType"] = static_cast<int>(aChannelType);
		msg[L"channelId"] = aChannelId;
		msg[L"sessionId"] = aSid;
		msg[L"channelName"] = aChannelName;
		msg[L"messageReceiver"] = aMessageReceiver;
		msg[L"messageTitle"] = aTopicName;
		msg[L"MCCID"] = aMccid;

		msg[L"messageId"] = aMessageId;
		msg[L"messageDate"] = aTimestamp;

		return msg;
	}

	CMessage Create_ANY2SM_BeginSessionAck(
		int aSessionExist,
		SessionScriptId aSsid,
		SessionId aSid)
	{
		CMessage msg(L"ANY2SM_BeginSessionAck");
		msg[L"SessionExists"] = aSessionExist;
		msg[L"SessionScriptId"] = aSsid;
		msg[L"SessionId"] = aSid;

		return msg;
	}

	CMessage Create_SM2ANY_SessionDeleted(
		ENUM_MESSAGE_CODE aStatus,
		const std::wstring& aReason)
	{
		CMessage msg(L"SM2ANY_SessionDeleted");
		msg[L"Reason"] = aReason;
		msg[L"Status"] = static_cast<int>(aStatus);
		
		return msg;
	}

	CMessage Create_MCP2ICCP_OAM_receiveMessage(
		MCCID aMccid,
		const SessionId& aSid,
		const std::wstring& aContent,
		const std::wstring& aChannelId,
		E_CHANNEL_TYPE aChannelType,
		MessageVector && aAttachments,
		const std::wstring& aTitle,
		const std::wstring& aChannelName,
		const std::wstring& aMessageId)
	{
		CMessage msg(L"MCP2ICCP/OAM_receiveMessage");

		CMessage jsondata;

		jsondata[L"channelType"] = utils::toStrW(static_cast<int>(aChannelType));
		jsondata[L"channelId"] = aChannelId;
		jsondata[L"sessionId"] = aSid;
		jsondata[L"channelName"] = aChannelName;
		jsondata[L"messageReceiver"] = L"MCollector";
		jsondata[L"messageTitle"] = aTitle;
		jsondata[L"MCCID"] = aMccid;

		jsondata[L"messageId"] = aMessageId;
		jsondata[L"messageDate"] = core::support::time_to_string<std::wstring>(core::support::now() + std::chrono::hours(core::support::get_timezone_offset() / 60));

		if (!aAttachments.size())
		{
			jsondata[L"message"] = aContent;
		}
		else
		{
			jsondata[L"message"] = L"File in attachment";
			jsondata[L"attachments"] = core::to_raw_data(std::move(aAttachments));
		}

		msg[L"jsondata"] = CMessageParser::ToStringUnicode(std::move(jsondata));

		return msg;
	}
	
	CMessage Create_S2MCC_SessionTransfer(
		const std::wstring& aEwt,
		const std::wstring& aMessage)
	{
		CMessage msg(L"S2MCC_SessionTransfer");
		msg[L"EWT"] = aEwt;
		msg[L"Message"] = aMessage;

		return msg;
	}
}

/******************************* eof *************************************/