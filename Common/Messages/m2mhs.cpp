/************************************************************************/
/* Name     : Common\m2mhs.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 15 Oct 2018                                               */
/************************************************************************/
#include "stdafx.h"
#include "m2mhs.h"

namespace m2mhs
{
	CMessage Create_M2MHS_UploadFile(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize)
	{
		CMessage msg(L"M2MHS_UploadFile");

		msg[L"MessageId"] = aMessageId;
		msg[L"Url0"] = aDest;
		msg[L"CommitUrl0"] = aCommitUrl;
		msg[L"FilePath0"] = aSource;
		msg[L"Version0"] = 0;
		msg[L"FileCount"] = 1;
		msg[L"FileName0"] = aFileName;
		msg[L"FileSize0"] = aFileSize;
		msg[L"DestinationAddress"] = CLIENT_ADDRESS::Any();

		return msg;
	}

	CMessage Create_M2MHS_DownloadFile(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize)
	{
		CMessage msg(L"M2MHS_DownloadFile");

		msg[L"MessageId"] = aMessageId;
		msg[L"Url0"] = aDest;
		msg[L"CommitUrl0"] = aCommitUrl;
		msg[L"FilePath0"] = aSource;
		msg[L"Version0"] = 0;
		msg[L"FileCount"] = 1;
		msg[L"FileName0"] = aFileName;
		msg[L"FileSize0"] = aFileSize;
		msg[L"DestinationAddress"] = CLIENT_ADDRESS::Any();

		return msg;
	}

	CMessage Create_M2MHS_UploadFileCompleted(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize)
	{
		CMessage msg(L"M2MHS_UploadFileCompleted");

		msg[L"MessageId"] = aMessageId;
		msg[L"Url0"] = aDest;
		msg[L"FilePath0"] = aSource;
		msg[L"CommitUrl0"] = aCommitUrl;
		msg[L"FileName0"] = aFileName;
		msg[L"FileSize0"] = aFileSize;

		return msg;
	}

	CMessage Create_M2MHS_UploadFileFailed(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize)
	{
		CMessage msg(L"M2MHS_UploadFileFailed");

		msg[L"MessageId"] = aMessageId;
		msg[L"Url0"] = aDest;
		msg[L"FilePath0"] = aSource;
		msg[L"CommitUrl0"] = aCommitUrl;
		msg[L"FileName0"] = aFileName;
		msg[L"FileSize0"] = aFileSize;

		return msg;
	}

	CMessage Create_M2MHS_DownloadFileCompleted(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize)
	{
		CMessage msg(L"M2MHS_DownloadFileCompleted");

		msg[L"MessageId"] = aMessageId;
		msg[L"Url0"] = aDest;
		msg[L"FilePath0"] = aSource;
		msg[L"CommitUrl0"] = aCommitUrl;
		msg[L"FileName0"] = aFileName;
		msg[L"FileSize0"] = aFileSize;

		return msg;
	}

	CMessage Create_M2MHS_DownloadFileFailed(
		const std::wstring& aMessageId,
		const std::wstring& aSource,
		const std::wstring& aDest,
		const std::wstring& aCommitUrl,
		const std::wstring& aFileName,
		size_t aFileSize)
	{
		CMessage msg(L"M2MHS_DownloadFileFailed");

		msg[L"MessageId"] = aMessageId;
		msg[L"Url0"] = aDest;
		msg[L"FilePath0"] = aSource;
		msg[L"CommitUrl0"] = aCommitUrl;
		msg[L"FileName0"] = aFileName;
		msg[L"FileSize0"] = aFileSize;

		return msg;
	}
}

/******************************* eof *************************************/