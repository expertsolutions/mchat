//
// Created by Andrea Giove on 17/04/16.
//

#ifndef TGBOT_VOICE_H
#define TGBOT_VOICE_H

#include <memory>
#include <string>
#include "Patterns/string_functions.h"

namespace TgBot {

/**
 * This object represents a voice note.
 * @ingroup types
 */
class Voice {
public:
	typedef std::shared_ptr<Voice> Ptr;

	/**
	 * Unique identifier for this file.
	 */
	std::string file_id;

	/**
	 * Duration of the audio in seconds as defined by sender.
	 */
	int32_t duration;

	/**
	 * Optional. MIME type of the file as defined by sender;
	 */
	std::string mime_type;

	/**
	 * Optional. File size.
	 */
	int32_t file_size;

	std::wstring Dump() const
	{
		std::wostringstream out;
		out << L"Voice [file_id: " << stow(file_id) << L"; ";
		out << L"duration: " << duration << L"; ";
		out << L"mime_type: " << stow(mime_type) << L"; ";
		out << L"file_size: " << file_size << L"]; ";

		return out.str();
	}
};
}

#endif //TGBOT_VOICE_H
