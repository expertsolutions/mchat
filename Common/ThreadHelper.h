

class CThreadWrapper
{
public:
	CThreadWrapper(std::thread&& _t)
		: t(std::move(_t))
	{

	}
	~CThreadWrapper()
	{
		if (t.joinable())
		{
			t.join();
		}
	}

private:
	std::thread t;
};
