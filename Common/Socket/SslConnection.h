/************************************************************************/
/* Name     : MCHAT\SslConnection.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Oct 2020                                               */
/************************************************************************/
#pragma once

#include "AbstractConnection.h"

class ChatSslConnection : public AbstractChatConnection
{
public:
	using Socket = boost::asio::ip::tcp::socket;
	using SslStream = boost::asio::ssl::stream<boost::asio::ip::tcp::socket>;

	explicit ChatSslConnection(
		boost::asio::io_service& aIoService,
		boost::asio::ssl::context& aContext,
		CSystemLog* log,
		ChatTextMessageHandler_s handler)
		: AbstractChatConnection(aIoService, log, handler)
		, _socket(aIoService, aContext)
	{}

	~ChatSslConnection() = default;

	void startListening() override
	{
		startListeningImpl(_socket);
	}

	bool HandShake(boost::asio::yield_context yield) override;
	void CloseSocket() override { _socket.lowest_layer().close(); };

	SslStream& GetSocketRef() { return _socket; }
	
	bool IsOpen() const override { return _socket.lowest_layer().is_open(); }

private:
	SslStream _socket;
};

/******************************* eof *************************************/
