/************************************************************************/
/* Name     : MVEON\Common\Socket\SslSpawnServer.cpp                    */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Feb 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include "SslSpawnServer.h"
#include "SslConnection.h"

namespace ssl_spawn_server
{
	namespace detail
	{
		template <class Connection>
		bool makeMySslServer(boost::asio::io_service* io_service, TConnectionServerParams params, CSystemLog* pLog)
		{
			boost::asio::spawn(*io_service, [io_service = io_service, params = std::move(params), pLog = pLog](boost::asio::yield_context yield)
				{
					auto LogStringModule = [pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
					{
						pLog->LogStringModule(level, L"Server", formatMessage, std::forward<decltype(args)>(args)...);
					};


					try
					{
						LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());

						LogStringModule(LEVEL_FINEST, L"Open socket...");

						boost::asio::ip::tcp::resolver resolver(*io_service);
						boost::asio::ip::tcp::resolver::query query(params._uri, params._port);
						boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
						boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

						LogStringModule(LEVEL_FINEST, L"Add acceptor...");
						/// Acceptor used to listen for incoming connections.
						boost::asio::ip::tcp::acceptor acceptor_(*io_service);
						LogStringModule(LEVEL_FINEST, L"1");
						acceptor_.open(endpoint.protocol());
						LogStringModule(LEVEL_FINEST, L"2");
						acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
						LogStringModule(LEVEL_FINEST, L"3");
						acceptor_.bind(endpoint);
						LogStringModule(LEVEL_FINEST, L"4");
						acceptor_.listen();
						LogStringModule(LEVEL_FINEST, L"5");

						LogStringModule(LEVEL_FINEST, L"Add ssl context...");
						boost::asio::ssl::context ctx(*io_service, boost::asio::ssl::context::sslv23);

						/*USING SERTIFICATE*/
						ctx.set_options(
							boost::asio::ssl::context::default_workarounds
							| boost::asio::ssl::context::no_sslv2
						);
						ctx.use_certificate_chain_file(params._sertificate);
						ctx.use_private_key_file(params._private_key, boost::asio::ssl::context::pem);

						boost::system::error_code errorCode;
						LogStringModule(LEVEL_INFO, L"Waiting for connection...");

						while (!(*io_service).stopped())
						{
							auto connect = std::make_shared<Connection>(*io_service, ctx, pLog, params._handler);
							decltype(auto) socket = connect->GetSocketRef().lowest_layer();

							acceptor_.async_accept(socket, yield[errorCode]);

							if (errorCode)
								throw errorCode;

							connect->startListening();
						}
					}
					catch (boost::system::error_code & error)
					{
						LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
					}
					catch (std::runtime_error & error)
					{
						LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
					}
					catch (...)
					{
						LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
					}

				});

			return true;
		}
	}
}

template bool ssl_spawn_server::detail::makeMySslServer<ChatSslConnection>(
	boost::asio::io_service*, 
	ssl_spawn_server::detail::TConnectionServerParams, 
	CSystemLog*);

/******************************* eof *************************************/