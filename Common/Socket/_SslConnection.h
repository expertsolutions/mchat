/************************************************************************/
/* Name     : Common\Socket\SslConnection.h                             */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Feb 2017                                               */
/************************************************************************/

#pragma once

#include <memory>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include "boost/asio/ssl.hpp"

#include "Log/SystemLog.h"
#include "MessageParser.h"
#include "Base.h"

namespace ssl_spawn_server
{
	class connection : public std::enable_shared_from_this<connection>
	{
	public:
		explicit connection(
			boost::asio::io_service& aIoService,
			boost::asio::ssl::context& aContext,
			CSystemLog* aLog,
			Base::ChatTextMessageHandler_s aHandler,
			uint32_t aClientId)
			: _socket(aIoService, aContext)
			, _timer(aIoService)
			, _strand(aIoService)
			, _log(aLog)
			, _handler(aHandler)
			, _clientId(aClientId)

		{
			_connection_name = L"connection " + std::to_wstring(++_count);
		}

		~connection()
		{
		}

		template<typename... TArgs>
		void LogStringModule(LogLevel level, const std::wstring& formatMessage, TArgs&&... args)
		{
			_log->LogStringModule(level, _connection_name/*L"connection"*/, formatMessage, std::forward<TArgs>(args)...);
		}

		boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& get() { return _socket; }
		void startListening();

	private:
		boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket;
		boost::asio::steady_timer _timer;
		boost::asio::io_service::strand _strand;
		CSystemLog* _log;
		Base::ChatTextMessageHandler_s _handler;
		uint32_t _clientId;

		static std::atomic<unsigned int> _count;
		std::wstring _connection_name;
	};
}

/******************************* eof *************************************/
