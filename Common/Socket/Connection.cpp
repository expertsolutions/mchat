/************************************************************************/
/* Name     : MCHAT\Connection.cpp                                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 20 Jul 2016                                               */
/************************************************************************/


#include "stdafx.h"
#include "ChatBase.h"
#include "Connection.h"
#include "KeepAliveConnection.h"
#include "MessageParser.h"
#include "MessageCollector.h"

#include "tgbot/net/HttpParser.h"

bool ChatConnection::HandShake(boost::asio::yield_context /*yield*/)
{
	return true;
}

//void ChatConnection::startListening()
//{
//	boost::asio::spawn(_strand,
//		[self = shared_from_this()](boost::asio::yield_context yield)
//	{
//		auto tryCatchDo = [self](CONNECTION_STATUS errorStatus, const auto& handler)
//		{
//			std::string errorMsg;
//			try
//			{
//				handler();
//			}
//			catch (boost::system::error_code & error)
//			{
//				if (error != boost::asio::error::eof)
//				{
//					errorMsg = error.message();
//				}
//			}
//			catch (std::runtime_error & error)
//			{
//				errorMsg = error.what();
//			}
//			catch (...)
//			{
//				errorMsg = "Socket failed with error: unhandled exception caught";
//			}
//
//			if (!errorMsg.empty())
//			{
//				self->_socket.close();
//				self->_timer.cancel();
//
//				self->LogStringModule(LEVEL_WARNING, L"Socket failed with error:", stow(errorMsg));
//				return false;
//			}
//
//			return true;
//		};
//		
//		try
//		{
//			std::string in_data;
//			boost::system::error_code errorCode;
//			boost::asio::streambuf buff_response;
//
//			self->_timer.expires_from_now(std::chrono::seconds(3));
//
//			size_t bytes_transferred = 0;
//
//			bool result = tryCatchDo(CONNECTION_STATUS::NO_ANS, [&]()
//				{
//					do
//					{
//						char reply_[C_TCP_PACK_SIZE] = {};
//						int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
//						if (errorCode)
//						{
//							throw errorCode;
//						}
//
//						bytes_transferred += len;
//
//						in_data.append(reply_, len);
//					} while (in_data.find("\r\n\r\n") == std::string::npos);
//				});
//
//			if (!result)
//			{
//				return;
//			}
//
//			size_t num_additional_bytes = bytes_transferred - (in_data.find("\r\n\r\n") + strlen("\r\n\r\n"));
//			TgBot::HttpParser::HeadersMap headers;
//			std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(in_data, headers));
//
//			auto header_it = headers.find("content-length");
//			if (header_it != headers.end())
//			{
//				auto content_length = stoull(header_it->second);
//				if (content_length > num_additional_bytes)
//				{
//					result = tryCatchDo(CONNECTION_STATUS::ERROR_ANS, [&]()
//						{
//							do
//							{
//								char reply_[C_TCP_PACK_SIZE] = {};
//								int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
//								if (errorCode)
//								{
//									throw errorCode;
//								}
//
//								num_additional_bytes += len;
//
//								in_data.append(reply_, len);
//
//							} while (content_length > num_additional_bytes);
//						});
//
//					if (!result)
//					{
//						return;
//					}
//				}
//			}
//
//			std::wstring answer(stow(in_data));
//
//			self->LogStringModule(LEVEL_FINEST, L"Read: %s", answer.c_str());
//			serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(wtos(answer), headers));
//
//			CMessage msg;
//			if (!serverResponse.empty())
//			{
//				msg = CMessageParser::ToMessage(std::move(serverResponse));
//			}
//
//			TMessageStatus tms = self->ParseMessageStatus(headers.find("status")->second);
//
//			msg.SetName(tms.methodName);
//			std::wstring response = self->_handler(std::move(msg), std::move(tms));
//
//			tryCatchDo(CONNECTION_STATUS::NO_ANS, [&]()
//				{
//					self->LogStringModule(LEVEL_FINEST, L"Write: %s", response.c_str());
//					boost::asio::async_write(self->_socket, boost::asio::buffer(wtos(response), response.size()), yield);
//					self->LogStringModule(LEVEL_FINEST, L"Write OK");
//
//					self->_socket.close();
//				});
//		}
//		catch (std::exception & e)
//		{
//			self->LogStringModule(LEVEL_FINEST, L"Exception: %s", stow(e.what()).c_str());
//
//			self->_socket.close();
//			self->_timer.cancel();
//		}
//		catch (...)
//		{
//			self->LogStringModule(LEVEL_WARNING, L"Connection failed with error: unhandled exception caught");
//		}
//	});
//
//	boost::asio::spawn(_strand,
//		[self = shared_from_this()](boost::asio::yield_context yield)
//	{
//		while (self->_socket.is_open())
//		{
//			boost::system::error_code ignored_ec;
//			self->_timer.async_wait(yield[ignored_ec]);
//			if (self->_timer.expires_from_now() <= std::chrono::seconds(0))
//			{
//				self->LogStringModule(LEVEL_FINEST, L"Socket timeout");
//				if (self->_socket.is_open())
//				{
//					self->_socket.close();
//					self->LogStringModule(LEVEL_FINEST, L"Close socket by timeout");
//				}
//
//			}
//		}
//	});
//}

/******************************* eof *************************************/