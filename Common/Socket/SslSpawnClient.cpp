/************************************************************************/
/* Name     : MVEON_TESTS\Socket\MakeSslClientToVeon.cpp                */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 19 Feb 2017                                               */
/************************************************************************/
#include "stdafx.h"
//#include <boost/thread.hpp>
//#include <boost/asio.hpp>
//#include <boost/bind.hpp>
//#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include "boost/asio/ssl.hpp"
//
#include "tgbot/net/httpparser.h"
#include "tgbot/tgtypeparser.h"

#include "MessageParser.h"
#include "Log/SystemLog.h"

#include "SslSpawnClient.h"

namespace ssl_spawn_client
{
	static unsigned int _count = 0;

	namespace detail
	{
		bool SslSpawnClient::HandShake(SslSpawnClient::TSocket& socket, boost::asio::yield_context yield, boost::system::error_code& errorCode)
		{
			socket.set_verify_mode(boost::asio::ssl::verify_none);
			socket.set_verify_callback([&](bool preverified,
				boost::asio::ssl::verify_context& ctx)
			{
				// The verify callback can be used to check whether the certificate that is
				// being presented is valid for the peer. For example, RFC 2818 describes
				// the steps involved in doing this for HTTPS. Consult the OpenSSL
				// documentation for more details. Note that the callback is called once
				// for each certificate in the certificate chain, starting from the root
				// certificate authority.

				// In this example we will simply print the certificate's subject name.
				char subject_name[256];
				X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
				X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

				std::cout << subject_name << std::endl;

				//LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");

				return preverified;
			});

			socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);

			if (errorCode)
			{
				return false;
			}
			return true;
		}

		std::unique_ptr<SslSpawnClient::TSocket> SslSpawnClient::CreateSocket(boost::asio::io_service* io_service) const
		{
			auto _socket = std::make_unique<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>(*io_service, _context);
			return std::move(_socket);
		}

	} // detail

}


/******************************* eof *************************************/