/************************************************************************/
/* Name     : MTELE\Connection.cpp                                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 14 Nov 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "boost/asio/ssl.hpp"
#include "SslConnection.h"
#include "MessageParser.h"
#include "tgbot/net/HttpParser.h"

const int TCP_PACK_SIZE = 2048;

namespace ssl_spawn_server
{
	std::atomic<unsigned int> connection::_count = 0;

	void connection::startListening()
	{
		boost::asio::spawn(_strand,
			[self = shared_from_this()/*, &collector*/](boost::asio::yield_context yield)
		{
			try
			{
				self->LogStringModule(LEVEL_FINEST, L"10");
				boost::system::error_code errorCode;

				self->_timer.expires_from_now(std::chrono::seconds(3));

				self->_socket.set_verify_mode(boost::asio::ssl::verify_none);
				self->_socket.set_verify_callback([&](bool preverified,
					boost::asio::ssl::verify_context& ctx)
				{
					// The verify callback can be used to check whether the certificate that is
					// being presented is valid for the peer. For example, RFC 2818 describes
					// the steps involved in doing this for HTTPS. Consult the OpenSSL
					// documentation for more details. Note that the callback is called once
					// for each certificate in the certificate chain, starting from the root
					// certificate authority.

					// In this example we will simply print the certificate's subject name.
					char subject_name[256];
					X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
					X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

					std::cout << subject_name << std::endl;

					self->LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");

					return preverified;
				});
				self->LogStringModule(LEVEL_FINEST, L"11");
				self->_socket.async_handshake(boost::asio::ssl::stream_base::server, yield[errorCode]);

				self->LogStringModule(LEVEL_FINEST, L"12");

				if (errorCode)
					throw errorCode;

				std::string in_data;

				size_t bytes_transferred = 0;
				do
				{
					char reply_[TCP_PACK_SIZE] = {};
					int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
					if (errorCode)
					{
						throw errorCode;
					}

					bytes_transferred += len;

					in_data.append(reply_, len);

					self->LogStringModule(LEVEL_FINEST, L"..................Read....................:\n %s \n................................", in_data.c_str());

				} while (in_data.find("\r\n\r\n") == std::string::npos);

				size_t num_additional_bytes = bytes_transferred/*in_data.size()*/ - (in_data.find("\r\n\r\n") + strlen("\r\n\r\n"));
				self->LogStringModule(LEVEL_FINEST, L"bytes_transferred == %i, %i byte need to read", bytes_transferred, num_additional_bytes);

				TgBot::HttpParser::HeadersMap headers;
				std::wstring serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(in_data, headers));

				auto header_it = headers.find("content-length");
				if (header_it != headers.end())
				{
					auto content_length = stoull(header_it->second);
					if (content_length > num_additional_bytes)
					{
						try
						{
							do
							{
								char reply_[TCP_PACK_SIZE] = {}; //ToDo: make size depended on content_length
								int len = self->_socket.async_read_some(boost::asio::buffer(reply_), yield);
								if (errorCode)
								{
									throw errorCode;
								}

								num_additional_bytes += len;

								in_data.append(reply_, len);

							} while (content_length > num_additional_bytes);
						}
						catch (boost::system::error_code& error)
						{
							if (error != boost::asio::error::eof)
								self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
						}
						catch (std::runtime_error & error)
						{
							self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
						}
						catch (...)
						{
							self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
						}
					}
				}
				else
				{
					self->LogStringModule(LEVEL_FINEST, L"No content-length, nothing to read");
				}

				std::wstring answer(stow(in_data));

				self->LogStringModule(LEVEL_FINEST, L"Read: %s", answer.c_str());
				serverResponse = stow(TgBot::HttpParser::getInstance().parseResponse(wtos(answer), headers));

				CMessage msg;
				std::string error;
				try
				{
					std::string contentType = "text";

					auto cit = headers.find("content-type");
					if (cit != headers.cend())
					{
						contentType = cit->second;
					}

					if (!serverResponse.empty())
					{
						if (contentType.find("json") != std::string::npos)
						{
							msg = CMessageParser::ToMessage(serverResponse);
						}
						else
						{
							msg = CMessageParser::ToMessageFromPlainText(serverResponse);
						}
						
					}
				}
				catch (std::exception& aError)
				{
					error = aError.what();
				}
				catch (...)
				{
					error = "Unknown exception when parsing messsge";
				}

				auto method = headers.find("method")->second;

				if (error.empty() 
					&& (msg.cbegin() == msg.cend()) 
					&& (method != "GET"))
				{
					error = "Parameters in request body or queryString are malformed";
				}

				std::string response;
				if (error.empty())
				{
					// todo
					//response = self->_handler(std::move(msg), headers.find("status")->second, method);
				}
				else
				{
					response = TgBot::HttpParser::getInstance().generateResponse(error, "text/plain", 400, "Bad Request");
				}

				self->LogStringModule(LEVEL_FINEST, L"Write: %s", stow(response).c_str());
				boost::asio::async_write(self->_socket, boost::asio::buffer(response, response.size()), yield);
			}
			catch (boost::system::error_code& error)
			{
				if (error != boost::asio::error::eof)
					self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.message()));
			}
			catch (std::runtime_error & error)
			{
				self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(error.what()));
			}
			catch (std::exception& e)
			{
				self->LogStringModule(LEVEL_FINEST, L"Exception: %s", stow(e.what()).c_str());

				//self->_socket.close();
				self->_socket.lowest_layer().close();
				self->_timer.cancel();
			}
			catch (...)
			{
				self->LogStringModule(LEVEL_WARNING, L"Socket failed with error: unhandled exception caught");
			}

		});

		boost::asio::spawn(_strand,
			[self = shared_from_this()/*, &collector*/](boost::asio::yield_context yield)
		{
			while (self->_socket.lowest_layer().is_open())
			{
				boost::system::error_code ignored_ec;
				self->_timer.async_wait(yield[ignored_ec]);
				if (self->_timer.expires_from_now() <= std::chrono::seconds(0))
				{
					//collector->deleteById(self->_sessionId);
					self->LogStringModule(LEVEL_WARNING, L"Socket async timer timeout expired");
					self->_socket.lowest_layer().close();
				}
			}
		});
	}
}
/******************************* eof *************************************/