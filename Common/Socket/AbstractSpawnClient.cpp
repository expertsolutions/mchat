#include "stdafx.h"
#include <boost/asio/steady_timer.hpp>

#include "AbstractSpawnClient.h"
#include "Socket/SpawnClient.h"
#include "Socket/SslSpawnClient.h"

#include "tgbot/net/HttpParser.h"
#include "MessageParser.h"
#include "Log/SystemLog.h"

namespace abstract_spawn_client
{
	const int TCP_PACK_SIZE = 1024;
	static unsigned int _count = 0;

	template <class Setup>
	bool AbstractSpawnClient<Setup>::MakeClient(TClientPtr clientPtr, TConnectionParams&& params, boost::asio::io_service* io_service, CSystemLog* pLog)
	{
		assert(params._statusHandler, "No status handler");

		boost::asio::spawn(*io_service, [io_service = io_service, params = std::move(params), pLog = pLog, self = clientPtr](boost::asio::yield_context yield)
		{
			std::wstring clientName = L"Client " + std::to_wstring(++_count);

			auto LogStringModule = [pLog, clientName = std::move(clientName)](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
			{
				pLog->LogStringModule(level, clientName.c_str(), formatMessage, std::forward<decltype(args)>(args)...);
			};

			auto read_complete_predicate = [&](int data_len)
			{
				if (!data_len)
					return true;
				return 	data_len < TCP_PACK_SIZE;
			};

			auto addPartyId = [](const std::string aPartyId)
			{
				if (aPartyId.empty())
				{
					return std::string{};
				}

				return std::string{ "/" } +aPartyId;
			};

			boost::asio::steady_timer timer(*io_service);
			boost::system::error_code errorCode;

			std::string errorMsg;

			const auto &requestMsg = params._msg;
			SessionScriptId sessionScriptId = requestMsg.SafeReadParam(
				L"scriptId",
				CMessage::CheckedType::Int64,
				0).AsUInt64();

			try
			{
				// makeClient
				LogStringModule(LEVEL_INFO, L"Connecting to: %s:%s", stow(params._uri).c_str(), stow(params._port).c_str());

				boost::asio::ip::tcp::resolver resolver(*io_service);

				LogStringModule(LEVEL_INFO, L"1");
				boost::asio::ip::tcp::resolver::query query(params._uri, params._port);

				LogStringModule(LEVEL_INFO, L"2");
				boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);

				LogStringModule(LEVEL_INFO, L"3");
				boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

				LogStringModule(LEVEL_INFO, L"4");

				auto _socket = self->CreateSocket(io_service);

				LogStringModule(LEVEL_INFO, L"5");
				if (!self->HandShake(*_socket, yield, errorCode))
				{
					LogStringModule(LEVEL_INFO, L"[Handshake] errorCode: %i", errorCode.value());
					throw errorCode;
				}
				LogStringModule(LEVEL_INFO, L"7");
				boost::asio::async_connect(_socket->lowest_layer(), iterator, yield[errorCode]);

				if (errorCode)
				{
					LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
					throw errorCode;
				}
				LogStringModule(LEVEL_INFO, L"8");

				std::wstring request = CMessageParser::ToStringUtf(requestMsg);
				std::string url;
				TgBot::HTTP_METHOD httpMethod = TgBot::HTTP_METHOD::HM_POST;
				bool bPut = false;

				auto getChatId = [&requestMsg](const wchar_t* param)
				{
					requestMsg.CheckParam(param, CMessage::CheckedType::String, CMessage::ParamType::Mandatory);
					return requestMsg.SafeReadParam(param, CMessage::CheckedType::String, L"").AsWideStr();
				};

				if (requestMsg == L"CHATMESSAGE" || requestMsg == L"END_SESSION" || requestMsg == L"SCRIPTMESSAGE")
				{
					ChatId cid = getChatId(L"cid");
					url = "http://" + params._uri + addPartyId(params._partyId) + "/chatsessions/" + wtos(cid) + "/chatmessages";
					httpMethod = TgBot::HTTP_METHOD::HM_POST;
				}
				else if (requestMsg == L"TRANSFER")
				{
					ChatId cid = getChatId(L"cid");
					bPut = true;
					url = "http://" + params._uri + addPartyId(params._partyId) + "/chatsessions/" + wtos(cid) + "/operator";
					httpMethod = TgBot::HTTP_METHOD::HM_PUT;
				}
				else if (requestMsg == L"SESSION_STATUS")
				{
					ChatId cid = getChatId(L"cid");
					bPut = true;
					url = "http://" + params._uri + addPartyId(params._partyId) + "/chatsessions/" + wtos(cid);
					httpMethod = TgBot::HTTP_METHOD::HM_PUT;
				}
				else if (requestMsg == L"INITIALIZING")
				{
					ChatId cid = getChatId(L"id");
					url = "http://" + params._uri + "/chatsessions";
					httpMethod = TgBot::HTTP_METHOD::HM_POST;
				}

				std::vector<TgBot::HttpReqArg> headerArgs, bodyArgs;
				headerArgs.push_back(TgBot::HttpReqArg("Connection", "close"));
				if (!params._xApiEnvironment.empty())
				{
					headerArgs.push_back(TgBot::HttpReqArg("X-Api-Environment", params._xApiEnvironment));
				}

				bodyArgs.push_back(TgBot::HttpReqArg("jsondata", wtos(request)));

				std::string httpRequest = TgBot::HttpParser::getInstance().generateRequest(url, headerArgs, bodyArgs, httpMethod);

				LogStringModule(LEVEL_FINEST, L"Write: %s", httpRequest.c_str());

				std::string in_data;
				int len = 0;

				_socket->async_write_some(boost::asio::buffer(httpRequest), yield[errorCode]);

				if (errorCode)
				{
					throw errorCode;
				}

				do
				{
					char reply_[TCP_PACK_SIZE] = {};
					_socket->async_read_some(boost::asio::buffer(reply_), yield);
					if (errorCode)
					{
						throw errorCode;
					}

					len = strlen(reply_);

					in_data.append(reply_, len < TCP_PACK_SIZE ? len : TCP_PACK_SIZE);

				} while (!read_complete_predicate(len));

				LogStringModule(LEVEL_FINEST, L"Read: %s", in_data.c_str());

				if (in_data == "500")
				{
					params._statusHandler(sessionScriptId, CONNECTION_STATUS::ERROR_ANS, L"500");
				}
				else
				{
					params._statusHandler(sessionScriptId, CONNECTION_STATUS::SUCCESS_ANS, L"OK");
				}
			}
			catch (boost::system::error_code& error)
			{
				if (error != boost::asio::error::eof)
				{
					errorMsg = error.message();
				}
			}
			catch (std::runtime_error & error)
			{
				errorMsg = error.what();
			}
			catch (...)
			{
				errorMsg = "unhandled exception caught";
			}

			if (!errorMsg.empty())
			{
				LogStringModule(LEVEL_WARNING, L"Socket failed with error: %s", stow(errorMsg));
				params._statusHandler(sessionScriptId, CONNECTION_STATUS::NO_ANS, stow(errorMsg));
			}
		});


		int test = 0;
		return false;
	}
}

template class abstract_spawn_client::AbstractSpawnClient<spawn_client::SpawnClientSetup>;
template class abstract_spawn_client::AbstractSpawnClient<ssl_spawn_client::SslSpawnClientSetup>;
