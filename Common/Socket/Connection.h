/************************************************************************/
/* Name     : MCHAT\Connection.h                                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 20 Jul 2016                                               */
/************************************************************************/
#pragma once

#include "AbstractConnection.h"

class ChatConnection : public AbstractChatConnection
{
public:
	using Socket = boost::asio::ip::tcp::socket;

	explicit ChatConnection(
		boost::asio::io_service& aIoService,
		CSystemLog* log,
		ChatTextMessageHandler_s handler)
		: AbstractChatConnection(aIoService, log, handler)
		, _socket(aIoService)
	{}

	~ChatConnection() = default;
	
	void startListening() override
	{
		startListeningImpl(_socket);
	}

	bool HandShake(boost::asio::yield_context /*yield*/) override;
	void CloseSocket() override { _socket.close(); };

	Socket& GetSocketRef() { return _socket; }
	
	bool IsOpen() const override { return _socket.is_open(); }

private:
	Socket _socket;
};

/******************************* eof *************************************/
