/************************************************************************/
/* Name     : MVEON\AttachmentsApi.cpp                                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 12 Feb 2017                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "AttachmentsApi.h"
#include "utils.h"

namespace attachments
{
	AttachmentsList extractAttachments(const CMessage& reqMsg)
	{
		AttachmentsList attachments;
		if (reqMsg.HasParam(L"attachments"))
		{
			auto messages = reqMsg[L"attachments"].AsMessagesVector();
			for (const auto& msg : messages)
			{
				std::wstring url = msg[L"url"].AsWideStr();
				int size = utils::toNum<int, wchar_t>(msg[L"fileSize"].AsWideStr());
				std::wstring name = msg[L"name"].AsWideStr();

				attachments.emplace_back(url, name, size);
			}

			//CMessage newAttachMsg = reqMsg[L"attachments"].AsMessage();
			//for (auto cit = newAttachMsg.begin(); cit != newAttachMsg.end(); ++cit)
			//{
			//	CMessage param = cit->second.AsMessage();

			//	std::wstring url = param[L"url"].AsWideStr();
			//	int size = utils::toNum<int, wchar_t>(param[L"size"].AsWideStr());
			//	std::wstring name = param[L"name"].AsWideStr();

			//	attachments.emplace_back(url, name, size);
			//}
		}

		return attachments;
	}

	//static std::wstring makeUri(
	//	const ChatId& cid,
	//	const std::wstring& sInitString,
	//	const std::wstring& attachName,
	//	const char endSimbol)
	//{
	//	std::wstring uri = sInitString;
	//	if (sInitString[sInitString.length() - 1] != (endSimbol))
	//		uri += endSimbol;

	//	uri += cid;
	//	uri += endSimbol;
	//	uri += attachName;
	//	return uri;
	//}


	//template <class TChaitId>
	//static std::wstring GetUri(
	//	const TChaitId& cid,
	//	const std::wstring& sInitString,
	//	const std::wstring& attachName,
	//	const char endSimbol)
	//{
	//}

	//template <>
	//static std::wstring GetUri<long long>(
	//	const long long& cid,
	//	const std::wstring& sInitString,
	//	const std::wstring& attachName,
	//	const char endSimbol)
	//{
	//}

	//template <>
	//static std::wstring GetUri<std::wstring>(
	//	const std::wstring& cid,
	//	const std::wstring& sInitString,
	//	const std::wstring& attachName,
	//	const char endSimbol)
	//{
	//}


	//std::wstring GetUri(
	//	const ChatId& cid,
	//	const std::wstring& sAttachmentString,
	//	const std::wstring& attachName)
	//{
	//	return makeUri(cid, sAttachmentString, attachName, '/');
	//}

	//std::wstring GetLocalUri(
	//	const ChatId& cid,
	//	const std::wstring& sAttachmentFolder,
	//	const std::wstring& attachName)
	//{
	//	//std::wstring fileUrl = sAttachmentFolder;
	//	//if (sAttachmentFolder[sAttachmentFolder.length() - 1] != ('\\'))
	//	//	fileUrl += L"\\";

	//	//fileUrl += cid + L"\\" + attachName;
	//	//return fileUrl;
	//	return makeUri(cid, sAttachmentFolder, attachName, '\\');
	//}



	std::string readAttachment(
		const std::wstring& attachPath,
		const std::wstring& attachSourceName,
		const std::wstring& sAttachmentString,
		const std::wstring& sAttachmentFolder)
	{
		std::string sSource;

		std::wstring localPath;
		int pos = attachPath.find(sAttachmentString);
		if (pos >= 0)
		{
			localPath = attachPath.substr(pos + sAttachmentString.length(), attachPath.length());
		}
		if (!localPath.empty())
		{
			if (sAttachmentFolder[sAttachmentFolder.length() - 1] == ('\\'))
			{
				localPath = sAttachmentFolder + localPath;
			}
			else
			{
				localPath = sAttachmentFolder + L"\\" + localPath;
			}

			try
			{
				std::ifstream _in(localPath.c_str(), std::ios::binary);
				DWORD   dwDataSize = 0;
				if (_in.seekg(0, std::ios::end))
				{
					dwDataSize = static_cast<DWORD>(_in.tellg());
				}
				std::string buff(dwDataSize, 0);
				if (dwDataSize && _in.seekg(0, std::ios::beg))
				{
					std::copy(std::istreambuf_iterator< char>(_in),
						std::istreambuf_iterator< char >(),
						buff.begin());

					sSource = buff;
				}
				_in.close();
			}
			catch (std::runtime_error &e)
			{
				sSource = (boost::format("Exception when reading \"%s\", what: %s") % localPath.c_str() % e.what()).str();
			}
			catch (...)
			{
				sSource = (boost::format("Exception when reading \"%s\"") % localPath.c_str()).str();
			}
		}

		return sSource;
	}

	std::wstring CreateGUID()
	{
		boost::uuids::random_generator gen;
		auto callUUID = gen();
		return boost::lexical_cast<std::wstring>(callUUID);
	}

	TAttachmentsParam saveAttachment(ChatId cid,
		const std::wstring& sAttachmentFolder,
		const std::wstring& sAttachmentString,
		const std::string& sContent,
		const std::wstring& attachName)
	{
		//std::wstring localPath = sAttachmentFolder;
		//std::wstring fileUrl = sAttachmentString;
		//if (sAttachmentFolder[sAttachmentFolder.length() - 1] != ('\\'))
		//	localPath += L"\\";

		//if (sAttachmentString[sAttachmentString.length() - 1] != ('/'))
		//	fileUrl += L"/";

		//localPath += cid;
		//fileUrl += cid;

		std::wstring localPath = GetLocalUri(cid, sAttachmentFolder, attachName);
		std::wstring fileUrl = GetUri(cid, sAttachmentString, attachName);

		auto folder = boost::filesystem::path(localPath).parent_path();

		if (!boost::filesystem::exists(folder) && !boost::filesystem::create_directories(folder))
		{
			throw(std::runtime_error((boost::format("Cannot create \"%s\" folder") % folder.c_str()).str()));
		}
		else
		{
			//localPath += L"\\";
			//localPath += attachName;

			//fileUrl += L"/";
			//fileUrl += attachName;

			std::fstream ofs(localPath, std::fstream::ios_base::out | std::fstream::ios_base::binary);
			ofs.write(sContent.c_str(), sContent.size());
			ofs.close();
		}

		return TAttachmentsParam(fileUrl, attachName, sContent.size());
	}
}

/******************************* eof *************************************/