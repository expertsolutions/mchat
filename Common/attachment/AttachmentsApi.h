/************************************************************************/
/* Name     : MVEON\AttachmentsApi.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCHAT                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 12 Feb 2017                                               */
/************************************************************************/
#pragma once
#include "TeleBase.h"

namespace attachments
{
	struct TAttachmentsParam
	{
		std::wstring Url;
		std::wstring SourceName;
		int FileSize;

		TAttachmentsParam(const std::wstring& _url, const std::wstring& _sourceName, int size)
			:Url(_url), SourceName(_sourceName), FileSize(size)
		{}
	};

	using AttachmentsList = std::vector<TAttachmentsParam>;

	std::wstring CreateGUID();
	
	template <class TChatId>
	std::wstring MakeUri(
		const TChatId& cid,
		const std::wstring& sInitString,
		const std::wstring& attachName,
		const char endSimbol)
	{
		std::wstring uri = sInitString;
		if (sInitString[sInitString.length() - 1] != (endSimbol))
			uri += endSimbol;

		uri += cid;
		uri += endSimbol;
		uri += attachName;
		return uri;
	}

	template <class TChaitId>
	static std::wstring GetUri(
		const TChaitId& cid,
		const std::wstring& sAttachmentFolder,
		const std::wstring& attachName)
	{
		throw std::runtime_error("function GetUri none implemented");
		return L"";
	}

	template <>
	static std::wstring GetUri<long long>(
		const long long& cid,
		const std::wstring& sAttachmentString,
		const std::wstring& attachName)
	{
		return MakeUri(std::to_wstring(cid), sAttachmentString, attachName, '/');
	}

	template <>
	static std::wstring GetUri<std::wstring>(
		const std::wstring& cid,
		const std::wstring& sAttachmentString,
		const std::wstring& attachName)
	{
		return MakeUri(cid, sAttachmentString, attachName, '/');
	}
	
	//std::wstring GetUri(
	//	const ChatId& cid,
	//	const std::wstring& sAttachmentString,
	//	const std::wstring& attachName);

	//std::wstring GetLocalUri(
	//	const ChatId& cid,
	//	const std::wstring& sAttachmentFolder,
	//	const std::wstring& attachName);

	template <class TChatId>
	static std::wstring GetLocalUri(
		const TChatId& cid,
		const std::wstring& sAttachmentFolder,
		const std::wstring& attachName)
	{
		throw std::runtime_error("function GetLocalUri none implemented");
		return L"";
	}

	template <>
	static std::wstring GetLocalUri<long long>(
		const long long& cid,
		const std::wstring& sAttachmentFolder,
		const std::wstring& attachName)
	{
		return MakeUri(std::to_wstring(cid), sAttachmentFolder, attachName, '\\');
	}

	template <>
	static std::wstring GetLocalUri<std::wstring>(
		const std::wstring& cid,
		const std::wstring& sAttachmentFolder,
		const std::wstring& attachName)
	{
		return MakeUri(cid, sAttachmentFolder, attachName, '\\');
	}

	TAttachmentsParam saveAttachment(ChatId cid,
		const std::wstring& sAttachmentFolder,
		const std::wstring& sAttachmentString,
		const std::string& sContent,
		const std::wstring& attachName);

	AttachmentsList extractAttachments(const CMessage& reqMsg);

	std::string readAttachment(
		const std::wstring& attachPath,
		const std::wstring& attachSourceName,
		const std::wstring& sAttachmentString,
		const std::wstring& sAttachmentFolder);

}

/******************************* eof *************************************/