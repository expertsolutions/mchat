/************************************************************************/
/* Name     : CreateSession.h                                           */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jun 2018                                               */
/************************************************************************/
#pragma once

#include "Router\router_compatibility.h"
#include "Base.h"
#include "ChatBase.h"
#include "IdGenerator.h"
#include "MessageParser.h"
#include "utils.h"
#include "ChatRouterClient.h"
#include "AttachmentsHelper.h"

#include <time.h>  
#include <stdlib.h> 

struct TextGenerator
{

	template <class T>
	struct RandomSymbol
	{
	};

	template <>
	struct RandomSymbol<char>
	{
		static char Generate()
		{
			static std::string values = "0123456789";
			return values[rand() % 10];
		}
	};

	template <>
	struct RandomSymbol<wchar_t>
	{
		static wchar_t Generate()
		{
			static std::wstring values = L"0123456789";
			return values[rand() % 10];
		}
	};


	//template <class T>
	//static T GenerateRandomSymbol()
	//{
	//	return 0;
	//}

	//template <>
	//static char GenerateRandomSymbol<char>()
	//{
	//	static std::string values = "0123456789";
	//	return values[rand() % 10];
	//}

	//template <class T = wchar_t>
	//static wchar_t GenerateRandomSymbol()
	//{
	//	static std::wstring values = L"0123456789";
	//	return values[rand() % 10];
	//}


	template <class T>
	static std::basic_string<T> GenerateText(size_t aLenght)
	{
		srand(static_cast<unsigned int>(time(NULL)));

		std::basic_stringstream<T> ss;

		for (size_t j = 0; j < aLenght; ++j)
		{
			for (int i = 0; i < 98; ++i)
			{
				ss << RandomSymbol<T>::Generate();
			}
			ss << std::endl;
		}

		return ss.str();
	}
};



struct SessionParams
{
	ChatId Cid;
	SessionId Sid;
	SessionScriptId Ssid;
	RequestId Rid;
};

MCCID CreateLocalSession(
	MyRouterClient& aRouterClient, 
	const SessionParams& aParams, 
	CSystemLog* aLog, 
	bool aIsEnableHttpServer,
	const std::wstring& aInitialMessage = std::wstring(),
	bool aIsInitialAttachment = false);

inline CMessage CreateSimpleTextMessage(
	const ChatId& aCid,
	bool aIsEnableHttpServer,
	const std::wstring aMessageText)
{
	CMessage msg{L"INITIALIZING"}, topic, customer;

	topic[L"name"] = L"Mobile connection";
	topic[L"split"] = L"Mobile";

	customer[L"name"] = L"Abonent name";
	customer[L"number"] = L"89067837625";
	customer[L"url"] = L"https://moskva.beeline.ru/customers/products/";
	customer[L"ipAddress"] = L"127.0.0.1";
	customer[L"browser"] = L"Netscape";
	customer[L"OSVersion"] = L"Win32";
	customer[L"appVersion"] = L"AdroidApp";
	customer[L"connectionType"] = L"type1";
	customer[L"phoneModel"] = L"Gnusmus 4.0";

	msg[L"id"] = aCid;
	msg[L"channel"] = L"WEB";
	msg[L"marketCode"] = L"VIP";

	if (aIsEnableHttpServer)
	{
		msg[L"httpServerEnable"] = true;
	}

	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::time_type::clock::now());

	msg[L"topic"] = topic;
	msg[L"customer"] = customer;

	return msg;
}

inline CMessage CreateTextMessage(const MessageId& aMessageId,
	const std::wstring aMessageText)
{
	CMessage msg;
	msg[L"id"] = aMessageId;
	msg[L"contentType"] = L"TEXT";

	std::wstring content = stow(to_utf8(aMessageText));

	msg[L"content"] = content;
	msg[L"code"] = 11;
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::support::now());

	return msg;
}

template <class...TArgs>
inline CMessage CreateStartSessionMessage(TArgs&&... aArgs)
{
	return CreateSimpleTextMessage(std::forward<TArgs>(aArgs)..., L"start message");
}

inline CMessage CreateANY2SM_BeginSessionAck(
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId)
{
	CMessage msg(L"ANY2SM_BeginSessionAck");
	msg[L"SessionExists"] = false;
	msg[L"SessionScriptId"] = aSessionScriptId;
	msg[L"SessionId"] = aSessionId;

	return msg;
}

inline CMessage CreateReceiveMessageRequest()
{
	/*"agentDetails":{
	"agentSkills":"",
	"agentId":"2147515285",
	"agentProfile":"29772",
	"agentLoginName":"APolitov"
	}*/

	CMessage msg(L"MCP2ICCP/OAM_receiveMessageRequest");

	CMessage agentDetails;
	agentDetails[L"agentSkills"] = L"";
	agentDetails[L"agentId"] = L"2147515285";
	agentDetails[L"agentProfile"] = L"29772";
	agentDetails[L"agentLoginName"] = L"APolitov";

	CMessage data;
	data[L"agentDetails"] = agentDetails;
	data[L"split"] = L"123123abc";

	msg[L"data"] = CMessageParser::ToStringUnicode(data);
	msg[L"requestId"] = L"{68050e30-56fd-4d6a-b30b-93d95053855e}";

	return msg;
}

inline CMessage CreateMCPReplyToMessage_WithAttachment(
	const MessageId& aMessageId,
	const ChatId& aCid,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId,
	const RequestId& aRequestId,
	const std::wstring& aRealFileName,
	const std::wstring& aMcpFileName,
	const unsigned int aFileSize,
	const std::wstring& aAttachmentDisk,
	const std::wstring& aAttachmentString,
	const std::wstring& aAttachmentFolder)
{
	CMessage msg(L"MCP2ICCP/OAM_replyToMessage");
	msg[L"ScriptID"] = aSessionScriptId;
	msg[L"requestId"] = aRequestId;
	msg[L"methodName"] = L"replyToMessage";

	CMessage data;
	data[L"channelName"] = L"Tlg";
	data[L"comp"] = L"ms-v40811-019";
	data[L"channelType"] = 16;
	data[L"channelId"] = L"9035782957";
	data[L"sessionId"] = aSessionId;
	data[L"messageId"] = aMessageId;// CIdGenerator::GetInstance()->makeSId();

	//// make directory
	//std::wstring localUri = attachments::CreateLocalPath(aCid, aAttachmentFolder, aFileName);

	//// make attachments
	//std::wstring uri = attachments::CreateRemotePath(aCid, aAttachmentString, aFileName);

	auto source = attachments::GetFileSource(std::string("d:/") + wtos(aRealFileName));

	auto params = attachments::saveAttachment(
		aCid,
		aAttachmentDisk,
		aAttachmentFolder,
		aAttachmentString,
		source,
		aMcpFileName);

	std::list<CMessage> attachmentsMsgs;

	CMessage attachMessage;
	attachMessage[L"name"] = aRealFileName;
	attachMessage[L"size"] = aFileSize;
	attachMessage[L"url"] = params.Url;

	attachmentsMsgs.emplace_back(std::move(attachMessage));
	data[L"attachments"] = core::to_raw_data(attachmentsMsgs);

	msg[L"data"] = CMessageParser::ToStringUnicode(data);

	return msg;
}

inline CMessage CreateAttachmentMessage_URL(
	const ChatId& aCid,
	const std::wstring& aUrl,
	const std::wstring& aFileName,
	size_t aFileSize)
{
	CMessage attachment;
	attachment[L"url"] = aUrl;
	attachment[L"fileSize"] = aFileSize;
	attachment[L"fileName"] = aFileName;

	CMessage msg;

	msg[L"id"] = aCid;
	msg[L"contentType"] = L"URL";
	msg[L"content"] = L"";
	msg[L"code"] = static_cast<int>(ENUM_MESSAGE_CODE::MD_ABONENT_CHATSESSION);
	msg[L"attachment"] = attachment;
	msg[L"timestamp"] = core::support::time_to_string<std::wstring>(core::time_type::clock::now());
	return msg;
}

inline CMessage CreateS2MCC_SessionTransfer(const SessionScriptId& aSessionScriptId, const std::wstring& aMessage)
{
	/*
	S2MCC_SessionTransfer;
	Message = "Transfer"; EWT ="1"; ScrGenAddr = 0x00000000-00000000;
	DestinationAddress = 0x0BB8376E-A76DDD7E; ScriptID = 0x0BB8374C-1F705636;
	*/
	CMessage msg(L"S2MCC_SessionTransfer");
	msg[L"ScriptID"] = aSessionScriptId;
	msg[L"EWT"] = L"1";
	msg[L"Message"] = aMessage;

	return msg;
}

inline CMessage CreateMCPReplyToMessage(
	const MessageId& aMessageId,
	const ChatId& aCid,
	const SessionId& aSessionId,
	const SessionScriptId& aSessionScriptId,
	const RequestId& aRequestId)
{
	CMessage msg(L"MCP2ICCP/OAM_replyToMessage");
	msg[L"ScriptID"] = aSessionScriptId;
	msg[L"requestId"] = aRequestId;
	msg[L"methodName"] = L"replyToMessage";

	CMessage data;
	data[L"channelName"] = L"Tlg";
	data[L"comp"] = L"ms-v40811-019";
	data[L"channelType"] = 16;
	data[L"channelId"] = L"9035782957";
	data[L"sessionId"] = aSessionId;
	data[L"messageId"] = aMessageId;
	data[L"message"] = L"������� ����� ��+ ����� 07.2022.\n"
		"��� ����������� �����.\n\n"
		"<br>� ���� ������ ����� ������ �� ������� �������� � �������[���� ������](mybee://myplan).";
	
	msg[L"data"] = CMessageParser::ToStringUnicode(data);

	return msg;
}


/******************************* eof *************************************/