/************************************************************************/
/* Name     : OCP\SendMessageTest.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Jun 2018                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "ChatRouterClient.h"
#include "CreateSession.h"
//#include "AttachmentsApi.h"
#include "MessageParser.h"
//#include "mcc2other.h"
#include "../Common/Messages//m2mhs.h"

BOOST_AUTO_TEST_SUITE(messages_from_ocp_tests)

BOOST_AUTO_TEST_CASE(SendAttachmentTest)
{
	try
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Send file");

		MyRouterClient router_client;

		SystemConfig settings;
		std::wstring sAttachmentString = settings->safe_get_config_value(L"AttachmentString", std::wstring(L"http://ms-ccvm002/Attachments/"));
		std::wstring sAttachmentFolder = settings->safe_get_config_value(L"AttachmentFolder", std::wstring(L"Attachments"));
		std::wstring sFtpHostname = settings->safe_get_config_value(L"FtpHostname", std::wstring(L"ms-mult007"));
		std::wstring fileName = L"_in.txt";
		std::wstring messageId = CIdGenerator::GetInstance()->makeSId();

		DWORD dwDataSize = 1000;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeSId();
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		MCCID expectedMccid = CreateLocalSession(router_client, params, log->GetInstance(), true);

		SpawnClientSetup::TConnectionParams params2;
		params2._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
		params2._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
		//params2._request_uri = "http://" + params2._uri + "/chatsessions/" + wtos(params.Cid) + "/chatmessages";

		std::wstring ftpDestin = 
			L"http://" 
			+ sFtpHostname + L"/" 
			+ params.Cid + L"/" 
			+ fileName;

		std::wstring mspDestin =
			L"http://"
			+ sFtpHostname + L"/"
			+ sAttachmentFolder + L"/"
			+ params.Cid + L"/"
			+ fileName;

		//std::wstring wMccid = utils::toStr<wchar_t>(mccid);

		std::wstring origin = L"http://tv-ocm002/Attachments/" + params.Cid + L"/" + fileName;
		std::wstring destin = L"http://ms-multvr001/Attachments/" + params.Cid + L"/" + fileName;

		MessageContainer msgList = {
			CreateAttachmentMessage_URL(
				params.Cid,
				origin,
				fileName,
				dwDataSize)
		};

		router_client.Expect(L"M2MHS_UploadFile",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			auto url0 = _msg[L"Url0"].AsWideStr();
			auto filePath0 = _msg[L"FilePath0"].AsWideStr();
			auto commitUrl0 = _msg[L"CommitUrl0"].AsWideStr();

			BOOST_CHECK(_msg.GetName() == L"M2MHS_UploadFile");
			BOOST_CHECK(url0 == utils::toLower(ftpDestin));
			BOOST_CHECK(commitUrl0 == utils::toLower(mspDestin));
			BOOST_CHECK(filePath0 == utils::toLower(origin));
			BOOST_CHECK(_msg[L"FileSize0"].AsInt() == dwDataSize);
			BOOST_CHECK(_msg[L"FileName0"].AsWideStr() == fileName);
			BOOST_CHECK(_msg[L"Version0"].AsInt() == 0);
			BOOST_CHECK(_msg[L"FileCount"].AsInt() == 1);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), params2, log->GetInstance()) == true);

		msgList.emplace_back(m2mhs::Create_M2MHS_UploadFileCompleted(
			messageId,
			origin, 
			ftpDestin,
			mspDestin,
			fileName,
			dwDataSize));

		router_client.Expect(L"MCP2ICCP/OAM_receiveMessage",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_receiveMessage");

			BOOST_CHECK(_msg.HasParam(L"jsondata"));
			std::wstring data = _msg.SafeReadParam(L"jsondata", CMessage::CheckedType::String, L"").AsWideStr();
			data = L"{\"jsondata\":" + data + L"}";
			CMessage data_msg = CMessageParser::ToMessage(data);

			auto jsondata = data_msg[L"jsondata"].AsMessage();

			//BOOST_CHECK(jsondata[L"message"].AsWideStr() == text);

			SessionId sessionId = utils::toNum<SessionId>(jsondata[L"sessionId"].AsWideStr());
			MCCID mccid = utils::toNum<MCCID>(jsondata[L"MCCID"].AsWideStr());

			BOOST_CHECK(jsondata[L"channelType"].AsWideStr() == L"14");
			//BOOST_CHECK(jsondata[L"channelId"].AsWideStr() == MSISDN);
			BOOST_CHECK(sessionId == params.Sid);
			BOOST_CHECK(jsondata[L"channelName"].AsWideStr() == L"SITE");
			BOOST_CHECK(jsondata[L"messageReceiver"].AsWideStr() == L"MCC_CHAT");
			BOOST_CHECK(jsondata[L"messageTitle"].AsWideStr() == L"Mobile connection");
			BOOST_CHECK(mccid == expectedMccid);

			auto attachments = jsondata[L"attachments"].AsMessagesVector();
			BOOST_CHECK(attachments.size() == 1);

			CMessage attach = *attachments.begin();

			DWORD fileSize = utils::toNum<DWORD>(attach[L"size"].AsWideStr());

			BOOST_CHECK(attach[L"name"].AsWideStr() == utils::toLower(fileName));
			BOOST_CHECK(fileSize == dwDataSize);
			BOOST_CHECK(attach[L"url"].AsWideStr() == utils::toLower(mspDestin));
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(Send_OAM_receiveMessageRequestAck_WithAttachment)
{
	try
	{
		MyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SystemConfig settings;
		std::wstring sAttachmentString = settings->safe_get_config_value(L"AttachmentString", std::wstring(L"http://ms-ccvm002/Attachments/"));
		std::wstring sAttachmentFolder = settings->safe_get_config_value(L"AttachmentFolder", std::wstring(L"Attachments"));
		std::wstring sFtpHostname = settings->safe_get_config_value(L"FtpHostname", std::wstring(L"ms-mult007"));

		std::wstring fileName = L"_in.txt";
		std::wstring urlFileName = L"url_in.txt";
		DWORD dwDataSize = 1000;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeSId();
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();
		std::wstring messageId = CIdGenerator::GetInstance()->makeSId();

		// 1. Create session
		MCCID expectedMccid = CreateLocalSession(router_client, params, log->GetInstance(), true);

		//std::wstring wMccid = utils::toStr<wchar_t>(mccid);

		//2. transfer
		std::wstring origin = L"http://tv-ocm002/Attachments/" + params.Cid + L"/" + urlFileName;
		std::wstring destin = L"http://ms-multvr001/Attachments/" + params.Cid + L"/" + urlFileName;

		std::wstring ftpDestin =
			L"http://"
			+ sFtpHostname + L"/"
			+ params.Cid + L"/"
			+ urlFileName;

		std::wstring mspDestin =
			L"http://"
			+ sFtpHostname + L"/"
			+ sAttachmentFolder + L"/"
			+ params.Cid + L"/"
			+ urlFileName;

		std::wstring transferMessage = L"transfer message";

		MessageContainer msgList = {
			CreateS2MCC_SessionTransfer(
			params.Ssid,
				transferMessage
				)
		};

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		//3. Send message with attachment

		SpawnClientSetup::TConnectionParams params2;
		params2._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
		params2._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
		//params2._request_uri = "http://" + params2._uri + "/chatsessions/" + wtos(params.Cid) + "/chatmessages";

		msgList = {
			CreateAttachmentMessage_URL(
				params.Cid,
				origin,
				fileName,
				dwDataSize)
		};

		router_client.Expect(L"M2MHS_UploadFile",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			auto url0 = _msg[L"Url0"].AsWideStr();
			auto filePath0 = _msg[L"FilePath0"].AsWideStr();
			auto commitUrl0 = _msg[L"CommitUrl0"].AsWideStr();

			BOOST_CHECK(_msg.GetName() == L"M2MHS_UploadFile");
			BOOST_CHECK(url0 == utils::toLower(ftpDestin));
			BOOST_CHECK(commitUrl0 == utils::toLower(mspDestin));
			BOOST_CHECK(filePath0 == utils::toLower(origin));
			BOOST_CHECK(_msg[L"FileSize0"].AsInt() == dwDataSize);
			BOOST_CHECK(_msg[L"FileName0"].AsWideStr() == fileName);
			BOOST_CHECK(_msg[L"Version0"].AsInt() == 0);
			BOOST_CHECK(_msg[L"FileCount"].AsInt() == 1);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), params2, log->GetInstance()) == true);

		//4. receiveMessageRequest
		CMessage receiveMessageRequest = CreateReceiveMessageRequest();

		msgList = { receiveMessageRequest };
		router_client.Expect(L"MCP2ICCP/OAM_receiveMessageRequestAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_receiveMessageRequestAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == receiveMessageRequest[L"requestId"].AsWideStr());
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);

			BOOST_CHECK(_msg.HasParam(L"jsondata"));
			std::wstring data = _msg.SafeReadParam(L"jsondata", CMessage::CheckedType::String, L"").AsWideStr();
			data = L"{\"jsondata\":" + data + L"}";
			CMessage data_msg = CMessageParser::ToMessage(data);

			auto jsondata = data_msg[L"jsondata"].AsMessagesVector();
			BOOST_CHECK(jsondata.size() == 1);

			const auto& msg = jsondata[0];

			MessageId msgId = msg[L"messageId"].AsWideStr();

			SessionId sessionId = utils::toNum<SessionId>(msg[L"sessionId"].AsWideStr());
			MCCID mccid = utils::toNum<MCCID>(msg[L"MCCID"].AsWideStr());

			BOOST_CHECK(msg[L"channelType"].AsWideStr() == L"14");
			//BOOST_CHECK(msg[L"channelId"].AsWideStr() == MSISDN);
			BOOST_CHECK(sessionId == params.Sid);
			BOOST_CHECK(msg[L"channelName"].AsWideStr() == L"SITE");
			BOOST_CHECK(msg[L"messageReceiver"].AsWideStr() == L"MCC_CHAT");
			BOOST_CHECK(msg[L"messageTitle"].AsWideStr() == L"Mobile connection");
			BOOST_CHECK(mccid == expectedMccid);
			BOOST_CHECK(msg[L"message"].AsWideStr() == transferMessage);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		// 5. Transfer again
		msgList = {
			CreateS2MCC_SessionTransfer(
			params.Ssid,
				transferMessage
				)
		};

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		// 6. Upload completed
		msgList.emplace_back(m2mhs::Create_M2MHS_UploadFileCompleted(
			messageId,
			origin,
			ftpDestin,
			mspDestin,
			fileName,
			dwDataSize));

		//7. receiveMessageRequest
		msgList.emplace_back(CreateReceiveMessageRequest());

		router_client.Expect(L"MCP2ICCP/OAM_receiveMessageRequestAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_receiveMessageRequestAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == receiveMessageRequest[L"requestId"].AsWideStr());
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);

			BOOST_CHECK(_msg.HasParam(L"jsondata"));
			std::wstring data = _msg.SafeReadParam(L"jsondata", CMessage::CheckedType::String, L"").AsWideStr();
			data = L"{\"jsondata\":" + data + L"}";
			CMessage data_msg = CMessageParser::ToMessage(data);

			auto jsondata = data_msg[L"jsondata"].AsMessagesVector();
			BOOST_CHECK(jsondata.size() == 2);

			for (const auto& msg : jsondata)
			{
				MessageId msgId = msg[L"messageId"].AsWideStr();

				SessionId sessionId = utils::toNum<SessionId>(msg[L"sessionId"].AsWideStr());
				MCCID mccid = utils::toNum<MCCID>(msg[L"MCCID"].AsWideStr());

				BOOST_CHECK(msg[L"channelType"].AsWideStr() == L"14");
				//BOOST_CHECK(msg[L"channelId"].AsWideStr() == MSISDN);
				BOOST_CHECK(sessionId == params.Sid);
				BOOST_CHECK(msg[L"channelName"].AsWideStr() == L"SITE");
				BOOST_CHECK(msg[L"messageReceiver"].AsWideStr() == L"MCC_CHAT");
				BOOST_CHECK(msg[L"messageTitle"].AsWideStr() == L"Mobile connection");
				BOOST_CHECK(mccid == expectedMccid);

				bool isAttachment = msg.IsParam(L"attachments");

				if (!isAttachment)
				{
					BOOST_CHECK(msg[L"message"].AsWideStr() == transferMessage);
				}
				else
				{
					auto attachments = msg[L"attachments"].AsMessagesVector();
					BOOST_CHECK(attachments.size() == 1);

					CMessage attach = *attachments.begin();

					DWORD fileSize = utils::toNum<DWORD>(attach[L"size"].AsWideStr());

					BOOST_CHECK(attach[L"name"].AsWideStr() == utils::toLower(fileName));
					BOOST_CHECK(fileSize == dwDataSize);
					BOOST_CHECK(attach[L"url"].AsWideStr() == utils::toLower(mspDestin));
				}
			}
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), expectedMccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(SendBigInitialMessageTest)
{
	try
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"SendBigInitialMessageTest");

		MyRouterClient router_client;

		SystemConfig settings;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeSId();
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		std::wstring messageText = TextGenerator::GenerateText<wchar_t>(30);
		
		MCCID expectedMccid = CreateLocalSession(router_client, params, log->GetInstance(), false, messageText, true);
		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(SendBigInitialMessageTest_Url)
{
	try
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"SendBigInitialMessageTest_Url");

		MyRouterClient router_client;

		SystemConfig settings;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeSId();
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		std::wstring messageText = TextGenerator::GenerateText<wchar_t>(30);

		MCCID expectedMccid = CreateLocalSession(router_client, params, log->GetInstance(), true, messageText, true);
		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_SUITE_END()

/******************************* eof *************************************/