/************************************************************************/
/* Name     : AllHeaders.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jun 2018                                               */
/************************************************************************/
#pragma once

#include <fstream>
#include "Log/SystemLog.h"
#include "Patterns/string_functions.h"
#include "Configuration\SystemConfiguration.h"
#include "Patterns\time_functions.h"
#include <boost/thread.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/coroutine/asymmetric_coroutine.hpp>
#include <boost/coroutine/symmetric_coroutine.hpp>
#include "Router\router_compatibility.h"
#include <boost/format.hpp>

#include "tgbot/net/HttpParser.h"

#include "Codec.h"
#include "ChatBase.h"

using ChatTextMessageHandler_test = std::function<void(const CMessage& msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)>;

using MessageContainer = std::list<CMessage>;

const unsigned int SAVE_TIME_OUT = 30;

/******************************* eof *************************************/