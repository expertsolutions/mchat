/************************************************************************/
/* Name     : AllHeaders.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jun 2018                                               */
/************************************************************************/

#pragma once
#include "myrouterclient.h"
#include "SpawnClient.h"
#include "SpawnServer.h"

struct SpawnClientSetup
{
	struct TConnectionParams : public spawn_client::ClientProxy::TConnectionParams
	{
		TConnectionParams()
		{
			this->_statusHandler = [](SessionScriptId /*sessionScriptId*/, CONNECTION_STATUS /*error*/, const std::wstring& /*message*/) {};
		}
	};

	using TConnectionServerParams = spawn_server::ServerProxy::TConnectionParams;

	template <class ... TArgs>
	static bool MakeClient(TArgs&& ... aArgs)
	{
		return spawn_client::ClientProxy::MakeClient(std::forward<TArgs>(aArgs)...);
	}

	template <class ... TArgs>
	static bool MakeServer(TArgs&& ... aArgs)
	{
		return spawn_server::ServerProxy::MakeServer(std::forward<TArgs>(aArgs)...);
	}
};

using MyRouterClient = CMyRouterClient2<SpawnClientSetup>;

/******************************* eof *************************************/