/************************************************************************/
/* Name     : MCP\McpMessageTest.h                                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : Multimedia                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jun 2018                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "ChatRouterClient.h"
#include "CreateSession.h"
//#include "AttachmentsApi.h"
#include "MessageParser.h"
#include "../Common/Messages/m2mhs.h"
#include "../Common/utils.h"

BOOST_AUTO_TEST_SUITE(messages_from_mcp_tests)

BOOST_AUTO_TEST_CASE(SendMCPReplyToMessage)
{
	try
	{
		MyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SystemConfig settings;

		std::wstring serverHost = settings->safe_get_config_value(L"ServerHost", std::wstring(L"127.0.0.1"));
		std::wstring serverPort = settings->safe_get_config_value(L"ServerPort", std::wstring(L"8081"));

		DWORD dwDataSize = 1000;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeSId();
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		MCCID mccid = CreateLocalSession(router_client, params, log->GetInstance(), false);

		std::wstring wMccid = utils::toStr<wchar_t>(mccid);

		MessageId messageId = CIdGenerator::GetInstance()->makeSId();

		MessageContainer msgList = {
			CreateMCPReplyToMessage(
			messageId,
				params.Cid,
				params.Sid,
				params.Ssid,
				params.Rid)
		};

		router_client.Expect(L"MCP2ICCP/OAM_replyToMessageAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_replyToMessageAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == params.Rid);
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), mccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(SendMCPReplyToMessage_WithAttachmentContentTypeUrl)
{
	try
	{
		MyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SystemConfig settings;
		std::wstring sAttachmentDisk = settings->safe_get_config_value(L"AttachmentDisk", std::wstring(L"d"));
		std::wstring sAttachmentFolder = settings->safe_get_config_value(L"AttachmentFolder", std::wstring(L"Attachments"));
		std::wstring sAttachmentString = settings->safe_get_config_value(L"AttachmentString", std::wstring(L"http://ms-ccvm002/Attachments/"));
		std::wstring sFtpHostname = settings->safe_get_config_value(L"FtpHostname", std::wstring(L"http://mult006/"));

		std::wstring serverHost = settings->safe_get_config_value(L"ServerHost", std::wstring(L"127.0.0.1"));
		std::wstring serverPort = settings->safe_get_config_value(L"ServerPort", std::wstring(L"8081"));

		std::wstring mcpFileName = L"mcp_in.txt";
		std::wstring realFileName = L"_in.txt";

		DWORD dwDataSize = 1000;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeSId();
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		MCCID mccid = CreateLocalSession(router_client, params, log->GetInstance(), true);

		std::wstring wMccid = utils::toStr<wchar_t>(mccid);

		std::wstring origin = L"http://tv-ocm002/Attachments/outbox/" + params.Cid + L"/" + mcpFileName;
		std::wstring destin = L"http://ms-multvr001/Attachments/" + params.Cid + L"/" + mcpFileName;

		std::wstring ftpDest = L"http://" + sFtpHostname + L"/outbox/" 
			+ params.Cid + L"/" + mcpFileName;

		MessageId messageId = CIdGenerator::GetInstance()->makeSId();

		MessageContainer msgList = {
			CreateMCPReplyToMessage_WithAttachment(
				messageId,
				params.Cid,
				params.Sid,
				params.Ssid,
				params.Rid,
				realFileName,
				mcpFileName,
				dwDataSize,
				sAttachmentDisk,
				sAttachmentString + L"/outbox",
				sAttachmentFolder + L"/outbox")
		};

		router_client.Expect(L"MCP2ICCP/OAM_replyToMessageAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_replyToMessageAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == params.Rid);
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);
		});

		router_client.Expect(L"M2MHS_DownloadFile",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			auto url0 = _msg[L"Url0"].AsWideStr();
			auto filePath0 = _msg[L"FilePath0"].AsWideStr();

			BOOST_CHECK(_msg.GetName() == L"M2MHS_DownloadFile");
			BOOST_CHECK(url0 == utils::toLower(destin));
			//BOOST_CHECK(_msg[L"CommitUrl0"].AsWideStr() == L"");
			BOOST_CHECK(filePath0 == utils::toLower(ftpDest));
			BOOST_CHECK(_msg[L"Version0"].AsInt() == 0);
			BOOST_CHECK(_msg[L"FileCount"].AsInt() == 1);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), mccid, log->GetInstance()) == true);

		/*spawn_server::TConnectionServerParams server_params;
		server_params._uri = wtos(serverHost);
		server_params._port = wtos(serverPort);
		server_params._clientId = CIdGenerator::GetInstance()->makeId() >> 32;

		msgList.emplace_back(
			m2mhs::Create_M2MHS_DownloadFileCompleted(
				messageId,
				origin,
				destin,
				destin,
				fileName,
				dwDataSize));

		server_params._handler = [&](const CMessage& aMsg)
		{
			BOOST_CHECK(aMsg[L"messageStatus"].AsWideStr() == utils::toStr<wchar_t>(static_cast<int>(MESSAGE_STATUS::RECIEVED)));
			BOOST_CHECK(aMsg[L"cid"].AsWideStr() == params.Cid);
			BOOST_CHECK(aMsg[L"messageId"].AsWideStr() == messageId);
			BOOST_CHECK(aMsg[L"code"].AsWideStr() == utils::toStr<wchar_t>(static_cast<int>(ENUM_MESSAGE_CODE::MD_OPERATOR_CHATSESSION)));
			BOOST_CHECK(aMsg[L"contentType"].AsWideStr() == L"URL");
			BOOST_CHECK(aMsg[L"content"].AsWideStr() == L"File in attachment");

			BOOST_CHECK(aMsg.HasParam(L"attachment"));

			CMessage attachment = aMsg[L"attachment"].AsMessage();

			BOOST_CHECK(attachment[L"name"].AsWideStr() == fileName);
			BOOST_CHECK(attachment[L"size"].AsWideStr() == utils::toStr<wchar_t>(dwDataSize));
			BOOST_CHECK(attachment[L"url"].AsWideStr() == utils::toLower(destin));

			router_client.Stop();

			return std::string();
		};

		router_client.Start(
			std::move(msgList), 
			std::move(server_params), 
			mccid,
			log->GetInstance());*/

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}

BOOST_AUTO_TEST_CASE(SendMCPReplyToMessage_WithAttachmentContentTypeFile)
{
	try
	{
		MyRouterClient router_client;

		singleton_auto_pointer<CSystemLog> log;

		SystemConfig settings;
		std::wstring sAttachmentDisk = settings->safe_get_config_value(L"AttachmentDisk", std::wstring(L"d"));
		std::wstring sAttachmentString = settings->safe_get_config_value(L"AttachmentString", std::wstring(L"http://ms-ccvm002/Attachments/"));
		std::wstring sAttachmentFolder = settings->safe_get_config_value(L"AttachmentFolder", std::wstring(L"D:/Attachments"));
		std::wstring fileName = L"_in.txt";
		DWORD dwDataSize = 1000;

		SessionParams params;
		params.Cid = CIdGenerator::GetInstance()->makeSId();
		params.Sid = CIdGenerator::GetInstance()->makeId();
		params.Ssid = router_client.GetClientId();// Must be the same
		params.Rid = CIdGenerator::GetInstance()->makeSId();

		MCCID mccid = CreateLocalSession(router_client, params, log->GetInstance(), false);

		std::wstring wMccid = utils::toStr<wchar_t>(mccid);

		MessageId messageId = CIdGenerator::GetInstance()->makeSId();

		MessageContainer msgList = {
			CreateMCPReplyToMessage_WithAttachment(
				messageId,
				params.Cid,
				params.Sid,
				params.Ssid,
				params.Rid,
				fileName,
				fileName,
				dwDataSize,
				sAttachmentDisk,
				sAttachmentString,
				sAttachmentFolder)
		};

		router_client.Expect(L"MCP2ICCP/OAM_replyToMessageAck",
			[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
		{
			BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_replyToMessageAck");
			BOOST_CHECK(_msg[L"requestId"].AsWideStr() == params.Rid);
			BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);
		});

		BOOST_CHECK(router_client.Start(std::move(msgList), mccid, log->GetInstance()) == true);

		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	catch (std::runtime_error& error)
	{
		BOOST_ERROR(error.what());
	}
	catch (...)
	{
		BOOST_ERROR("Unknown exception");
	}
}
BOOST_AUTO_TEST_SUITE_END()

/******************************* eof *************************************/