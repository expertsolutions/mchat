/************************************************************************/
/* Name     : ChatTests\CreateMessages.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Project  : MTELE                                                     */
/* Company  : Expert Solutions                                          */
/* Date     : 17 Jun 2018                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/test/unit_test.hpp>
#include "CreateSession.h"
#include "../Common/Messages/m2mhs.h"

MCCID CreateLocalSession(
	MyRouterClient& aRouterClient, 
	const SessionParams& aParams, 
	CSystemLog* aLog,
	bool aIsEnableHttpServer,
	const std::wstring& aInitialMessage,
	bool aIsInitialAttachment)
{
	/// 1. Send CreateSession && Contact && Customer Message
	aLog->LogString(LEVEL_INFO, L"Create session");

	SystemConfig settings;
	std::wstring MSISDN = L"89067837625";

	SpawnClientSetup::TConnectionParams params;
	params._uri = wtos(settings->safe_get_config_value(L"ConnectorHost", std::wstring(L"0.0.0.0")));
	params._port = wtos(settings->safe_get_config_value(L"ConnectorPort", std::wstring(L"80")));
	//params._request_uri = "http://" + params._uri + "/chatsessions";
	//params._method = TgBot::HTTP_METHOD::HM_POST;

	//unsigned int update_id = 538984094;
	//MessageId messageId = CIdGenerator::GetInstance()->makeId() >> 32;
	ChatId chat_id = aParams.Cid;

	MessageId beginSessionId = L"";
	MessageId textMessageId = CIdGenerator::GetInstance()->makeSId();
	//std::wstring text = L"Text message here...";

	MessageContainer msgList = {
		CreateStartSessionMessage(chat_id, aIsEnableHttpServer)
	};

	MCCID expectedMccid = 0;

	aRouterClient.Expect(L"ANY2SM_BeginSession",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg.GetName() == L"ANY2SM_BeginSession");
		BOOST_CHECK(_msg[L"ChannelId"].AsWideStr() == MSISDN);
		BOOST_CHECK(_msg[L"ChannelName"].AsWideStr() == L"SITE");
		BOOST_CHECK(_msg[L"Split"].AsWideStr() == L"Mobile");
		BOOST_CHECK(_msg[L"ChannelType"].AsWideStr() == L"14");
		BOOST_CHECK(_msg[L"TopicName"].AsWideStr() == L"Mobile connection");

		//const auto incomingMessageId = _msg[L"MessageId"].AsUInt();

		expectedMccid = _from.AsMessageParamterInt();

		//BOOST_CHECK(incomingMessageId == beginSessionId);
	});

	BOOST_CHECK(aRouterClient.Start(std::move(msgList), params, aLog->GetInstance()) == true);

	/// 2. Send answer from SM: ANY2SM_BeginSessionAck
	SessionId expectedSessionId = aParams.Sid;
	SessionScriptId expectedSessionScriptId = aParams.Ssid;

	msgList = { CreateANY2SM_BeginSessionAck(expectedSessionId, expectedSessionScriptId) };

	BOOST_CHECK(aRouterClient.Start(std::move(msgList), expectedMccid, aLog->GetInstance()) == true);

	std::this_thread::sleep_for(std::chrono::seconds(1));

	/// 3. Initial message

	if (!aInitialMessage.empty())
	{
		msgList = { CreateTextMessage(textMessageId, aInitialMessage) };

		if (aIsInitialAttachment && aIsEnableHttpServer)
		{
			aRouterClient.Expect(L"M2MHS_UploadFile",
				[&](const CMessage& _msg, const CLIENT_ADDRESS & /*_from*/, const CLIENT_ADDRESS & /*_to*/)
			{
				BOOST_CHECK(_msg.GetName() == L"M2MHS_UploadFile");
			});
		}

		//params._request_uri = "http://" + params._uri + "/chatsessions/" + wtos(chat_id) + "/chatmessages";
		BOOST_CHECK(aRouterClient.Start(std::move(msgList), params, aLog->GetInstance()) == true);
		std::this_thread::sleep_for(std::chrono::seconds(1));

		if (aIsInitialAttachment && aIsEnableHttpServer)
		{
			//Upload completed
			msgList.emplace_back(m2mhs::Create_M2MHS_UploadFileCompleted(
				textMessageId,
				L"no matter",
				L"no matter",
				L"no matter",
				L"no matter",
				0));
		}
	}

	/// 4. Send request from MCP: MCP2ICCP/OAM_receiveMessageRequest
	CMessage receiveMessageRequest = CreateReceiveMessageRequest();

	msgList.emplace_back(receiveMessageRequest);

	aRouterClient.Expect(L"MCP2ICCP/OAM_receiveMessageRequestAck",
		[&](const CMessage& _msg, const CLIENT_ADDRESS & _from, const CLIENT_ADDRESS & /*_to*/)
	{
		BOOST_CHECK(_msg.GetName() == L"MCP2ICCP/OAM_receiveMessageRequestAck");
		BOOST_CHECK(_msg[L"requestId"].AsWideStr() == receiveMessageRequest[L"requestId"].AsWideStr());
		BOOST_CHECK(_msg[L"intStatus"].AsInt() == 0);

		//BOOST_CHECK(_msg[L"ftpHostname"].AsWideStr() == sFtpHostname);
		//BOOST_CHECK(_msg[L"ftpUsername"].AsWideStr() == sFtpUsername);
		//BOOST_CHECK(_msg[L"ftpPassword"].AsWideStr() == sFtpPassword);

		BOOST_CHECK(_msg.HasParam(L"jsondata"));
		std::wstring data = _msg.SafeReadParam(L"jsondata", CMessage::CheckedType::String, L"").AsWideStr();
		data = L"{\"jsondata\":" + data + L"}";
		CMessage data_msg = CMessageParser::ToMessage(data);

		auto jsondata = data_msg[L"jsondata"].AsMessagesVector();
		if (aInitialMessage.empty())
		{
			BOOST_CHECK(jsondata.size() == 1);
		}
		else
		{
			BOOST_CHECK(jsondata.size() == 2);
		}

		const auto& msg1 = jsondata[0];

		BOOST_CHECK(msg1[L"message"].AsWideStr() ==
			L"Market code: VIP\n Customer name: Abonent name\n Ip address: 127.0.0.1\n "\
			L"OS version: Win32\n App version: AdroidApp\n Connection type: type1\n Phone model: Gnusmus 4.0");

		SessionId sessionId = utils::toNum<SessionId>(msg1[L"sessionId"].AsWideStr());
		MCCID mccid = utils::toNum<MCCID>(msg1[L"MCCID"].AsWideStr());

		BOOST_CHECK(msg1[L"channelType"].AsWideStr() == L"14");
		BOOST_CHECK(msg1[L"channelId"].AsWideStr() == MSISDN);
		BOOST_CHECK(sessionId == expectedSessionId);
		BOOST_CHECK(msg1[L"channelName"].AsWideStr() == L"SITE");
		BOOST_CHECK(msg1[L"messageReceiver"].AsWideStr() == L"MCC_CHAT");
		BOOST_CHECK(msg1[L"messageTitle"].AsWideStr() == L"Mobile connection");
		BOOST_CHECK(mccid == expectedMccid);

		if (!aInitialMessage.empty())
		{
			const auto& msg2 = jsondata[1];

			BOOST_CHECK(msg2.IsParam(L"attachments"));
		}
	});

	BOOST_CHECK(aRouterClient.Start(std::move(msgList), expectedMccid, aLog->GetInstance()) == true);

	std::this_thread::sleep_for(std::chrono::seconds(1));

	return expectedMccid;
}


/******************************* eof *************************************/