// Client.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <fstream>
#include "Log/SystemLog.h"
#include "Patterns/string_functions.h"
#include "Configuration\SystemConfiguration.h"
#include "Patterns\time_functions.h"
#include <boost/thread.hpp>
#include "boost/asio/ssl.hpp"
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>


#include "tgbot/net/HttpParser.h"

#include "MakeClient.h"
#include "Codec.h"
#include "MessageParser.h"


template <class TValueClass>
void foreach_func(const boost::property_tree::wptree& tree, const std::wstring& _data, TValueClass& response)
{
	for (const auto& basic_tree : tree)
	{
		if (basic_tree.second.size())
		{
			foreach_func(basic_tree.second, basic_tree.first, response);
		}
		else
		{
			response[basic_tree.first] = basic_tree.second.data();
		}
	}
}

namespace stuff
{
	template<class _InIt,class _Fn1, typename... TArgs>
	inline void for_each(_InIt _First, _InIt _Last, _Fn1& _Func, TArgs&&... args)
	{	// perform function for each element
		for (; _First != _Last; ++_First)
			_Func(*_First, std::forward<TArgs>(args)...);
	}
}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		std::cerr << "Usage: client.exe <CTN>\n";
		std::cerr << "  For example, try:\n";
		std::cerr << "    client.exe 89067837625\n";
		return 1;
	}


	singleton_auto_pointer<CSystemLog> log;
	log->LogString(LEVEL_INFO, L"Starting run");


	SystemConfig settings;

	std::wstring ctn = stow(argv[1]);

	TConnectionParams params;
	params._uri  = wtos(settings->safe_get_config_value(L"Host", std::wstring(L"0.0.0.0")));
	params._port = wtos(settings->safe_get_config_value(L"Port", std::wstring(L"80")));

	std::wstring session_id = L"78503A1D3135430DB28B221D70E33354";

	//CREATE SESSION
	if (true)
	{
		boost::property_tree::wptree requestTree, responseTree;

		requestTree.put(L"id", session_id);
		requestTree.put(L"channel", L"WEB");
		requestTree.put(L"marketCode", L"VIP");
		//requestTree.put(L"httpServerEnable", true);

		boost::property_tree::wptree topicTree;
		topicTree.put(L"name", L"Mobile connection");
		topicTree.put(L"split", L"Mobile");
		requestTree.push_back(std::make_pair(L"topic", topicTree));

		boost::property_tree::wptree customerTree;
		customerTree.put(L"name", L"Abonent name");
		customerTree.put(L"number", ctn);
		customerTree.put(L"url", L"https://moskva.beeline.ru/customers/products/");
		customerTree.put(L"ipAddress", L"127.0.0.1");
		customerTree.put(L"browser", L"Netscape");
		requestTree.push_back(std::make_pair(L"customer", customerTree));
		requestTree.put(L"timestamp", core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

		//std::wstring request = core::support::ptree_parser::tree_to_json_string(requestTree);
		std::wstring request = CMessageParser::ToStringUtf(requestTree);

		std::vector<TgBot::HttpReqArg> args;
		args.push_back(TgBot::HttpReqArg("jsondata", wtos(request)));

		std::string url = "http://" + params._uri + "/chatsessions";

		params._request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);

		makeMyClient(params, log->GetInstance());
		//boost::asio::io_service io;
		//makeMySslClient(&io, params, log->GetInstance());

		std::string response  = TgBot::HttpParser::getInstance().parseResponse(params._response);
	}

	//::Sleep(10000);

	//SEND MESSAGE
	//if (true)
	//{
	//	boost::property_tree::wptree requestTree, responseTree;
	//	
	//	requestTree.put(L"id", session_id);
	//	requestTree.put(L"contentType", L"TEXT");

	//	std::wstring content = stow(to_utf8(L"message 1\\\\ \\\""));

	//	requestTree.put(L"content", content);
	//	requestTree.put(L"code", 11);
	//	requestTree.put(L"timestamp", core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

	//	////std::wstring request = core::support::ptree_parser::tree_to_json_string(requestTree);
	//	//std::wstring request = CMessageParser::ToStringUnicode(requestTree);
	//	//auto msg = CMessageParser::ToMessage(request);
	//	//auto content2 = msg.SafeReadParam(L"content", CMessage::CheckedType::String, L"").AsWideStr();

	//	//bool bb = content2 == content;

	//	std::wstring request = CMessageParser::ToStringUtf(requestTree);

	//	std::string url = "http://" + params._uri + "/chatsessions/" + wtos(session_id) + "/chatmessages";
	//	std::vector<TgBot::HttpReqArg> args;
	//	args.push_back(TgBot::HttpReqArg("jsondata", wtos(request)));

	//	params._request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);

	//	makeMyClient(params, log->GetInstance());

	//	std::string response = TgBot::HttpParser::getInstance().parseResponse(params._response);

	//}

	//SEND FILE MESSAGE 2
	if (true)
	{
		std::string sSource;

		std::ifstream _in(L"d:/_in.txt", std::ios::binary);
		DWORD   dwDataSize = 0;
		if (_in.seekg(0, std::ios::end))
		{
			dwDataSize = static_cast<DWORD>(_in.tellg());
		}
		std::string buff(dwDataSize, 0);
		if (dwDataSize && _in.seekg(0, std::ios::beg))
		{
			std::copy(std::istreambuf_iterator< char>(_in),
				std::istreambuf_iterator< char >(),
				buff.begin());

			sSource = buff;
		}
		_in.close();


		boost::property_tree::wptree requestTree, responseTree;

		requestTree.put(L"id", session_id);
		requestTree.put(L"contentType", L"FILE");
		requestTree.put(L"content", stow(encodeBase64(sSource)));
		requestTree.put(L"fileName", L"_in.txt");
		requestTree.put(L"code", 11);
		requestTree.put(L"timestamp", core::support::time_to_string<std::wstring>(core::time_type::clock::now()));

		//std::wstring request = core::support::ptree_parser::tree_to_json_string(requestTree);
		std::wstring request = CMessageParser::ToStringUtf(requestTree);

		std::string url = "http://" + params._uri + "/chatsessions/" + wtos(session_id) + "/chatmessages";
		std::vector<TgBot::HttpReqArg> args;
		args.push_back(TgBot::HttpReqArg("jsondata", wtos(request)));

		params._request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);

		makeMyClient(params, log->GetInstance());

		std::string response = TgBot::HttpParser::getInstance().parseResponse(params._response);

	}

	 //GET SESSION STATUS
	if (true)
	{
		std::string url = "http://" + params._uri + "/chatsessions/" + wtos(session_id) + "?timestamp=" + core::support::time_to_string<std::string>(core::time_type::clock::now());
		std::vector<TgBot::HttpReqArg> args;
		params._request = TgBot::HttpParser::getInstance().generateRequest(url, args, false);

		makeMyClient(params, log->GetInstance());

		std::string response = TgBot::HttpParser::getInstance().parseResponse(params._response);

	}

    return 0;
}

